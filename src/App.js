import React from "react";
import { Provider } from "react-redux";
import store from "./store";
import { BrowserRouter, Switch } from "react-router-dom";
//import './App.scss';

import Home from "./components/pages/Home";
import Login from "./components/pages/Login";
import AstrologerReg from "./components/pages/AstrologerReg";
import AstrologerRegMgs from "./components/pages/AstrologerRegMgs";
import AstrologerProfile from "./components/pages/profile/AstrologerProfile";
import Location from "./components/pages/profile/Location";
import Bank from "./components/pages/profile/Bank";
import Professional from "./components/pages/profile/Professional";
import Personal from "./components/pages/profile/Personal";
import AstroProfile from "./components/pages/profile/astoProfile";
import AstroAppointment from "./components/pages/profile/AstroAppointment";
import OrderHistory from "./components/pages/profile/page11";
import SubmitThank from "./components/pages/profile/page15";
import AstroReview from "./components/pages/profile/page16";
import ConsultationEarning from "./components/pages/profile/page18";
import Logs from "./components/pages/profile/page19";
import DownloadApp from "./components/pages/profile/page23";
import YearlyHoroscopes from "./components/pages/profile/page24";
import AstrologerList from "./components/pages/profile/page25";
import MatchingReport from "./components/pages/profile/page29";
import FreeKundali from "./components/pages/profile/page31";
import YearlyHoroscopesSingle from "./components/pages/profile/page36";
import EnergyHealing from "./components/pages/profile/page40";
import OnlinePoojas from "./components/pages/profile/page41";
import YearlyHoroscopesSingle1 from "./components/pages/profile/page46";
import GetYourReport from "./components/pages/profile/page51";
import EducationReading from "./components/pages/profile/page52";
import ReportConfirmationPay from "./components/pages/profile/page55";
import UserChat from "./components/pages/profile/page59";
import DailyHoroscpeToday from "./components/pages/profile/page60";
import DailyPunchangToday from "./components/pages/profile/page61";
import MyAstrologer from "./components/pages/profile/page75";
import AvaliableBalance from "./components/pages/profile/page76";
import PaymenGatway from "./components/pages/profile/page79";
import WalletLog from "./components/pages/profile/page81";
import GstBreakup from "./components/pages/profile/page82";
import UserSignup from "./components/pages/profile/page86";
import UserDetails from "./components/pages/profile/page88";
import LogOut from "./components/pages/profile/page91";
import NewPassword from "./components/pages/profile/page93";
import PasswordReset from "./components/pages/profile/page94";
import UserDetailsForReport from "./components/pages/profile/page96";
import ReportSubmit from "./components/pages/profile/page99";
import Notifications from "./components/pages/profile/page100";
import ForgotPassword from "./components/pages/ForgotPassword";
import ForgotPasswordMgs from "./components/pages/ForgotPasswordMgs";
import FreeMatchMaking from "./components/pages/profile/FreeMatchMaking";
import PrivateRoute from "./routing/privateRoute";
import PublicRoute from "./routing/publicRoute";
import ClientProfile from "./components/pages/clientProfile/clientProfile";
import ReportUserDetail from "./components/pages/profile/ReportUserDetail";

import "../node_modules/aos/dist/aos.css";
import "bootstrap/dist/css/bootstrap.min.css";
import AdminRoute from "./routing/adminRoute";
import CreateReport from "./components/pages/profile/createReport";
import userLog from "./components/pages/profile/userLog";
require("./assets/fontawesome-free/css/all.min.css");
require("./assets/css/menu.css");
require("./assets/css/fontface.css");
require("./assets/css/style.css");
require("./assets/css/responsive.css");

function App() {
  return (
    <>
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <PrivateRoute exact path="/" component={Home} />
            <PublicRoute restricted={true} path="/login" component={Login} />
            <PrivateRoute
              path="/astrologerregmgs"
              component={AstrologerRegMgs}
            />
            <PrivateRoute path="/astrologerreg" component={AstrologerReg} />
            <PrivateRoute
              path="/astrologerprofile"
              component={AstrologerProfile}
            />
            <PrivateRoute path="/location" component={Location} />
            <PrivateRoute path="/bank" component={Bank} />
            <PrivateRoute path="/professional" component={Professional} />
            <PrivateRoute path="/personal" component={Personal} />
            <PrivateRoute path="/astroProfile" component={AstroProfile} />
            <PrivateRoute
              path="/astroAppointment"
              component={AstroAppointment}
            />
            <PrivateRoute path="/orderHistory" component={OrderHistory} />
            <PrivateRoute path="/submitThank" component={SubmitThank} />
            <PrivateRoute path="/astroReview" component={AstroReview} />
            <PrivateRoute
              path="/consultationEarning"
              component={ConsultationEarning}
            />
            <PrivateRoute path="/logs" component={Logs} />
            <PrivateRoute path="/downloadApp" component={DownloadApp} />
            <PrivateRoute
              path="/yearlyHoroscopes"
              component={YearlyHoroscopes}
            />
            <PrivateRoute path="/astrologerList" component={AstrologerList} />
            <PrivateRoute path="/matchingReport" component={MatchingReport} />
            <PrivateRoute path="/freeKundali" component={FreeKundali} />
            <PrivateRoute
              path="/yearlyHoroscopesSingle"
              component={YearlyHoroscopesSingle}
            />
            <PrivateRoute path="/energyHealing" component={EnergyHealing} />
            <PrivateRoute path="/onlinePoojas" component={OnlinePoojas} />
            <PrivateRoute
              path="/yearlyHoroscopesSingle1"
              component={YearlyHoroscopesSingle1}
            />
            <PrivateRoute path="/getYourReport" component={GetYourReport} />
            <PrivateRoute
              path="/educationReading"
              component={EducationReading}
            />
            <PrivateRoute
              path="/reportConfirmationPay"
              component={ReportConfirmationPay}
            />
            <PrivateRoute path="/userChat" component={UserChat} />
            <PrivateRoute
              path="/dailyHoroscpeToday"
              component={DailyHoroscpeToday}
            />
            <PrivateRoute
              path="/dailyPunchangToday"
              component={DailyPunchangToday}
            />
            <PrivateRoute path="/myAstrologer" component={MyAstrologer} />
            <PrivateRoute
              path="/avaliableBalance"
              component={AvaliableBalance}
            />
            <PrivateRoute path="/paymenGatway" component={PaymenGatway} />
            <PrivateRoute path="/walletLog" component={WalletLog} />
            <PrivateRoute path="/gstBreakup" component={GstBreakup} />
            <PublicRoute
              restricted={false}
              path="/userSignup"
              component={UserSignup}
            />
            <PrivateRoute path="/userDetails" component={UserDetails} />
            <PrivateRoute path="/logOut" component={LogOut} />
            <PrivateRoute path="/newPassword" component={NewPassword} />
            <PublicRoute
              restricted={false}
              path="/passwordReset"
              component={PasswordReset}
            />
            <PrivateRoute
              path="/userDetailsForReport"
              component={UserDetailsForReport}
            />
            <PrivateRoute path="/reportSubmit" component={ReportSubmit} />
            <PrivateRoute path="/notifications" component={Notifications} />
            <AdminRoute path="/AstroAppointment" component={AstroAppointment} />
            <PublicRoute
              restricted={false}
              path="/ForgotPassword"
              component={ForgotPassword}
            />
            <PublicRoute
              restricted={false}
              path="/ForgotPasswordMgs"
              component={ForgotPasswordMgs}
            />
            <PrivateRoute path="/page11" component={OrderHistory} />
            <PrivateRoute path="/page15" component={SubmitThank} />
            <PrivateRoute path="/page16" component={AstroReview} />
            <PrivateRoute path="/page18" component={ConsultationEarning} />
            <PrivateRoute path="/page19" component={Logs} />
            <PrivateRoute path="/page23" component={DownloadApp} />
            <PrivateRoute path="/page24" component={YearlyHoroscopes} />
            <PrivateRoute path="/page25" component={AstrologerList} />
            <PrivateRoute path="/page16" component={AstroReview} />
            <PrivateRoute path="/page18" component={ConsultationEarning} />
            <PrivateRoute path="/page100" component={Notifications} />
            <PrivateRoute path="/page91" component={LogOut} />
            <PrivateRoute path="/freekundali" component={FreeKundali} />
            <PrivateRoute path="/matchMaking" component={FreeMatchMaking} />
            <PrivateRoute path="/clientProfile" component={ClientProfile} />
            <PrivateRoute
              path="/reportUserDetail"
              component={ReportUserDetail}
            />
            <PrivateRoute path="/createReport" component={CreateReport} />
            <PrivateRoute path="/userLog" component={userLog} />
          </Switch>
        </BrowserRouter>
      </Provider>
    </>
  );
}

export default App;
