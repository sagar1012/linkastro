import React, { Component } from "react";
import axios from "axios";
import { reactLocalStorage } from "reactjs-localstorage";
import "react-toastify/dist/ReactToastify.css";
import CryptoJS from "crypto-js";
import { toast } from "react-toastify";
import { Link, useHistory, withRouter } from "react-router-dom";
import history from "../history";

const API_URL = "https://linkastro.com:4000";

class HttpService extends Component {
  constructor(props) {
    super(props);
  }

  get(url, params = {}) {
    return call("GET", url, params);
  }

  post(url, params = {}, payload = {}) {
    return call("POST", url, params, payload);
  }

  delete(url, params = {}) {
    return call("DELETE", url, params);
  }

  put(url, params = {}, payload = {}) {
    return call("PUT", url, params, payload);
  }
}

function call(method, URL, params, payload = {}) {
  var bytes = CryptoJS.AES.decrypt(
    reactLocalStorage.getObject("token"),
    "secret key 123"
  );
  var accessToken = bytes.toString(CryptoJS.enc.Utf8);
  console.log(accessToken);
  const opts = {
    method,
    url: API_URL + URL,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-access-token": accessToken,
    },
  };

  if (params) opts.params = params;
  if (payload) opts.data = payload;
  // if (headers) opts.headers = headers
  return axios(opts);
}

axios.interceptors.response.use(
  (res) => {
    if (
      res?.data?.result === 0 &&
      res?.data?.message === "Token does not exist" &&
      reactLocalStorage.getObject("AdminVerified") === 0
    ) {
      reactLocalStorage.remove("token");

      toast.error(
        "Token expired",
        { position: toast.POSITION.TOP_RIGHT },
        history.push("/login")
      );
      document.body.classList.remove("loading-indicator");
    } else {
      return res;
    }
  },
  (err) => {
    return Promise.reject(err.error);
  }
);

export default new HttpService();
