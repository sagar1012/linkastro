import httpService from "./httpService";

class ClientService {
  getPersonalDetail() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/personaldetails")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }
  getNotificationList(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/notificationList", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }
  getAstroListv1(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/getAstroListv1", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }
  reportDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/report/reportDetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getNotificationCount() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/getNotificationCount")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getMyAstrologerDetail(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/myAstrologerListv1", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  //.....
  //==== Client update profile ====
  //.....

  uploadImage(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/uploadProfilePicture", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getProfileDetailClient() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/getPersonalDetailClientv1")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getBirthDetailClient() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/getBirthDetails")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getLocationDetailClient() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/getlocationDetails")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  updatePersonalDetailClient(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/updatePersonalDetailClient", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getGeoDetails(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post(`/public/client/geodetails/${payload}`)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  updateDateOfBirth(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/updatedob", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  updateLocationDetails(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/updateLocationDetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  //.....
  //==== Client Review Rating===
  //.....

  getClientReviewRating(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/getRatingClient", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getastroProfileRating(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/astroProfileRating", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  addReviewRatings(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/reviewRating", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  //.....
  //==== User Sign Up===
  //.....

  UserNameExistOrNot(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/public/client/checkUserNameExistOrNot", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  SendOTPtoMobile(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/public/client/authorization/sendOTPToMobile", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  VerifyOTP(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/public/client/authorization/verifyOTPsentToMobile", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  //.....
  //==== Forgot Password===
  //.....

  ForgotPasswordSendOTP(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post(
        "/public/client/authorization/forgotPasswordSendOTPOnMobile",
        "",
        payload
      )
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  ForgotPasswordVerifyOTP(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/public/client/authorization/verifyOTPsentToMobile", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  ForgotPasswordSetPassword(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post(
        "/public/client/authorization/forgetPasswordSetPassword",
        "",
        payload
      )
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  //.....
  //==== User Report===
  //.....

  getClientList(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/astrologer/report/getClientList", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }
  getPersonalDetailProfile() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/astrologer/profile/getPersonalDetailsv1")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  putSubmitReport(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/astrologer/report/createReportDetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  getClientNotification(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/notificationList", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }
  clearNotificationLog() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/reviewrating/clearNotificationLog")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true };
      });
  }

  //.....
  //=== Wallet Services ===
  //.....

  getWalletBalance() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/wallet/getWalletBalancev1")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getWalletTransactionHistory() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/wallet/getWalletTransactionHistoryv1")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data.dataHistory,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return {
          data: resp.data.dataHistory,
          status: true,
          message: resp.data.message,
        };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getrechargeCashback() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/wallet/rechargeCashback")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  checkReferalCode(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/profile/checkReferalCode", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  //.....
  //=== Free match Making ===
  //.....

  getLatiLongDetail(payload) {
    document.body.classList.add("loading-indicator");
    return httpService
      .get(`/public/client/geodetails/${payload}`)
      .then((resp) => {
        console.log(resp);
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getMatchReportDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/matchdetailreport", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getPercentDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/matchbirthdetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  //.....
  //=== FreeKundali Service ===
  //.....

  getAstrologerDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/astrodetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getPanchangeDetailList(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/panchangdetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getNormologyList(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/numerology", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getKundaliChartDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/kundaliChart", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getVimshottaridashaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/vimshottaridasha", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getMahaDashaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/mahadasha", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getAntarDashaDetail(planet, payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post(`/api/client/astrology/antarDasha${planet}`, "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getPratyantarDashaDetail(planet, payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post(`/api/client/astrology/pratyantarDasha${planet}`, "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getKaalsarpDoshaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/kalsarpa", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getManglikDoshaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/manglikdetails", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getPitraDoshaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/pitradosha", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getSadhesatiStatusDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/sadhesatistatus", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getSadhesatiDashaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/sadhesati", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  //.....
  //=== DailyHoroscope Service ===
  //.....

  getDailyPanchangPrediction() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/dailypanchangprediction")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getDailyPredictionPrevious() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/dailyprediction/previous")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getDailyPredictionNext() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/dailyprediction/next")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  //.....
  //=== DailyPunchangeToday Service ===
  //.....

  getDailyPanchangeDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/dailypanchang", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  getDayChodhadiyaDetail(payload = {}) {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/astrology/chaughadiya", "", payload)
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return {
            data: resp.data,
            status: false,
            message: resp.data.message,
          };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        return { data: "err", status: false };
      });
  }

  //.....
  //=== User Logs ===
  //.....

  getClientChatHistory() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/chat/getChatHistory")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getClientCallHistory() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/call/getClientCallLogv1")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }

  getClientReportList() {
    document.body.classList.add("loading-indicator");
    return httpService
      .post("/api/client/report/getReportList")
      .then((resp) => {
        if (resp.data.result === 0) {
          document.body.classList.remove("loading-indicator");
          return { data: resp.data, status: false, message: resp.data.message };
        }
        document.body.classList.remove("loading-indicator");
        return { data: resp.data, status: true, message: resp.data.message };
      })
      .catch((err) => {
        document.body.classList.remove("loading-indicator");
        return { data: "err", status: false };
      });
  }
}

export default new ClientService();
