import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./assets/scss/main.scss";
import { BrowserRouter, Route, Switch, useHistory } from "react-router-dom";

ReactDOM.render(<App />, document.getElementById("root"));
