/* global localStorage */
import { AUTH_USER } from "./types";
import {
  URL_LOGIN,
  URL_SIGNUP,
  SEND_OTP,
  URL_CHECK_USERNAME,
  URL_VERYFY_OTP,
} from "../utils/api-url";
import axios from "axios";
import { toast } from "react-toastify";
import { reactLocalStorage } from "reactjs-localstorage";
import "react-toastify/dist/ReactToastify.css";
import CryptoJS from "crypto-js";

export const signup = (userData, history) => async (dispatch) => {
  try {
    const token = await axios.post(URL_SIGNUP, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        // 'x-access-token': accessToken,
      },
    });

    if (token.data.result === 1) {
      // var ciphertext = CryptoJS.AES.encrypt(token.data.data.accessToken, 'secret key 123').toString();
      //reactLocalStorage.setObject('token',ciphertext);
      ///dispatch({ type: AUTH_USER, payload: token.data.data });
      toast.success(token.message, { position: toast.POSITION.TOP_RIGHT });
      console.log("signup", token);
      history.push("/astrologerregmgs");
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const login = (userData, history) => async (dispatch) => {
  try {
    const token = await axios.post(URL_LOGIN, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    });

    if (token.data.result === 1) {
      var UserToken = token.data.data.accessToken;
      var TokenText = CryptoJS.AES.encrypt(
        UserToken,
        "secret key 123"
      ).toString();
      //var AuthData = CryptoJS.AES.encrypt(token.data.data, 'secret key 123');
      // reactLocalStorage.setObject('Auth',token.data.data);
      reactLocalStorage.setObject("token", TokenText);
      const currentTime = new Date().getTime();
      localStorage.setItem("accepted", currentTime);
      localStorage.setItem("AdminVerified", token.data.data.adminVerified);

      dispatch({ type: AUTH_USER, payload: token.data.data });
      toast.success(token.message, { position: toast.POSITION.TOP_RIGHT });
      if (token.data.data.adminVerified === 1) {
        history.push("/AstroAppointment");
      } else {
        history.push("/");
      }

      console.log("login", token);
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
    //console.log('cust err ', error)
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const sendOtp = (userData) => async (dispatch) => {
  try {
    const token = await axios.post(SEND_OTP, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    });
    if (token.data.result === 1) {
      return token;
      console.log("signup", token);
    } else {
      return token;
      toast.error(token.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
    //console.log('cust err ', error)
    //const err = JSON.parse(error.message);
    // dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const veryfyOtp = (userData) => async (dispatch) => {
  try {
    const token = await axios.post(URL_VERYFY_OTP, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    });
    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
    //console.log('cust err ', error)
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const checkUserName = (userData) => async (dispatch) => {
  try {
    const token = await axios.post(URL_CHECK_USERNAME, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    });
    if (token.data.result === 1) {
      toast.error(token.message, { position: toast.POSITION.TOP_RIGHT });
      return token;
    } else {
      return token;
      toast.error(token.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
    //console.log('cust err ', error)
    // const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const logout = () => (dispatch) => {
  localStorage.removeItem("Auth");
  dispatch({
    type: AUTH_USER,
    payload: {},
  });
};
