/* global document */
import {
  URL_GET_PROFILE_DETAIL,
  URL_GET_BANK_DETAILS,
  URL_UPDATE_BANK_DETAILS,
  URL_GET_PERSONAL_DETAILS,
  URL_UPDATE_PERSONAL_DETAILS,
  URL_GET_PROFESSIONAL_DETAILS,
  URL_UPDATE_PROFESSIONAL_DETAILS,
  URL_GET_LOCATION_DETAILS,
  URL_UPDATE_LOCATION_DETAILS,
  URL_GET_CATEGORY,
  URL_GET_SKILLS,
  URL_GET_LANGUAGE,
  URL_UPDATE_PROFILE,
  URL_GET_IMAGE,
  URL_UPLOAD_BANK_ID,
  URL_GET_BANK_ID,
  URL_UPLOAD_GALLARY,
  URL_GET_GALLARY,
} from "../utils/api-url";
import axios from "axios";
import { toast } from "react-toastify";
import { reactLocalStorage } from "reactjs-localstorage";
import "react-toastify/dist/ReactToastify.css";
import CryptoJS from "crypto-js";

// Update Locations details //
export const updateLocation = (userData, history) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_UPDATE_LOCATION_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      //dispatch({ type: GET_LOCATIONDETAILS, payload: token.data.data });
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      //$('#react-tabs-4').trigger( "click" );
      var l = document.getElementById("react-tabs-4");
      l.click();
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const updateProfile = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_UPDATE_PROFILE, userData, {
      headers: {
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      // console.log(token.data.data.profileImagePath);
      const ImgData = {
        filePath: token.data.data.profileImagePath,
      };

      const imageUrlData = await axios.post(URL_GET_IMAGE, ImgData, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      });

      if (imageUrlData.data.result === 1) {
        //reactLocalStorage.setObject('profile_pic',imageUrlData.data.data);
        return imageUrlData.data.data;
      } else {
        //reactLocalStorage.setObject('profile_pic',require('../../../assets/images/priest_vishnuvardhana@2x.png'));
        toast.success(token.data.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// upload bank id proof

export const updateBankIdProof = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_UPLOAD_BANK_ID, userData, {
      headers: {
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      // console.log(token.data.data.profileImagePath);
      const ImgData = {
        filePath: token.data.data.profileImagePath,
      };

      //console.log(ImgData);
      const imageUrlData = await axios.post(URL_GET_BANK_ID, ImgData, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      });

      if (imageUrlData.data.result === 1) {
        //reactLocalStorage.setObject('profile_pic',imageUrlData.data.data);
        return imageUrlData.data.data;
      } else {
        toast.success(token.data.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const GetProfilePic = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_GET_IMAGE, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token.data.data;
    } else {
      return token.data.data;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

///get  bank  id proof

export const GetBankIdproof = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_GET_BANK_ID, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token.data.data;
    } else {
      return token.data.data;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// get locations details //
export const getLocationDetails = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_LOCATION_DETAILS,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// update bankdetails //
export const updateBankDetails = (userData, history) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_UPDATE_BANK_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      //dispatch({ type: GET_LOCATIONDETAILS, payload: token.data.data });
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      //$('#react-tabs-6').trigger( "click" );
      var l = document.getElementById("react-tabs-6");
      l.click();
    } else {
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    // dispatch({ type: GET_ERRORS, payload: err });
  }
};

export const getBankDetails = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_BANK_DETAILS,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// update bankdetails //
export const updatePersonalDetails =
  (userData, history) => async (dispatch) => {
    try {
      var bytes = CryptoJS.AES.decrypt(
        reactLocalStorage.getObject("token"),
        "secret key 123"
      );
      var accessToken = bytes.toString(CryptoJS.enc.Utf8);
      const token = await axios.post(URL_UPDATE_PERSONAL_DETAILS, userData, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      });

      if (token.data.result === 1) {
        //dispatch({ type: GET_LOCATIONDETAILS, payload: token.data.data });
        toast.success(token.data.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        // $('#react-tabs-2').trigger( "click" );
        var l = document.getElementById("react-tabs-2");
        l.click();
      } else {
        toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      }
    } catch (error) {
      // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
      //const err = JSON.parse(error.message);
      // dispatch({ type: GET_ERRORS, payload: err });
    }
  };

export const getPersonalDetails = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(
      URL_GET_PERSONAL_DETAILS,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// update bankdetails //
export const updateProfessionalDetails =
  (userData, history) => async (dispatch) => {
    try {
      var bytes = CryptoJS.AES.decrypt(
        reactLocalStorage.getObject("token"),
        "secret key 123"
      );
      var accessToken = bytes.toString(CryptoJS.enc.Utf8);
      const token = await axios.post(
        URL_UPDATE_PROFESSIONAL_DETAILS,
        userData,
        {
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
            "x-access-token": accessToken,
          },
        }
      );

      if (token.data.result === 1) {
        //dispatch({ type: GET_LOCATIONDETAILS, payload: token.data.data });

        //$('#react-tabs-0').trigger( "click" );
        var l = document.getElementById("react-tabs-0");
        l.click();
        //toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      } else {
        toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      }
    } catch (error) {
      // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
      //const err = JSON.parse(error.message);
      // dispatch({ type: GET_ERRORS, payload: err });
    }
  };

export const getProfessionalDetails = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_PROFESSIONAL_DETAILS,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// Get skills

export const getskills = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_SKILLS,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    /// dispatch({ type: GET_ERRORS, payload: err });
  }
};

// Get skills

export const getcategorys = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_CATEGORY,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// Get skills

export const getlanguages = () => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_LANGUAGE,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
      toast.success(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    // const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// upload gallary image

export const uploadGallayImage = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_UPLOAD_GALLARY, userData, {
      headers: {
        Accept: "multipart/form-data",
        "Content-type": "multipart/form-data",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

// get Gallery image

export const getGallayImage = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(
      URL_GET_GALLARY,
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      }
    );

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};

//get short profile detail
export const getProfileDetail = (userData) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    var adminVerified = reactLocalStorage.getObject("AdminVerified");
    if (adminVerified === 0) {
      return;
    } else {
      var token;
      return (token = await axios.post(
        URL_GET_PROFILE_DETAIL,
        {},
        {
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
            "x-access-token": accessToken,
          },
        }
      ));
    }

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      //toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // toast.error("Network error", { position: toast.POSITION.TOP_RIGHT });
    //const err = JSON.parse(error.message);
    //dispatch({ type: GET_ERRORS, payload: err });
  }
};
