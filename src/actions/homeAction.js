import { GET_CONTACTS, GET_CONTACT, REMOVE_CONTACT, CONTACT_LOADING,GET_ERRORS } from '../actions/types';
import { URL_FRONT_CONTACT_US,URL_APPLY_JOB } from '../utils/api-url';
import Http from '../utils/http';
import { reset } from 'redux-form';

import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
 
 // **** Contact Inquiry  ********//
export const submitContactus = (userData, history) => async dispatch => {
  try {
    dispatch({ type: GET_CONTACTS, payload: true });
    const user = await Http.post(URL_FRONT_CONTACT_US, userData);
   
  if(user.status==true){
        dispatch({ type: GET_CONTACT, payload: {} });
         toast.success(user.message, { position: toast.POSITION.TOP_CENTER });
         //history.push("/user-list");
         return true;
      }else{
         toast.error(user.message, { position: toast.POSITION.TOP_CENTER });
         return false;
    }
  
  } catch (error) {
    const err = JSON.parse(error.message);
    dispatch({ type: GET_ERRORS, payload: err });
  }
};

 // **** Apply Job  ********//
export const applyJob = (userData, history) => async dispatch => {
  try {
    dispatch({ type: GET_CONTACTS, payload: true });
    const user = await Http.post(URL_APPLY_JOB, userData);
   
  if(user.status==true){
         toast.success(user.message, { position: toast.POSITION.TOP_CENTER });
         //history.push("/user-list");
         return true;
      }else{
         toast.error(user.message, { position: toast.POSITION.TOP_CENTER });
         return false;
    }
  
  } catch (error) {
    const err = JSON.parse(error.message);
    dispatch({ type: GET_ERRORS, payload: err });
  }
};
