import { URL_GET_CALLOG_DETAIL,URL_GET_CHATHISTORY_DETAIL,URL_GET_REPORTLOG_DETAIL} from '../utils/api-url';
import axios from 'axios';
import { toast } from 'react-toastify';
import { reactLocalStorage } from 'reactjs-localstorage';
import 'react-toastify/dist/ReactToastify.css';
import CryptoJS from 'crypto-js';


export const getCallLogDetail = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_CALLOG_DETAIL ,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};

export const getChathistoryDetail = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_CHATHISTORY_DETAIL ,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};

export const getReportDetail = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_REPORTLOG_DETAIL ,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};

