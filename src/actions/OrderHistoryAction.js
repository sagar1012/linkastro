import axios from 'axios';
import { toast } from 'react-toastify';
import { reactLocalStorage } from 'reactjs-localstorage';
import 'react-toastify/dist/ReactToastify.css';
import { URL_GET_CHAR_DETAIL ,URL_GET_CLIENTRATINNG_DETAIL , URL_GET_ALLCHATORD_DETAIL  ,URL_GET_MISSEDORDCHAT_DETAIL  ,URL_GET_FINISHEDORDCHAT_DETAIL  ,URL_GET_ONGOINGORDCHAT_DETAIL  } from '../utils/api-url';
import CryptoJS from 'crypto-js';

 
export const getAllChatOrder = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_ALLCHATORD_DETAIL ,userData,
        {

/*
        body:JSON.stringify({
          'pageNo':1
        }),
*/   
/*params:{
  'pageNo':'1'
},
*/
     /* match: {
  isExact: true,
  params: {pageNo:'1'},
},*/

  // params: this.match.params,
  // params: this.match.params ,  
  // `${this.props.match.params.pageNo}`,
 
  
      	/*params:JSON.stringify({
      		'pageNo':1
      	}),
*/
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,


        }
      }
    );
   
   if(token.data.result === 1 )
    {
      return token;
   }else{
         return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};
export const getMissedChat= (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_MISSEDORDCHAT_DETAIL ,userData,
      {
        // body: "param=pageNo",
        pageNo:'1', 
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        },
      	    Params: JSON.stringify({
            moodlewsrestformat: 'application/json',
            pageNo: '1',
                   }),
      });
   
   if(token.data.result === 1 && token.Params['pageNo']===1)
    {
      return token;
      console.log("hello")

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      console.log("hello")
   }
 }
    catch(error) {
    // Block of code to handle errors
      console.log("hello")
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};


export const getFinishedChat = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_FINISHEDORDCHAT_DETAIL ,userData,
      {

        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};
export const getOngoiengChat = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_ONGOINGORDCHAT_DETAIL,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};


export const getClientratingDetail = (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_CLIENTRATINNG_DETAIL,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};
export const getCharDetail= (userData,data) => async dispatch=>{

 try {
    var bytes = CryptoJS.AES.decrypt(reactLocalStorage.getObject('token'), 'secret key 123');
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

  const token = await axios.post(URL_GET_CHAR_DETAIL,userData,
      {
        headers: {
          Accept: 'application/json',
          'Content-type': 'application/json',
          'x-access-token': accessToken,

        }
      }
    );
   
   if(token.data.result === 1)
    {
      return token;

   }else{
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
   }
 }
    catch(error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
   }

};

