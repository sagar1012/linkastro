import {
  URL_GET_REPORTLIST_DETAILS,
  URL_GET_FOLLOWER_DETAILS,
  URL_GET_EARNING_DETAILS,
  URL_GET_WALLET_DETAILS,
  URL_GET_SHADULE_DETAILS,
  URL_UPDATE_SHADULE_DETAILS,
  URL_GET_REPORT_DETAILS,
  URL_GET_PERSONALV_DETAILS,
} from "../utils/api-url";
import axios from "axios";
import { toast } from "react-toastify";
import { reactLocalStorage } from "reactjs-localstorage";
import "react-toastify/dist/ReactToastify.css";
import CryptoJS from "crypto-js";

// get earning

export const getEarningDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_EARNING_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

// get Ballance

export const getwalletDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_WALLET_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

// Get follower

export const getFollowerDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_FOLLOWER_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

//Upload shadule detail

export const updateShadulerDetails =
  (userData, history) => async (dispatch) => {
    try {
      var bytes = CryptoJS.AES.decrypt(
        reactLocalStorage.getObject("token"),
        "secret key 123"
      );
      var accessToken = bytes.toString(CryptoJS.enc.Utf8);
      const token = await axios.post(URL_UPDATE_SHADULE_DETAILS, userData, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          "x-access-token": accessToken,
        },
      });

      if (token.data.result === 1) {
        //dispatch({ type: GET_LOCATIONDETAILS, payload: token.data.data });
        //$('#react-tabs-4').trigger( "click" );
      } else {
        toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      }
    } catch (error) {}
  };

// Get follower

export const getShaduleDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_SHADULE_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

export const getReportDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_REPORT_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

export const getReportListDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);

    const token = await axios.post(URL_GET_REPORTLIST_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};

export const getPersonalvDetail = (userData, data) => async (dispatch) => {
  try {
    var bytes = CryptoJS.AES.decrypt(
      reactLocalStorage.getObject("token"),
      "secret key 123"
    );
    var accessToken = bytes.toString(CryptoJS.enc.Utf8);
    const token = await axios.post(URL_GET_PERSONALV_DETAILS, userData, {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "x-access-token": accessToken,
      },
    });

    if (token.data.result === 1) {
      return token;
    } else {
      return token;
      toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
    }
  } catch (error) {
    // Block of code to handle errors
    // toast.error('Network error', { position: toast.POSITION.TOP_RIGHT });
  }
};
