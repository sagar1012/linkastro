import React, { useEffect, useState } from "react";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import "./clientProfile.css";
import {
  Form,
  Nav,
  Tab,
  ToggleButtonGroup,
  ToggleButton,
  Button,
} from "react-bootstrap";
import {
  TimePicker,
  DatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import { useFormik } from "formik";
import * as Yup from "yup";
import httpService from "../../../services/httpService";
import { ToastContainer, toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import clientServiceAPI from "../../../services/clientServiceAPI";

const ClientProfile = () => {
  const history = useHistory();
  const [image, setImage] = useState("");
  const [isDateOpen, setDateIsOpen] = useState(false);
  const [isTimeOpen, setTimeIsOpen] = useState(false);

  useEffect(() => {
    getPersonalDetails();
    getBirthDetails();
    getLocationDetail();
  }, []);

  const formik = useFormik({
    initialValues: {
      name: "",
      Lastname: "",
      mobile: "",
      email: "",
      gender: "male",
      birthDay: "",
      birthMonth: "",
      birthYear: "",
      birthHour: "",
      birthMin: "",
      birthTime: "",
      birthPlace: "",
      BirthDate: "",
      BirthTime: "",
      country: "",
      state: "",
      city: "",
      pincode: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("First Name Required"),
      Lastname: Yup.string().required("Last Name Required"),
      mobile: Yup.string().required("Mobile Number Required"),
      email: Yup.string().required("Please Enter Valid Email"),
      gender: Yup.string().required("Please Select gender"),
      birthDay: Yup.string().required("Date of Birth Required"),
      birthMonth: Yup.string().required(""),
      birthYear: Yup.string().required(""),

      birthHour: Yup.string().required("Time OF Birth Required"),
      birthMin: Yup.string().required(""),
      birthTime: Yup.string().required(""),
      birthPlace: Yup.string().required("Place of Birth Required"),
      country: Yup.string().required("Country Name is required"),
      state: Yup.string().required("State is Required"),
      city: Yup.string().required("Please Enter City Name"),
      pincode: Yup.string().required("Pincode Is Required"),
    }),
    onSubmit: () => {},
  });

  const getPersonalDetails = () => {
    clientServiceAPI
      .getProfileDetailClient()
      .then((resp) => {
        if (resp?.data?.result === 1) {
          let personalDetailClient = resp?.data?.data;
          formik.setFieldValue("name", personalDetailClient?.firstName);
          formik.setFieldValue("Lastname", personalDetailClient?.lastName);
          formik.setFieldValue("mobile", personalDetailClient?.contactNo);
          formik.setFieldValue("email", personalDetailClient?.emailId);
          formik.setFieldValue("gender", personalDetailClient?.gender);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getBirthDetails = () => {
    clientServiceAPI
      .getBirthDetailClient()
      .then((resp) => {
        if (resp?.data?.result === 1) {
          let BirthDetails = resp?.data?.data;
          formik.setFieldValue(
            "BirthDate",
            moment(BirthDetails?.birthdate).format("YYYY-MM-DD")
          );
          formik.setFieldValue(
            "BirthTime",
            moment(BirthDetails?.birthtime).format("hh:mm:a")
          );
          formik.setFieldValue(
            "birthDay",
            moment(BirthDetails?.birthdate).format("DD")
          );
          formik.setFieldValue(
            "birthMonth",
            moment(BirthDetails?.birthdate).format("MM")
          );
          formik.setFieldValue(
            "birthYear",
            moment(BirthDetails?.birthdate).format("YYYY")
          );
          formik.setFieldValue(
            "birthHour",
            BirthDetails?.birthtime.split(":")[0]
          );
          formik.setFieldValue(
            "birthMin",
            BirthDetails?.birthtime.split(":")[1]
          );
          formik.setFieldValue(
            "birthTime",
            BirthDetails?.birthtime.split(":")[2]
          );
          formik.setFieldValue("birthPlace", BirthDetails?.birthlocation);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getLocationDetail = () => {
    clientServiceAPI
      .getLocationDetailClient()
      .then((resp) => {
        if (resp?.data?.result === 1) {
          let locationDetail = resp?.data?.data;
          formik.setFieldValue("country", locationDetail?.country);
          formik.setFieldValue("state", locationDetail?.state);
          formik.setFieldValue("city", locationDetail?.city);
          formik.setFieldValue("pincode", locationDetail?.pincode);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const handleDateChange = (e) => {
    formik.setFieldValue("birthDay", moment(e).format("DD"));
    formik.setFieldValue("birthMonth", moment(e).format("MM"));
    formik.setFieldValue("birthYear", moment(e).format("YYYY"));
    formik.setFieldValue("BirthDate", moment(e).format("YYYY-MM-DD"));
  };

  const handleTimeChange = (e) => {
    formik.setFieldValue("birthHour", moment(e).format("hh"));
    formik.setFieldValue("birthMin", moment(e).format("mm"));
    formik.setFieldValue("birthTime", moment(e).format("A"));
    formik.setFieldValue("BirthTime", moment(e).format("hh:mm:a"));
  };

  const personalDetail = (e) => {
    e.preventDefault();
    formik.handleSubmit();
    const body = {
      firstName: formik.values.name,
      lastName: formik.values.Lastname,
      gender: formik.values.gender,
      email: formik.values.email,
      mobileNo: formik.values.mobile,
    };
    clientServiceAPI
      .updatePersonalDetailClient(body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success(
            resp.data.message,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            },
            history.push("/")
          );
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const birthDetail = (e) => {
    e.preventDefault();
    formik.handleSubmit();

    clientServiceAPI
      .getGeoDetails(formik?.values?.birthPlace)
      .then((resp) => {
        updateBirthDate(resp?.data?.data[0]);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const updateBirthDate = (data) => {
    const body = {
      timezone: "5.5",
      birthtime: formik?.values?.BirthTime,
      dob: formik?.values?.BirthDate,
      lattitude: data?.latitude,
      longitude: data?.longitude,
      pinCode: formik?.values?.pincode,
      location: formik?.values?.birthPlace,
      firstName: formik?.values?.name,
      mobileNo: formik?.values?.mobile,
      email: formik?.values?.email,
      lastName: formik.values.Lastname,
      gender: formik.values.gender,
      state: formik?.values?.state,
    };

    clientServiceAPI
      .updateDateOfBirth(body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success(
            resp.data.message,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            },
            history.push("/")
          );
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const locationDetail = (e) => {
    e.preventDefault();
    formik.handleSubmit();

    const body = {
      country: formik?.values?.country,
      state: formik?.values?.state,
      city: formik?.values?.city,
      pinCode: formik?.values?.pincode,
    };

    clientServiceAPI
      .updateLocationDetails(body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success(
            resp.data.message,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            },
            history.push("/")
          );
        }
      })
      .catch((err) => {});
  };

  const handleClose = () => {
    history.push("/");
  };

  const onChangeFile = (event) => {
    console.log(event.target.files);
    setImage(event.target.files[0].name);
    changeImageHandler(event.target.files[0]);
  };

  const changeImageHandler = (FileName) => {
    const body = {
      fileName: FileName,
    };
    clientServiceAPI.uploadImage(body).then((resp) => {
      console.log(resp);
    });
  };

  return (
    <>
      <div style={{ background: "#FA8345" }}>
        <Header />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <ToastContainer />
        <div className="container">
          <div className="bg-white p-4 br-8 my-4 position-relative">
            <h4>Your Profile</h4>
            <img
              src="assets/img/cancel.svg"
              className="position-absolute fom-close"
              onClick={handleClose}
            />
            <form>
              <Tab.Container defaultActiveKey="first">
                <div style={{ padding: "50px 200px" }}>
                  <Nav role="tablist">
                    <Nav.Item role="presentation">
                      <Nav.Link eventKey="first">
                        <h5>Personal</h5>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item role="presentation">
                      <Nav.Link eventKey="second">
                        <h5>Birth Details</h5>
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item role="presentation">
                      <Nav.Link eventKey="third">
                        <h5>Location</h5>
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>

                  <div style={{ paddingTop: "25px" }}>
                    <Tab.Content>
                      <Tab.Pane eventKey="first">
                        <Form.Group
                          controlId="formBasicImage"
                          className="fmc-style"
                          style={{ textAlign: "center" }}
                        >
                          <input
                            type="file"
                            id="file"
                            style={{ display: "none" }}
                            onChange={(e) => onChangeFile(e)}
                          />
                          <label htmlFor="file">
                            <img
                              name="image"
                              src={
                                image
                                  ? image
                                  : require("../../../assets/images/download.png")
                              }
                              className="avtimg"
                            />
                            <img
                              name="plus"
                              src={require("../../../assets/images/plus.png")}
                              className="plusimg"
                            />
                          </label>
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicName"
                          className="fmc-style"
                        >
                          <Form.Label>First Name*</Form.Label>
                          <Form.Control
                            type="text"
                            name="name"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.name}
                          />
                          {formik.touched.name && formik.errors.name ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.name}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicLastName"
                          className="fmc-style"
                        >
                          <Form.Label>Last Name*</Form.Label>
                          <Form.Control
                            type="text"
                            name="Lastname"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.Lastname}
                          />
                          {formik.touched.Lastname && formik.errors.Lastname ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.Lastname}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicMobile"
                          className="fmc-style"
                        >
                          <Form.Label>Mobile Number *</Form.Label>
                          <Form.Control
                            type="text"
                            name="mobile"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.mobile}
                          />
                          {formik.touched.mobile && formik.errors.mobile ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.mobile}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicEmail"
                          className="fmc-style"
                        >
                          <Form.Label>Email *</Form.Label>
                          <Form.Control
                            type="email"
                            name="email"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.email}
                          />
                          {formik.touched.email && formik.errors.email ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.email}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicGender"
                          className="fmc-style"
                        >
                          <Form.Label>Gender *</Form.Label>
                          <br></br>
                          <div className="gender-btn">
                            <ToggleButtonGroup
                              type="radio"
                              name="gender"
                              value={formik.values.gender}
                              id="gender1"
                              onChange={formik.handleChange}
                            >
                              <ToggleButton
                                size="md"
                                variant="outline-primary"
                                value="male"
                                checked={"male" === formik.values.gender}
                                onChange={formik.handleChange}
                              >
                                Male
                              </ToggleButton>
                              <ToggleButton
                                size="md"
                                variant="outline-primary"
                                value="female"
                                checked={"female" === formik.values.gender}
                                onChange={formik.handleChange}
                              >
                                Female
                              </ToggleButton>
                              <ToggleButton
                                size="md"
                                variant="outline-primary"
                                value="other"
                                checked={"other" === formik.values.gender}
                                onChange={formik.handleChange}
                              >
                                Other
                              </ToggleButton>
                              {formik.errors.gender ? (
                                <div style={{ color: "red" }}>
                                  {formik.errors.gender}
                                </div>
                              ) : null}
                            </ToggleButtonGroup>
                          </div>
                        </Form.Group>
                        <div className="pt-3">
                          <Button
                            variant="primary"
                            block
                            size="sm"
                            type="Submit"
                            onClick={personalDetail}
                          >
                            Save & Continue
                          </Button>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <Form.Group
                          controlId="formBasicBirthDate"
                          className="fmc-style"
                        >
                          <Form.Label
                            style={{ marginTop: "12px", marginRight: "30px" }}
                          >
                            Date OF Birth
                          </Form.Label>
                          <br></br>
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              open={isDateOpen}
                              onOpen={() => setDateIsOpen(true)}
                              onClose={() => setDateIsOpen(false)}
                              name="BirthDate"
                              value={formik.values.BirthDate}
                              onChange={handleDateChange}
                              TextFieldComponent={() => null}
                            />
                          </MuiPickersUtilsProvider>
                          <input
                            type="button"
                            name="birthDay"
                            value={
                              formik.values.birthDay !== "Invalid date"
                                ? formik.values.birthDay
                                : "Day"
                            }
                            className={
                              formik.values.birthDay !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setDateIsOpen(true)}
                          />
                          <input
                            type="button"
                            name="birthMonth"
                            value={
                              formik.values.birthMonth !== "Invalid date"
                                ? formik.values.birthMonth
                                : "Month"
                            }
                            className={
                              formik.values.birthMonth !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setDateIsOpen(true)}
                          />
                          <input
                            type="button"
                            name="birthYear"
                            value={
                              formik.values.birthYear !== "Invalid date"
                                ? formik.values.birthYear
                                : "Year"
                            }
                            className={
                              formik.values.birthYear !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setDateIsOpen(true)}
                          />
                          {formik.errors.birthDay ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.birthDay}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicBirthTime"
                          className="fmc-style"
                        >
                          <Form.Label
                            style={{ marginTop: "12px", marginRight: "30px" }}
                          >
                            Time OF Birth
                          </Form.Label>
                          <br></br>
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <TimePicker
                              open={isTimeOpen}
                              onOpen={() => setTimeIsOpen(true)}
                              onClose={() => setTimeIsOpen(false)}
                              name="BirthTime"
                              value={formik.values.BirthTime}
                              onChange={handleTimeChange}
                              TextFieldComponent={() => null}
                            />
                          </MuiPickersUtilsProvider>
                          <input
                            type="button"
                            name="birthHour"
                            value={
                              formik.values.birthHour !== "Invalid date"
                                ? formik.values.birthHour
                                : "HH"
                            }
                            className={
                              formik.values.birthHour !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setTimeIsOpen(true)}
                          />
                          <input
                            type="button"
                            name="birthMin"
                            value={
                              formik.values.birthMin !== "Invalid date"
                                ? formik.values.birthMin
                                : "MM"
                            }
                            className={
                              formik.values.birthMin !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setTimeIsOpen(true)}
                          />
                          <input
                            type="button"
                            name="birthTime"
                            value={
                              formik.values.birthTime !== "Invalid date"
                                ? formik.values.birthTime
                                : "AM"
                            }
                            className={
                              formik.values.birthTime !== "Invalid date"
                                ? "btn btn-primary m-2 rounded"
                                : "btn btn-white border m-2 rounded"
                            }
                            onClick={(e) => setTimeIsOpen(true)}
                          />
                          {formik.errors.birthHour ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.birthHour}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicBirth"
                          className="fmc-style"
                        >
                          <Form.Label>Place of Birth *</Form.Label>
                          <Form.Control
                            type="text"
                            name="birthPlace"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.birthPlace}
                          />
                          {formik.touched.birthPlace &&
                          formik.errors.birthPlace ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.birthPlace}
                            </div>
                          ) : null}
                        </Form.Group>
                        <div className="pt-3">
                          <Button
                            variant="primary"
                            block
                            size="sm"
                            type="Submit"
                            onClick={birthDetail}
                          >
                            Save & Continue
                          </Button>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey="third">
                        <h5 style={{ textAlign: "center" }}>
                          <i
                            className="fas fa-location-arrow"
                            style={{ margin: "12px", color: "#00a1ff" }}
                          ></i>
                          Select yout current location
                        </h5>
                        <Form.Group
                          controlId="formBasicCountry"
                          className="fmc-style"
                        >
                          <Form.Label>Country*</Form.Label>
                          <Form.Control
                            type="text"
                            name="contry"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.country}
                          />
                          {formik.touched.country && formik.errors.country ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.country}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicState"
                          className="fmc-style"
                        >
                          <Form.Label>State *</Form.Label>
                          <Form.Control
                            type="text"
                            name="state"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.state}
                          />
                          {formik.touched.state && formik.errors.state ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.state}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicCity"
                          className="fmc-style"
                        >
                          <Form.Label>City *</Form.Label>
                          <Form.Control
                            type="text"
                            name="city"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.city}
                          />{" "}
                          {formik.touched.city && formik.errors.city ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.city}
                            </div>
                          ) : null}
                        </Form.Group>
                        <Form.Group
                          controlId="formBasicPincode"
                          className="fmc-style"
                        >
                          <Form.Label>Pincode *</Form.Label>
                          <Form.Control
                            type="text"
                            name="pincode"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.pincode}
                          />
                          {formik.touched.pincode && formik.errors.pincode ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.pincode}
                            </div>
                          ) : null}
                        </Form.Group>
                        <div className="pt-3">
                          <Button
                            variant="primary"
                            block
                            size="sm"
                            type="Submit"
                            onClick={locationDetail}
                          >
                            Save
                          </Button>
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </div>
                </div>
              </Tab.Container>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default ClientProfile;
