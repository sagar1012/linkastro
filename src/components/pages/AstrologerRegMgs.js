import React, { Component } from 'react';
import Header from '../templates/Header';
import Footer from '../templates/Footer';

export default class AstrologerRegMgs extends Component {
  
    render() {
        return (
            <div className="main-wrapper">
                <Header />
                <section className="middle-section">
                    <div className="sign-page thank-page">
                        <div className="container">
                            <div className="sign-section section-bk">
                                <div className="thank-you-inn">
                                    <div className="thank-you-img">
                                        <img src={ require('../../assets/images/thank-you-img.png') } alt=""/>
                                    </div>
                                    <div className="thank-you-txt">Thank you</div>
                                </div>
                                <div className="thank-you-txt-p">
                                    <p>Your details are submitted</p>
                                    <p>We will contact you shortly</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}
