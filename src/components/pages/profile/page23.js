import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class DownloadApp extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content bg-color1">
          <div className="bg-white p-5 thank-container text-center my-5">

            <h4 className="text-color2 fw-500 mb-5">Want to Talk to Astrologers Or join the Live Video?</h4>

            <p className="mb-4 fw-500">Download Our Mobile App <br />Available Now!</p>


            <a href="#" className="mx-1"><img src="assets/img/googlePlay.png" width="120" /></a>
            <a href="#" className="mx-1"><img src="assets/img/AppStore.png" width="120" /></a>
          </div>


        </section>
        <Footer />
      </div>
    );
  }
}

export default DownloadApp;