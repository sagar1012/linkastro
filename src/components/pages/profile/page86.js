import React, { useState } from "react";
import {
  Button,
  Row,
  Col,
  Container,
  Form,
  InputGroup,
  ToggleButtonGroup,
  ToggleButton,
} from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Otp } from "react-otp-timer";
import ReactModal from "react-modal";
import httpService from "../../../services/httpService";
import { ToastContainer, toast } from "react-toastify";
import { useHistory } from "react-router-dom";

const UserSignup = () => {
  const [OTPvalue, setOTP] = useState(false);
  const history = useHistory();

  const [showPassword, setShowPassword] = useState(false);
  const [showRePassword, setShowRePassword] = useState(false);

  const PasswordHandler = () => {
    setShowPassword(showPassword ? false : true);
  };
  const RePasswordHandler = () => {
    setShowRePassword(showRePassword ? false : true);
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      mobile: "",
      email: "",
      password: "",
      Repassword: "",
      checkbox: false,
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .max(15, "Must be 15 characters or less")
        .required("Required"),
      mobile: Yup.string().max(10, "Must be 10 numbers").required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
      password: Yup.string()
        .required("Invalid Password!..")
        .min(8, "Password is too short - should be 8 chars minimum."),
      Repassword: Yup.string().oneOf(
        [Yup.ref("password"), null],
        "Passwords must match"
      ),
      checkbox: Yup.bool().oneOf(
        [true],
        "Accept Terms & Conditions is required"
      ),
    }),
    onSubmit: (values) => {
      if (values.checkbox === true) {
        const body = {
          userName: values.mobile,
        };
        httpService
          .post("/public/client/checkUserNameExistOrNot", "", body)
          .then((resp) => {
            if (resp.data.result === 0) {
              toast.error(resp.data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            }
            if (resp.data.result === 1 && resp.data.data.exist) {
              toast.info(resp.data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            }

            if (!resp.data.data.exist) {
              const payload = {
                mobileNo: values.mobile,
              };
              httpService
                .post(
                  "/public/client/authorization/sendOTPToMobile",
                  "",
                  payload
                )
                .then((resp) => {
                  if (resp.data.result === 0) {
                    toast.error(resp.data.message, {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                    });
                  } else {
                    setShow(true);
                    toast.success(resp.data.message, {
                      position: "top-right",
                      autoClose: 5000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                    });
                  }
                })
                .catch((err) => {
                  console.log("error");
                });
            }
          })
          .catch((err) => {
            console.log("error");
          });
      }
    },
  });
  const submitOTP = () => {
    const body = {
      mobileNo: formik.values.mobile,
      OTP: OTPvalue,
    };
    httpService
      .post("/public/client/authorization/verifyOTPsentToMobile", "", body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.success(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        if (resp?.data?.result === 1 && resp?.data?.verifyToken) {
          const payload = {
            firstName: formik.values.name,
            mobileNo: formik.values.mobile,
            password: formik.values.password,
            email: formik.values.email,
            verifyToken: (resp?.data?.verifyToken).toString(),
            gender: formik.values.gender,
            language: "en",
            city: formik.values.city,
            state: formik.values.state,
            country: "india",
            pinCode: "400073",
            address: "vasai",
            version: "1.15",
            deviceModel: "Samsung",
            accessPlatform: "android",
          };
          httpService
            .post("/public/client/signupv1", "", payload)
            .then((resp) => {
              if (resp.data.result === 0) {
                setShow(false);
                toast.error(resp.data.message, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              } else {
                setShow(false);
                toast.success(
                  resp.data.message,
                  {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  },
                  history.push("/login")
                );
              }
            })
            .catch((err) => {
              console.log("error");
            });
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };
  const customStyles = {
    content: {
      color: "darkred",
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
    overlay: {
      backgroundColor: "#444",
      opacity: "0.9",
    },
  };
  const style = {
    otpTimer: {
      margin: "10px",
      color: "blue",
    },
    resendBtn: {
      backgroundColor: "#0980FE",
      color: "white",
    },
  };

  const handleRoute = () => {
    history.push("/login");
  };

  return (
    <div className="layout-body">
      <Header />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <ToastContainer />
      <section className="layout-content bg-color1">
        <Container>
          <Row>
            <Col md={{ span: 10, offset: 1 }}>
              <div className="bg-white p-4 br-8 my-4 position-relative">
                <h5 className="font-weight-normal">Sign Up</h5>
                <img
                  src="assets/img/cancel.svg"
                  className="position-absolute fom-close"
                  onClick={handleRoute}
                />
                <Row>
                  <Col md={{ span: 8, offset: 2 }}>
                    <Form onSubmit={formik.handleSubmit}>
                      <Form.Group
                        controlId="formBasicName"
                        className="fmc-style"
                      >
                        <Form.Label>First Name *</Form.Label>
                        <Form.Control
                          type="text"
                          name="name"
                          placeholder="Enter Your Name"
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          value={formik.values.name}
                        />
                        {formik.touched.name && formik.errors.name ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.name}
                          </div>
                        ) : null}
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicMobile"
                        className="fmc-style"
                      >
                        <Form.Label>Mobile Number *</Form.Label>
                        <Form.Control
                          type="text"
                          name="mobile"
                          placeholder="Enter Mobile Number"
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          value={formik.values.mobile}
                        />
                        {formik.touched.mobile && formik.errors.mobile ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.mobile}
                          </div>
                        ) : null}
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicEmail"
                        className="fmc-style"
                      >
                        <Form.Label>Email *</Form.Label>
                        <Form.Control
                          type="email"
                          name="email"
                          placeholder="Enter email"
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          value={formik.values.email}
                        />
                        {formik.touched.email && formik.errors.email ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.email}
                          </div>
                        ) : null}
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicGender"
                        className="fmc-style"
                      >
                        <Form.Label>Gender *</Form.Label>
                        <br></br>
                        <div className="gender-btn">
                          <ToggleButtonGroup
                            type="radio"
                            name="gender"
                            defaultValue="male"
                          >
                            <ToggleButton
                              size="md"
                              variant="outline-primary"
                              value="male"
                              onClick={formik.handleChange}
                            >
                              Male
                            </ToggleButton>
                            <ToggleButton
                              size="md"
                              variant="outline-primary"
                              value="female"
                              onClick={formik.handleChange}
                            >
                              Female
                            </ToggleButton>
                            <ToggleButton
                              size="md"
                              variant="outline-primary"
                              value="other"
                              onClick={formik.handleChange}
                            >
                              Other
                            </ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicCity"
                        className="fmc-style"
                      >
                        <Form.Label>City</Form.Label>
                        <Form.Control
                          type="text"
                          name="city"
                          onChange={formik.handleChange}
                          placeholder="Enter city"
                        />
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicState"
                        className="fmc-style"
                      >
                        <Form.Label>State</Form.Label>
                        <Form.Control
                          type="text"
                          name="state"
                          onChange={formik.handleChange}
                          placeholder="Enter State"
                        />
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicPassword"
                        className="fmc-style position-relative"
                      >
                        <Form.Label>Create Password *</Form.Label>
                        <InputGroup>
                          <i
                            onClick={PasswordHandler}
                            className={
                              showPassword ? "fas fa-eye " : "fas fa-eye-slash"
                            }
                            style={{
                              position: "absolute",
                              right: 0,
                              zIndex: 5,
                              top: "13px",
                            }}
                          ></i>
                          <Form.Control
                            type={showPassword ? "text" : "password"}
                            name="password"
                            placeholder="Password"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.passsword}
                          />
                        </InputGroup>
                        {formik.touched.password && formik.errors.password ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.password}
                          </div>
                        ) : null}
                      </Form.Group>
                      <Form.Group
                        controlId="formBasicRePassword"
                        className="fmc-style"
                      >
                        <Form.Label>Re Enter Password *</Form.Label>
                        <InputGroup>
                          <i
                            onClick={RePasswordHandler}
                            className={
                              showRePassword
                                ? "fas fa-eye "
                                : "fas fa-eye-slash"
                            }
                            style={{
                              position: "absolute",
                              right: 0,
                              zIndex: 5,
                              top: "13px",
                            }}
                          ></i>
                          <Form.Control
                            type={showRePassword ? "text" : "password"}
                            name="Repassword"
                            placeholder="Re Password"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.Repasssword}
                          />
                        </InputGroup>
                        {formik.touched.Repassword &&
                        formik.errors.Repassword ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.Repassword}
                          </div>
                        ) : null}
                      </Form.Group>
                      <div className="p-3">
                        <Button variant="primary" block size="sm" type="Submit">
                          Continue
                        </Button>
                      </div>
                      <center>
                        <div className="custom-checkbox">
                          <input
                            id="checkbox-1"
                            className="checkbox-custom"
                            name="checkbox"
                            type="checkbox"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.checkbox}
                          />
                          <label
                            htmlFor="checkbox-1"
                            className="checkbox-custom-label text-color10 fs-12"
                          >
                            I AGREE and ACCEPT Terms & Conditions and Privacy
                            Policy
                          </label>
                          {formik.errors.checkbox ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.checkbox}
                            </div>
                          ) : null}
                        </div>
                      </center>
                    </Form>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
          <ReactModal
            isOpen={show}
            contentLabel="onRequestClose Example"
            onRequestClose={handleClose}
            shouldCloseOnOverlayClick={false}
            style={customStyles}
          >
            <h3>Enter OTP</h3>
            <div className="btn-modal-container">
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  id="otp"
                  name="otp"
                  maxLength="4"
                  onInput={(e) => setOTP(e.target.value.replace(/[^0-9]/g, ""))}
                />
                <span id="otpError">
                  Please check the OTP on your Mobile Number
                </span>
                <br />

                <br />
                <Otp
                  style={style}
                  minutes={2.0}
                  resendEvent={formik.handleSubmit}
                />
              </div>
              {/*<button type="button" className="btn btn-default" onClick={this.resendOTP}>Resend OTP</button>*/}
              <button
                type="button"
                className="btn btn-default"
                onClick={submitOTP}
              >
                Continue
              </button>
              <button
                type="button"
                className="btn btn-default"
                onClick={handleClose}
              >
                Close
              </button>
            </div>
          </ReactModal>
        </Container>
      </section>
      <Footer />
    </div>
  );
};
export default UserSignup;
