import React, { Component, useState, useEffect } from 'react';
import { Container, Row, Col, Button, Tab, Tabs, Nav, Table } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';
import httpService from '../../../services/httpService';


function YearlyHoroscopesSingle1(prop) {

  const [yearlyHoroscope, setYearlyHoroscope] = useState();
  const [horo, setHoro] = useState();
  const [year, setYear] = useState();
  const [logoImage, setLogoImage] = useState();


  const d = new Date();

  useEffect(() => {
    setYear(d.getFullYear())
    setHoro((prop.location.search.split('?')[1]).toLowerCase());
    getYearlyHoro();
    setLogoImage(prop.location.search.split('?')[1])
  }, []);

  const getYearlyHoro = () => {
    const body = {
      pageNo: 1
    }
    httpService.get('/' + (prop.location.search.split('?')[1]).toLowerCase(), '', body)
      .then((resp) => {
        setYearlyHoroscope(resp.data)
      })
      .catch((err) => {
        console.log('error');
      })
  }

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-2">
          <Container>
            <div className="px-md-5 d-flex justify-content-between align-items-center">
              <h5 className="mb-0 font-weight-normal">Yearly Horoscopes</h5>
              <div className="d-flex justify-content-end s-h-r-col align-items-center">
                <div className="text-right mr-2">
                  {horo && <b className="d-block text-capitalize">{horo}</b>}
                    Year {year}
                </div>
                <div className="s-h-img">
                  {logoImage && logoImage === 'Aries' && <img src="assets/img/horiscope/icon_Aries.svg" />}
                  {logoImage && logoImage === 'Taurus' && <img src="assets/img/horiscope/icon_Taurus.svg" />}
                  {logoImage && logoImage === 'Gemini' && <img src="assets/img/horiscope/icon_Gemini.svg" />}
                  {logoImage && logoImage === 'Cancer' && <img src="assets/img/horiscope/icon_Cancer.svg" />}
                  {logoImage && logoImage === 'Leo' && <img src="assets/img/horiscope/icon_Leo.svg" />}
                  {logoImage && logoImage === 'Virgo' && <img src="assets/img/horiscope/icon_Virgo.svg" />}
                  {logoImage && logoImage === 'Libra' && <img src="assets/img/horiscope/icon_Libra.svg" />}
                  {logoImage && logoImage === 'Scorpio' && <img src="assets/img/horiscope/icon_Scorpio.svg" />}
                  {logoImage && logoImage === 'Sagittarius' && <img src="assets/img/horiscope/icon_Sagittarius.svg" />}
                  {logoImage && logoImage === 'Capricon' && <img src="assets/img/horiscope/icon_Capricon.svg" />}
                  {logoImage && logoImage === 'Aquarius' && <img src="assets/img/horiscope/icon_Aquarius.svg" />}
                  {logoImage && logoImage === 'Pisces' && <img src="assets/img/horiscope/icon_Pisces.svg" />}

                </div>

              </div>
            </div>
          </Container>
        </div>
        <Container>
          <div className="rounded border  my-5 overflow-hidden">
            {horo && <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5 text-capitalize">About {horo}</label>}
            {yearlyHoroscope && <div className="px-sm-5 py-sm-4 p-2">
              <p dangerouslySetInnerHTML={{ __html: yearlyHoroscope }} ></p>
            </div>}
          </div>

          <div className="rounded border  my-5 overflow-hidden ">
            <label className="fw-500 p-3 px-sm-5 mb-0 bg-color2 text-white d-block">Ratings</label>
            <div className="px-sm-5">
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Personal Life</span>
                <div className="sm-capsul text-color2">7.5</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Profession</span>
                <div className="sm-capsul text-color2">24.5/36</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Health</span>
                <div className="sm-capsul text-color2">24.5/36</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Travel</span>
                <div className="sm-capsul text-color2">7.5</div>
              </div>

              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Luck</span>
                <div className="sm-capsul text-color2">7.5</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Emotions</span>
                <div className="sm-capsul text-color2">7.5</div>
              </div>
            </div>
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
}

export default YearlyHoroscopesSingle1;
