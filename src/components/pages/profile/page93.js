import React, { Component } from 'react';
import { Button, Row, Col, Container, Form } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';



class NewPassword extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content bg-color1">

          <div className="centered-div ">
            <div className="bg-white my-4 p-4 px-5 br-8 mw-350 position-relative text-center">
              <img src="assets/img/cancel.svg" className="position-absolute fom-close" />
              <h6 className="font-weight-normal my-3">Enter New Password</h6>

              <div className="my-5">
                <Form.Group controlId="formBasicPassword" className="fmc-style position-relative mb-4">

                  <Form.Control type="password" placeholder="Password" />

                </Form.Group>
                <Form.Group controlId="formBasicPassword" className="fmc-style">

                  <Form.Control type="password" placeholder="Re - Password" />
                </Form.Group>
              </div>
              <Button variant="primary" block size="sm">Reset</Button>
            </div>
          </div>


        </section>
        <Footer />
      </div>
    );
  }
}

export default NewPassword;