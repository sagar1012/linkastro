import React, { Component } from 'react';
import { updateBankDetails,getBankDetails,updateBankIdProof,GetBankIdproof } from '../../../actions/profileAction';
import { connect } from 'react-redux';

class Bank extends Component {
     
   state = {
      errors: {},
      fields: {},
   };

   async componentDidMount() {
      document.title = 'Astrologer Profile';
      const resultData =  await this.props.getBankDetails();
      if(resultData.data.result===1){
         var fields = this.state.fields;
         var rowData = resultData.data.data;
         Object.keys(rowData)
         .forEach(function eachKey(key) {
            fields[ key ] = rowData[ key ];
         });
         this.setState(fields);
         const ImgData = {
            filePath: rowData.bankIdProof,
         }
         const profile_pic =  await this.props.GetBankIdproof(ImgData);
         this.setState({ 'profile_picture':profile_pic })
      }
   } 
   onChange = (e) => { 
      this.handleValidation()
      var formIsValid = true;
      const errors = {};
      const fields = this.state.fields;
      if(e.target.name === 'accountName') {
         if(e.target.value.match('^[a-zA-Z ]*$') != null){
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'accountName' ]; 
         }
      } else if(e.target.name === 'ifscCode') {
         if(e.target.value.match( '^[a-zA-Z0-9]{0,11}$' ) != null){
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'ifscCode' ]; 
         }
      } else if(e.target.name === 'bankName' ) {
         if(e.target.value.match('^[a-zA-Z ]*$') != null){
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = ''; 
         }
      } else if(e.target.name === 'accountNumber') {
         if(e.target.value.match('^[0-9]*$') != null) {
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'accountNumber' ]; 
         }
      } else if(e.target.name === 'panNumber') {
         if(e.target.value.match('^[a-zA-Z0-9]{0,10}$') != null) {
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'panNumber' ]; 
         }
      
      } else if(e.target.name === 'adharNumber'){
         if(e.target.value.match('^[0-9]*$') != null){
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'adharNumber' ];
         }
      
      } else if(e.target.name==='accountType') {
         if(e.target.value.match('^[a-zA-Z ]*$') != null) {
            fields[ e.target.name ] = e.target.value; 
         } else {
            fields[ e.target.name ] = this.state.fields[ 'accountType' ]; 
         }
      
      } else {
         fields[ e.target.name ] = e.target.value; 
      }
      this.setState(fields);
      
      if(e.target.name === 'accountName'){
         var accountName = document.getElementById('acount_holder').value; 
         if(accountName === ''){
            formIsValid = false;
            errors[ 'accountName' ] = 'Please enter Account Holder Name';
         } else {
            if (/[^a-zA-Z ]/.test(accountName)) {
               formIsValid = false;
               errors[ 'accountName' ] = 'Invalid Account Holder Name';
            } else {
               errors[ 'accountName' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if (e.target.name==='ifscCode') {
         var ifscCode = document.getElementById('ifscCode').value;
         var nIfsc  = ifscCode.toUpperCase();
         if(nIfsc===0){
            formIsValid = false;
            errors[ 'ifscCode' ] = 'Please enter IFSC Code';
         } else {
            var patt =/^[A-Z]{4}[A-Z0-9]{7}$/;
            if (!patt.test(nIfsc)) {
               formIsValid = false;
               errors[ 'ifscCode' ] = 'Invalid IFSC Code';
            }/*else if(ifscCode.length < 11){ 
            formIsValid = false;
            errors["ifscCode"] = "Invalid IFSC Code";
            }*/
            else{
               errors[ 'ifscCode' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if (e.target.name === 'bankName') {
         var bankName = document.getElementById('bankName').value;
         if(bankName === '') {
            formIsValid = false;
            errors[ 'bankName' ] = 'Please enter Bank Name';
         } else {
            if (/[^a-zA-Z ]/.test(bankName)) {
               formIsValid = false;
               errors[ 'bankName' ] = 'Invalid Bank Name';
            } else {
               errors[ 'bankName' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if (e.target.name === 'accountNumber') {
         var accountNumber = document.getElementById('accountNumber').value;
         if(accountNumber.length === 0 ) {
            formIsValid = false;
            errors[ 'accountNumber' ] = 'Please enter Bank Account Number';
         }else{
            if (/[^0-9]/.test(fields[ 'accountNumber' ])) {
            formIsValid = false;
            errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
            } else if(accountNumber.length < 9) {
               formIsValid = false;
               errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
            } else if(accountNumber.length > 20) {
               formIsValid = false;
               errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
            } else {
               errors[ 'accountNumber' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if (e.target.name === 'panNumber') {
         var panNumber = document.getElementById('panNumber').value;
         var NpanNumber  = panNumber.toUpperCase()
         if(NpanNumber.length===0){
            formIsValid = false;
            errors[ 'panNumber' ] = 'Please enter PAN Number';
         } else {
            var patt2 =/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if (!patt2.test(NpanNumber)) {
               formIsValid = false;
               errors[ 'panNumber' ] = 'Invalid PAN Number';
            }/*else if(NpanNumber.length < 10){
            formIsValid = false;
            errors["panNumber"] = "Invalid PAN Number";
            }*/else {
               errors[ 'panNumber' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if (e.target.name === 'adharNumber') {
         var adharNumber = document.getElementById('adharNumber').value;
         if(adharNumber.length === 0){
            formIsValid = false;
            errors[ 'adharNumber' ] = 'Please enter Aadhar Card Number';
         } else {
            if (/[^0-9]/.test(fields[ 'adharNumber' ])) {
               formIsValid = false;
               errors[ 'adharNumber' ] = 'Invalid Adhar Card Number';
            } else if(adharNumber.length < 12) {
               formIsValid = false;
               errors[ 'adharNumber' ] = 'Invalid Adhar Card Number';
            } else {
               errors[ 'adharNumber' ] = '';
            }
         }
         this.setState({ errors: errors });
      } else if(e.target.name === 'accountType') {
         var accountType = document.getElementById('accountType').value;
         if(accountType.length === 0) {
            formIsValid = false;
            errors[ 'accountType' ] = 'Please enter Account Type';
         } else {
            if (/[^a-zA-Z]/.test(accountType)) {
               formIsValid = false;
               errors[ 'accountType' ] = 'Invalid Account Type';
            } else {
               errors[ 'accountType' ] = '';
            }
         }
         this.setState({ errors: errors });
      }  
   }
     
  /*   onChange = (e) => { 
       this.handleValidation()
       let formIsValid = true;
       let errors = {};
       let fields = this.state.fields;
       fields[e.target.name] = e.target.value; 
       this.setState(fields);
   }*/
     // upload bank id proof
   onChangeFile = async (e) => {  
      var file = e.target.files[ 0 ];
      var formData = new FormData();
      formData.append('fileName', file.name)
      formData.append('fileData', file)
      var ImagePath =  await this.props.updateBankIdProof(formData);
      this.setState({ 'profile_picture':ImagePath })
   }

   handleValidation(){
      const fields = this.state.fields;
      const errors = {};
      let formIsValid = true;
      var accountName = document.getElementById('acount_holder').value;
      if(accountName === '') {
         formIsValid = false;
         errors[ 'accountName' ] = 'Please enter Account Holder Name';
      } else {
         if (/[^a-zA-Z ]/.test(accountName)) {
            formIsValid = false;
            errors[ 'accountName' ] = 'Invalid Account Holder Name';
         } else {
            errors[ 'accountName' ] = '';
         }
      }
       
      var ifscCode = document.getElementById('ifscCode').value;
      var nIfsc  = ifscCode.toUpperCase();
      if(ifscCode === 0 ) {
         formIsValid = false;
         errors[ 'ifscCode' ] = 'Please enter IFSC Code';
      } else {
         var patt3 =/^[A-Z]{4}[A-Z0-9]{7}$/;
         if (!patt3.test(nIfsc)) {
            formIsValid = false;
            errors[ 'ifscCode' ] = 'Invalid IFSC Code';
         }/*else if(ifscCode.length < 11){ 
            formIsValid = false;
            errors["ifscCode"] = "Invalid IFSC Code";
         }*/else {
            errors[ 'ifscCode' ] = '';
         }
      }
      
      var bankName = document.getElementById('bankName').value;
      if(bankName===''){
         formIsValid = false;
         errors[ 'bankName' ] = 'Please enter Bank Name';
      } else {
         if (/[^a-zA-Z ]/.test(bankName)) {
            formIsValid = false;
            errors[ 'bankName' ] = 'Invalid Bank Name';
         } else {
            errors[ 'bankName' ] = '';
         }
      }

      var accountNumber = document.getElementById('accountNumber').value;
      if(accountNumber.length === 0){
         formIsValid = false;
         errors[ 'accountNumber' ] = 'Please enter Bank Account Number';
      } else {
         if (/[^0-9]/.test(fields[ 'accountNumber' ])) {
            formIsValid = false;
            errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
         } else if(accountNumber.length < 9) {
            formIsValid = false;
            errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
         } else if(accountNumber.length > 20){
            formIsValid = false;
            errors[ 'accountNumber' ] = 'Please enter valid Bank Account Number';
         } else {
            errors[ 'accountNumber' ] = '';
         }
      }
       
      var panNumber = document.getElementById('panNumber').value;
      var NpanNumber  = panNumber.toUpperCase()
      if(NpanNumber.length===0){
         formIsValid = false;
         errors[ 'panNumber' ] = 'Please enter PAN Number';
      } else {
         var patt =/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
         if (!patt.test(NpanNumber)) {
            formIsValid = false;
            errors[ 'panNumber' ] = 'Invalid PAN Number';
         }/*else if(panNumber.length < 10 || panNumber.length  > 10){
         formIsValid = false;
         errors["panNumber"] = "Invalid PAN Number";
         }*/else {
            errors[ 'panNumber' ] = '';
         }
      }
      
      var adharNumber = document.getElementById('adharNumber').value;
      if(adharNumber.length===0){
         formIsValid = false;
         errors[ 'adharNumber' ] = 'Please enter Aadhar Card Number';
      } else {
         if (/[^0-9]/.test(fields[ 'adharNumber' ])) {
            formIsValid = false;
            errors[ 'adharNumber' ] = 'Invalid Adhar Card Number';
         } else if(adharNumber.length < 12) {
            formIsValid = false;
            errors[ 'adharNumber' ] = 'Invalid Adhar Card Number';
         } else {
            errors[ 'adharNumber' ] = '';
         }
      }
       
      var accountType = document.getElementById('accountType').value;
      if(accountType.length === 0){
         formIsValid = false;
         errors[ 'accountType' ] = 'Please enter Account Type';
      } else {
         if (/[^a-zA-Z]/.test(accountType)) {
            formIsValid = false;
            errors[ 'accountType' ] = 'Invalid Account Type';
         } else {
            errors[ 'accountType' ] = '';
         }
      }
      this.setState({ errors: errors });
      return formIsValid;
   }

   onSubmit = async e => {
      e.preventDefault();
      if(this.handleValidation()){
      const userData = {
         accountName : this.state.fields[ 'accountName' ],
         ifscCode    : this.state.fields[ 'ifscCode' ].toUpperCase(),
         bankName    : this.state.fields[ 'bankName' ],
         accountNumber: this.state.fields[ 'accountNumber' ],
         panNumber   : this.state.fields[ 'panNumber' ].toUpperCase(),
         adharNumber : this.state.fields[ 'adharNumber' ],
         accountType : this.state.fields[ 'accountType' ],
      }
         await this.props.updateBankDetails(userData);
      } else {
       //toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
      }
   }

   render() {
      return (
          <div className="astrologer-user-tab">
              <form onSubmit={ this.onSubmit }>
                  <div className="astrologer-user-dtl" id="bank">
                      <div className="sign-form">
                          <div className="form-group focused">
                              <input type="text" className="form-control name-val" onChange={ this.onChange }  id="acount_holder" placeholder=" " name="accountName" value={ this.state.fields[ 'accountName' ] }/>
                              <label for="acount_holder" className="form-label">Account Holder Name<span style={ { 'color':'red' } }>*</span></label>
                              <span style={ { color: 'red' } }>{this.state.errors[ 'accountName' ]}</span>
                          </div>
                          <div className="form-div">
                              <div className="row">
                                  <div className="col-md-6">
                                      <div className="form-group focused"> 
                                          <input type="text" className="form-control name-val" onChange={ this.onChange } id="bankName" placeholder=" " name="bankName" value={ this.state.fields[ 'bankName' ] } />
                                          <label for="bankName" className="form-label">Bank Name<span style={ { 'color':'red' } }>*</span></label>
                                          <span style={ { color: 'red' } }>{this.state.errors[ 'bankName' ]}</span>
                                      </div>
                                  </div>
                                  <div className="col-md-6">
                                      <div className="form-group focused">
                                          <input type="text" className="form-control alphaN" style={ { 'text-transform':'uppercase' } } onChange={ this.onChange } id="ifscCode" placeholder=" " maxLength="11" name="ifscCode" value={ this.state.fields[ 'ifscCode' ] }/>
                                          <label for="ifscCode" className="form-label">IFSC <span style={ { 'color':'red' } }>*</span></label>
                                          <span style={ { color: 'red' } }>{this.state.errors[ 'ifscCode' ]}</span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div className="form-group focused">
                              <input type="text" className="form-control Numeric1"  onChange={ this.onChange } id="accountNumber" placeholder=" " name="accountNumber" maxlength="20" value={ this.state.fields[ 'accountNumber' ] } onInput={ (e)=>{  e.target.value = e.target.value.replace(/[^0-9]/g,'')} } />
                              <label for="accountNumber" className="form-label">Bank Account Number<span style={ { 'color':'red' } }>*</span></label>
                              <span style={ { color: 'red' } }>{this.state.errors[ 'accountNumber' ]}</span>
                          </div>
                          <div className="form-div">
                              <div className="row">
                                  <div className="col-md-6">
                                      <div className="form-group focused">
                                          <input type="text" style={ { 'text-transform':'uppercase' } } className="form-control alphaN" onChange={ this.onChange } id="panNumber" placeholder=" " maxLength="10"  name="panNumber" value={ this.state.fields[ 'panNumber' ] }/>
                                          <label for="panNumber" className="form-label">PAN <span style={ { 'color':'red' } }>*</span></label>
                                          <span style={ { color: 'red' } }>{this.state.errors[ 'panNumber' ]}</span>
                                      </div>
                                  </div>
                                  <div className="col-md-6">
                                      <div className="form-group focused">
                                          <input type="text" className="form-control Numeric1" onChange={ this.onChange } id="adharNumber"  placeholder=" " maxlength="12" name="adharNumber" value={ this.state.fields[ 'adharNumber' ] } onInput={ (e)=>{  e.target.value = e.target.value.replace(/[^0-9]/g,'')} }/>
                                          <label for="adharNumber" className="form-label">Aadhar Number<span style={ { 'color':'red' } }>*</span></label>
                                          <span style={ { color: 'red' } }>{this.state.errors[ 'adharNumber' ]}</span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div className="form-group focused">
                              <input type="text" className="form-control name-val" onChange={ this.onChange } id="accountType" placeholder=" " name="accountType" value={ this.state.fields[ 'accountType' ] } />
                              <label for="accountType" className="form-label">Account Type<span style={ { 'color':'red' } } >*</span></label>
                              <span style={ { color: 'red' } }>{this.state.errors[ 'accountType' ]}</span>
                          </div>
                          <div className="form-group">
                              <label>Upload ID proof</label>
                              { this.state.profile_picture? <span>
                                  <a target='_blank' rel="noopener" href={ this.state.profile_picture } >
                                      <i className="fa fa-eye" aria-hidden="true"></i>
                                  </a></span>: ''}
                              {/*<span className="user-img"><img src={this.state.profile_picture} alt=""/></span>*/}
                              <div className="row">
                                  <div className="col-5">
                                      <div className="add-btn-div">
                                          <input type="file" onChange={ this.onChangeFile }  id="fileName" name="fileName" />
                                          <label className="file-add">Add</label> 
                                      </div>
                                  </div>
                          
                              </div>
                          </div>

                          <div className="form-group">
                              <button type="submit" className="btn continue_btn">Save & Continue</button>
                          </div>
                      </div>
                  </div>
              </form>
          </div>                            
      )
   }
}
const mapStateToProps = state => ({
  bankDetails: state.bankDetails,
  errors: state.errors.error
});
export default connect( mapStateToProps, { updateBankDetails,getBankDetails,updateBankIdProof,GetBankIdproof })(Bank);