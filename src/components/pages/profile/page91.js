import React, { Component } from 'react';
import { Button, Row, Col, Container, Form } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';



class LogOut extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content bg-color1">

          <div className="centered-div">
            <div className="bg-white p-4 px-5 br-8 mw-350 position-relative text-center">
              <img src="assets/img/cancel.svg" className="position-absolute fom-close" />
              <h6 className="font-weight-normal my-5">Loged Out</h6>

              <Button variant="outline-primary" block size="sm" className="mb-3">Login</Button>
              <Button variant="primary" block size="sm">Done</Button>
            </div>
          </div>


        </section>
        <Footer />
      </div>
    );
  }
}

export default LogOut;