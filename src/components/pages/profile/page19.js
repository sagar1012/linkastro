import React, { Component } from "react";
import {
  Button,
  Container,
  Table,
  Row,
  Col,
  Tab,
  Nav,
  Tabs,
} from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { reactLocalStorage } from "reactjs-localstorage";
import { connect } from "react-redux";
import Moment from "moment";
import {
  getCallLogDetail,
  getChathistoryDetail,
  getReportDetail,
} from "../../../actions/LogAction";

class Logs extends Component {
  async componentDidMount() {
    var token = reactLocalStorage.getObject("token");
    const data = Object.keys(token).length;

    // chat detail----------------
    if (data > 0) {
      let em = [];
      const CallLog = await this.props.getCallLogDetail();
      const LogData = CallLog.data.checkResult;

      console.log("calll", CallLog);
      console.log("calllLog", LogData);

      if (LogData?.length > 0) {
        var temp = "";

        LogData.forEach((u) => {
          temp +=
            "<div class='border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480' id='call5'>";
          temp += "<img src=" + u.profileImageDownloadLink + " />";
          temp += "<div class='flex-1 fs-12 ml-md-3 my-xs-2' id='call1'>";
          temp +=
            "<p id='call2' class='mb-1' id='call3'>" +
            u.first_name +
            " " +
            u.last_name +
            "</p>";
          temp +=
            "<span class='px-md-3 pt-2 fs-12'>" +
            u.call_duration +
            " " +
            "min" +
            "</span>";
          temp += "</div>";
          temp +=
            "<div class='' id='call4'>" +
            Moment(u.updated_on).format("DD-MM-YYYY") +
            "</div></div>";
        });
        document.getElementById("data1").innerHTML = temp;
      }
    }

    // !!!!!!!!call ddetail----------------
    if (data > 0) {
      const ChateDetail = await this.props.getChathistoryDetail();
      const LogChate = ChateDetail.data.data;

      console.log("chate", ChateDetail);
      console.log("chate", LogChate);

      if (LogChate?.length > 0) {
        var temp = "";

        LogChate.forEach((u) => {
          temp +=
            "<div class='border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480' id='call5'>";
          temp += "<img src=" + u.profilePictureDownloadLink + " >";
          temp += "<div class='flex-1 fs-12 ml-md-3 my-xs-2' id='call1'>";
          temp +=
            "<p id='call2' class='mb-1' id='call3'>" +
            u.firstName +
            " " +
            u.lastName +
            "</p>";
          temp +=
            "<span class='px-md-3 pt-2 fs-12'>" +
            Math.ceil(
              (new Date(u.session_end_time).getTime() -
                new Date(u.session_start_time).getTime()) /
                60000
            ) +
            " " +
            "min" +
            "</span>";
          temp += "</div>";
          temp +=
            "<div class='' id='call4'>" +
            Moment(u.created_on).format("DD-MM-YYYY") +
            "</div></div>";
        });
        document.getElementById("chate1").innerHTML = temp;
      }
    }

    // report detail-------------------
    if (data > 0) {
      const ReportDetail = await this.props.getReportDetail();
      const LogReport = ReportDetail.data.data;

      console.log("Report", ReportDetail);
      console.log("Reportlo", LogReport);

      if (LogReport?.length > 0) {
        var temp = "";

        LogReport.forEach((u) => {
          temp +=
            "<div class='border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480' id='call5'>";
          temp += "<img src=" + u.profileImageDownloadLink + " >";
          temp += "<div class='flex-1 fs-12 ml-md-3 my-xs-2' id='call1'>";
          temp +=
            "<p id='call2' class='mb-1' id='call3'>" +
            u.first_name +
            " " +
            u.last_name +
            "</p>";
          temp +=
            "<span class='px-md-3 pt-2 fs-12'>" + u.report_types + "</span>";
          temp += "</div>";
          temp += "<div>";
          temp +=
            "<div class='' id='call4'>" +
            Moment(u.updated_on).format("DD-MM-YYYY") +
            "</div>";
          temp +=
            "<p  id='color'class='fas mb-0 text-success '>" + u.status + "</p>";
          temp += "</div></div>";

          document.getElementById("Report1").innerHTML = temp;
        });
      }
    }
  }

  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <div className="bg-white shadow-bold p-3">
              <Container>
                <Row className="align-items-center">
                  <Col md={5}>
                    <h5 className="font-weight-normal mb-0">Logs</h5>{" "}
                  </Col>
                  <Col md={7}>
                    <div className="tab--style">
                      <Nav variant="pills" className="flex-row border rounded">
                        <Nav.Item>
                          <Nav.Link eventKey="first">Call</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="second">Chat</Nav.Link>
                        </Nav.Item>

                        <Nav.Item>
                          <Nav.Link eventKey="third">Report </Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </div>
                  </Col>
                </Row>
                <div className="d-flex">
                  <h5 className="font-weight-normal mb-0">
                    Consultation and Earning
                  </h5>
                  <div className="flex-1"></div>
                </div>
              </Container>
            </div>

            <Container>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <div className="px-md-5 p-sm-2">
                    <div className="px-md-4" id="data2">
                      <div className="my-3 text-right">
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      </div>

                      <div id="data1"></div>

                      <div className="border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480">
                        <img src="assets/img/priest.png" />
                        <div className="flex-1 fs-12 ml-md-3 my-xs-2">
                          <p className="mb-1">Ashish Pawar</p>
                          <span>
                            Ansjhg laksh msksnffi sick knjlsji nau cjcnjcu nxn
                            xfnfhj diskj
                          </span>
                        </div>
                        <div className="px-md-3 pt-2 fs-12">27/09/2020</div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>

                <Tab.Pane eventKey="second">
                  <div className="px-md-5 p-sm-2">
                    <div className="px-md-4">
                      <div className="my-3 text-right">
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      </div>
                      <div id="chate1"></div>
                    </div>
                  </div>
                </Tab.Pane>

                <Tab.Pane eventKey="third">
                  <div className="px-md-5 p-sm-2">
                    <div className="px-md-4">
                      <div className="my-3 text-right">
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      </div>
                      <div id="Report1"></div>

                      <div className="border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480">
                        <img src="assets/img/priest.png" />
                        <div className="flex-1 fs-12 ml-md-3 my-xs-2">
                          <p className="mb-1"></p>
                          <span></span>
                        </div>
                        <div className="px-md-3 pt-2 fs-12 text-sm-right">
                          <p className="mb-0 text-color1" id="p1"></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
              </Tab.Content>

              <div className="p-5 text-center">
                <Button
                  variant="primary"
                  className="btn-capsule px-5"
                  size="sm"
                >
                  View More
                </Button>
              </div>
            </Container>
          </Tab.Container>
        </section>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  getCallLogDetail: state.getCallLogDetail,
  getChathistoryDetail: state.getChathistoryDetail,
  getReportDetail: state.getReportDetail,
  errors: state.errors.error,
});

export default connect(mapStateToProps, {
  getCallLogDetail,
  getChathistoryDetail,
  getReportDetail,
})(Logs);
