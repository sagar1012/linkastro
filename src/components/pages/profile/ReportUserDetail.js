import React, { useEffect, useState } from "react";
import {
  Col,
  Container,
  Row,
  Form,
  Button,
  ToggleButtonGroup,
  ToggleButton,
} from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import {
  TimePicker,
  DatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import "./ReportUserDetail.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import clientServiceAPI from "../../../services/clientServiceAPI";

const ReportUserDetail = (props) => {
  const memberId = props?.location?.state?.memberId;
  const reportType = props?.location?.state?.reportType;
  const history = useHistory();
  const [timeOpen, setTimeOpen] = useState(false);
  const [dateOpen, setDateOpen] = useState(false);
  const [partnerTimeOpen, setPartnerTimeOpen] = useState(false);
  const [partnerDateOpen, setPartnerDateOpen] = useState(false);
  const [checkbox, setCheckbox] = useState(false);
  const [astrologer, setAstrologer] = useState();
  const [astroSkill, setAstroSkill] = useState();

  const formik = useFormik({
    initialValues: {
      mfirstName: "",
      mlastName: "",
      mBirthDate: "",
      mBirthDay: "",
      mBirthMonth: "",
      mBirthYear: "",
      mBirthTime: "",
      mBirthHour: "",
      mBirthMin: "",
      mBirthAM: "",
      mBirthPlace: "",
      mgender: "male",
      MaritalStatus: "single",
      reportLanguage: ["hindi"],
      question: "",
      partnerFirstName: "",
      partnerLastName: "",
      PartnerBirthDate: "",
      PartnerBirthDay: "",
      PartnerBirthMonth: "",
      PartnerBirthYear: "",
      PartnerBirthTime: "",
      PartnerBirthHour: "",
      PartnerBirthMin: "",
      PartnerBirthAM: "",
      PartnerBirthPlace: "",
      partnergender: "",
      partnerDetail: "false",
    },
    validationSchema: Yup.object({
      mfirstName: Yup.string().required("First Name Required"),
      mlastName: Yup.string().required("Last Name Required"),
      mgender: Yup.string().required("Please Select gender"),
      mBirthDay: Yup.string().required("Date of Birth Required"),
      mBirthHour: Yup.string().required("Time OF Birth Required"),
      mBirthPlace: Yup.string().required("Place of Birth Required"),
      question: Yup.string().required("Enter your Question Here"),
      MaritalStatus: Yup.string().required("Marital Status Required"),
      reportLanguage: Yup.array().required("Please Select Language"),
      partnerDetail: Yup.boolean(),

      partnerFirstName: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("First Name is required"),
        otherwise: Yup.string(),
      }),
      partnerLastName: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("Last Name Required"),
        otherwise: Yup.string(),
      }),
      partnergender: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("Please Select gender"),
        otherwise: Yup.string(),
      }),
      PartnerBirthDay: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("Date of Birth Required"),
        otherwise: Yup.string(),
      }),
      PartnerBirthHour: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("Time OF Birth Required"),
        otherwise: Yup.string(),
      }),
      PartnerBirthPlace: Yup.string().when("partnerDetail", {
        is: true,
        then: Yup.string().required("Place of Birth Required"),
        otherwise: Yup.string(),
      }),
    }),

    onSubmit: (values) => {
      const body = {
        astromemberId: `${memberId}`,
        reportType: `${reportType}`,
        mFirstName: values.mfirstName,
        mLastName: values.mlastName,
        mbirthDate: values.mBirthDate,
        mbirthTime: values.mBirthTime,
        mLocation: values.mBirthPlace,
        mGender: values.mgender,
        maritalStatus: values.MaritalStatus,
        language: `${values.reportLanguage}`,
        question: values.question,
        fFirstName: values.partnerFirstName,
        fLastName: values.partnerLastName,
        fbirthDate: values.PartnerBirthDate,
        fbirthTime: values.PartnerBirthTime,
        fLocation: values.PartnerBirthPlace,
        fGender: values.partnergender,
      };

      clientServiceAPI.reportDetail(body).then((resp) => {
        if (resp.status) {
          toast.success(
            resp.data.message,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            },
            history.push("/")
          );
        } else {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    },
  });

  const handleDateChange = (e) => {
    formik.setFieldValue("mBirthDay", moment(e).format("DD"));
    formik.setFieldValue("mBirthMonth", moment(e).format("MM"));
    formik.setFieldValue("mBirthYear", moment(e).format("YYYY"));
    formik.setFieldValue("mBirthDate", moment(e).format("YYYY-DD-MM"));
  };

  const handleTimeChange = (e) => {
    formik.setFieldValue("mBirthHour", moment(e).format("hh"));
    formik.setFieldValue("mBirthMin", moment(e).format("mm"));
    formik.setFieldValue("mBirthAM", moment(e).format("A"));
    formik.setFieldValue("mBirthTime", moment(e).format("hh:mm:a"));
  };

  const partnerDateChange = (e) => {
    formik.setFieldValue("PartnerBirthDay", moment(e).format("DD"));
    formik.setFieldValue("PartnerBirthMonth", moment(e).format("MM"));
    formik.setFieldValue("PartnerBirthYear", moment(e).format("YYYY"));
    formik.setFieldValue("PartnerBirthDate", moment(e).format("YYYY-DD-MM"));
  };

  const partnerTimeChange = (e) => {
    formik.setFieldValue("PartnerBirthHour", moment(e).format("hh"));
    formik.setFieldValue("PartnerBirthMin", moment(e).format("mm"));
    formik.setFieldValue("PartnerBirthAM", moment(e).format("A"));
    formik.setFieldValue("PartnerBirthTime", moment(e).format("hh:mm:a"));
  };

  const checkboxHandler = (e) => {
    setCheckbox(e.target.checked);
    formik.setFieldValue("partnerDetail", e.target.checked);
  };

  useEffect(() => {
    getAstrologersList();
    getPersonalDetails();
  }, []);

  const getAstrologersList = () => {
    const body = {
      pageNo: 1,
    };
    clientServiceAPI
      .getAstroListv1(body)
      .then((resp) => {
        if (resp.status) {
          setAstrologer(resp?.data);
          const skilArr = resp?.data?.professionalskills;
          skilArr.map((item) => {
            if (item.memberid === memberId) {
              setAstroSkill(item.skillsList);
            }
          });
        } else {
          console.log("error");
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getPersonalDetails = () => {
    clientServiceAPI
      .getPersonalDetail()
      .then((resp) => {
        if (resp?.data?.result === 1) {
          let personalDetailClient = resp?.data?.personal?.result[0];
          console.log(personalDetailClient);
          formik.setFieldValue("mfirstName", personalDetailClient?.first_name);
          formik.setFieldValue("mlastName", personalDetailClient?.last_name);
          formik.setFieldValue(
            "mBirthDate",
            moment(personalDetailClient?.birth_date).format("YYYY-DD-MM")
          );
          formik.setFieldValue(
            "mBirthDay",
            moment(personalDetailClient?.birth_date).format("DD")
          );
          formik.setFieldValue(
            "mBirthMonth",
            moment(personalDetailClient?.birth_date).format("MM")
          );
          formik.setFieldValue(
            "mBirthYear",
            moment(personalDetailClient?.birth_date).format("YYYY")
          );

          formik.setFieldValue(
            "mBirthTime",
            moment(personalDetailClient?.birth_time).format("hh:mm:a")
          );
          formik.setFieldValue(
            "mBirthHour",
            personalDetailClient?.birth_time.split(":")[0]
          );
          formik.setFieldValue(
            "mBirthMin",
            personalDetailClient?.birth_time.split(":")[1]
          );
          formik.setFieldValue(
            "mBirthAM",
            personalDetailClient?.birth_time.split(":")[2]
          );
          formik.setFieldValue("mgender", personalDetailClient?.gender);
          formik.setFieldValue(
            "mBirthPlace",
            personalDetailClient?.birth_location
          );
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <Row>
              <div className="col-md-6">
                <Col
                  className="bg-gray px-md-5 d-flex align-items-center flex-wrap"
                  style={{
                    padding: "12px",
                    justifyContent: "space-between",
                  }}
                >
                  <div className="d-flex">
                    <div
                      className=""
                      style={{ background: "#F2F2F2", marginRight: "12px" }}
                    >
                      <img
                        src="assets/img/horiscope/Icon_Education.svg"
                        style={{ height: "35px", width: "40px" }}
                      />
                    </div>
                    <div>
                      <h6 className="font-weight-bold mb-0 mr-sm-5">
                        Education Reading
                      </h6>

                      <span className=" font-weight-bold mb-0">
                        For 3 years
                      </span>
                    </div>
                  </div>
                  <div>
                    <a
                      href="/getYourReport"
                      className="font-weight-bold"
                      style={{ color: "#3b93f1" }}
                    >
                      <u>change</u>
                    </a>
                  </div>
                </Col>
              </div>
              <div className="col-md-6">
                <Col
                  className="bg-gray px-md-5 d-flex align-items-center flex-wrap"
                  style={{ padding: "12px", justifyContent: "space-between" }}
                >
                  {astrologer?.checkResult?.map((member, index) => {
                    if (member?.member_id === memberId) {
                      return (
                        <div className="d-flex" key={index}>
                          <div
                            className=""
                            style={{
                              background: "#F2F2F2",
                              marginRight: "12px",
                            }}
                          >
                            <img
                              src={
                                member.profile_picture
                                  ? member.profile_picture
                                  : require("../../../assets/images/priest_vishnuvardhana@2x.png")
                              }
                              style={{ height: "35px", width: "40px" }}
                            />
                          </div>
                          <div className="asdetail">
                            <h6 className="font-weight-bold mb-0 mr-sm-5">
                              {member.display_name}
                            </h6>

                            <p className=" mb-0">
                              {member.experience} yrs exp.{" "}
                              {astroSkill?.map((item) => {
                                return item.skill_name + ", ";
                              })}
                            </p>
                          </div>
                        </div>
                      );
                    }
                  })}
                  <div>
                    <a
                      href="/educationReading"
                      className="font-weight-bold"
                      style={{ color: "#3b93f1" }}
                    >
                      <u>change</u>
                    </a>
                  </div>
                </Col>
              </div>
            </Row>
          </Container>
        </div>
        <div>
          <div
            style={{
              textAlign: "center",
              fontSize: "25px",
              fontWeight: "400",
              margin: "24px",
            }}
          >
            <span>Please Provide Details for Report</span>
          </div>
          <Container>
            <Form onSubmit={formik.handleSubmit}>
              <Row>
                <div className="col-md-6">
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="mfirstName">First Name</Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <Form.Control
                        type="text"
                        name="mfirstName"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.mfirstName}
                      />
                      {formik.touched.mfirstName && formik.errors.mfirstName ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.mfirstName}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="mlastName">Last Name</Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <Form.Control
                        type="text"
                        name="mlastName"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.mlastName}
                      />
                      {formik.touched.mlastName && formik.errors.mlastName ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.mlastName}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="dateofbirth">
                        Date OF Birth
                      </Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DatePicker
                          open={dateOpen}
                          onOpen={() => setDateOpen(true)}
                          onClose={() => setDateOpen(false)}
                          name="mBirthDate"
                          value={formik.values.mBirthDate}
                          onChange={handleDateChange}
                          TextFieldComponent={() => null}
                        />
                      </MuiPickersUtilsProvider>
                      <input
                        type="button"
                        name="mBirthDay"
                        value={
                          formik.values.mBirthDay
                            ? formik.values.mBirthDay
                            : "Day"
                        }
                        className={
                          formik.values.mBirthDay
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setDateOpen(true)}
                      />
                      <input
                        type="button"
                        name="mBirthMonth"
                        value={
                          formik.values.mBirthMonth
                            ? formik.values.mBirthMonth
                            : "Month"
                        }
                        className={
                          formik.values.mBirthMonth
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setDateOpen(true)}
                      />
                      <input
                        type="button"
                        name="mBirthYear"
                        value={
                          formik.values.mBirthYear
                            ? formik.values.mBirthYear
                            : "Year"
                        }
                        className={
                          formik.values.mBirthYear
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setDateOpen(true)}
                      />
                      {formik.touched.mBirthDay && formik.errors.mBirthDay ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.mBirthDay}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="dateofbirth">
                        Time OF Birth
                      </Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <TimePicker
                          open={timeOpen}
                          onOpen={() => setTimeOpen(true)}
                          onClose={() => setTimeOpen(false)}
                          name="mBirthTime"
                          value={formik.values.mBirthTime}
                          onChange={handleTimeChange}
                          TextFieldComponent={() => null}
                        />
                      </MuiPickersUtilsProvider>
                      <input
                        type="button"
                        name="mBirthHour"
                        value={
                          formik.values.mBirthHour
                            ? formik.values.mBirthHour
                            : "HH"
                        }
                        className={
                          formik.values.mBirthHour
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setTimeOpen(true)}
                      />
                      <input
                        type="button"
                        name="mBirthMin"
                        value={
                          formik.values.mBirthMin
                            ? formik.values.mBirthMin
                            : "MM"
                        }
                        className={
                          formik.values.mBirthMin
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setTimeOpen(true)}
                      />
                      <input
                        type="button"
                        name="mBirthAM"
                        value={
                          formik.values.mBirthAM ? formik.values.mBirthAM : "AM"
                        }
                        className={
                          formik.values.mBirthAM
                            ? "btn btn-primary m-2 tglbtn"
                            : "btn btn-white border m-2 tglbtn"
                        }
                        onClick={(e) => setTimeOpen(true)}
                      />
                      {formik.touched.mBirthHour && formik.errors.mBirthHour ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.mBirthHour}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="BirthPlace">BirthPlace</Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <Col>
                        <i
                          className="fa fa-search iconf"
                          aria-hidden="true"
                        ></i>
                        <Form.Control
                          type="text"
                          name="mBirthPlace"
                          className="inplace"
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          value={formik.values.mBirthPlace}
                        />
                        {formik.touched.mBirthPlace &&
                        formik.errors.mBirthPlace ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.mBirthPlace}
                          </div>
                        ) : null}
                      </Col>
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="gender">Gender *</Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <ToggleButtonGroup
                        type="radio"
                        name="mgender"
                        value={formik.values.mgender}
                        defaultValue={formik.values.mgender}
                        id="gender1"
                        onChange={formik.handleChange}
                      >
                        <ToggleButton
                          size="sm"
                          variant="outline-primary tglbtn m-2"
                          value="male"
                          onChange={formik.handleChange}
                        >
                          Male
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary tglbtn m-2"
                          value="female"
                          onChange={formik.handleChange}
                        >
                          Female
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary tglbtn m-2"
                          value="other"
                          onChange={formik.handleChange}
                        >
                          Other
                        </ToggleButton>
                      </ToggleButtonGroup>
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="maritalStatus">
                        Marital Status *
                      </Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <ToggleButtonGroup
                        type="radio"
                        name="maritalStatus"
                        defaultValue="single"
                        id="mrt"
                      >
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="single"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Single
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="married"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Married
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="widowed"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Widowed
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="divorced"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Divorced
                        </ToggleButton>
                      </ToggleButtonGroup>
                      {formik.touched.maritalStatus &&
                      formik.errors.maritalStatus ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.maritalStatus}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="reportLanguage">
                        Report Language *
                      </Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <ToggleButtonGroup
                        type="checkbox"
                        name="reportLanguage"
                        value={formik.values.reportLanguage}
                        defaultValue={formik.values.reportLanguage}
                      >
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="hindi"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Hindi
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="english"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          English
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="Gujrati"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Gujrati
                        </ToggleButton>
                        <ToggleButton
                          size="sm"
                          variant="outline-primary m-2"
                          value="marathi"
                          className="tglbtn"
                          onChange={formik.handleChange}
                        >
                          Marathi
                        </ToggleButton>
                      </ToggleButtonGroup>
                      {formik.touched.reportLanguage &&
                      formik.errors.reportLanguage ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.reportLanguage}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                </div>
                <div className="col-md-6">
                  <Form.Group as={Row} className="fmc-style col-12 pr-0">
                    <div className="col-3 pl-0">
                      <Form.Label htmlFor="name">Add a Question</Form.Label>
                    </div>
                    <div className="col-9 pr-0 pl-0">
                      <Form.Control
                        type="text"
                        as="textarea"
                        name="question"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.question}
                      />
                      {formik.touched.question && formik.errors.question ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.question}
                        </div>
                      ) : null}
                    </div>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="col-12 pr-0"
                    style={{ alignItems: "center" }}
                  >
                    <div className="dtlptnr">
                      <Form.Control
                        type="checkbox"
                        name="partnerDetail"
                        className="checkboxFour "
                        onChange={checkboxHandler}
                      />

                      <Form.Label
                        htmlFor="checkbox"
                        className="font-weight-bold"
                        style={{
                          paddingLeft: "18px",
                        }}
                      >
                        Enter Partner Detail
                      </Form.Label>
                    </div>
                  </Form.Group>
                  {!checkbox && (
                    <div>
                      <Button
                        variant="primary"
                        size="sm"
                        type="Submit"
                        style={{ width: "100%" }}
                      >
                        Continue
                      </Button>
                    </div>
                  )}
                  {checkbox && (
                    <div>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="partnerFirstName">
                            First Name
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <Form.Control
                            type="text"
                            name="partnerFirstName"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.partnerFirstName}
                          />
                          {formik.touched.partnerFirstName &&
                          formik.errors.partnerFirstName ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.partnerFirstName}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="partnerLastName">
                            Last Name
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <Form.Control
                            type="text"
                            name="partnerLastName"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.partnerLastName}
                          />
                          {formik.touched.partnerLastName &&
                          formik.errors.partnerLastName ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.partnerLastName}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="dateofbirth">
                            Date OF Birth
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <DatePicker
                              open={partnerDateOpen}
                              onOpen={() => setPartnerDateOpen(true)}
                              onClose={() => setPartnerDateOpen(false)}
                              name="PartnerBirthDate"
                              value={formik.values.PartnerBirthDate}
                              onChange={partnerDateChange}
                              TextFieldComponent={() => null}
                            />
                          </MuiPickersUtilsProvider>
                          <input
                            type="button"
                            name="PartnerBirthDay"
                            value={
                              formik.values.PartnerBirthDay
                                ? formik.values.PartnerBirthDay
                                : "Day"
                            }
                            className={
                              formik.values.PartnerBirthDay
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerDateOpen(true)}
                          />
                          <input
                            type="button"
                            name="PartnerBirthMonth"
                            value={
                              formik.values.PartnerBirthMonth
                                ? formik.values.PartnerBirthMonth
                                : "Month"
                            }
                            className={
                              formik.values.PartnerBirthMonth
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerDateOpen(true)}
                          />
                          <input
                            type="button"
                            name="PartnerBirthYear"
                            value={
                              formik.values.PartnerBirthYear
                                ? formik.values.PartnerBirthYear
                                : "Year"
                            }
                            className={
                              formik.values.PartnerBirthYear
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerDateOpen(true)}
                          />
                          {formik.touched.PartnerBirthDay &&
                          formik.errors.PartnerBirthDay ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.PartnerBirthDay}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="dateofbirth">
                            Time OF Birth
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <TimePicker
                              open={partnerTimeOpen}
                              onOpen={() => setPartnerTimeOpen(true)}
                              onClose={() => setPartnerTimeOpen(false)}
                              name="PartnerBirthTime"
                              value={formik.values.PartnerBirthTime}
                              onChange={partnerTimeChange}
                              TextFieldComponent={() => null}
                            />
                          </MuiPickersUtilsProvider>
                          <input
                            type="button"
                            name="PartnerBirthHour"
                            value={
                              formik.values.PartnerBirthHour
                                ? formik.values.PartnerBirthHour
                                : "HH"
                            }
                            className={
                              formik.values.PartnerBirthHour
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerTimeOpen(true)}
                          />
                          <input
                            type="button"
                            name="PartnerBirthMin"
                            value={
                              formik.values.PartnerBirthMin
                                ? formik.values.PartnerBirthMin
                                : "MM"
                            }
                            className={
                              formik.values.PartnerBirthMin
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerTimeOpen(true)}
                          />
                          <input
                            type="button"
                            name="PartnerBirthAM"
                            value={
                              formik.values.PartnerBirthAM
                                ? formik.values.PartnerBirthAM
                                : "AM"
                            }
                            className={
                              formik.values.PartnerBirthAM
                                ? "btn btn-primary m-2 tglbtn"
                                : "btn btn-white border m-2 tglbtn"
                            }
                            onClick={(e) => setPartnerTimeOpen(true)}
                          />
                          {formik.touched.PartnerBirthHour &&
                          formik.errors.PartnerBirthHour ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.PartnerBirthHour}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="PartnerBirthPlace">
                            BirthPlace
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <i
                            className="fa fa-search iconf"
                            aria-hidden="true"
                          ></i>
                          <Form.Control
                            type="text"
                            name="PartnerBirthPlace"
                            className="inplace"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.PartnerBirthPlace}
                          />
                          {formik.touched.PartnerBirthPlace &&
                          formik.errors.PartnerBirthPlace ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.PartnerBirthPlace}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                      <Form.Group as={Row} className="fmc-style col-12 pr-0">
                        <div className="col-3 pl-0">
                          <Form.Label htmlFor="partnergender">
                            Gender *
                          </Form.Label>
                        </div>
                        <div className="col-9 pr-0 pl-0">
                          <ToggleButtonGroup
                            type="radio"
                            name="partnergender"
                            defaultValue={checkbox ? "male" : ""}
                            id="gender2"
                            onChange={formik.handleChange}
                          >
                            <ToggleButton
                              size="sm"
                              variant="outline-primary m-2"
                              value="male"
                              className="tglbtn"
                              onChange={formik.handleChange}
                            >
                              Male
                            </ToggleButton>
                            <ToggleButton
                              size="sm"
                              variant="outline-primary m-2"
                              value="female"
                              className="tglbtn"
                              onChange={formik.handleChange}
                            >
                              Female
                            </ToggleButton>
                            <ToggleButton
                              size="sm"
                              variant="outline-primary m-2"
                              value="other"
                              className="tglbtn"
                              onChange={formik.handleChange}
                            >
                              Other
                            </ToggleButton>
                          </ToggleButtonGroup>
                          {formik.touched.partnergender &&
                          formik.errors.partnergender ? (
                            <div style={{ color: "red" }}>
                              {formik.errors.partnergender}
                            </div>
                          ) : null}
                        </div>
                      </Form.Group>
                    </div>
                  )}
                </div>
              </Row>
              {checkbox && (
                <div className="p-3">
                  <Button
                    variant="primary"
                    size="sm"
                    type="Submit"
                    style={{ marginLeft: "257px", width: "50%" }}
                  >
                    Continue
                  </Button>
                </div>
              )}
            </Form>
          </Container>
        </div>
      </section>
      <Footer />
    </div>
  );
};

export default ReportUserDetail;
