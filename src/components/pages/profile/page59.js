import React, { Component } from 'react';
import { Button, Row, Col, Container, Form, Modal } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class UserChat extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.handleShow1 = this.handleShow1.bind(this);
    this.handleClose1 = this.handleClose1.bind(this);

    this.state = {
      show: false,
      show1: false,
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleClose1() {
    this.setState({ show1: false });
  }

  handleShow1() {
    this.setState({ show1: true });
  }
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <div className="d-flex justify-content-between chat-head align-items-center fdc-480">
                <div className="d-flex">
                  <img src="assets/img/priest.png" className="ch-u-img" />
                  <div className="ml-3">
                    <h5 className="mb-0 font-weight-normal">Aacharya Shantanu ji</h5>
                    <span>Typing....</span>
                  </div>
                </div>
                <Button onClick={this.handleShow} variant="primary" size="sm" className="px-5 my-xs-2">End Chat</Button>

              </div>

            </Container>

          </div>


          <Container>
            <div className="px-sm-5 my-4">
              <Row>
                <Col md={{ span: 8, offset: 3 }} lg={7}>

                  {/* <div className="bg-color2 p-4"></div> */}

                  <div className="chat-column">
                    <div className="chat-message">

                      <div className="session-time"><span className="mr-3">Session Time</span> 01:03:15</div>

                      <div className="chat-message_content interlocutor-message">
                        <div className="chat-message_content_text">
                          hi
      </div>
                        <div className="chat-message_content_date">

                          10.12.2019
    </div>
                      </div>
                      <div className="chat-message_content interlocutor-message">
                        <div className="chat-message_content_text">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Id unde laboriosam facilis esse a rem pariatur sequi doloribus corporis doloremqu
      </div>
                        <div className="chat-message_content_date">

                          10.12.2019
    </div>
                      </div>

                      <div className="req-popup p-2 rounded my-3 shadow-bolder text-center">
                        <p className="fs-12">Accept Chat</p>

                        <Button variant="primary" size="sm" className="btn-xs mr-3 text-uppercase">Deny</Button>
                        <Button variant="primary" size="sm" className="btn-xs text-uppercase">Accept</Button>

                      </div>

                      <div className="mb-2 p-2 rounded text-center chat-paused text-white">
                        <small>Chat is Paused Costumer is recharging his wallet</small>
                      </div>

                      <div className="mb-2 p-2 rounded text-center chat-resumed bg-color1 text-white">
                        <small>Chat is Paused Costumer is recharging his wallet</small>
                      </div>


                      <div className="req-accept">Astrologer has Accepted your Chat Request</div>

                      <div className="chat-message_content own-message">
                        <div className="chat-message_content_text">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Id unde laboriosam facilis esse a rem pariatur sequi doloribus corporis doloremque?
      </div>
                        <div className="chat-message_content_date">

                          10.12.2019
    </div>
                      </div>

                      <div className="chat-message_content own-message">
                        <div className="chat-message_content_text">
                          Lorem ipsum dolor
      </div>
                        <div className="chat-message_content_date">

                          10.12.2019
    </div>
                      </div>
                    </div>
                  </div>
                </Col>



              </Row>
            </div>
          </Container>

          <div className="chat-footer bg-gray p-3">
            <Container>
              <div className="px-sm-5">
                <Row>
                  <Col md={{ span: 8, offset: 3 }} lg={7}>
                    <div className="d-flex align-items-center chat-form">
                      <textarea placeholder="Type here..." rows="1" className="message-input flex-1"></textarea>
                      <button className="btn send-btn"></button>
                    </div>
                  </Col>
                </Row>
              </div>
            </Container>
          </div>


          <Modal size="sm" centered show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <p className="mb-0">Alert</p>
            </Modal.Header>
            <Modal.Body>

              <div className="text-center py-4">


                <h6 className="mb-2 text-color1"><small>Low Balance Only</small>  1 min <small>Left</small></h6>
                <p className="fs-12 mb-5">Chat will be paused if you go for Recharge</p>

                <Button variant="primary" size="sm" className="w-75" onClick={this.handleClose}>Recharge</Button>
              </div>
            </Modal.Body>

          </Modal>


          <Modal size="sm" centered show={this.state.show1} onHide={this.handleClose1}>
            <Modal.Header closeButton>
              <p className="mb-0 text-uppercase">Review</p>
            </Modal.Header>
            <Modal.Body>

              <div className="">
                <span className="fs-12">Rating</span>
                <div className="mb-2 rounded p-2 border">rating</div>
                <span className="fs-12">Comment</span>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Control className="fs-12" as="textarea" rows="3" />
                </Form.Group>
                <Form.Check className="mb-3" inline label="Anonymous" id="radio-1" type="radio" />

                {/* <h6 className="mb-2 text-color1"><small>Low Balance Only</small>  1 min <small>Left</small></h6>
                <p className="fs-12 mb-5">Chat will be paused if you go for Recharge</p> */}

                <Button variant="primary" size="sm" block onClick={this.handleClose1}>Submit</Button>
              </div>
            </Modal.Body>

          </Modal>

        </section>
        <Footer />
      </div>
    );
  }
}

export default UserChat;