import React, { Component } from 'react';
import { Button, Row, Col, Container, Form, Tabs, Tab, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';



class UserDetails extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content bg-color1">
          <Container>
            <Row>
              <Col md={{ span: 10, offset: 1 }}>

                <div className="bg-white p-4 br-8 my-4 position-relative">
                  <h5 className="font-weight-normal">Your Profile</h5>
                  <img src="assets/img/cancel.svg" className="position-absolute fom-close" />
                  <Row>
                    <Col md={{ span: 8, offset: 2 }}>
                      <Tabs defaultActiveKey="personal" id="uncontrolled-tab-example" className="tab--style1">
                        <Tab eventKey="personal" title="Personal">

                          <div className="my-4">
                            <div className="nu-img-w">
                              <img src="assets/img/user.png" className="nu-img" />
                              <div className="set-up">
                                <div class="upload-btn-wrapper">
                                  <button class="btn"><img src="assets/img/icon_add.svg" /></button>
                                  <input type="file" name="myfile" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <Form>
                            <Form.Group controlId="formBasicEmail" className="fmc-style">
                              <Form.Label>Full Name *</Form.Label>
                              <Form.Control type="text" placeholder="Enter email" />
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail" className="fmc-style">
                              <Form.Label>Mobile Number *</Form.Label>
                              <Form.Control type="text" placeholder="Enter email" />
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail" className="fmc-style">
                              <Form.Label>Email *</Form.Label>
                              <Form.Control type="email" placeholder="Enter email" />
                            </Form.Group>
                            <Form.Label className="text-color10">Gender *</Form.Label>
                            <div className="gender-btn px-4 mb-5">
                              <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
                                <ToggleButton size="sm" variant="outline-primary" value={1}>Male</ToggleButton>
                                <ToggleButton size="sm" variant="outline-primary" value={2}>Female</ToggleButton>
                                <ToggleButton size="sm" variant="outline-primary" value={3}>Other</ToggleButton>
                              </ToggleButtonGroup>
                            </div>

                            <div className="p-3">
                              <Button variant="primary" block size="sm" type="submit">
                                Save & Continue
                        </Button>
                            </div>



                          </Form>
                        </Tab>
                        <Tab eventKey="birth" title="Birth Details">

                        </Tab>
                        <Tab eventKey="location" title="Location">

                        </Tab>
                      </Tabs>








                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </Container>

        </section>
        <Footer />
      </div>
    );
  }
}

export default UserDetails;