import React, { useState } from "react";
import { Button, Row, Col, Container, Form } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";

import clientServiceAPI from "../../../services/clientServiceAPI";

const CreateReport = (props) => {
  const reportDetail = props?.location?.state;
  const history = useHistory();
  const [preview, setPreview] = useState(false);

  const formik = useFormik({
    initialValues: {
      content: "",
    },
    validateYupSchema: Yup.object({
      content: Yup.string().required("enter Content here"),
    }),
    onSubmit: (values) => {},
  });

  const submitFormHandler = () => {
    const body = {
      content: formik?.values?.content,
      status: "pending",
    };
    clientServiceAPI
      .putSubmitReport(body)
      .then((resp) => {
        if (resp.status) {
          console.log(resp);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const handlereview = () => {
    setPreview(true);
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-4">
          <Container>
            <h5 className="mb-0 font-weight-normal">Create Report</h5>
          </Container>
        </div>
        {!preview && (
          <Container>
            <Row>
              <Col md={{ span: 8, offset: 1 }}>
                <Form onSubmit={formik.handleSubmit}>
                  <div className="p-2 shadow-bold fs-12 rounded mt-2 mb-5">
                    <Form.Group>
                      <Form.Label htmlFor="content">Report Content</Form.Label>
                      <Form.Control
                        type="text"
                        as="textarea"
                        name="content"
                        value={formik.values.content}
                        onChange={formik.handleChange}
                        onBlur={formik.handleChange}
                      />
                      {formik.touched.content && formik.errors.content ? (
                        <div style={{ color: "red" }}>
                          {formik.errors.content}
                        </div>
                      ) : null}
                    </Form.Group>
                  </div>
                  <div className="px-5 mb-5">
                    <Button
                      type="Submit"
                      variant="primary"
                      block
                      size="sm"
                      className="fw-500"
                      onClick={handlereview}
                    >
                      Review
                    </Button>
                  </div>
                </Form>
              </Col>
            </Row>
          </Container>
        )}
        {preview && (
          <Container>
            <Row>
              <Col md={{ span: 8, offset: 1 }}>
                <Form onSubmit={formik.handleSubmit}>
                  <div className="p-2 shadow-bold fs-12 rounded mt-2 mb-5">
                    <div>
                      {" "}
                      <label>
                        <b>Astrologer Name: {reportDetail?.AstroName}</b>
                      </label>
                    </div>
                    <div>
                      <label>
                        <b>Report Type: {reportDetail?.reportType} </b>
                      </label>
                    </div>
                    <div>
                      <label>
                        <b>Astrologer Answer:</b>
                      </label>
                    </div>
                    <div>
                      <p>{formik.values.content}</p>
                    </div>
                  </div>
                  <div className="px-5 mb-5">
                    <Button
                      type="Submit"
                      variant="primary"
                      block
                      size="sm"
                      className="fw-500"
                      onClick={submitFormHandler}
                    >
                      Submit
                    </Button>
                  </div>
                </Form>
              </Col>
            </Row>
          </Container>
        )}
      </section>
      <Footer />
    </div>
  );
};

export default CreateReport;
