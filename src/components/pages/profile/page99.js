import React, { Component } from "react";
import { Button, Row, Col, Container, Form } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";

const ReportSubmit = (props) => {
  console.log(props);
  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content bg-color1">
        <div className="centered-div ">
          <div className="bg-white my-4 p-4 px-5 br-8 mw-350 position-relative text-center">
            <img
              src="assets/img/cancel.svg"
              className="position-absolute fom-close"
            />

            <img src="assets/img/conf.png" width="55" className="mt-5" />
            <h6 className="font-weight-normal mt-3 mb-5">Report submitted</h6>

            <Button variant="primary" block size="sm">
              Ok
            </Button>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
};

export default ReportSubmit;
