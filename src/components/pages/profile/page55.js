import React, { Component } from 'react';
import { Button, Row, Col, Container, Table, Modal } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class ReportConfirmationPay extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <div className="d-flex align-items-center justify-content-between px-sm-5 flex-wrap">
                <h5 className="mb-0 font-weight-normal ml-sm-5">Report Confirmation & Pay</h5>
                <span className="text-color2 fs-12 fw-500 mr-sm-5">Educational report-3 years</span>
              </div>

            </Container>

          </div>


          <Container>
            <div className="px-sm-5 my-4">
              <Row>
                <Col md={6}>
                  <div className="shadow-bolder px-3 py-4 br-8 cont-box">
                    <h6 className="fw-500 border-bottom pb-3 text-center">Your Details for Report</h6>
                    <Table striped size="sm" className="mt-5">
                      <tbody>
                        <tr>
                          <td className="px-3 py-2">Name</td>
                          <td className="px-3 text-color2 text-right py-2">Ashish Pawar</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Date of Birth</td>
                          <td className="px-3 text-color2 text-right py-2">27/09/1981</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Time of Birth</td>
                          <td className="px-3 text-color2 text-right py-2">12:20 am</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Place of Birth</td>
                          <td className="px-3 text-color2 text-right py-2">Ashish Pawar</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Gender</td>
                          <td className="px-3 text-color2 text-right py-2">Ashish Pawar</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Marital Status</td>
                          <td className="px-3 text-color2 text-right py-2">Ashish Pawar</td>
                        </tr>
                        <tr>
                          <td className="px-3 py-2">Report language</td>
                          <td className="px-3 text-color2 text-right py-2">Ashish Pawar</td>
                        </tr>

                      </tbody>
                    </Table>
                  </div>
                </Col>
                <Col md={6}>
                  <div className="shadow-bolder p-2 br-8 cont-box pay-head">
                    <h6 className="fw-500 border-bottom pb-2">Pay Now</h6>
                    <div className="d-flex align-items-center justify-content-between border-bottom pb-2 mb-2">
                      <div className="to-pay d-flex align-items-center">
                        <img src="assets/img/priest.png" />
                        <div className="flex-1 ml-2">
                          <div className="fw-500"><small>Aacharya Shantanu ji</small></div>
                          <small>Educational report-3 years</small>
                        </div>

                      </div>
                      <label className="mb-0 fw-500">₹ 9999 <i class="fas fa-wallet"></i></label>
                    </div>

                    <div className="border-bottom pb-1 mb-2">
                      <p className="d-flex justify-content-between mb-1">
                        <small className="fw-500">Amount</small>
                        <small className="fw-500">₹ 200</small>
                      </p>
                      <p className="d-flex justify-content-between mb-1">
                        <small className="fw-500">Wallet Balance utilised</small>
                        <small className="fw-500">₹ 200</small>
                      </p>
                      <p className="d-flex justify-content-between mb-1">
                        <small className="fw-500">Discount</small>
                        <small className="fw-500">₹ 200</small>
                      </p>
                    </div>

                    <div className="border-bottom pb-1 mb-1">
                      <p className="d-flex justify-content-between mb-1">
                        <small className="fw-500">Payable Amount</small>
                        <small className="fw-500">₹ 200</small>
                      </p>
                      <p className="d-flex justify-content-between mb-1">
                        <small className="fw-500">GST @ 18%</small>
                        <small className="fw-500">₹ 200</small>
                      </p>
                    </div>
                    <div className="border-bottom pb-1 mb-3">
                      <p className="d-flex justify-content-between mb-1">
                        <span className="fw-500 fs-12">Total Amount</span>
                        <small className="fw-500">₹ 200</small>
                      </p>
                    </div>
                    <Button className="fs-12 py-2 mb-2" variant="primary" block size="" onClick={this.handleShow}>Pay</Button>
                    <Button disabled className="fs-12 py-2 mb-2 d-flex justify-content-around" variant="primary" block size="">
                      <span>Paytm</span>
                      <span>₹ 200</span>
                    </Button>
                    <Button className="fs-12 py-2 mb-2" variant="primary" block size="" disabled>Pay with other payment options</Button>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>


          <Modal centered show={this.state.show} onHide={this.handleClose}>

            <Modal.Body>

              <div className="bg-white thank-container text-center py-5 px-4">
                <img src="assets/img/conf.png" width="70" />
                <h5 className="text-color2 mb-2 mt-2">Congratulations!</h5>

                <p className="fs-12 mb-3 text-color2">Payment Successfully</p>
                <p className="fs-12 mb-3 px-4">Your details has beed submitted You will get the response within 24 to 48 hours. In case of any queries please contact us</p>

                <Button variant="primary" size="sm" className="w-75" onClick={this.handleClose}>Ok</Button>
              </div>
            </Modal.Body>

          </Modal>

        </section>
        <Footer />
      </div>
    );
  }
}

export default ReportConfirmationPay;