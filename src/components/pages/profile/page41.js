import React, { Component } from 'react';
import { Container, Row, Col, Button, Form, Badge } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class OnlinePoojas extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <div className="px-md-5">
                <h5 className="font-weight-normal mb-0">Yearly Horoscopes</h5>
              </div>
            </Container>
          </div>
          <Container>
            <div className="my-5">
              <Row className="justify-content-center">

                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Aries.svg" />
                    <p className="mb-0 fw-500">KaalSarp dosh</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2}>
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Taurus.svg" />
                    <p className="mb-0 fw-500">Pitra Pooja</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Gemini.svg" />
                    <p className="mb-0 fw-500">Dhan laabh</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Cancer.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>

              </Row>

              <Row className="justify-content-center">

                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Leo.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Virgo.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Libra.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Scorpio.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>

              </Row>

              <Row className="justify-content-center">

                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Sagittarius.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Capricon.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2} >
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Aquarius.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>
                <Col xs={6} md={3} lg={2}>
                  <div className="bg-white rounded p-2 shadow-bold text-center horis-div">
                    <img src="assets/img/horiscope/icon_Pisces.svg" />
                    <p className="mb-0 fw-500">Loram Epsom</p>
                  </div>
                </Col>

              </Row>
            </div>
          </Container>

        </section>
        <Footer />
      </div>
    );
  }
}

export default OnlinePoojas;