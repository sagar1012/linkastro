import React, { useEffect, useState } from "react";
import Header from "../../templates/Header";
import { Row, Col, Container, Button } from "react-bootstrap";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import {
  TimePicker,
  DatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { useFormik, ErrorMessage } from "formik";
import * as Yup from "yup";
import history from "../../../history";
import httpService from "../../../services/httpService";
import clientServiceAPI from "../../../services/clientServiceAPI";

const FreeMatchMaking = () => {
  const [isTimeOpenM, setTimeIsOpenM] = useState(false);

  const [isDateOpenM, setDateIsOpenM] = useState(false);

  const [isTimeOpenF, setTimeIsOpenF] = useState(false);

  const [isDateOpenF, setDateIsOpenF] = useState(false);

  const formik = useFormik({
    initialValues: {
      Malename: "",
      maleBirthDay: "",
      maleBirthMonth: "",
      maleBirthYear: "",
      maleBirthHour: "",
      maleBirthMin: "",
      maleBirthPeriods: "",
      MaleBirth: "",

      Femalename: "",
      femaleBirthDay: "",
      femaleBirthMonth: "",
      femaleBirthYear: "",
      femaleBirthHour: "",
      femaleBirthMin: "",
      femaleBirthPeriods: "",
      FemaleBirth: "",
    },
    validationSchema: Yup.object({
      Malename: Yup.string().required("Full Name Required"),
      maleBirthDay: Yup.string().required(""),
      maleBirthMonth: Yup.string().required(""),
      maleBirthYear: Yup.string().required("Date OF Birth Required"),
      maleBirthHour: Yup.string().required(""),
      maleBirthMin: Yup.string().required(""),
      maleBirthPeriods: Yup.string().required("Time OF Birth Required"),
      MaleBirth: Yup.string().required("Place of Birth Required"),

      Femalename: Yup.string().required("Full Name Required"),
      femaleBirthDay: Yup.string().required(""),
      femaleBirthMonth: Yup.string().required(""),
      femaleBirthYear: Yup.string().required("Date OF Birth Required"),
      femaleBirthHour: Yup.string().required(""),
      femaleBirthMin: Yup.string().required(""),
      femaleBirthPeriods: Yup.string().required("Time OF Birth Required"),
      FemaleBirth: Yup.string().required("Place of Birth Required"),
    }),
    onSubmit: (values) => {
      // alert(JSON.stringify(values, null, 2));
      history.push({
        pathname: "/MatchingReport",
        state: { values },
      });
    },
  });

  const handleDateChangeM = (e) => {
    formik.setFieldValue("maleBirthDay", moment(e).format("DD"));
    formik.setFieldValue("maleBirthMonth", moment(e).format("MM"));
    formik.setFieldValue("maleBirthYear", moment(e).format("YYYY"));
  };

  const handleTimeChangeM = (e) => {
    formik.setFieldValue("maleBirthHour", moment(e).format("hh"));
    formik.setFieldValue("maleBirthMin", moment(e).format("mm"));
    formik.setFieldValue("maleBirthPeriods", moment(e).format("a"));
  };

  const handleTimeChangeF = (e) => {
    formik.setFieldValue("femaleBirthHour", moment(e).format("hh"));
    formik.setFieldValue("femaleBirthMin", moment(e).format("mm"));
    formik.setFieldValue("femaleBirthPeriods", moment(e).format("a"));
  };

  const handleDateChangeF = (e) => {
    formik.setFieldValue("femaleBirthDay", moment(e).format("DD"));
    formik.setFieldValue("femaleBirthMonth", moment(e).format("MM"));
    formik.setFieldValue("femaleBirthYear", moment(e).format("YYYY"));
  };

  const getPersonalDetails = () => {
    clientServiceAPI
      .getPersonalDetail()
      .then((resp) => {
        if (resp.status) {
          let personalDetailClient = resp.data.personal.result[0];

          formik.setFieldValue("Malename", personalDetailClient?.first_name);
          formik.setFieldValue(
            "maleBirthDay",
            moment(personalDetailClient?.birth_date).format("DD")
          );
          formik.setFieldValue(
            "maleBirthMonth",
            moment(personalDetailClient?.birth_date).format("MM")
          );
          formik.setFieldValue(
            "maleBirthYear",
            moment(personalDetailClient?.birth_date).format("YYYY")
          );
          formik.setFieldValue(
            "maleBirthHour",
            personalDetailClient?.birth_time.split(":")[0]
          );
          formik.setFieldValue(
            "maleBirthMin",
            personalDetailClient?.birth_time.split(":")[1]
          );
          formik.setFieldValue(
            "maleBirthPeriods",
            personalDetailClient?.birth_time.split(":")[2]
          );
          formik.setFieldValue(
            "MaleBirth",
            personalDetailClient?.birth_location
          );
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  useEffect(() => {
    getPersonalDetails();
  }, []);

  return (
    <div>
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <div className="px-md-5">
              <Row className="align-items-center">
                <Col md={4}>
                  <h2 className="font-weight-normal mb-1">Free Match Making</h2>
                </Col>
                <Col md={6}>
                  <h6 className="font-weight-normal  mb-1">
                    Check Love compatibility and Matchmaking prediction
                  </h6>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <form onSubmit={formik.handleSubmit}>
          <Container>
            <Row style={{ marginTop: "54px" }}>
              <Col
                md={6}
                sm={12}
                lg={6}
                className="bg-white shadow-bold rounded"
                style={{ margin: "20px", maxWidth: "45%" }}
              >
                <div style={{ margin: "30px" }}>
                  <h3 style={{ color: "#42a4fa" }}> Male</h3>
                  <div className="form-group">
                    <label
                      htmlFor="Malename"
                      style={{ marginTop: "15px", marginRight: "59px" }}
                    >
                      Full Name
                    </label>
                    <input
                      type="text"
                      name="Malename"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values["Malename"]}
                      style={{
                        border: ".25rem",
                        borderBottom: "1px solid #ced4de",
                      }}
                    />
                    {formik?.touched["Malename"] &&
                    formik?.errors["Malename"] ? (
                      <div className="text-danger">
                        {formik?.errors["Malename"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="dateofbirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Date OF Birth
                    </label>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <DatePicker
                        open={isDateOpenM}
                        onOpen={() => setDateIsOpenM(true)}
                        onClose={() => setDateIsOpenM(false)}
                        name="maleBirthDate"
                        value={formik.values.maleBirthDate}
                        onChange={handleDateChangeM}
                        TextFieldComponent={() => null}
                      />
                    </MuiPickersUtilsProvider>
                    <input
                      type="button"
                      name="maleBirthDay"
                      value={
                        formik.values.maleBirthDay !== "Invalid date"
                          ? formik.values.maleBirthDay
                          : "Day"
                      }
                      className={
                        formik.values.maleBirthDay !== "Invalid date"
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenM(true)}
                    />
                    <input
                      type="button"
                      name="maleBirthMonth"
                      value={
                        formik.values.maleBirthMonth !== "Invalid date"
                          ? formik.values.maleBirthMonth
                          : "Month"
                      }
                      className={
                        formik.values.maleBirthMonth
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenM(true)}
                    />
                    <input
                      type="button"
                      name="maleBirthYear"
                      value={
                        formik.values.maleBirthYear !== "Invalid date"
                          ? formik.values.maleBirthYear
                          : "Year"
                      }
                      className={
                        formik.values.maleBirthYear !== "Invalid date"
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenM(true)}
                    />
                    {formik?.touched["maleBirthYear"] &&
                    formik?.errors["maleBirthYear"] ? (
                      <div className="text-danger">
                        {formik?.errors["maleBirthYear"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="dateofbirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Time OF Birth
                    </label>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <TimePicker
                        open={isTimeOpenM}
                        onOpen={() => setTimeIsOpenM(true)}
                        onClose={() => setTimeIsOpenM(false)}
                        name="maleBirthTime"
                        value={formik.values.maleBirthTime}
                        onChange={handleTimeChangeM}
                        TextFieldComponent={() => null}
                      />
                    </MuiPickersUtilsProvider>
                    <input
                      type="button"
                      name="maleBirthHour"
                      value={
                        formik.values.maleBirthHour !== "Invalid date"
                          ? formik.values.maleBirthHour
                          : "HH"
                      }
                      className={
                        formik.values.maleBirthHour !== "Invalid date"
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenM(true)}
                    />
                    <input
                      type="button"
                      name="maleBirthMin"
                      value={
                        formik.values.maleBirthMin !== "Invalid date"
                          ? formik.values.maleBirthMin
                          : "MM"
                      }
                      className={
                        formik.values.maleBirthMin !== "Invalid date"
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenM(true)}
                    />
                    <input
                      type="button"
                      name="maleBirthPeriods"
                      value={
                        formik.values.maleBirthPeriods !== "Invalid date"
                          ? formik.values.maleBirthPeriods
                          : "AM"
                      }
                      className={
                        formik.values.maleBirthPeriods !== "Invalid date"
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenM(true)}
                    />
                    {formik?.touched["maleBirthPeriods"] &&
                    formik?.errors["maleBirthPeriods"] ? (
                      <div className="text-danger">
                        {formik?.errors["maleBirthPeriods"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="MaleBirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Place of Birth
                    </label>
                    <i className="fa fa-search" aria-hidden="true"></i>
                    <input
                      type="text"
                      name="MaleBirth"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.MaleBirth}
                      style={{
                        border: ".25rem",
                        borderBottom: "1px solid #ced4de",
                      }}
                    />
                    {formik?.touched["MaleBirth"] &&
                    formik?.errors["MaleBirth"] ? (
                      <div className="text-danger">
                        {formik?.errors["MaleBirth"]}
                      </div>
                    ) : null}
                  </div>
                </div>
              </Col>
              <Col
                md={6}
                sm={12}
                lg={6}
                className="bg-white shadow-bold rounded"
                style={{ margin: "20px", maxWidth: "45%" }}
              >
                <div style={{ margin: "30px" }}>
                  <h3 style={{ color: "#42a4fa" }}>Female</h3>

                  <div className="form-group">
                    <label
                      htmlFor="FemaleName"
                      style={{ marginTop: "15px", marginRight: "59px" }}
                    >
                      Full Name
                    </label>
                    <input
                      type="text"
                      name="Femalename"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values["Femalename"]}
                      style={{
                        border: ".25rem",
                        borderBottom: "1px solid #ced4de",
                      }}
                    />
                    {formik?.touched["Femalename"] &&
                    formik?.errors["Femalename"] ? (
                      <div className="text-danger">
                        {formik?.errors["Femalename"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="dateofbirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Date OF Birth
                    </label>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <DatePicker
                        open={isDateOpenF}
                        onOpen={() => setDateIsOpenF(true)}
                        onClose={() => setDateIsOpenF(false)}
                        name="femaleBirthDate"
                        value={formik.values.femaleBirthDate}
                        onChange={handleDateChangeF}
                        TextFieldComponent={() => null}
                      />
                    </MuiPickersUtilsProvider>
                    <input
                      type="button"
                      name="femaleBirthDay"
                      value={
                        formik.values.femaleBirthDay
                          ? formik.values.femaleBirthDay
                          : "Day"
                      }
                      className={
                        formik.values.femaleBirthDay
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenF(true)}
                    />
                    <input
                      type="button"
                      name="femaleBirthMonth"
                      value={
                        formik.values.femaleBirthMonth
                          ? formik.values.femaleBirthMonth
                          : "Month"
                      }
                      className={
                        formik.values.femaleBirthMonth
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenF(true)}
                    />
                    <input
                      type="button"
                      name="femaleBirthYear"
                      value={
                        formik.values.femaleBirthYear
                          ? formik.values.femaleBirthYear
                          : "Year"
                      }
                      className={
                        formik.values.femaleBirthYear
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setDateIsOpenF(true)}
                    />
                    {formik?.touched["femaleBirthYear"] &&
                    formik?.errors["femaleBirthYear"] ? (
                      <div className="text-danger">
                        {formik?.errors["femaleBirthYear"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="dateofbirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Time OF Birth
                    </label>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <TimePicker
                        open={isTimeOpenF}
                        onOpen={() => setTimeIsOpenF(true)}
                        onClose={() => setTimeIsOpenF(false)}
                        name="femaleBirthTime"
                        value={formik.values.femaleBirthTime}
                        onChange={handleTimeChangeF}
                        TextFieldComponent={() => null}
                      />
                    </MuiPickersUtilsProvider>
                    <input
                      type="button"
                      name="femaleBirthHour"
                      value={
                        formik.values.femaleBirthHour
                          ? formik.values.femaleBirthHour
                          : "HH"
                      }
                      className={
                        formik.values.femaleBirthHour
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenF(true)}
                    />
                    <input
                      type="button"
                      name="femaleBirthMin"
                      value={
                        formik.values.femaleBirthMin
                          ? formik.values.femaleBirthMin
                          : "MM"
                      }
                      className={
                        formik.values.femaleBirthMin
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenF(true)}
                    />
                    <input
                      type="button"
                      name="femaleBirthPeriods"
                      value={
                        formik.values.femaleBirthPeriods
                          ? formik.values.femaleBirthPeriods
                          : "AM"
                      }
                      className={
                        formik.values.femaleBirthPeriods
                          ? "btn btn-primary m-2"
                          : "btn btn-white border m-2"
                      }
                      onClick={(e) => setTimeIsOpenF(true)}
                    />
                    {formik?.touched["femaleBirthPeriods"] &&
                    formik?.errors["femaleBirthPeriods"] ? (
                      <div className="text-danger">
                        {formik?.errors["femaleBirthPeriods"]}
                      </div>
                    ) : null}
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="FemaleBirth"
                      style={{ marginTop: "12px", marginRight: "30px" }}
                    >
                      Place of Birth
                    </label>
                    <i className="fa fa-search" aria-hidden="true"></i>
                    <input
                      type="text"
                      name="FemaleBirth"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.FemaleBirth}
                      style={{
                        border: ".25rem",
                        borderBottom: "1px solid #ced4de",
                      }}
                    />
                    {formik?.touched["FemaleBirth"] &&
                    formik?.errors["FemaleBirth"] ? (
                      <div className="text-danger">
                        {formik?.errors["FemaleBirth"]}
                      </div>
                    ) : null}
                  </div>
                </div>
              </Col>
            </Row>
            <div className="p-3">
              <Button
                variant="primary"
                size="sm"
                type="Submit"
                style={{ marginLeft: "257px", width: "50%" }}
              >
                Continue
              </Button>
            </div>
          </Container>
        </form>
      </section>
    </div>
  );
};
export default FreeMatchMaking;
