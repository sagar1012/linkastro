import React, { Component } from 'react';
import { Button, Row, Col, Container, Table, Modal } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class GstBreakup extends Component {

  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-2">
            <Container>
              <div className="d-flex align-items-center p-1 bg-gray py-3 br-30 px-4 flex-wrap">

                <p className="mb-0 font-weight-bold mr-5"><img src="assets/img/icon_wallet.svg" className="mr-3" width="30" />Available Balance</p> <span className="font-weight-bold">₹ 9999</span>

              </div>

            </Container>

          </div>


          <Container>
            <div className="shadow-bolder p-2 br-8 balance-box my-4">
              <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
                <span>Total Amount</span>
                <span>₹ 500.00</span>
              </p>
              <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
                <span>GST @18%</span>
                <span>₹ 500.00</span>
              </p>
              <hr />
              <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
                <span>Total Payable Amount</span>
                <span>₹ 500.00</span>
              </p>

              <div className="bg-gray p-2 rounded align-items-center d-flex my-5">
                <span className="b-in-label mr-2">5% OFF</span>
                <span className="fw-500 text-color2 fs-12 flex-1">₹ 5 cashback in wallet after recharge</span>
              </div>

              <div className="cou-app mx-3 my-4 p-2 text-center rounded">
                AXNHG 1986Hd ANGD
              </div>

              <div className="cb-apply text-center text-white my-4">
                12 will be credited in your wallet
              </div>

              <div className="mb-4">
                <div className="coupon-in d-flex">
                  <input type="text" placeholder="Enter a Valid Coupon code" />
                  <Button variant="primary" size="sm">Apply</Button>
                </div>
                <small className="fw-500 sm-hint">Amount will be credited in your wallet</small>
              </div>

              <Button variant="primary" block className="d-flex px-4 py-2 fs-12 justify-content-between">
                <span className="fw-500">Paytm</span>
                <span>₹ 590.00</span>
              </Button>

              <Button block variant="outline-primary" className="fs-12 px-4 py-2 my-2 fw-500">Pay with other payment options</Button>
            </div>
          </Container>




        </section>
        <Footer />
      </div>
    );
  }
}

export default GstBreakup;