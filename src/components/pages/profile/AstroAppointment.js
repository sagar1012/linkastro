import React, { Component, useState } from "react";
import { Container, Row, Col, Button, Form, Badge } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { reactLocalStorage } from "reactjs-localstorage";
import { connect } from "react-redux";
import TimePicker from "react-time-picker";
import {
  getReportListDetail,
  getFollowerDetail,
  getEarningDetail,
  getwalletDetail,
  getShaduleDetail,
  updateShadulerDetails,
  getReportDetail,
  getPersonalvDetail,
} from "../../../actions/AstroAppointmentAction";
/*                                                              '/actions/AstroAppointmentAction'
                                                             '/src/components/pages/profile/AstroAppointment.js' 
                                                             '../../Action/TestFiveAction.js' 
                                                             '/src/component/ApiTestFive.js' */

import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import history from "../../../history";
import Moment from "moment";
import { TextField } from "@material-ui/core";

class AstroAppointment extends Component {
  constructor() {
    super();
    this.state = {
      Followers: "",
      Balance: "",
      Earning: "",

      selectedDate: "00:00",
      ReportDataNew: [],
      fields: {},

      /*            call_hrs:'',
                  call_status:'',
                  chat_hrs:'',
                  chat_status:'',
                  ReportData:'',
      */
    };
    // this.
  }

  async componentDidMount() {
    var token = reactLocalStorage.getObject("token");
    const data = Object.keys(token).length;

    // ------------------Earning------------------
    if (data > 0) {
      const EarningData = await this.props.getEarningDetail();
      if (
        EarningData &&
        EarningData.data &&
        EarningData.data.data &&
        EarningData.data.data.length
      ) {
        const todayEarning = EarningData.data.data[0].totalAmountToday;
        const yestEarning = EarningData.data.data[0].totalAmountYest;
        // console.log('EarningData' ,EarningData)
        var v = document.getElementById("p");

        if (EarningData.data.result === 1) {
          if (todayEarning > yestEarning) {
            v.className += " fas fa-long-arrow-alt-up text-success";
          } else {
            v.className += " fas fa-long-arrow-alt-down text-danger";
          }
          this.setState({
            Earning: todayEarning,
          });
        }
      }
    }

    // -------------------balance-----------------
    if (data > 0) {
      const WalletDetail = await this.props.getwalletDetail();
      if (WalletDetail && WalletDetail.data && WalletDetail.data.data) {
        const Balance = WalletDetail.data.data.balance;
        /*console.log('WalletDetail' ,WalletDetail)*/

        if (WalletDetail && WalletDetail.data) {
          if (WalletDetail.data.result === 1) {
            this.setState({
              balance: Balance,
            });
          }
        }
      }
    }

    // -------------------followers------------

    if (data > 0) {
      const resultData = await this.props.getFollowerDetail();
      if (resultData && resultData.data && resultData.data.data) {
        const followers = resultData.data.data[0].followers;
        // console.log('followerData' ,resultData)
        if (resultData.data.result === 1) {
          this.setState({
            Followers: followers,
          });
        }
      }
    }

    // ----------- chate & shadule  detail---------------

    if (data > 0) {
      const ShaduleData = await this.props.getShaduleDetail();
      console.log(ShaduleData, "shaduleData");

      if (ShaduleData?.data?.result === 1) {
        var fields = this.state.fields;
        var rowData = ShaduleData.data.checkResult[0];

        Object.keys(rowData).forEach(function eachKey(key) {
          fields[key] = rowData[key];
          fields["chat_status"] = rowData.chat_status;
          fields["chat_hrs"] = rowData.chat_hrs;
          fields["call_status"] = rowData.call_status;
          fields["call_hrs"] = rowData.call_hrs;
        });

        this.setState(fields);
      }
    }

    // -------reportCount--------
    if (data > 0) {
      const ReportData = await this.props.getReportDetail();
      if (ReportData && ReportData.data && ReportData.data.checkResult) {
        const Report = ReportData?.data?.checkResult[0].reportCount;
        /*console.log('ReportData' ,ReportData)
        console.log('ReportData' ,Report)*/
        if (ReportData?.data?.result === 1) {
          this.setState({
            ReportData: Report,
          });
        }
      }
    }

    // -------reportList--------
    if (data > 0) {
      const ReportList = await this.props.getReportListDetail();
      const ReportData = ReportList?.data?.reportList;
      console.log("ReportList", ReportList);
      console.log("ReportList", ReportData);

      if (ReportData && ReportData.length > 0) {
        // var temp = "";
        this.setState({
          ReportDataNew: ReportData,
        });

        // ReportData.forEach((u) => {
        //   temp +=
        //     "<div class='shadow-lighter rounded px-3 py-2 d-flex mb-2 align-items-center'>";
        //   temp += "<div class='flex-1'>";
        //   temp += "<p  class='fs-12 mb-0'>" + u.report_types + "</p>";
        //   temp +=
        //     "<small>" + Moment(u.created_on).format("DD-MM-YYYY") + "</small>";
        //   temp += "</div>";
        //   temp +=
        //     "<Button class='btn btn-primary' type='Button'" +
        //     // "onClick=" +
        //     // `${this.HandleViewPage.bind(this)}` +
        //     ">" +
        //     "view Detials" +
        //     "</Button>";
        //   temp += "</div>";
        // });
        // document.getElementById("Data").innerHTML = temp;
      }
    }
  }

  handleDateChange = (event) => {
    // setSelectedDate(event.target.value);
    const selectedDate = this.state.selectedDate;
    this.setState({
      selectedDate: event.target.value,
    });
    console.log(event.target.value);
  };

  handleChange = (e) => {
    const fields = this.state.fields;

    if (e.target.name === "chat_status") {
      if (e.target.value != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["chat_status"];
      }
    } else if (e.target.name === "chat_hrs") {
      if (e.target.value != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["chat_hrs"];
      }
    } else if (e.target.name === "call_status") {
      if (e.target.value != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["call_status"];
      }
    } /*else if (e.target.name === 'call_hrs') {
            if (e.target.value!= null) {
                fields[e.target.name] = e.target.value;
            } else {
                fields[e.target.name] = this.state.fields['call_hrs'];
            }
        }*/ else {
      fields[e.target.name] = e.target.value;
    }
    this.setState(fields);
  };

  onclick = async (e) => {
    e.preventDefault();

    const userData = {
      chatStatus: this.state.fields["chat_status"],
      chatHrs: this.state.fields["chat_hrs"],
      callStatus: this.state.fields["call_status"],
      callHrs: this.state.fields["call_hrs"],
    };

    console.log("userData", userData);

    await this.props.updateShadulerDetails(userData, this.props.history);
  };

  HandleViewPage(e, reportType) {
    e.preventDefault();
    this.props.history.push({
      pathname: "/userDetailsForReport",
      state: {
        reportType: reportType,
      },
    });
  }

  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold">
            <Container>
              <div className="py-2 px-md-5">
                <div className="px-md-5">
                  <Row>
                    <Col md={9}>
                      <ul className="some-static pl-md-5">
                        <li>
                          <h4 className="mb-0 font-weight-normal">
                            <i id="p"></i>
                            {this.state.Earning}
                          </h4>
                          <small>r Today</small>
                        </li>
                        <li>
                          <h4 className="mb-0 font-weight-normal">
                            {this.state.balance}
                          </h4>
                          <small>r Total</small>
                        </li>
                        <li>
                          <h4 className="mb-0 font-weight-normal">
                            {this.state.Followers}{" "}
                          </h4>
                          <small>Followers</small>
                        </li>
                      </ul>
                    </Col>
                    <Col md={3}>
                      <Button
                        block
                        variant="primary"
                        type="submit"
                        className="fw-500"
                        size="sm"
                        onClick={() => history.push("/page11")}
                      >
                        All Orders
                      </Button>
                    </Col>
                  </Row>
                </div>
              </div>
            </Container>
          </div>
          <Container>
            <div className="py-3 px-5 shadow-bold">
              <div className="px-md-5">
                <label className="">You are currently</label>
                <Row>
                  <Col md={8}>
                    <Row>
                      <Col md={6}>
                        <Form.Group className="style--input">
                          <Form.Control
                            as="select"
                            name="chat_status"
                            value={this.state.fields["chat_status"]}
                            onChange={this.handleChange}
                          >
                            //{" "}
                            <option>
                              1 {this.state.fields["chat_status"]}
                            </option>
                            <option>Online</option>
                            <option>Avalable in</option>
                            <option>Ofline in</option>
                          </Form.Control>
                        </Form.Group>
                      </Col>
                      <Col md={6}>
                        <Form.Group
                          className="style--input"
                          controlId="exampleForm.ControlSelect1"
                        >
                          <Form.Control
                            as="select"
                            name="chat_hrs"
                            value={this.state.fields["chat_hrs"]}
                            onChange={this.handleChange}
                          >
                            /*/*
                            <option>11:1{this.state.fields["chat_hrs"]}</option>
                            */*/
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                          </Form.Control>
                        </Form.Group>
                      </Col>
                    </Row>
                  </Col>
                  <Col md={{ span: 3, offset: 1 }}>
                    <Button
                      block
                      variant="danger"
                      className="fw-500 text-uppercase position-relative"
                      size="sm"
                    >
                      <img
                        src="assets/img/3.png"
                        className="btn-ico"
                        alt="link-astro"
                      />{" "}
                      Call
                    </Button>
                  </Col>
                </Row>

                <Row>
                  <Col md={8}>
                    <Row>
                      <Col md={6}>
                        <Form.Group
                          className="style--input"
                          controlId="exampleForm.ControlSelect1"
                        >
                          <Form.Control
                            as="select"
                            name="call_status"
                            value={this.state.fields["call_status"]}
                            onChange={this.handleChange}
                          >
                            /*/*
                            <option>
                              1 {this.state.fields["call_status"]}
                            </option>
                            */*/
                            <option>Online</option>
                            <option>Avalable in</option>
                            <option>Ofline</option>
                          </Form.Control>
                        </Form.Group>
                      </Col>

                      <Col md={6}>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                          <Form.Control
                            className="border border-primary"
                            name="call_hrs"
                            value={this.state.fields["call_hrs"]}
                            onChange={this.handleDateChange}
                          ></Form.Control>
                        </Form.Group>
                      </Col>

                      {/*<Col md={6}>
                        <Form.Group className="style--input" controlId="exampleForm.ControlSelect1">
                          <Form.Control as="select" name="call_hrs" value={this.state.fields['call_hrs']}
                          onChange={this.handleDateChange}
                            >
                            <option >hello</option>
                            <option >hello1</option>
                             <option>hello3</option>
                          </Form.Control>
                        </Form.Group>
                      </Col>*/}
                    </Row>
                  </Col>
                  <Col md={{ span: 3, offset: 1 }}>
                    <Button
                      block
                      variant="primary"
                      className="fw-500 text-uppercase position-relative"
                      size="sm"
                    >
                      <img
                        src="assets/img/4.png"
                        className="btn-ico"
                        alt="link-astro"
                      />
                      Chat
                    </Button>
                  </Col>
                </Row>

                <div className="my-4">
                  <Button
                    block
                    variant="primary"
                    size="sm"
                    onClick={this.onclick}
                  >
                    Save
                  </Button>
                </div>
              </div>
            </div>

            <div className="shadow-light py-4 px-5 my-5">
              <div className="px-md-5">
                <Row>
                  <Col md={3}>
                    <label className="fw-500 mb-3">
                      <img
                        src="assets/img/chat_blue.svg"
                        className="mr-3"
                        width="16"
                        alt="link-astro"
                      />
                      Chat Requests
                    </label>
                    <Button block variant="primary" size="sm">
                      View Kundali
                    </Button>
                  </Col>
                  <Col md={{ span: 3, offset: 6 }}>
                    <Link to="page19" className="d-block text-right mb-3">
                      View All
                    </Link>
                    <Button block variant="primary" size="sm">
                      Chat now
                    </Button>
                  </Col>
                </Row>
              </div>
            </div>

            <div className="shadow-light py-4 px-5 my-4">
              <div className="px-md-5">
                <Row>
                  <Col md={3}>
                    <label className="fw-500 mb-3">
                      <img
                        src="assets/img/phone_blue.svg"
                        className="mr-3"
                        width="16"
                        alt="link-astro"
                      />
                      Call Requests
                    </label>
                  </Col>
                  <Col md={{ span: 3, offset: 6 }}>
                    <Link to="page19" className="d-block text-right mb-3">
                      View All
                    </Link>
                  </Col>
                </Row>
                <div className="badge-full">No Missed Call Found</div>
              </div>
            </div>

            <div className="shadow-light py-4 px-5 my-4">
              <div className="px-md-5">
                <Row>
                  <Col md={3}>
                    <label className="fw-500 mb-3 ">
                      <img
                        src="assets/img/file_blue.svg"
                        className="mr-3"
                        width="16"
                        alt="link-astro"
                      />
                      Report Requests
                    </label>
                  </Col>
                  <Col md={{ span: 3, offset: 6 }}>
                    <Link to="page19" className="d-block text-right mb-3">
                      {this.state.ReportData}View All
                    </Link>
                  </Col>
                </Row>
                {/* <div id="Data" class="rr-container"> </div>*/}
                <div class="rr-container">
                  {this.state.ReportDataNew.map((u, index) => (
                    <div
                      key={index}
                      class="shadow-lighter rounded px-3 py-2 d-flex mb-2 align-items-center"
                    >
                      <div class="flex-1">
                        <p class="fs-12 mb-0">{u.report_types}</p>
                        <small>
                          {Moment(u.created_on).format("DD-MM-YYYY")}
                        </small>
                      </div>
                      <Button
                        class="btn btn-primary"
                        type="Button"
                        onClick={(e) => this.HandleViewPage(e, u.report_types)}
                      >
                        view Detials
                      </Button>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="shadow-light py-4 px-5 my-4">
              <div className="px-md-5">
                <Row>
                  <Col md={9}>
                    <label className="fw-500 mb-0 mt-2">Kundali</label>
                  </Col>
                  <Col md={3}>
                    <Button variant="primary" block>
                      Create Kundali
                    </Button>
                  </Col>
                </Row>
              </div>
            </div>

            <div className="shadow-light py-4 px-5 my-4">
              <div className="px-md-5">
                <label className="fw-500 mb-3">Statistics</label>
                <div className="Statistics rounded py-2 px-3 mb-3">
                  Performance
                </div>

                <div className="Statistics rounded py-2 px-3 mb-3">
                  Astrologer Review
                </div>

                <div className="Statistics rounded py-2 px-3 mb-3">
                  Consultation and Earning
                </div>
              </div>
            </div>
          </Container>
        </section>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  getFollowerDetail: state.getFollowerDetail,
  getEarningDetail: state.getEarningDetail,
  getWalletDetail: state.getWalletDetail,
  getShaduleDetail: state.getShaduleDetail,
  updateShadulerDetails: state.updateShadulerDetails,
  getReportDetail: state.getReportDetail,
  getReportListDetail: state.getReportListDetail,
  getPersonalvDetail: state.getPersonalvDetail,
  errors: state.errors.error,
});
export default connect(mapStateToProps, {
  getReportListDetail,
  getFollowerDetail,
  getEarningDetail,
  getwalletDetail,
  updateShadulerDetails,
  getShaduleDetail,
  getReportDetail,
  getPersonalvDetail,
})(AstroAppointment);
