import React, { useEffect, useState } from "react";
import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import clientServiceAPI from "../../../services/clientServiceAPI";

function DailyHoroscpeToday() {
  const [horoscopeData, setHoroscopeData] = useState();
  console.log(horoscopeData);

  const getDailyHoroscope = () => {
    clientServiceAPI
      .getDailyPanchangPrediction()
      .then((resp) => {
        setHoroscopeData(resp.data.dailyhoroscope);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  useEffect(() => {
    getDailyHoroscope();
  }, []);

  const getYesterDayHoroscope = () => {
    clientServiceAPI
      .getDailyPredictionPrevious()
      .then((resp) => {
        setHoroscopeData(resp.data.data);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getTomorrowHoroscope = () => {
    clientServiceAPI
      .getDailyPredictionNext()
      .then((resp) => {
        setHoroscopeData(resp.data.data);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <Tab.Container id="left-tabs-example" defaultActiveKey="second">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <Row className="align-items-center">
                <Col md={{ span: 5, offset: 1 }}>
                  <h5 className="font-weight-normal mb-0">Daily Horoscope</h5>{" "}
                </Col>
                <Col md={5}>
                  <div className="tab--style">
                    <Nav variant="pills" className="flex-row border rounded">
                      <Nav.Item>
                        <Nav.Link
                          eventKey="first"
                          onClick={getYesterDayHoroscope}
                        >
                          Yesterday
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="second" onClick={getDailyHoroscope}>
                          Today
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link
                          eventKey="third"
                          onClick={getTomorrowHoroscope}
                        >
                          Tomorrow{" "}
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          {horoscopeData && horoscopeData.biorhythms && (
            <div className="d-flex align-items-center justify-content-center bg-color8 p-3">
              {horoscopeData.biorhythms && horoscopeData.biorhythms.average && (
                <div className="big-num mr-3">
                  {horoscopeData.biorhythms.average.percent}
                </div>
              )}
              <div className="dh-rating p-3 rounded">
                {horoscopeData.biorhythms &&
                  horoscopeData.biorhythms.emotional && (
                    <p className="text-white mb-1">
                      <span>
                        {horoscopeData.biorhythms.emotional.percent}% Emotional
                      </span>
                    </p>
                  )}
                {horoscopeData.biorhythms &&
                  horoscopeData.biorhythms.intellectual && (
                    <p className="text-white mb-1">
                      <span>
                        {horoscopeData.biorhythms.intellectual.percent}%
                        Intellectual
                      </span>
                    </p>
                  )}
                {horoscopeData.biorhythms && horoscopeData.biorhythms.physical && (
                  <p className="text-white mb-1">
                    <span>
                      {horoscopeData.biorhythms.physical.percent}% Physical
                    </span>
                  </p>
                )}
              </div>
            </div>
          )}

          <Container>
            <Tab.Content>
              <Tab.Pane eventKey="first">
                <Row className="align-items-center">
                  <Col md={{ span: 10, offset: 1 }}>
                    <div className="rounded overflow-hidden border my-4">
                      <label className="p-3 fw-500 bg-color2 text-white d-block">
                        Conclusion
                      </label>
                      <div className="p-3">
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Health</span>
                            <p className="mb-4">{horoscopeData.health}</p>
                          </>
                        )}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Emotion</span>
                            <p className="mb-4">{horoscopeData.emotions}</p>
                          </>
                        )}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Profession</span>
                            <p className="mb-4">{horoscopeData.profession}</p>
                          </>
                        )}

                        {/* {horoscopeData && (
                          <>
                            <span className="fw-500">Summary</span>
                            <p className="mb-4">{horoscopeData.summary}</p>
                          </>
                        )} */}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Travel</span>
                            <p className="mb-4">{horoscopeData.travel}</p>
                          </>
                        )}

                        {horoscopeData && (
                          <>
                            <span className="fw-500">Luck</span>
                            <p className="mb-4">{horoscopeData.luck}</p>
                          </>
                        )}
                      </div>
                    </div>
                  </Col>
                </Row>
              </Tab.Pane>

              <Tab.Pane eventKey="second">
                <Row className="align-items-center">
                  <Col md={{ span: 10, offset: 1 }}>
                    <div className="rounded overflow-hidden border my-4">
                      <label className="p-3 fw-500 bg-color2 text-white d-block">
                        Conclusion
                      </label>
                      <div className="p-3">
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Health</span>
                            <p className="mb-4">{horoscopeData.health}</p>
                          </>
                        )}
                        {/* <span className="fw-500">Emotion</span>
                        <p className="mb-4">
                          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                          </p>

                        <span className="fw-500">Profession</span>
                        <p className="mb-4">
                          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                          </p> */}

                        {horoscopeData && (
                          <>
                            <span className="fw-500">Summary</span>
                            <p className="mb-4">{horoscopeData.summary}</p>
                          </>
                        )}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Travel</span>
                            <p className="mb-4">{horoscopeData.travel}</p>
                          </>
                        )}

                        {/* <span className="fw-500">Luck</span>
                        <p className="mb-4">
                          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                          </p> */}
                      </div>
                    </div>
                  </Col>
                </Row>
              </Tab.Pane>

              <Tab.Pane eventKey="third">
                <Row className="align-items-center">
                  <Col md={{ span: 10, offset: 1 }}>
                    <div className="rounded overflow-hidden border my-4">
                      <label className="p-3 fw-500 bg-color2 text-white d-block">
                        Conclusion
                      </label>
                      <div className="p-3">
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Health</span>
                            <p className="mb-4">{horoscopeData.health}</p>
                          </>
                        )}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Emotion</span>
                            <p className="mb-4">{horoscopeData.emotions}</p>
                          </>
                        )}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Profession</span>
                            <p className="mb-4">{horoscopeData.profession}</p>
                          </>
                        )}

                        {/* {horoscopeData && (
                          <>
                            <span className="fw-500">Summary</span>
                            <p className="mb-4">{horoscopeData.summary}</p>
                          </>
                        )} */}
                        {horoscopeData && (
                          <>
                            <span className="fw-500">Travel</span>
                            <p className="mb-4">{horoscopeData.travel}</p>
                          </>
                        )}

                        {horoscopeData && (
                          <>
                            <span className="fw-500">Luck</span>
                            <p className="mb-4">{horoscopeData.luck}</p>
                          </>
                        )}
                      </div>
                    </div>
                  </Col>
                </Row>
              </Tab.Pane>
            </Tab.Content>
          </Container>
        </Tab.Container>
      </section>
      <Footer />
    </div>
  );
}

export default DailyHoroscpeToday;
