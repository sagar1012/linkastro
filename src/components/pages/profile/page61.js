import React, { Component, useState, useEffect } from "react";
import { Button, Row, Col, Container, Table, Modal } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import httpService from "../../../services/httpService";
import clientServiceAPI from "../../../services/clientServiceAPI";

function DailyPunchangToday() {
  const [dailyPunchangToday, setDailyPunchangToday] = useState();
  const [dayChoghadiya, setDayChoghadiya] = useState();

  const today = new Date();
  const date = today.getDate();
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const dayName = days[today.getDay()];
  const monthName = monthNames[today.getMonth()];
  const currentYear = today.getFullYear();

  useEffect(() => {
    getDailyPunchangToday();
    getDayChodhadiya();
  }, []);

  const getDailyPunchangToday = () => {
    const body = {
      day: today.getDate(),
      month: today.getMonth() + 1,
      year: currentYear,
      hour: today.getHours(),
      min: today.getMinutes(),
      lat: 19.07283,
      lon: 72.88261,
      tzone: 5.5,
    };
    clientServiceAPI
      .getDailyPanchangeDetail(body)
      .then((resp) => {
        setDailyPunchangToday(resp.data.data);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getDayChodhadiya = () => {
    const body = {
      day: today.getDate(),
      month: today.getMonth() + 1,
      year: currentYear,
      hour: today.getHours(),
      min: today.getMinutes(),
      lat: 19.07283,
      lon: 72.88261,
      tzone: 5.5,
    };
    clientServiceAPI
      .getDayChodhadiyaDetail(body)
      .then((resp) => {
        setDayChoghadiya(resp.data.daynight.chaughadiya);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <Row className="align-items-center">
              <Col md={{ span: 7, offset: 1 }}>
                <h5 className="mb-0 font-weight-normal">Daily Panchang</h5>
              </Col>
              <Col md={4}>
                <Button variant="primary" className="px-4 my-xs-2">
                  Today
                </Button>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="bg-color1 p-sm-4 p-2 brs-bottom">
          <Container>
            <Row className="align-items-center">
              <Col md={{ span: 5, offset: 2 }}>
                <div className="orange-inner p-3 px-lg-5 d-flex align-items-center">
                  <img src="assets/img/panchang/planet.png" />
                  <div className="text-white">
                    <p className="mb-1">Shukla Paksha</p>
                    <p className="mb-1">Shukla Saptami</p>
                    <p className="mb-1">Bhadra Tithi</p>
                    <p className="mb-1">Vasant</p>
                  </div>
                </div>
              </Col>
              <Col md={2}>
                <div className="text-sm-right dp-today">
                  <h4>{date}</h4>
                  <p>
                    {dayName} {monthName} {currentYear}
                  </p>
                  {/* <p>Bangalore</p> */}
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <Container>
          {dailyPunchangToday && (
            <Row className="align-items-center my-4">
              <Col md={{ span: 7, offset: 2 }}>
                <div className="planet-time p-3 d-flex justify-content-center flex-wrap">
                  <div className="text-center p-t-col">
                    <img src="assets/img/panchang/icon_sunrise.svg" />
                    <p className="">Sunrise</p>
                    <span className="fw-500">{dailyPunchangToday.sunrise}</span>
                  </div>
                  <div className="text-center p-t-col">
                    <img src="assets/img/panchang/icon_sunset.svg" />
                    <p className="">Sunset</p>
                    <span className="fw-500">{dailyPunchangToday.sunset}</span>
                  </div>
                  <div className="text-center p-t-col">
                    <img src="assets/img/panchang/moonrise.svg" />
                    <p className="">Moonrise</p>
                    <span className="fw-500">
                      {dailyPunchangToday.moonrise}
                    </span>
                  </div>
                  <div className="text-center p-t-col">
                    <img src="assets/img/panchang/moonset.svg" />
                    <p className="">Moonset</p>
                    <span className="fw-500">{dailyPunchangToday.moonset}</span>
                  </div>
                </div>
              </Col>
            </Row>
          )}

          {dailyPunchangToday && (
            <div className="p-sm-3 p-2 rounded border tithi-box my-4">
              <div className="px-sm-4">
                <span className="fw-500 text-black mb-1">Tithi</span>
                <p className="fw-500 mb-1 text-color4">
                  Shukukla Saptami up to{" "}
                  {dailyPunchangToday.tithi.end_time.hour}:
                  {dailyPunchangToday.tithi.end_time.minute}:
                  {dailyPunchangToday.tithi.end_time.second}
                </p>
                <p>{dailyPunchangToday.tithi.details.summary}</p>
              </div>
              <hr />
              <div className="px-sm-4">
                <span className="fw-500 text-black mb-1">Nakshatra</span>
                <p className="fw-500 mb-1 text-color4">
                  Shukukla Saptami up to{" "}
                  {dailyPunchangToday.nakshatra.end_time.hour}:
                  {dailyPunchangToday.nakshatra.end_time.minute}:
                  {dailyPunchangToday.nakshatra.end_time.second}
                </p>
                <p>{dailyPunchangToday.nakshatra.details.summary}</p>
              </div>
              <hr />
              <div className="px-sm-4">
                <span className="fw-500 text-black mb-1">Yoga</span>
                <p className="fw-500 mb-1 text-color4">
                  Shukukla Saptami up to {dailyPunchangToday.yog.end_time.hour}:
                  {dailyPunchangToday.yog.end_time.minute}:
                  {dailyPunchangToday.yog.end_time.second}
                </p>
                <p>{dailyPunchangToday.yog.details.summary}</p>
              </div>
              <hr />
              <div className="px-sm-4">
                <span className="fw-500 text-black mb-1">Karan</span>
                <p className="fw-500 mb-1 text-color4">
                  Shukukla Saptami up to{" "}
                  {dailyPunchangToday.karan.end_time.hour}:
                  {dailyPunchangToday.karan.end_time.minute}:
                  {dailyPunchangToday.karan.end_time.second}
                </p>
                <p>{dailyPunchangToday.karan.details.summary}</p>
              </div>
            </div>
          )}

          {dailyPunchangToday && (
            <div className="rounded border my-4">
              <Table striped className="fw-500 mb-0">
                <tbody>
                  {/* <tr>
                  <td className="pl-sm-5">Purnimanta</td>
                  <td className="text-color2">Vaishakh</td>
                </tr> */}
                  <tr>
                    <td className="pl-sm-5">Paksha</td>
                    <td className="text-color2">{dailyPunchangToday.paksha}</td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Sun Sign</td>
                    <td className="text-color2">
                      {dailyPunchangToday.sun_sign}
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Moon Sign</td>
                    <td className="text-color2">
                      {dailyPunchangToday.moon_sign}
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Vikarm Samvat</td>
                    <td className="text-color2">
                      {dailyPunchangToday.vikram_samvat}
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Shak Samva</td>
                    <td className="text-color2">
                      {dailyPunchangToday.shaka_samvat_name}
                    </td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Season - Ritu</td>
                    <td className="text-color2">{dailyPunchangToday.ritu}</td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Ayana</td>
                    <td className="text-color2">{dailyPunchangToday.ayana}</td>
                  </tr>
                  <tr>
                    <td className="pl-sm-5">Disha</td>
                    <td className="text-color2">
                      {dailyPunchangToday.disha_shool}
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
          )}

          {dailyPunchangToday && (
            <div className="rounded border  my-5 overflow-hidden ">
              <label className="fw-500 p-3 px-sm-5 mb-0 bg-color2 text-white d-block">
                Auspicious Timing
              </label>
              <div className="px-sm-5">
                <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                  <span>Abhijit Muhurta</span>
                  <div className="sm-capsul text-color2 px-sm-5">
                    {dailyPunchangToday.abhijit_muhurta.start} to{" "}
                    {dailyPunchangToday.abhijit_muhurta.end}
                  </div>
                </div>
              </div>
            </div>
          )}

          <div className="rounded border  my-5 overflow-hidden ">
            <label className="fw-500 p-3 px-sm-5 mb-0 bg-color2 text-white d-block">
              Inauspicious Timing
            </label>
            <div className="p-sm-5">
              {dailyPunchangToday && (
                <Row className="justify-content-center">
                  <Col md={4} lg={3}>
                    <div className="rounded bg-gray text-center p-2 ">
                      <div className="fw-500 p-2 border-bottom">Guli kaal</div>
                      <p className="my-3">
                        {dailyPunchangToday.guliKaal.start}
                      </p>
                      <span>To</span>
                      <p className="my-3">{dailyPunchangToday.guliKaal.end}</p>
                    </div>
                  </Col>
                  <Col md={4} lg={3}>
                    <div className="rounded bg-gray text-center p-2 my-xs-2">
                      <div className="fw-500 p-2 border-bottom">Rahu kaal</div>
                      <p className="my-3">
                        {dailyPunchangToday.rahukaal.start}
                      </p>
                      <span>To</span>
                      <p className="my-3">{dailyPunchangToday.rahukaal.end}</p>
                    </div>
                  </Col>
                  <Col md={4} lg={3}>
                    <div className="rounded bg-gray text-center p-2 my-xs-2">
                      <div className="fw-500 p-2 border-bottom">
                        Yamghant kaal
                      </div>
                      <p className="my-3">
                        {dailyPunchangToday.yamghant_kaal.start}
                      </p>
                      <span>To</span>
                      <p className="my-3">
                        {dailyPunchangToday.yamghant_kaal.end}
                      </p>
                    </div>
                  </Col>
                </Row>
              )}
            </div>
          </div>

          {dayChoghadiya && (
            <Row>
              <Col md={{ span: 5, offset: 1 }}>
                <div className="choghadiya-box cb-l-bg rounded">
                  <p className="mb-0 fw-500 p-2 text-center">Day Choghadiya</p>
                  <div className="d-flex bg-white">
                    <span className="flex-1 text-center fw-500 p-2">Time</span>
                    <span className="flex-1 text-center fw-500 p-2">Time</span>
                  </div>

                  {dayChoghadiya.day.map((e, index) => {
                    return (
                      <div key={index} className="d-flex rounded">
                        <span className="flex-1 text-center p-2 fs-12">
                          {e.time}
                        </span>
                        <span className="flex-1 text-center p-2 fs-12">
                          {e.muhurta}
                        </span>
                      </div>
                    );
                  })}
                </div>
              </Col>
              <Col md={{ span: 5, offset: 1 }}>
                <div className="choghadiya-box cb-l-bg rounded">
                  <p className="mb-0 fw-500 p-2 text-center">
                    Night Choghadiya
                  </p>
                  <div className="d-flex bg-white">
                    <span className="flex-1 text-center fw-500 p-2">Time</span>
                    <span className="flex-1 text-center fw-500 p-2">Time</span>
                  </div>

                  {dayChoghadiya.night.map((e, index) => {
                    return (
                      <div key={index} className="d-flex rounded">
                        <span className="flex-1 text-center p-2 fs-12">
                          {e.time}
                        </span>
                        <span className="flex-1 text-center p-2 fs-12">
                          {e.muhurta}
                        </span>
                      </div>
                    );
                  })}
                </div>
              </Col>
            </Row>
          )}
        </Container>
      </section>
      <Footer />
    </div>
  );
}

export default DailyPunchangToday;
