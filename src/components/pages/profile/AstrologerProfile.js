import React, { Component } from 'react';
import { reactLocalStorage } from 'reactjs-localstorage';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';
import Personal from './Personal';
import Location from './Location';
import Bank from './Bank';
import Professional from './Professional';

export default class AstrologerProfile extends Component {
    componentDidMount() {
        document.title = 'Astrologer Profile';
        this.handleAuth()
    }

    handleAuth() {
        //var auth = reactLocalStorage.getObject('Auth');
        var token = reactLocalStorage.getObject('token');
        var data = Object.keys(token).length
        if (data === 0) {
            this.props.history.push('/AstroAppointment')
        }    
        
    }

    render() {
        return (
            <div className="main-wrapper">
                <Header />
                <section className="middle-section">
                    <div className="astrologer-page">
                        <div className="container">
                            <div className="astrologer-section section-bk">
                                <div className="astrologer-main">
                                    <div className="page-head">Astrologer Profile</div>

                                    <div className="astrologer-pro-page">

                                        <Tabs>
                                            <div className="astrologer-user-Link">
                                                <TabList>
                                                    <Tab>Personal</Tab>
                                                    <Tab>Location</Tab>
                                                    <Tab>Bank</Tab>
                                                    <Tab>Professional</Tab>
                                                </TabList>
                                            </div>
                                            <div className="astrologer-user-tabs">
                                                <TabPanel>
                                                    <Personal />
                                                </TabPanel>
                                                <TabPanel>
                                                    <Location />
                                                </TabPanel>
                                                <TabPanel>
                                                    <Bank />
                                                </TabPanel>
                                                <TabPanel>
                                                    <Professional />
                                                </TabPanel>
                                            </div>
                                        </Tabs>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}
