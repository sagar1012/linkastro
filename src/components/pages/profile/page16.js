import React, { Component } from 'react';
import { Button, Container, Table } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class AstroReview extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <h5 className="font-weight-normal mb-0">Astrologers Review</h5>
            </Container>
          </div>

          <Container>
            <div className="px-md-5 my-4">
              <div className="bg-gray p-4">
                <Table responsive className="table--style">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>User Name</th>
                      <th>Review</th>
                      <th>Rating</th>
                      <th>Reply</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>

                      <td><div>27/09/2020</div></td>
                      <td><div>UserNameUser UserNameUser</div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">View</button></div></td>
                      <td><div><b>5</b></div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">Reply</button></div></td>
                    </tr>
                    <tr>

                      <td><div>27/09/2020</div></td>
                      <td><div>UserNameUser UserNameUser</div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">View</button></div></td>
                      <td><div><b>5</b></div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">Reply</button></div></td>
                    </tr>
                    <tr>

                      <td><div>27/09/2020</div></td>
                      <td><div>UserNameUser UserNameUser</div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">View</button></div></td>
                      <td><div><b>5</b></div></td>
                      <td><div><button class="btn btn-reply fw-500 px-4">Reply</button></div></td>
                    </tr>
                  </tbody>
                </Table>
              </div>

              <div className="p-5 text-center">
                <Button variant="primary" className="btn-capsule px-5" size="sm">View More</Button>
              </div>
            </div>
          </Container>


        </section>
        <Footer />
      </div>
    );
  }
}

export default AstroReview;