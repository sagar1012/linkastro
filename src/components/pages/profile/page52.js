import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container, Overlay, Popover } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { useHistory } from "react-router-dom";
import clientServiceAPI from "../../../services/clientServiceAPI";

const EducationReading = (props) => {
  const reportType = props?.location?.state?.type;
  const history = useHistory();
  const [AstrologersList, setAstrologersList] = useState();

  useEffect(() => {
    getAstrologersList();
  }, []);

  const getAstrologersList = (val) => {
    const body = {
      pageNo: 1,
      searchtext: val,
    };
    clientServiceAPI
      .getAstroListv1(body)
      .then((resp) => {
        console.log(resp);
        if (resp.status) {
          setAstrologersList(resp.data);
        } else {
          console.log("error");
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const onSearch = (value) => {
    getAstrologersList(value);
  };

  const getReportHandler = (member_id) => {
    history.push({
      pathname: "/reportUserDetail",
      state: {
        memberId: member_id,
        reportType: reportType,
      },
    });
  };

  const profileHandler = (member_id) => {
    history.push({
      pathname: "/astroProfile",
      state: {
        AstroId: member_id,
      },
    });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-gray p-3">
          <Container>
            <Row>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  100% Secure Payment
                </div>
              </Col>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  Verified Astrologer
                </div>
              </Col>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  Private & Confidential
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <Container>
          <div className="filter-wrap py-2 px-3 my-4">
            <Row className="align-items-center">
              <Col md={4}>
                <h5 className="set-er-ico">
                  Education Reading <br />
                  <small className="fs-12">For 3 Years</small>
                </h5>
              </Col>
              <Col md={8}>
                <div className="d-flex">
                  {/* <Button className="filter-dd" onClick={handleClick}>Filter <i class="fas fa-filter"></i>!</Button>

                    <Overlay
                      show={show}
                      target={target}
                      placement="bottom"
                      container={ref.current}
                      containerPadding={20}
                    >
                      <Popover id="popover-contained">
                        <Popover.Title as="h3">Popover bottom</Popover.Title>
                        <Popover.Content>
                          <strong>Holy guacamole!</strong> Check this info.
                        </Popover.Content>
                      </Popover>
                    </Overlay> */}

                  <div className="search-input ml-3 flex-1">
                    <input
                      type="text"
                      onChange={(e) => onSearch(e.target.value)}
                      placeholder="Search Astrologers, by Name, Skill, language."
                    />
                    <i className="fas fa-search"></i>
                  </div>
                </div>
              </Col>
            </Row>
          </div>

          {AstrologersList && (
            <div className="p-sm-3">
              <Row>
                {AstrologersList?.checkResult?.map((e, index) => {
                  return (
                    <Col key={index} sm={6} lg={4}>
                      <div className="rounded shadow-light p-2 er-card fs-12">
                        <div
                          className="d-flex"
                          onClick={(event) => profileHandler(e.member_id)}
                        >
                          <div className="al-view text-center">
                            <div className="al-img">
                              <img
                                src="assets/img/priest.png"
                                className="al-pic"
                              />
                              <img
                                src="assets/img/Icon _verified_user.svg"
                                className="al-verf"
                              />
                              <span>{e.minutes_free} min free</span>
                            </div>

                            <div className="mt-2">******</div>
                            <span className="rv">330 Review</span>
                          </div>

                          <div className="flex-1 px-2">
                            <table className="w-100">
                              <tr>
                                <td className="fw-500">{e.display_name}</td>
                              </tr>
                              <tr
                              // style={{
                              //   display: "flex",
                              //   flexDirection: "row",
                              //   flexWrap: "wrap",
                              //   alignContent: "stretch",
                              //   justifyContent: "space-evenly",
                              // }}
                              >
                                {AstrologersList.professionalskills[index]
                                  .categoriesList[0] &&
                                  AstrologersList.professionalskills[
                                    index
                                  ].categoriesList.map((item, index) => {
                                    return (
                                      <td key={index}>{item.category_label}</td>
                                    );
                                  })}
                              </tr>
                              <tr
                                style={{
                                  display: "flex",
                                }}
                              >
                                {AstrologersList.professionalskills[index]
                                  .languageList[0] &&
                                  AstrologersList.professionalskills[
                                    index
                                  ].languageList.map((item, index) => {
                                    return <td key={index}>{item.language}</td>;
                                  })}
                              </tr>
                            </table>
                          </div>
                        </div>

                        <div className="bg-gray p-2 mx-m8 mt-2">
                          <table className="w-100">
                            <tr>
                              <td className="pb-2">{e.experience} yrs exp.</td>
                            </tr>
                            <tr>
                              <td>
                                Fees{" "}
                                <span className="tild-del">
                                  ₹ {e.astrologer_charges}/min
                                </span>
                                &nbsp;
                                <span className="text-color2 fw-500">
                                  ₹ 25/min
                                </span>
                              </td>
                              <td className="text-right">
                                <button
                                  className="btn btn-blue btn-xs font-weight-bold text-uppercase"
                                  onClick={(event) =>
                                    getReportHandler(e.member_id)
                                  }
                                >
                                  <img
                                    src="assets/img/horiscope/report-s.svg"
                                    width="14"
                                    className="mr-2"
                                  />{" "}
                                  Get Report
                                </button>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div className="text-right mb-m5">
                          <a href="#">Available Now</a>
                        </div>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </div>
          )}

          <div className="p-2 my-5 text-center">
            <Button className="btn-capsule px-5" size="sm">
              View More
            </Button>
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default EducationReading;
