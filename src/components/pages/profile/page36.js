import React, { Component } from 'react';
import { Container, Row, Col, Button, Tab, Tabs, Nav, Table } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class YearlyHoroscopesSingle extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">




          <div className="bg-white shadow-bold p-2">
            <Container>
              <div className="px-md-5 d-flex justify-content-between align-items-center">
                <h5 className="mb-0 font-weight-normal">Yearly Horoscopes</h5>
                <div className="d-flex justify-content-end s-h-r-col align-items-center">
                  <div className="text-right mr-2">
                    <b className="d-block">Aries</b>
                      Year 2020
                    </div>
                  <div className="s-h-img">
                    <img src="assets/img/horiscope/icon_Aries.svg" />
                  </div>

                </div>
              </div>
            </Container>
          </div>
          <Container>
            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Characteristic</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Highly Compatible Zodiac</label>
              <div className="px-sm-5 py-sm-2 p-2">
                <Row>
                  <Col md={3} lg={2}>
                    <div className="bg-white rounded p-2 shadow-bold text-center horis-div mb-0">
                      <img src="assets/img/horiscope/icon_Aries.svg" />
                      <p className="mb-0">Aries</p>
                    </div>
                  </Col>

                  <Col md={3} lg={2}>
                    <div className="bg-white rounded p-2 shadow-bold text-center horis-div mb-0">
                      <img src="assets/img/horiscope/icon_Aries.svg" />
                      <p className="mb-0">Aries</p>
                    </div>
                  </Col>
                </Row>
              </div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">


              <Table className="table--style1 fw-500" striped responsive>

                <tbody>
                  <tr>
                    <td className="pl-md-5">Strength</td>
                    <td className="text-color2">Courageous, Confident, Enthusiastic, Passionate</td>
                  </tr>
                  <tr>
                    <td className="pl-md-5">Weakness</td>
                    <td className="text-color2">Impatient, Short-Tempered, Aggressive</td>
                  </tr>
                  <tr>
                    <td className="pl-md-5">Favorable Colors</td>
                    <td className="text-color2">Red</td>
                  </tr>
                  <tr>
                    <td className="pl-md-5">Favorable Number</td>
                    <td className="text-color2">1, 8, 9</td>
                  </tr>
                </tbody>

              </Table>


            </div>



            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Career</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Love and Relationships</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Finance</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Health</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

            <div className="rounded border  my-5 overflow-hidden">
              <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block px-md-5">Takeaway tips</label>
              <div className="px-sm-5 py-sm-4 p-2">
                Ariens are famed for their fiery, positive, outgoing natures. Considered among the most enthusiastic of the zocliac children, they have high energy levels and often fast-paced lifestyles. Their fiery determination to accomplish things sometimes encourages hot-headedness and rudeness. Ariens do all things in their own way, with energetic determination and regardless of obstacles.
</div>

            </div>

          </Container>







        </section>
        <Footer />
      </div>
    );
  }
}

export default YearlyHoroscopesSingle;  