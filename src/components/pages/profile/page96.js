import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container, Table, Modal } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import moment from "moment";
import { useHistory } from "react-router-dom";
import clientServiceAPI from "../../../services/clientServiceAPI";

const UserDetailsForReport = (props) => {
  const reportCType = props?.location?.state?.reportType;
  const [clientData, setClientData] = useState("");
  const [AstroProfile, setAstroProfile] = useState("");
  const history = useHistory();

  console.log(reportCType, AstroProfile);

  useEffect(() => {
    getClientListReport();
    getProfileDetail();
  }, []);

  const getClientListReport = () => {
    const body = {
      from_member_id: "10267",
    };
    clientServiceAPI
      .getClientList(body)
      .then((resp) => {
        if (resp.status) {
          setClientData(resp?.data?.checkResult[0]);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getProfileDetail = () => {
    clientServiceAPI
      .getPersonalDetailProfile()
      .then((resp) => {
        if (resp.status) {
          setAstroProfile(resp?.data?.data?.displayName);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const ReportHandler = () => {
    history.push({
      pathname: "/createReport",
      state: {
        reportType: reportCType,
        AstroName: AstroProfile,
      },
    });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-4">
          <Container>
            <h5 className="mb-0 font-weight-normal">User Details</h5>
          </Container>
        </div>

        <Container>
          <Row>
            <Col md={{ span: 8, offset: 1 }}>
              <h6 className="text-color2 mt-4 mb-2">
                Educational report-3 years
              </h6>

              <div className="bg-gray p-1 rounded overflow-hidden mb-4">
                <Table striped size="sm" className="fs-12 mb-0">
                  <tbody>
                    <tr>
                      <td className="px-2">Name</td>
                      <td className="text-color2 text-right px-2">
                        {clientData?.mfirst_name} {clientData?.mlast_name}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Date of Birth</td>
                      <td className="text-color2 text-right px-2">
                        {moment(clientData?.mbirth_date).format("DD-MM-YYYY")}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Time of Birth </td>
                      <td className="text-color2 text-right px-2">
                        {moment(clientData?.mbirth_time).format("hh:mm:a")}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Place of Birth</td>
                      <td className="text-color2 text-right px-2">
                        {clientData?.mlocation}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Gender</td>
                      <td className="text-color2 text-right px-2">
                        {clientData?.mgender}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Marital Status</td>
                      <td className="text-color2 text-right px-2">
                        {clientData?.marital_status}
                      </td>
                    </tr>
                    <tr>
                      <td className="px-2">Report language</td>
                      <td className="text-color2 text-right px-2">
                        {clientData?.language}{" "}
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
              {clientData?.ffirst_name && (
                <div>
                  <h6 className="text-color2 mt-4 mb-2">Partner Details</h6>

                  <div className="bg-gray p-1 rounded overflow-hidden mb-4">
                    <Table striped size="sm" className="fs-12 mb-0">
                      <tbody>
                        <tr>
                          <td className="px-2">Name</td>
                          <td className="text-color2 text-right px-2">
                            {clientData?.ffirst_name} {clientData?.flast_name}
                          </td>
                        </tr>
                        <tr>
                          <td className="px-2">Date of Birth</td>
                          <td className="text-color2 text-right px-2">
                            {moment(clientData?.fbirth_date).format(
                              "DD-MM-YYYY"
                            )}
                          </td>
                        </tr>
                        <tr>
                          <td className="px-2">Time of Birth </td>
                          <td className="text-color2 text-right px-2">
                            {moment(clientData?.fbirth_time).format("hh:mm:a")}
                          </td>
                        </tr>
                        <tr>
                          <td className="px-2">Place of Birth</td>
                          <td className="text-color2 text-right px-2">
                            {clientData?.flocation}
                          </td>
                        </tr>
                        <tr>
                          <td className="px-2">Gender</td>
                          <td className="text-color2 text-right px-2">
                            {clientData?.fgender}
                          </td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
              )}
              <span className="">Question</span>
              <div className="p-2 shadow-bold fs-12 rounded mt-2 mb-5">
                {clientData?.question}
              </div>
              <div className="px-5 mb-5">
                <Button
                  variant="primary"
                  block
                  size="sm"
                  className="fw-500"
                  onClick={ReportHandler}
                >
                  Create Report
                </Button>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default UserDetailsForReport;
