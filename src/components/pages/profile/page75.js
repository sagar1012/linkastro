import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container, Overlay, Popover } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import clientServiceAPI from "../../../services/clientServiceAPI";

const MyAstrologer = () => {
  const [AstrologersList, setAstrologersList] = useState();

  useEffect(() => {
    getMyAstrologersList();
  }, []);

  const getMyAstrologersList = (val) => {
    const body = {
      pageNo: 1,
      searchtext: val,
    };
    clientServiceAPI
      .getMyAstrologerDetail(body)
      .then((resp) => {
        console.log(resp);
        if (resp.status) {
          setAstrologersList(resp.data);
        } else {
          console.log("error");
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <div className="filter-wrap py-2 px-3">
              <Row className="align-items-center">
                <Col md={4}>
                  <h5 className="msa-ico">
                    Astrologers <br />{" "}
                    <small className="fs-12">
                      Connect with our top Astrologers
                    </small>
                  </h5>
                </Col>
                <Col md={8}>
                  <div className="d-flex">
                    {/* <Button className="filter-dd" onClick={handleClick}>Filter <i class="fas fa-filter"></i>!</Button>

                    <Overlay
                      show={show}
                      target={target}
                      placement="bottom"
                      container={ref.current}
                      containerPadding={20}
                    >
                      <Popover id="popover-contained">
                        <Popover.Title as="h3">Popover bottom</Popover.Title>
                        <Popover.Content>
                          <strong>Holy guacamole!</strong> Check this info.
    </Popover.Content>
                      </Popover>
                    </Overlay> */}

                    <div className="search-input ml-3 flex-1">
                      <input
                        type="text"
                        placeholder="Search Astrologers, by Name, Skill, language."
                      />
                      <i class="fas fa-search"></i>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>

        <Container>
          {AstrologersList && (
            <div className="my-4">
              <Row>
                {AstrologersList?.checkResult?.map((item, index) => {
                  return (
                    <Col md={6} lg={4} key={index}>
                      <div className="rounded shadow-light p-2 my-at-card fs-12">
                        <div className="d-flex ">
                          <div className="al-view text-center">
                            <div className="al-img">
                              <img
                                src="assets/img/priest.png"
                                className="al-pic"
                              />
                              <img
                                src="assets/img/Icon _verified_user.svg"
                                className="al-verf"
                              />
                            </div>
                          </div>
                          <div className="flex-1 px-2 ">
                            <i class="fas fa-angle-right al-noti"></i>
                            <table className="w-100">
                              <tr>
                                <td className="fw-500">{item.display_name}</td>
                              </tr>
                              <tr>
                                {AstrologersList.professionalskills[index]
                                  .categoriesList[0] &&
                                  AstrologersList.professionalskills[
                                    index
                                  ].categoriesList.map((item, index) => {
                                    return (
                                      <td key={index}>{item.category_label}</td>
                                    );
                                  })}
                              </tr>
                              <tr>
                                <td>{item.experience} yrs exp.</td>
                              </tr>
                            </table>
                          </div>
                        </div>

                        <div className="bg-gray p-md-3 p-2 mx-m8 mb-m8 mt-3">
                          <table className="w-100">
                            <tr>
                              <td className="text-center">
                                <button className="btn btn-blue btn-xs font-weight-bold text-uppercase">
                                  <img
                                    src="assets/img/3.png"
                                    className="mr-1"
                                    width="14"
                                  />
                                  Call
                                </button>
                              </td>
                              <td className="text-center">
                                <button className="btn btn-blue btn-xs font-weight-bold text-uppercase">
                                  <img
                                    src="assets/img/4.png"
                                    className="mr-1"
                                    width="14"
                                  />
                                  Chat
                                </button>
                              </td>
                              <td className="text-center">
                                <button className="btn btn-blue btn-xs font-weight-bold text-uppercase">
                                  <i class="fas fa-share-alt mr-1"></i>
                                  share
                                </button>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </div>
          )}
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default MyAstrologer;
