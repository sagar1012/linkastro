import React from "react";
import { Container, Row, Col, Button, Form, Badge } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { useHistory } from "react-router-dom";

const GetYourReport = () => {
  const history = useHistory();

  const goToNextPage = (type) => {
    history.push({
      pathname: "/educationReading",
      state: { type },
    });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <div className="px-md-5 d-flex align-items-center flex-wrap">
              <span className="gyr-ico">
                <img src="assets/img/horiscope/report-s.svg" />
              </span>
              <h5 className="font-weight-normal mb-0 mr-sm-5">
                Get Your Report
              </h5>

              <p className="mb-0">
                Get detailed Personalised Reports by our Qualified Astrologers.
              </p>
            </div>
          </Container>
        </div>
        <Container>
          <div className="my-5">
            <Row className="justify-content-center">
              <Col xs={6} md={3} lg={2}>
                <div
                  className="bg-white rounded p-3 shadow-bold text-center gr-div"
                  onClick={(e) => goToNextPage(1)}
                  style={{ cursor: "pointer" }}
                >
                  <img src="assets/img/horiscope/Icon_Education.svg" />
                  <p className="mb-0 fw-500">Education</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div
                  className="bg-white rounded p-3 shadow-bold text-center gr-div"
                  onClick={(e) => goToNextPage(2)}
                >
                  <img src="assets/img/horiscope/icon_ Health.svg" />
                  <p className="mb-0 fw-500">Health</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_ Career.svg" />
                  <p className="mb-0 fw-500">Career & Business</p>
                </div>
              </Col>
            </Row>

            <Row className="justify-content-center">
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_ Wealth.svg" />
                  <p className="mb-0 fw-500">Wealth</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_ Health.svg" />
                  <p className="mb-0 fw-500">Love & Relationship</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_travel.svg" />
                  <p className="mb-0 fw-500">Travel</p>
                </div>
              </Col>
            </Row>

            <Row className="justify-content-center">
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_ Marriage.svg" />
                  <p className="mb-0 fw-500">Matchmaking</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_Yearly_Reading.svg" />
                  <p className="mb-0 fw-500">Yearly</p>
                </div>
              </Col>
              <Col xs={6} md={3} lg={2}>
                <div className="bg-white rounded p-3 shadow-bold text-center gr-div">
                  <img src="assets/img/horiscope/icon_Pisces.svg" />
                  <p className="mb-0 fw-500">3 Questions</p>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default GetYourReport;
