import React, { Component } from 'react';
import { Button, Row, Col, Container, Table, Modal } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class PaymenGatway extends Component {

  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-2">
            <Container>
              <div className="d-flex align-items-center p-1 bg-gray py-3 br-30 px-4 flex-wrap">

                <p className="mb-0 font-weight-bold mr-5"><img src="assets/img/icon_wallet.svg" className="mr-3" width="30" />Available Balance</p> <span className="font-weight-bold">₹ 9999</span>

              </div>

            </Container>

          </div>


          <Container>
            <div className="balance-box my-4">
              <p className="text-color9 ml-4 mb-1 ">Preferred Payment</p>
              <div className="bg-gray p-3 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/paytm.png" width="40" /> &nbsp;Paytm</span>
                <span className="text-color2">550.00 &nbsp;&nbsp;<i class="fas fa-check-circle"></i></span>
              </div>

              <p className="text-color9 ml-4 my-2">Other wallet</p>

              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/PhonePe.png" width="18" /> &nbsp;Phone Pay</span>
                <span className="text-color2">Link Account</span>
              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/amazon pay.png" width="18" /> &nbsp;Amazon Pay</span>
                <span className="text-color2">Link Account</span>
              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/g-pay.png" width="18" /> &nbsp;Google Pay</span>
                <span className="text-color2">Link Account</span>
              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/MobiKwik.png" width="18" /> &nbsp;Mobikwik</span>
                <span className="text-color2">Link Account</span>
              </div>

              <p className="text-color9 ml-4 my-2">Credit/Debit Cards</p>

              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex align-items-center">
                <span className="text-color2">2236-XXXXXXXX-3264</span>

              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 d-flex align-items-center">
                <span><i class="fas fa-credit-card"></i> <span className="font-weight-bold text-color2 ml-2">Add new card</span></span>

              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 d-flex align-items-center">

                <span className="font-weight-bold text-muted">Save and Pay via Cards.</span>
              </div>
              <div className="bg-gray p-2 px-sm-4 fs-12 fw-500 d-flex justify-content-between align-items-center">
                <span><img src="assets/img/visa-card-logo-png-1.png" width="100" /> </span>

              </div>
              <p className="text-color9 ml-4 my-2">Net Banking</p>
              <div className="bg-gray p-2 px-sm-4 fs-12 d-flex align-items-center">

                <ul className="bank-list">
                  <li>
                    <span></span>
                    <p>SBI</p>
                  </li>
                  <li>
                    <span></span>
                    <p>HDFC</p>
                  </li>
                  <li>
                    <span></span>
                    <p>ICICI</p>
                  </li>
                  <li>
                    <span></span>
                    <p>AXIS</p>
                  </li>
                  <li>
                    <span></span>
                    <p>KOTAK</p>
                  </li>
                </ul>
              </div>
              <p className="text-color2 ml-4 my-1 font-weight-bold">More Banks</p>


            </div>
          </Container>




        </section>
        <Footer />
      </div>
    );
  }
}

export default PaymenGatway;