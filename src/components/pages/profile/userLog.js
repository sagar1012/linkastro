import React, { useEffect, useState } from "react";
import { Button, Container, Row, Col, Tab, Nav } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import clientServiceAPI from "../../../services/clientServiceAPI";
import { toast } from "react-toastify";
import moment from "moment";

const UserLog = () => {
  const [chatHistory, setChatHistory] = useState();
  const [callLogs, setCallLogs] = useState();
  const [reportList, setReportList] = useState();
  console.log(chatHistory);
  console.log(callLogs);
  console.log(reportList);

  useEffect(() => {
    getChatHistory();
    getCallLogs();
    getReportDetail();
  }, []);

  const getChatHistory = () => {
    clientServiceAPI.getClientChatHistory().then((resp) => {
      if (resp.status) {
        setChatHistory(resp?.data?.data);
      } else {
        toast.error(resp.message, { position: "top-right" });
      }
    });
  };

  const getCallLogs = () => {
    clientServiceAPI.getClientCallHistory().then((resp) => {
      if (resp.status) {
        setCallLogs(resp?.data?.data);
      } else {
        toast.error(resp.message, { position: "top-right" });
      }
    });
  };

  const getReportDetail = () => {
    clientServiceAPI.getClientReportList().then((resp) => {
      if (resp.status) {
        setReportList(resp?.data?.data);
      } else {
        toast.error(resp.message, { position: "top-right" });
      }
    });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <div className="filter-wrap py-2 px-3 my-4">
                <Row className="align-items-center">
                  <Col md={5}>
                    <h5 className="font-weight-normal mb-0 ml-3">Logs</h5>{" "}
                  </Col>
                  <Col md={7}>
                    <div className="tab--style">
                      <Nav variant="pills" className="flex-row border rounded">
                        <Nav.Item>
                          <Nav.Link eventKey="first">Call</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                          <Nav.Link eventKey="second">Chat</Nav.Link>
                        </Nav.Item>

                        <Nav.Item>
                          <Nav.Link eventKey="third">Report </Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </div>
                  </Col>
                </Row>
              </div>
            </Container>
          </div>

          <Container>
            <Tab.Content>
              <Tab.Pane eventKey="first">
                <div className="px-md-5 p-sm-2">
                  <div className="px-md-4" id="data2">
                    <div className="my-3 text-right">
                      {callLogs && (
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      )}
                    </div>

                    <div id="data1"></div>
                    {callLogs &&
                      callLogs.map((item, index) => {
                        return (
                          <div
                            key={index}
                            className="border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480"
                          >
                            <img src="assets/img/priest.png" />
                            <div className="flex-1 fs-12 ml-md-3 my-xs-2">
                              <p className="mb-1">
                                {item?.firstName} {item?.lastName}
                              </p>
                              <span>
                                Ansjhg laksh msksnffi sick knjlsji nau cjcnjcu
                                nxn xfnfhj diskj
                              </span>
                            </div>
                            <div className="px-md-3 pt-2 fs-12">
                              {moment(item?.created_on).format("DD-MM-YYYY")}
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  {callLogs && callLogs.length >= 10 && (
                    <div className="p-5 text-center">
                      <Button
                        variant="primary"
                        className="btn-capsule px-5"
                        size="sm"
                      >
                        View More
                      </Button>
                    </div>
                  )}
                  {!callLogs && (
                    <div className="p-2 text-center my-4">
                      <h6 className="rec">No Record Found</h6>
                    </div>
                  )}
                </div>
              </Tab.Pane>

              <Tab.Pane eventKey="second">
                <div className="px-md-5 p-sm-2">
                  <div className="px-md-4">
                    <div className="my-3 text-right">
                      {chatHistory && (
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      )}
                    </div>
                    <div id="chate1"></div>
                    {chatHistory &&
                      chatHistory.map((item, index) => {
                        return (
                          <div
                            key={index}
                            className="border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480"
                          >
                            <img src="assets/img/priest.png" />
                            <div className="flex-1 fs-12 ml-md-3 my-xs-2">
                              <p className="mb-1">
                                {item?.firstName} {item?.lastName}
                              </p>
                              <span>
                                Ansjhg laksh msksnffi sick knjlsji nau cjcnjcu
                                nxn xfnfhj diskj
                              </span>
                            </div>
                            <div className="px-md-3 pt-2 fs-12">
                              {moment(item?.created_on).format("DD-MM-YYYY")}
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  {chatHistory && chatHistory.length >= 10 && (
                    <div className="p-5 text-center">
                      <Button
                        variant="primary"
                        className="btn-capsule px-5"
                        size="sm"
                      >
                        View More
                      </Button>
                    </div>
                  )}
                  {!chatHistory && (
                    <div className="p-2 text-center my-4">
                      <h6 className="rec">No Record Found</h6>
                    </div>
                  )}
                </div>
              </Tab.Pane>

              <Tab.Pane eventKey="third">
                <div className="px-md-5 p-sm-2">
                  <div className="px-md-4">
                    <div className="my-3 text-right">
                      {reportList && (
                        <a href="#" className="text-color2">
                          Clear all
                        </a>
                      )}
                    </div>
                    <div id="Report1"></div>

                    {reportList &&
                      reportList.map((item, index) => {
                        return (
                          <div
                            key={index}
                            className="border rounded py-2 px-3 u-chat-list d-flex mb-2 fdc-480"
                          >
                            <img src="assets/img/priest.png" />
                            <div className="flex-1 fs-12 ml-md-3 my-xs-2">
                              <p className="mb-1">
                                {item?.firstName} {item?.lastName}
                              </p>
                              <span>
                                Ansjhg laksh msksnffi sick knjlsji nau cjcnjcu
                                nxn xfnfhj diskj
                              </span>
                            </div>
                            <div className="px-md-3 pt-2 fs-12">
                              {moment(item?.created_on).format("DD-MM-YYYY")}
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  {reportList && reportList.length >= 10 && (
                    <div className="p-5 text-center">
                      <Button
                        variant="primary"
                        className="btn-capsule px-5"
                        size="sm"
                      >
                        View More
                      </Button>
                    </div>
                  )}
                  {!reportList && (
                    <div className="p-2 text-center my-4">
                      <h6 className="rec">No Record Found</h6>
                    </div>
                  )}
                </div>
              </Tab.Pane>
            </Tab.Content>
          </Container>
        </Tab.Container>
      </section>
      <Footer />
    </div>
  );
};
export default UserLog;
