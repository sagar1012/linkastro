import React, { Component } from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import { getskills, getcategorys, getlanguages, getProfessionalDetails, updateProfessionalDetails, uploadGallayImage, getGallayImage } from '../../../actions/profileAction';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';

class Professional extends Component {
    state = {
        multiValueSkills: [],
        multiValueCat: [],
        multiValueLan: [],
        skillsoptions: [],
        catoptions: [],
        lanoptions: [],
        GalleryDataFile: [],
        errors: {},
        fields: {},
        skill_status: false,
        skill_status_2: true,
        cat_status: false,
        cat_status_2: true,
        lan_status: false,
        lan_status_2: true,
        shortBioRD: 'readonly',
        longBioRD: 'readonly'

    };

    async componentDidMount() {
        document.title = 'Astrologer Profile';
        const skillsData = await this.props.getskills();
        const categoryData = await this.props.getcategorys();
        const lngData = await this.props.getlanguages();
        const GalleryData = await this.props.getGallayImage();

        this.setState({ skillsoptions: skillsData.data.data })
        this.setState({ catoptions: categoryData.data.data })
        this.setState({ lanoptions: lngData.data.data })
        this.setState({ GalleryDataFile: GalleryData.data.data })

        const event = await this.props.getProfessionalDetails();

        if (event.data.result === 1) {
            var fields = this.state.fields;
            var loctionData = event.data.data;
            Object.keys(loctionData)
                .forEach(function eachKey(key) {
                    fields[key] = loctionData[key];
                });

            this.setState(fields);
            this.setState({ multiValueSkills: this.state.fields['skillsList'] });
            this.setState({ multiValueCat: this.state.fields['categoriesList'] });
            this.setState({ multiValueLan: this.state.fields['languageList'] });

            var result = this.state.fields['skillsList'].map(function (el) {
                var o = Object.assign({}, el);
                o.skillName = el.skill_name;
                return o;
            })

            var result1 = this.state.fields['categoriesList'].map(function (el) {
                var o_1 = Object.assign({}, el);
                o_1.categoryLabel = el.category_label;
                return o_1;
            })

            var result2 = this.state.fields['languageList'].map(function (el) {
                var o_2 = Object.assign({}, el);
                o_2.categoryLabel = el.language;
                return o_2;
            })

            this.setState({ selectedValue: result });
            this.setState({ selectedValue1: result1 });
            this.setState({ selectedValue2: result2 });
        }
    }

    async refreshlist() {
        const skillsData = await this.props.getskills();
        const categoryData = await this.props.getcategorys();
        const lngData = await this.props.getlanguages();
        const GalleryData = await this.props.getGallayImage();

        /*categoryLabel*/
        this.setState({ skillsoptions: skillsData.data.data })
        this.setState({ catoptions: categoryData.data.data })
        this.setState({ lanoptions: lngData.data.data })
        this.setState({ GalleryDataFile: GalleryData.data.data })

        const event = await this.props.getProfessionalDetails();

        if (event.data.result === 1) {
            var fields = this.state.fields;
            var loctionData = event.data.data;
            Object.keys(loctionData)
                .forEach(function eachKey(key) {
                    fields[key] = loctionData[key];
                });

            this.setState(fields);
            this.setState({ multiValueSkills: this.state.fields['skillsList'] });
            this.setState({ multiValueCat: this.state.fields['categoriesList'] });
            this.setState({ multiValueLan: this.state.fields['languageList'] });

            var result = this.state.fields['skillsList'].map(function (el) {
                var o = Object.assign({}, el);
                o.skillName = el.skill_name;
                return o;
            })

            var result1 = this.state.fields['categoriesList'].map(function (el) {
                var o_1 = Object.assign({}, el);
                o_1.categoryLabel = el.category_label;
                return o_1;
            })

            var result2 = this.state.fields['languageList'].map(function (el) {
                var o_2 = Object.assign({}, el);
                o_2.categoryLabel = el.language;
                return o_2;
            })

            this.setState({ selectedValue: result });
            this.setState({ selectedValue1: result1 });
            this.setState({ selectedValue2: result2 });
        }
    }

    handleSelectskills = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueSkills: event
        });
    };

    onRemoveskills = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueSkills: event
        });
    }

    handleSelectcat = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueCat: event
        });
    };

    onRemovecat = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueCat: event
        });
    }

    handleSelectlan = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueLan: event
        });
    };

    onRemovelan = (event, selectedList, selectedItem) => {
        this.setState({
            multiValueLan: event
        });
    }

    onChange = (e) => {
        this.handleValidation();
        const fields = this.state.fields;
        if (e.target.name === 'experience') {
            if (e.target.value.match('^[0-9]*$') != null) {
                fields[e.target.name] = e.target.value;
            } else {
                fields[e.target.name] = this.state.fields['experience'];
            }
        } else {
            fields[e.target.name] = e.target.value;
        }
        this.setState(fields);
    }

    onClick = (e) => {
        if (e.target.name === 'skills') {
            this.setState({ 'skill_status': true });
            this.setState({ 'skill_status_2': false });
        } else if (e.target.name === 'category') {
            this.setState({ 'cat_status': true });
            this.setState({ 'cat_status_2': false });
        } else if (e.target.name === 'language') {
            this.setState({ 'lan_status': true });
            this.setState({ 'lan_status_2': false });
        } else if (e.target.name === 'shortBio') {
            //$('#shortBio').focus();
            document.getElementById('shortBio').focus();
        } else if (e.target.name === 'longBio') {
            document.getElementById('longBio').focus();
            //$('#longBio').focus();
        }
    }

    handleValidation() {
        //let fields = this.state.fields;
        const errors = {};
        let formIsValid = true;
        //var shortBio = $("#shortBio").val();
        var shortBio = document.getElementById('shortBio').value;
        if (shortBio === '') {
            formIsValid = false;
            errors['shortBio'] = 'Please enter Short Bio';
        } else {
            errors['shortBio'] = '';
        }
        var longBio = document.getElementById('longBio').value;
        //var longBio = $("#longBio").val();
        if (longBio === '') {
            formIsValid = false;
            errors['longBio'] = 'Please enter Long Bio';
        } else {
            errors['longBio'] = '';
        }
        //var experience = $("#experience").val();
        var experience = document.getElementById('experience').value;
        if (experience === '') {
            formIsValid = false;
            errors['experience'] = 'Please enter Experience';
        } else {
            if (/[^0-9]/.test(experience)) {
                formIsValid = false;
                errors['experience'] = 'Invalid Experience';
            } else if (experience.length > 2) {
                formIsValid = false;
                errors['experience'] = 'Invalid Experience';
            } else {
                errors['experience'] = '';
            }
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    uploadMultipleFiles = async (e) => {
        // async uploadMultipleFiles(e) {
        var formData = new FormData();
        if (e.target.files.length > 5) {
            toast.error('You can select only 5 images at a time.', { position: toast.POSITION.TOP_RIGHT })
        } else {
            for (let i = 0; i < e.target.files.length; i++) {
                formData.append('fileData', e.target.files[i]);
            }
            //var ImagePath = await this.props.uploadGallayImage(formData);
            const GalleryData = await this.props.getGallayImage();
            this.setState({ GalleryDataFile: GalleryData.data.data })
        }
    }

    onSubmit = async e => {
        e.preventDefault();
        if (this.handleValidation()) {
            var skills = this.state.multiValueSkills;
            var arr = [];
            for (let i = 0; i < skills.length; i++) {
                arr.push(skills[i].id);
            }

            var category = this.state.multiValueCat;
            var arr1 = [];
            for (let i = 0; i < category.length; i++) {
                arr1.push(category[i].id);
            }

            var language = this.state.multiValueLan;
            var arr2 = [];
            for (let i = 0; i < language.length; i++) {
                arr2.push(language[i].id);
            }

            const userData = {
                skills: arr.toString(),
                category: arr1.toString(),
                language: arr2.toString(),
                shortBio: this.state.fields['shortBio'],
                longBio: this.state.fields['longBio'],
                exp: this.state.fields['experience'],
            }
            // console.log(userData);
            await this.props.updateProfessionalDetails(userData,this.props.history);
            this.refreshlist();
            this.setState({ 'skill_status': false });
            this.setState({ 'cat_status': false });
            this.setState({ 'lan_status': false });
            console.log('1')
        } else {

            console.log('0')
            toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
        }
    }

    render() {
        // console.log(this.state.fields['skillsList']);
        var skills = this.state.fields['skillsList'] || [];
        var category = this.state.fields['categoriesList'] || [];
        var language = this.state.fields['languageList'] || [];
        var GalleryDataFile = this.state.GalleryDataFile || [];

        return (
            <div className="astrologer-user-tab">
                <div className="astrologer-user-dtl">
                    <form onSubmit={this.onSubmit}>
                        <div className="astrologer-profe">
                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Skills<span style={{ 'color': 'red' }}>*</span></h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="skills" onClick={this.onClick}>Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12" style={{ display: this.state.skill_status === true ? 'block' : 'none' }}>
                                            <div className="multiselect-div">
                                                <Multiselect
                                                    options={this.state.skillsoptions}
                                                    selectedValues={this.state.selectedValue}
                                                    onRemove={this.onRemoveskills}
                                                    displayValue='skillName'
                                                    onSelect={this.handleSelectskills}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec" style={{ display: this.state.skill_status_2 === true ? 'block' : 'none' }}>
                                    <div className="astro-sel">
                                        <ul >
                                            {skills.map((item, index) => {
                                                return (
                                                    <li><button type="button" className="btn">{item.skill_name}</button></li>)
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Category<span style={{ color: 'red' }}>*</span></h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="category" onClick={this.onClick}>Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12" style={{ display: this.state.cat_status === true ? 'block' : 'none' }}>
                                            <div className="multiselect-div">
                                                <Multiselect
                                                    options={this.state.catoptions}
                                                    selectedValues={this.state.selectedValue1}
                                                    onRemove={this.onRemovecat}
                                                    displayValue="categoryLabel"
                                                    onSelect={this.handleSelectcat}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec" style={{ display: this.state.cat_status_2 === true ? 'block' : 'none' }}>
                                    <div className="astro-sel">
                                        <ul>
                                            {category.map((item, index) => {
                                                return (
                                                    <li><button type="button" className="btn">{item.category_label}</button></li>)
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Languages<span style={{ color: 'red' }}>*</span></h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="language" onClick={this.onClick}>Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12" style={{ display: this.state.lan_status === true ? 'block' : 'none' }}>
                                            <div className="multiselect-div">
                                                <Multiselect
                                                    options={this.state.lanoptions}
                                                    selectedValues={this.state.selectedValue2}
                                                    onRemove={this.onRemovelan}
                                                    displayValue="categoryLabel"
                                                    onSelect={this.handleSelectlan}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec" style={{ display: this.state.lan_status_2 === true ? 'block' : 'none' }}>
                                    <div className="astro-sel">
                                        <ul>
                                            {language.map((item, index) => {
                                                return (
                                                    <li><button type="button" className="btn">{item.language}</button></li>)
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Short Bio<span style={{ color: 'red' }}>*</span></h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="shortBio" onClick={this.onClick}>Edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec">
                                    <div className="astro-input">
                                        <div className="form-group">
                                            <textarea maxlength="2500" className="form-control" placeholder="" onChange={this.onChange} id="shortBio" name="shortBio" value={this.state.fields['shortBio']}></textarea>
                                            <span style={{ color: 'red' }}>{this.state.errors['shortBio']}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Experience<span style={{ color: 'red' }}>*</span></h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="experience" onClick={this.onClick} >Edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec">
                                    <div className="astro-input">
                                        <div className="form-group">
                                            <input type="text" className="form-control Numeric1" placeholder="" onChange={this.onChange} id="experience" name="experience" value={this.state.fields['experience']} maxlength="2" />
                                            <span style={{ color: 'red' }}>{this.state.errors['experience']}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>Long Bio</h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                <button className="btn" type="button" name="longBio" onClick={this.onClick}>Edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec">
                                    <div className="astro-input">
                                        <div className="form-group">
                                            <textarea className="form-control" placeholder="" onChange={this.onChange} id="longBio" name="longBio" value={this.state.fields['longBio']}></textarea>
                                            <span style={{ color: 'red' }}>{this.state.errors['longBio']}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="astro-profe">
                                <div className="astro-profe-head">
                                    <div className="row">
                                        <div className="col-8">
                                            <h2>My Photo Gallery</h2>
                                        </div>
                                        <div className="col-4">
                                            <div className="astro-add-btn text-right">
                                                { /*<button className="btn" type="button">Add</button>*/}
                                                <span class="user-gallery">
                                                    <input type="file" className="form-control" onChange={this.uploadMultipleFiles} multiple />

                                                    <label className="Add-btn">Add</label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="astro-profe-sec">
                                    <div className="astro-gallery">
                                        <ul>

                                            {GalleryDataFile.map((item, index) => {
                                                return (
                                                    <li><img src={item} alt="" /></li>)
                                            })}
                                            {/* <li><img src={ require('../../../assets/images/DSCN3935.png')} alt=""/></li>
                                                    <li><img src={ require('../../../assets/images/unnamed.png')} alt=""/></li>
                                                    <li><img src={ require('../../../assets/images/DSCN3935.png')} alt=""/></li>
                                                    <li><img src={ require('../../../assets/images/unnamed.png')} alt=""/></li>*/}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit"  class="btn continue_btn"> Save &amp; Continue</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    skills: state.skills,
    categorys: state.categorys,
    languages: state.languages,
    professionaDeatils: state.locationDeatils,
    errors: state.errors.error
});
export default connect(mapStateToProps, { getskills, getcategorys, getlanguages, getProfessionalDetails, updateProfessionalDetails, uploadGallayImage, getGallayImage })(Professional);