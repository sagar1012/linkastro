import React, { Component, useState, useEffect } from "react";
import {
  Button,
  Row,
  Col,
  Container,
  ProgressBar,
  Table,
} from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import httpService from "../../../services/httpService";
import clientServiceAPI from "../../../services/clientServiceAPI";

function MatchingReport(props) {
  const [matchReport, setMatchReport] = useState();
  const [percent, setMatchPercent] = useState();
  const [infoData, setInfoData] = useState("");
  const [LatiLongMale, setLatiLongMale] = useState();
  const [LatiLongFemale, setLatiLongFemale] = useState();

  useEffect(() => {
    setInfoData(props?.location?.state?.values);
    getLatiLong();
  }, [infoData]);

  useEffect(() => {
    getMatchReport();
    getPercentage();
  }, [LatiLongMale && LatiLongFemale]);

  const getLatiLong = () => {
    if (infoData) {
      clientServiceAPI.getLatiLongDetail(infoData?.MaleBirth).then((resp) => {
        setLatiLongMale(resp?.data?.data[0]);
      });
      clientServiceAPI
        .getLatiLongDetail(infoData?.FemaleBirth)
        .then((resp) => {
          setLatiLongFemale(resp?.data?.data[0]);
        })
        .catch((err) => {
          console.log("error");
        });
    }
  };

  const getMatchReport = () => {
    const body = {
      mday: parseInt(infoData?.maleBirthDay),
      mmonth: parseInt(infoData?.maleBirthMonth),
      myear: parseInt(infoData?.maleBirthYear),
      mhour: parseInt(infoData?.maleBirthHour),
      mmin: parseInt(infoData?.maleBirthMin),
      mlat: LatiLongMale?.latitude,
      mlon: LatiLongMale?.longitude,
      mtzone: 5.5,
      fday: parseInt(infoData?.femaleBirthDay),
      fmonth: parseInt(infoData?.femaleBirthMonth),
      fyear: parseInt(infoData?.femaleBirthYear),
      fhour: parseInt(infoData?.femaleBirthHour),
      fmin: parseInt(infoData?.femaleBirthMin),
      flat: LatiLongFemale?.latitude,
      flon: LatiLongFemale?.longitude,
      ftzone: 5.5,
    };
    clientServiceAPI
      .getMatchReportDetail(body)
      .then((resp) => {
        setMatchReport(resp?.data?.matchreport);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getPercentage = () => {
    const body = {
      mday: parseInt(infoData?.maleBirthDay),
      mmonth: parseInt(infoData?.maleBirthMonth),
      myear: parseInt(infoData?.maleBirthYear),
      mhour: parseInt(infoData?.maleBirthHour),
      mmin: parseInt(infoData?.maleBirthMin),
      mlat: LatiLongMale?.latitude,
      mlon: LatiLongMale?.longitude,
      mtzone: 5.5,
      fday: parseInt(infoData?.femaleBirthDay),
      fmonth: parseInt(infoData?.femaleBirthMonth),
      fyear: parseInt(infoData?.femaleBirthYear),
      fhour: parseInt(infoData?.femaleBirthHour),
      fmin: parseInt(infoData?.femaleBirthMin),
      flat: LatiLongFemale?.latitude,
      flon: LatiLongFemale?.longitude,
      ftzone: 5.5,
    };
    clientServiceAPI
      .getPercentDetail(body)
      .then((resp) => {
        setMatchPercent(resp?.data?.matchbirth?.matchPercentage);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <div className="px-md-5">
              <Row className="align-items-center">
                <Col md={6}>
                  <h5 className="font-weight-normal mb-0">
                    Free Matching Report for
                  </h5>
                </Col>
                <Col md={6}>
                  <div className="match-capsule d-flex align-items-center justify-content-around">
                    <h6 className="font-weight-normal  mb-0">
                      {infoData?.Malename}
                    </h6>
                    <img src="assets/img/love.svg" />
                    <h6 className="font-weight-normal mb-0">
                      {infoData?.Femalename}
                    </h6>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <Container>
          <div className="px-md-5">
            {matchReport && (
              <div className="px-md-5 py-4 shadow-light">
                <div className="p-3 bg-color1 br-8 d-flex justify-content-around mb-4 mtc-p ml-auto mr-auto">
                  <h5 className="font-weight-normal mb-0 text-white">
                    Matching Points
                  </h5>
                  <div className="d-flex justify-content-around align-items-center">
                    <h5 className="font-weight-normal mb-0 text-white">
                      {matchReport?.total?.received_points}
                    </h5>
                    <h6 className="font-weight-normal mb-0 text-white mx-4">
                      Out of
                    </h6>
                    <h5 className="font-weight-normal mb-0 text-white">
                      {matchReport?.total?.total_points}
                    </h5>
                  </div>
                </div>
                {percent && (
                  <div className="mmp m-auto">
                    <label className="fw-500">Match Making Percentage</label>
                    <div className="d-flex align-items-center">
                      <ProgressBar
                        className="progress-color2 flex-1 mr-2"
                        now={60}
                      />
                      <b>{percent}%</b>
                    </div>
                  </div>
                )}
              </div>
            )}

            <div className="rounded border  my-5">
              <label className="fw-500 p-3 mb-0">
                Matchmaking Birth Details
              </label>

              <Table className="table--style1 text-center" striped responsive>
                <thead>
                  <tr className="">
                    <th>Male</th>
                    <th>Birth Details</th>
                    <th>Female</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="text-color2">
                      {infoData?.maleBirthDay}/{infoData?.maleBirthMonth}/
                      {infoData?.maleBirthYear}
                    </td>
                    <td>Date of Birth</td>
                    <td className="text-color2">
                      {infoData?.femaleBirthDay}/{infoData?.femaleBirthMonth}/
                      {infoData?.femaleBirthYear}
                    </td>
                  </tr>
                  <tr>
                    <td className="text-color2">
                      {infoData?.maleBirthHour} : {infoData?.maleBirthMin}
                    </td>
                    <td>Birth Time</td>
                    <td className="text-color2">
                      {infoData?.femaleBirthHour} : {infoData?.femaleBirthMin}
                    </td>
                  </tr>
                  <tr>
                    <td className="text-color2">{LatiLongMale?.latitude}</td>
                    <td>Latitude</td>
                    <td className="text-color2">{LatiLongFemale?.latitude}</td>
                  </tr>
                  <tr>
                    <td className="text-color2">{LatiLongMale?.longitude}</td>
                    <td>Longitude</td>
                    <td className="text-color2">{LatiLongFemale?.longitude}</td>
                  </tr>
                </tbody>
              </Table>
            </div>

            <div className="rounded border  my-5">
              <label className="fw-500 p-3 mb-0">Ashtakoot Points</label>

              <Table className="table--style1" striped responsive>
                <thead>
                  <tr className="">
                    <th>Attribute</th>
                    <th>Desc</th>
                    <th>Male</th>
                    <th>Female</th>
                    <th>Out of</th>
                    <th> Received</th>
                  </tr>
                </thead>
                {matchReport && (
                  <tbody>
                    {matchReport.bhakut && (
                      <tr>
                        <td className="">Bhakut</td>
                        <td>{matchReport.bhakut.description}</td>
                        <td className="">
                          {matchReport.bhakut.male_koot_attribute}
                        </td>
                        <td>{matchReport.bhakut.female_koot_attribute}</td>
                        <td>{matchReport.bhakut.total_points}</td>
                        <td className="text-color2">
                          {matchReport.bhakut.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.gan && (
                      <tr>
                        <td className="">Gan</td>
                        <td>{matchReport.gan.description}</td>
                        <td className="">
                          {matchReport.gan.male_koot_attribute}
                        </td>
                        <td>{matchReport.gan.female_koot_attribute}</td>
                        <td>{matchReport.gan.total_points}</td>
                        <td className="text-color2">
                          {matchReport.gan.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.maitri && (
                      <tr>
                        <td className="">Maitri</td>
                        <td>{matchReport.maitri.description}</td>
                        <td className="">
                          {matchReport.maitri.male_koot_attribute}
                        </td>
                        <td>{matchReport.maitri.female_koot_attribute}</td>
                        <td>{matchReport.maitri.total_points}</td>
                        <td className="text-color2">
                          {matchReport.maitri.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.nadi && (
                      <tr>
                        <td className="">Nadi</td>
                        <td>{matchReport.nadi.description}</td>
                        <td className="">
                          {matchReport.nadi.male_koot_attribute}
                        </td>
                        <td>{matchReport.nadi.female_koot_attribute}</td>
                        <td>{matchReport.nadi.total_points}</td>
                        <td className="text-color2">
                          {matchReport.nadi.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.tara && (
                      <tr>
                        <td className="">Tara</td>
                        <td>{matchReport.tara.description}</td>
                        <td className="">
                          {matchReport.tara.male_koot_attribute}
                        </td>
                        <td>{matchReport.tara.female_koot_attribute}</td>
                        <td>{matchReport.tara.total_points}</td>
                        <td className="text-color2">
                          {matchReport.tara.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.varna && (
                      <tr>
                        <td className="">Varna</td>
                        <td>{matchReport.varna.description}</td>
                        <td className="">
                          {matchReport.varna.male_koot_attribute}
                        </td>
                        <td>{matchReport.varna.female_koot_attribute}</td>
                        <td>{matchReport.varna.total_points}</td>
                        <td className="text-color2">
                          {matchReport.varna.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.vashya && (
                      <tr>
                        <td className="">Vashya</td>
                        <td>{matchReport.vashya.description}</td>
                        <td className="">
                          {matchReport.vashya.male_koot_attribute}
                        </td>
                        <td>{matchReport.vashya.female_koot_attribute}</td>
                        <td>{matchReport.vashya.total_points}</td>
                        <td className="text-color2">
                          {matchReport.vashya.received_points}
                        </td>
                      </tr>
                    )}
                    {matchReport.yoni && (
                      <tr>
                        <td className="">Yoni</td>
                        <td>{matchReport.yoni.description}</td>
                        <td className="">
                          {matchReport.yoni.male_koot_attribute}
                        </td>
                        <td>{matchReport.yoni.female_koot_attribute}</td>
                        <td>{matchReport.yoni.total_points}</td>
                        <td className="text-color2">
                          {matchReport.yoni.received_points}
                        </td>
                      </tr>
                    )}
                  </tbody>
                )}
                <tfoot>
                  {matchReport && (
                    <tr>
                      <th>Total</th>
                      <th>-</th>
                      <th>-</th>
                      <th>-</th>
                      <th>{matchReport?.total?.total_points}</th>
                      <th className="text-color2">
                        {matchReport?.total?.received_points}
                      </th>
                    </tr>
                  )}
                </tfoot>
              </Table>
            </div>

            {matchReport && (
              <div className="rounded border  my-5 overflow-hidden">
                <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block">
                  Matching Report
                </label>
                <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                  <span>Ashtakoot</span>
                  <div className="sm-capsul text-color2">
                    {matchReport?.total?.received_points}/
                    {matchReport?.total?.total_points}
                  </div>
                </div>
                {/* <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Ashtakoot</span>
                <div className="sm-capsul text-color2">24.5/36</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Ashtakoot</span>
                <div className="sm-capsul text-color2">24.5/36</div>
              </div>
              <div className="matc-r d-flex justify-content-between align-items-center my-2 mx-2">
                <span>Ashtakoot</span>
                <div className="sm-capsul text-color2">24.5/36</div>
              </div> */}
              </div>
            )}

            {matchReport && (
              <div className="rounded border  my-5 overflow-hidden">
                <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block">
                  Matching Conclusion
                </label>
                <p className="px-3 py-4">{matchReport?.conclusion?.report}</p>
              </div>
            )}
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
}

export default MatchingReport;
