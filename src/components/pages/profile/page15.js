import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class SubmitThank extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content bg-color1">
          <div className="bg-white p-5 thank-container text-center my-5">
            <img src="assets/img/conf.png" width="70" />
            <p className="text-color2 mb-3 mt-2">Thank you</p>

            <p className="fs-12 mb-5">Your details are submitted <br />We will contact you shortly</p>


            <Button variant="primary" size="sm" className="w-75">Ok</Button>
          </div>


        </section>
        <Footer />
      </div>
    );
  }
}

export default SubmitThank;