import React, { useEffect, useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import Progressbar from "./../../commonComponents/progressBar";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import clientServiceAPI from "../../../services/clientServiceAPI";
import { Form } from "react-bootstrap";
import { toast } from "react-toastify";

const AstroProfile = (props) => {
  const memeberID = props?.location?.state?.AstroId;
  const [astrologer, setAstrologer] = useState();
  const [astroSkill, setAstroSkill] = useState();
  const [reviewRating, setReviewRating] = useState();
  const [StartResult1, setStartResult1] = useState("");
  const [StarResultThree, setResultThree] = useState("");
  const [StarResultFour, setResultFour] = useState("");
  const [commentText, setCommentText] = useState();
  const [rating, setRating] = useState(0);

  const allClientStar = [
    ...StartResult1,
    ...StarResultThree,
    ...StarResultFour,
  ];

  let FiveStrCnt = 0;
  let FourStrCnt = 0;
  var threestr = 0;
  var twostr = 0;
  var onestr = 0;

  useEffect(() => {
    getAstrologersList();
    getReviewRating();
  }, []);

  const getAstrologersList = () => {
    const body = {
      pageNo: 1,
    };
    clientServiceAPI
      .getAstroListv1(body)
      .then((resp) => {
        if (resp.status) {
          const astroArr = resp?.data?.checkResult;
          astroArr.map((item) => {
            if (item.member_id === memeberID) {
              setAstrologer(item);
            }
          });

          const skilArr = resp?.data?.professionalskills;
          skilArr.map((item) => {
            if (item.memberid === memeberID) {
              setAstroSkill(item);
            }
          });
        } else {
          console.log("error");
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getReviewRating = () => {
    const body = {
      astroId: memeberID,
    };
    clientServiceAPI.getClientReviewRating(body).then((resp) => {
      setReviewRating(resp?.data?.checkResult[0]);
      setStartResult1(resp?.data?.checkResult1);
      setResultThree(resp?.data?.checkResult3);
      setResultFour(resp?.data?.checkResult4);
    });
  };

  const inputText = (e) => {
    setCommentText(e.target.value);
  };

  const getRating = (rat) => {
    setRating(rat);
  };

  const addReviewRating = () => {
    if (!commentText) {
      toast.error("please enter comment here", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      const body = {
        ratingNumber: rating,
        reviewText: commentText,
        astroId: memeberID,
        type: "report",
        anonymous: 0,
      };
      clientServiceAPI.addReviewRatings(body).then((resp) => {
        if (resp.status) {
          toast.success(resp.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setCommentText("");
          setRating("");
        } else {
          toast.error(resp.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      });
    }
  };

  return (
    <div className="main-wrapper">
      <Header />
      <section className="middle-section">
        <Container>
          <h4 className="hero-md my-5">Astrologer Profile</h4>

          <div className="d-flex flex-wrap mt-4 fdc-480">
            <div className="circle-md-img position-relative">
              <img
                src="assets/img/priest.png"
                className="priest-img"
                alt="link-astro"
              />

              <img
                src="assets/img/Icon _verified_user.svg"
                className="verf position-absolute"
                alt="link-astro"
              />
            </div>

            <div className="details-r flex-1">
              <h5>{astrologer?.display_name}</h5>
              <p>
                <span>
                  <i
                    className={
                      reviewRating?.average >= 1
                        ? "fas fa-heart mr-1"
                        : " far fa-heart mr-1 "
                    }
                    style={{
                      color: "#FA8345",
                    }}
                  ></i>
                  <i
                    className={
                      reviewRating?.average >= 2
                        ? "fas fa-heart mr-1"
                        : " far fa-heart mr-1 "
                    }
                    style={{
                      color: "#FA8345",
                    }}
                  ></i>
                  <i
                    className={
                      reviewRating?.average >= 3
                        ? "fas fa-heart mr-1"
                        : " far fa-heart mr-1 "
                    }
                    style={{
                      color: "#FA8345",
                    }}
                  ></i>
                  <i
                    className={
                      reviewRating?.average >= 4
                        ? "fas fa-heart mr-1"
                        : " far fa-heart mr-1 "
                    }
                    style={{
                      color: "#FA8345",
                    }}
                  ></i>
                  <i
                    className={
                      reviewRating?.average >= 5
                        ? "fas fa-heart mr-1"
                        : " far fa-heart mr-1 "
                    }
                    style={{
                      color: "#FA8345",
                    }}
                  ></i>
                </span>{" "}
                <span>{reviewRating?.views} Reviews</span>
              </p>
              <hr />
              <table className="priest-brief">
                <tr>
                  <td>
                    <img src="assets/img/chat_blue.svg" className="det-ico" />{" "}
                    400 mins
                  </td>
                  <td>
                    <img src="assets/img/phone_blue.svg" className="det-ico" />{" "}
                    400 mins
                  </td>
                  <td>
                    <img
                      src="assets/img/file_blue.svg"
                      width="15"
                      className="mr-3"
                    />{" "}
                    40 report
                  </td>
                </tr>
                <tr>
                  <td colSpan="3">
                    <img src="assets/img/lang_blue.svg" className="det-ico" />{" "}
                    {astroSkill &&
                      astroSkill?.languageList?.map((item) => {
                        return item?.language + ", ";
                      })}
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <img src="assets/img/star_blue.svg" className="det-ico" />{" "}
                    {astrologer?.experience} Years of Experience
                  </td>
                </tr>
              </table>
            </div>
          </div>

          <div className="my-4">
            <h6 className="font-weight-light">Skills</h6>
            <ul className="skills-label">
              {astroSkill &&
                astroSkill?.skillsList.map((item, index) => {
                  return <li key={index}>{item?.skill_name}</li>;
                })}
            </ul>
          </div>

          <div className="my-4">
            <h6 className="font-weight-light">Category</h6>
            <ul className="skills-label">
              {astroSkill &&
                astroSkill?.categoriesList.map((item, index) => {
                  return <li key={index}>{item?.category_label}</li>;
                })}
            </ul>
          </div>
          <h6 className="font-weight-light">About Me</h6>

          <div className="border p-4 br-8 mb-5">{astrologer?.short_bio}</div>

          <h6 className="font-weight-light">My Phots Gallery</h6>
          <div className="d-flex justify-content-between align-items-center my-4">
            <h6 className="font-weight-light mb-0">Rating & Reviews</h6>
            <button className="btn btn-reply text-uppercase">View all</button>
          </div>

          <Row>
            <Col md={2}></Col>
            <Col md={4}>
              <div className="overall-rate text-center">
                <h3>
                  {reviewRating?.average}.0 <i className="fas fa-heart"></i>
                </h3>
                <p>
                  {reviewRating?.comment} rating and {reviewRating?.views}{" "}
                  <br />
                  reviews
                </p>
              </div>
            </Col>
            <Col md={4}>
              {allClientStar.map((item) => {
                if (item.star === 5) {
                  FiveStrCnt += item.client;
                }
              })}
              <Progressbar progress={FiveStrCnt} star={5} />
              {allClientStar.map((item) => {
                if (item.star === 4) {
                  FourStrCnt += item.client;
                }
              })}
              <Progressbar progress={FourStrCnt} star={4} />
              {allClientStar.map((item) => {
                if (item.star === 3) {
                  threestr += item.client;
                }
              })}
              <Progressbar progress={threestr} star={3} />
              {allClientStar.map((item) => {
                if (item.star === 2) {
                  twostr += item.client;
                }
              })}
              <Progressbar progress={twostr} star={2} />
              {allClientStar.map((item) => {
                if (item.star === 1) {
                  onestr += item.client;
                }
              })}
              <Progressbar progress={onestr} star={1} />
            </Col>
            <Col md={2}></Col>
          </Row>

          <div className="commentWrapper mt-5">
            <div className="comment-panel mb-4">
              <div className="comment-box bg-white">
                <div className="d-flex justify-content-between">
                  <div>
                    <p className="mb-0 fs-12">Ra*****</p>
                    <span className="fs-12">Jan 18, 2020 at 10:00 am</span>
                  </div>
                  <div>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                  </div>
                </div>
                <hr />
                <div className="fs-12">I really appreciate your work.</div>
                <p className="text-right mb-0">
                  <button className="btn btn-reply">Reply</button>
                </p>
              </div>

              <div className="comment-box pl-5">
                <div className="d-flex justify-content-between">
                  <div>
                    <p className="mb-0 fs-12">Ra*****</p>
                    <span className="fs-12">Jan 18, 2020 at 10:00 am</span>
                  </div>
                  <div>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                  </div>
                </div>
                <hr />
                <div className="fs-12">I really appreciate your work.</div>
                <p className="text-right mb-0"></p>
              </div>
            </div>

            <div className="comment-panel">
              <div className="comment-box bg-white">
                <div className="d-flex justify-content-between">
                  <div>
                    <p className="mb-0 fs-12">Ra*****</p>
                    <span className="fs-12">Jan 18, 2020 at 10:00 am</span>
                  </div>
                  <div>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                    <i
                      class="fas fa-heart mr-1"
                      style={{
                        color: "#FA8345",
                      }}
                    ></i>
                  </div>
                </div>
                <hr />
                <div className="fs-12">I really appreciate your work.</div>
                <p className="text-right mb-0">
                  <button className="btn btn-reply">Reply</button>
                </p>
              </div>
            </div>

            <div className="comment-panel">
              <div className="comment-box bg-white">
                <div>
                  <div className="mb-3">
                    <i
                      className={
                        rating >= 1
                          ? "fas fa-heart mr-1"
                          : " far fa-heart mr-1 "
                      }
                      style={{
                        color: "#FA8345",
                      }}
                      onClick={(e) => getRating(1)}
                    ></i>
                    <i
                      className={
                        rating >= 2
                          ? "fas fa-heart mr-1"
                          : " far fa-heart mr-1 "
                      }
                      style={{
                        color: "#FA8345",
                      }}
                      onClick={(e) => getRating(2)}
                    ></i>
                    <i
                      className={
                        rating >= 3
                          ? "fas fa-heart mr-1"
                          : " far fa-heart mr-1 "
                      }
                      style={{
                        color: "#FA8345",
                      }}
                      onClick={(e) => getRating(3)}
                    ></i>
                    <i
                      className={
                        rating >= 4
                          ? "fas fa-heart mr-1"
                          : " far fa-heart mr-1 "
                      }
                      style={{
                        color: "#FA8345",
                      }}
                      onClick={(e) => getRating(4)}
                    ></i>
                    <i
                      className={
                        rating >= 5
                          ? "fas fa-heart mr-1"
                          : " far fa-heart mr-1 "
                      }
                      style={{
                        color: "#FA8345",
                      }}
                      onClick={(e) => getRating(5)}
                    ></i>
                  </div>
                  <Form.Group as={Row} className="fmc-style col-12">
                    <Form.Control
                      type="text"
                      as="textarea"
                      name="comment"
                      value={commentText}
                      onChange={inputText}
                    />
                  </Form.Group>
                </div>
                <p className="text-right mb-0">
                  <button
                    className="btn btn-reply"
                    type="Submit"
                    onClick={addReviewRating}
                  >
                    comment
                  </button>
                </p>
              </div>
            </div>
          </div>

          <div className="p-5 text-center">
            <Button variant="primary" className="btn-capsule px-5" size="sm">
              View More
            </Button>
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default AstroProfile;
