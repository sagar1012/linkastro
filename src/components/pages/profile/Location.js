import React, { Component } from "react";
import {
  updateLocation,
  getLocationDetails,
} from "../../../actions/profileAction";
import { connect } from "react-redux";

class Location extends Component {
  state = {
    errors: {},
    fields: {},
  };

  async componentDidMount() {
    document.title = "Astrologer Profile";
    const event = await this.props.getLocationDetails();
    if (event?.data?.result === 1) {
      var fields = this.state.fields;
      var loctionData = event.data.data;
      Object.keys(loctionData).forEach(function eachKey(key) {
        fields[key] = loctionData[key];
      });
      this.setState(fields);
    }
  }

  onChange = (e) => {
    this.handleValidation();
    const fields = this.state.fields;
    if (e.target.name === "pinCode") {
      if (e.target.value.match("^[0-9]*$") != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["pinCode"];
      }
    } else {
      fields[e.target.name] = e.target.value;
    }
    this.setState(fields);
  };

  handleValidation() {
    const fields = this.state.fields;
    const errors = {};
    let formIsValid = true;

    var address = document.getElementById("address").value;
    if (address === "") {
      formIsValid = false;
      errors["address"] = "Please enter Address";
    } else {
      errors["address"] = "";
    }

    var city = document.getElementById("city").value;
    if (city === "") {
      formIsValid = false;
      errors["city"] = "Please enter City";
    } else {
      errors["city"] = "";
    }

    var state = document.getElementById("state").value;
    if (state === "") {
      formIsValid = false;
      errors["state"] = "Please enter State";
    } else {
      errors["state"] = "";
    }

    var country = document.getElementById("country").value;
    if (country === "") {
      formIsValid = false;
      errors["country"] = "Please enter Country";
    } else {
      errors["country"] = "";
    }
    var pinCode = document.getElementById("pinCode").value;
    if (pinCode === 0) {
      formIsValid = false;
      errors["pinCode"] = "Please enter Pincode";
    } else {
      if (/[^0-9]/.test(fields["pinCode"])) {
        formIsValid = false;
        errors["pinCode"] = "Invalid Pincode";
      } else if (pinCode.length < 6) {
        formIsValid = false;
        errors["pinCode"] = "Invalid Pincode";
      } else {
        errors["pinCode"] = "";
      }
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  onSubmit = async (e) => {
    e.preventDefault();
    if (this.handleValidation()) {
      const userData = {
        address: this.state.fields["address"],
        city: this.state.fields["city"],
        state: this.state.fields["state"],
        country: this.state.fields["country"],
        pinCode: this.state.fields["pinCode"],
      };
      await this.props.updateLocation(userData, this.props.history);
    } else {
      //toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
    }
  };

  render() {
    return (
      <div className="astrologer-user-tab">
        <div className="astrologer-user-dtl">
          <form onSubmit={this.onSubmit}>
            <div className="sign-form">
              <div className="form-group focused">
                {/*<input type="text" className="form-control" onChange={this.onChange} id="address" placeholder="" name="address" value={this.state.fields['address']} />*/}

                <textarea
                  className="form-control"
                  onChange={this.onChange}
                  id="address"
                  placeholder=""
                  name="address"
                  value={this.state.fields["address"]}
                ></textarea>

                <label for="address" className="form-label">
                  Address<span style={{ color: "red" }}>*</span>
                </label>
                {/*<input type="text" className="form-control" />*/}
                <span style={{ color: "red" }}>
                  {this.state.errors["address"]}
                </span>
              </div>
              <div className="form-group focused">
                <input
                  type="text"
                  className="form-control"
                  onChange={this.onChange}
                  id="city"
                  placeholder=" "
                  name="city"
                  value={this.state.fields["city"]}
                />
                <label for="city" className="form-label">
                  City<span style={{ color: "red" }}>*</span>
                </label>
                <span style={{ color: "red" }}>
                  {this.state.errors["city"]}
                </span>
              </div>
              <div className="form-group focused">
                <input
                  type="text"
                  className="form-control"
                  onChange={this.onChange}
                  id="state"
                  placeholder=" "
                  name="state"
                  value={this.state.fields["state"]}
                />
                <label for="state" className="form-label">
                  State<span style={{ color: "red" }}>*</span>
                </label>
                <span style={{ color: "red" }}>
                  {this.state.errors["state"]}
                </span>
              </div>
              <div className="form-group focused">
                <input
                  type="text"
                  className="form-control"
                  onChange={this.onChange}
                  id="country"
                  placeholder=" "
                  name="country"
                  value={this.state.fields["country"]}
                />
                <label for="country" className="form-label">
                  Country<span style={{ color: "red" }}>*</span>
                </label>
                <span style={{ color: "red" }}>
                  {this.state.errors["country"]}
                </span>
              </div>
              <div className="form-group focused">
                <input
                  maxlength="6"
                  type="text"
                  className="form-control Numeric1"
                  placeholder=" "
                  onChange={this.onChange}
                  id="pinCode"
                  name="pinCode"
                  value={this.state.fields["pinCode"]}
                  onInput={(e) => {
                    e.target.value = e.target.value.replace(/[^0-9]/g, "");
                  }}
                />
                <label for="pinCode" className="form-label">
                  Pincode<span style={{ color: "red" }}>*</span>
                </label>
                <span style={{ color: "red" }}>
                  {this.state.errors["pinCode"]}
                </span>
              </div>

              <div className="form-group">
                <button type="submit" className="btn continue_btn">
                  Save & Continue
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  locationDeatils: state.locationDeatils,
  errors: state.errors.error,
});
export default connect(mapStateToProps, { updateLocation, getLocationDetails })(
  Location
);
