import React, { Component } from 'react';
import { Button, Container, Table, Row, Col } from 'react-bootstrap';
import Header from '../../templates/Header';
import Footer from '../../templates/Footer';


class ConsultationEarning extends Component {
  render() {
    return (
      <div className="layout-body">
        <Header />
        <section className="layout-content">
          <div className="bg-white shadow-bold p-3">
            <Container>
              <Row className="align-items-center">
                <Col md={4}><h5 className="font-weight-normal mb-0">Consultation and Earning</h5> </Col>
                <Col md={8}>
                  <div className="gray-capsule py-2 px-4">

                    <Button variant="primary" size="sm" class="px-3">Search</Button>
                  </div>
                </Col>
              </Row>
              {/* <div className="d-flex">
                <h5 className="font-weight-normal mb-0">Consultation and Earning</h5>
                <div className="flex-1">
                  hgjh
              </div>
              </div> */}
            </Container>
          </div>

          <Container>

            <div className="bg-gray p-4 my-4">
              <Table responsive className="table--style">
                <thead>
                  <tr>
                    <th>Order Id</th>
                    <th>User Id</th>
                    <th>Duration</th>
                    <th>Astrologer Earning Inr</th>
                    <th>Consultation Date</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>

                    <td><div>9878654</div></td>
                    <td><div>Ashish Pawar</div></td>
                    <td><div><b>15 min</b></div></td>
                    <td><div><b>300 rs</b></div></td>
                    <td><div>27/09/2020</div></td>
                  </tr>
                  <tr>
                    <td><div>9878654</div></td>
                    <td><div>Ashish Pawar</div></td>
                    <td><div><b>15 min</b></div></td>
                    <td><div><b>300 rs</b></div></td>
                    <td><div>27/09/2020</div></td>
                  </tr>
                  <tr>
                    <td><div>9878654</div></td>
                    <td><div>Ashish Pawar</div></td>
                    <td><div><b>15 min</b></div></td>
                    <td><div><b>300 rs</b></div></td>
                    <td><div>27/09/2020</div></td>
                  </tr>

                </tbody>
              </Table>
            </div>

            <div className="p-5 text-center">
              <Button variant="primary" className="btn-capsule px-5" size="sm">View More</Button>
            </div>

          </Container>


        </section>
        <Footer />
      </div>
    );
  }
}

export default ConsultationEarning;