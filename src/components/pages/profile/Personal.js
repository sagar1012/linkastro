import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { reactLocalStorage } from "reactjs-localstorage";
import { connect } from "react-redux";
import Moment from "moment";
import {
  updatePersonalDetails,
  getPersonalDetails,
  updateProfile,
  GetProfilePic,
} from "../../../actions/profileAction";

class Personal extends Component {
  state = {
    readOnly: true,
    fields: {},
    errors: {},
    form_data: {},
    //startDate: new Date(),
    bithDate: "",
    files: {},
    //profile_picture:reactLocalStorage.getObject('profile_pic') || require('../../../assets/images/priest_vishnuvardhana@2x.png')
  };

  async componentDidMount() {
    document.title = "Astrologer Profile";
    //var auth  = reactLocalStorage.getObject('Auth');
    var token = reactLocalStorage.getObject("token");
    var data = Object.keys(token).length;
    if (data > 0) {
      const resultData = await this.props.getPersonalDetails();
      console.log(resultData, "resultData");
      if (resultData.data.result === 1) {
        var fields = this.state.fields;
        var rowData = resultData.data.data;

        Object.keys(rowData).forEach(function eachKey(key) {
          fields[key] = rowData[key];
          fields["dob"] = Moment(rowData.dob).format("DD/MM/YYYY");
          fields["firstName"] = rowData.firstName + " " + rowData.lastName;
        });

        console.log("001", rowData);
        console.log("002", fields);

        this.setState(fields);
        this.setState({ startDate: new Date(rowData.dob) });
        const ImgData = {
          filePath: rowData.profilepic,
        };
        const profile_pic = await this.props.GetProfilePic(ImgData);
        this.setState({ profile_picture: profile_pic });
        //console.log(this.state.profile_picture);
      }
    }
  }

  onChange = (e) => {
    this.handleValidation();
    const fields = this.state.fields;
    if (e.target.name === "contactNo") {
      if (e.target.value.match("^[0-9]*$") != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["contactNo"];
      }
    } else if (e.target.name === "alternateContact") {
      if (e.target.value.match("^[0-9]*$") != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["alternateContact"];
      }
    } else {
      fields[e.target.name] = e.target.value;
    }
    this.setState(fields);
  };

  onChangeFile = async (e) => {
    var file = e.target.files[0];
    var formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("fileData", file);
    var ImagePath = await this.props.updateProfile(formData);
    this.setState({ profile_picture: ImagePath });
  };

  handleChange = (date) => {
    let formIsValid = true;
    const errors = {};
    const fields = this.state.fields;
    fields["dob"] = date;
    this.setState({
      startDate: date,
    });
    //errors["dob"] = "";

    document.getElementById("dob_error").innerHTML = "";
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    var da = today.getDate() - birthDate.getDate();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (m < 0) {
      m += 12;
    }
    if (da < 0) {
      da += 30;
    }

    if (age < 18 || age > 100) {
      //errors["dob"] = "Your age should be 18 years old";

      document.getElementById("dob_error").innerHTML =
        "Your age should be 18 years old";
      formIsValid = false;
    } else {
      document.getElementById("dob_error").innerHTML =
        "Your age should be 18 years old";
      //errors["dob"] = "";
    }
  };

  handleValidation() {
    const fields = this.state.fields;
    const errors = {};
    let formIsValid = true;

    if (!fields["displayName"]) {
      formIsValid = false;
      errors["displayName"] = "Please enter Display Name.";
    } else {
      errors["displayName"] = "";
    }

    if (!fields["firstName"]) {
      formIsValid = false;
      errors["firstName"] = "Please enter Full Name";
    } else {
      errors["firstName"] = "";
    }
    /*   if(!fields["lastName"]){
              formIsValid = false;
              errors["lastName"] = "Please enter lastname.";
           }else{
              errors["lastName"] = "";
           }*/

    var contactNo = document.getElementById("contactNo").value;
    if (!fields["contactNo"]) {
      formIsValid = false;
      errors["contactNo"] = "Please enter Mobile Number";
    } else {
      if (/[^0-9]/.test(contactNo)) {
        formIsValid = false;
        errors["contactNo"] = "Please enter valid Mobile Number";
      } else if (contactNo.length < 10) {
        formIsValid = false;
        errors["contactNo"] = "Please enter valid Mobile Number";
      } else if (contactNo.length > 12) {
        formIsValid = false;
        errors["contactNo"] = "Please enter valid Mobile Number";
      } else {
        errors["contactNo"] = "";
      }
    }

    var alternateContact = document.getElementById("alternateContact").value;
    if (alternateContact.length > 0) {
      if (/[^0-9]/.test(alternateContact)) {
        formIsValid = false;
        errors["alternateContact"] =
          "Please enter valid Alternate Mobile Number";
      } else if (alternateContact.length < 10) {
        formIsValid = false;
        errors["alternateContact"] =
          "Please enter valid Alternate Mobile Number";
      } else if (alternateContact.length > 10) {
        formIsValid = false;
        errors["alternateContact"] =
          "Please enter valid Alternate Mobile Number";
      } else {
        errors["alternateContact"] = "";
      }
    }

    var emailId = document.getElementById("emailId").value;
    if (emailId === "") {
      formIsValid = false;
      errors["emailId"] = "Please enter Email Address";
    }
    if (typeof fields["emailId"] !== "undefined") {
      const lastAtPos = fields["emailId"].lastIndexOf("@");
      const lastDotPos = fields["emailId"].lastIndexOf(".");
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          fields["emailId"].indexOf("@@") === -1 &&
          lastDotPos > 2 &&
          fields["emailId"].length - lastDotPos > 2
        )
      ) {
        formIsValid = false;
        errors["emailId"] = "Please enter valid Email Address";
      }
    }

    if (
      this.state.startDate === "" ||
      this.state.startDate === undefined ||
      this.state.startDate == null
    ) {
      formIsValid = false;
      //errors["dob"] = "Please enter Date Of Birth";

      document.getElementById("dob_error").innerHTML =
        "Please enter Date Of Birth";
    } else {
      //errors["dob"] = "";

      document.getElementById("dob_error").innerHTML = "";
      var today = new Date();
      var birthDate = new Date(this.state.startDate);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      var da = today.getDate() - birthDate.getDate();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      if (m < 0) {
        m += 12;
      }
      if (da < 0) {
        da += 30;
      }

      if (age < 18 || age > 100) {
        //errors["dob"] = "Your age should be 18 years old";
        document.getElementById("dob_error").innerHTML =
          "Your age should be 18 years old";
        formIsValid = false;
      } else {
        errors["dob"] = "";
      }
    }

    /*if(!fields["gender"]){
           formIsValid = false;
           errors["gender"] = "Please select gender";
        }*/
    this.setState({ errors: errors });
    return formIsValid;
  }

  onSubmit = async (e) => {
    e.preventDefault();
    if (this.handleValidation()) {
      const DOB = Moment(this.state.startDate).format("YYYY-MM-DD");
      var name = this.state.fields["firstName"];
      // console.log(name);
      var ret = name.split(" ");
      var str1 = ret[0];
      var str2 = ret[1];
      const userData = {
        displayName: this.state.fields["displayName"],
        firstName: str1,
        lastName: str2,
        mobileNo: this.state.fields["contactNo"],
        alternateContact: this.state.fields["alternateContact"],
        email: this.state.fields["emailId"],
        birthDate: DOB,
        gender: this.state.fields["gender"],
      };
      // console.log(userData);
      await this.props.updatePersonalDetails(userData, this.props.history);
    } else {
      //toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
    }
  };

  render() {
    return (
      <React.Fragment>
        <form onSubmit={this.onSubmit}>
          <div className="astrologer-user-tab">
            <div className="astrologer-user-pro">
              <div className="astro-user-img">
                <span className="user-img">
                  <img src={this.state.profile_picture} alt="" />
                </span>
                <span className="user-status">
                  <input
                    type="file"
                    onChange={this.onChangeFile}
                    id="fileName"
                    name="fileName"
                  />
                  <label></label>
                </span>
              </div>
              <div className="astro-user-name">
                <div className="sign-form">
                  <div className="form-group focused">
                    <input
                      type="text"
                      className="form-control"
                      onChange={this.onChange}
                      id="displayName"
                      placeholder=" "
                      name="displayName"
                      value={this.state.fields["displayName"]}
                    />
                    <label for="displayName" class="form-label">
                      Display Name<span style={{ color: "red" }}>*</span>
                    </label>
                    <span style={{ color: "red" }}>
                      {this.state.errors["displayName"]}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="astrologer-user-dtl">
              <div className="sign-form">
                {/*<div className="form-group">
                                    <label>Full Name<span style={{"color":"red"}}>*</span></label>
                                    <input type="text" className="form-control" placeholder="Full Name" value="Ashish Pawar" />
                                </div>*/}

                <div className="form-group focused">
                  <input
                    type="text"
                    className="form-control"
                    onChange={this.onChange}
                    id="firstName"
                    placeholder=" "
                    name="firstName"
                    value={this.state.fields["firstName"]}
                  />
                  <label for="firstName" class="form-label">
                    Full Name<span style={{ color: "red" }}>*</span>
                  </label>
                  <span style={{ color: "red" }}>
                    {this.state.errors["firstName"]}
                  </span>
                </div>

                <div className="form-div">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group focused">
                        <input
                          type="text"
                          className="form-control Numeric1"
                          onChange={this.onChange}
                          id="contactNo"
                          placeholder=" "
                          name="contactNo"
                          value={this.state.fields["contactNo"]}
                          maxlength="10"
                          onInput={(e) => {
                            e.target.value = e.target.value.replace(
                              /[^0-9]/g,
                              ""
                            );
                          }}
                        />
                        <label for="contactNo" class="form-label">
                          Mobile Number<span style={{ color: "red" }}>*</span>
                        </label>
                        <span style={{ color: "red" }}>
                          {this.state.errors["contactNo"]}
                        </span>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group focused">
                        <input
                          type="text"
                          className="form-control Numeric1"
                          onChange={this.onChange}
                          id="alternateContact"
                          placeholder=" "
                          name="alternateContact"
                          value={this.state.fields["alternateContact"]}
                          maxlength="10"
                          onInput={(e) => {
                            e.target.value = e.target.value.replace(
                              /[^0-9]/g,
                              ""
                            );
                          }}
                        />
                        <label for="alternateContact" class="form-label">
                          Alternate Mobile Number
                        </label>
                        <span style={{ color: "red" }}>
                          {this.state.errors["alternateContact"]}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group focused">
                  <input
                    type="text"
                    className="form-control"
                    onChange={this.onChange}
                    id="emailId"
                    placeholder=" "
                    name="emailId"
                    value={this.state.fields["emailId"]}
                  />
                  <label for="emailId" class="form-label">
                    Email<span style={{ color: "red" }}>*</span>
                  </label>
                  <span style={{ color: "red" }}>
                    {this.state.errors["emailId"]}
                  </span>
                </div>
                <div class="form-group focused">
                  <label class="form-label">
                    Date of Birth<span style={{ color: "red" }}>*</span>
                  </label>
                  <div class="select-option-form">
                    <DatePicker
                      showYearDropdown
                      dateFormat="dd/MM/yyyy"
                      id="dob"
                      name="date"
                      className="form-control"
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      value={this.state.fields["dob"]}
                    />
                    <span style={{ color: "red" }} id="dob_error"></span>
                    {/* <ul>
                                            <li class="active">Day</li>
                                            <li>Month</li>
                                            <li>Year</li>
                                        </ul> */}
                  </div>
                </div>
                <div class="form-group">
                  <label>
                    Gender<span style={{ color: "red" }}>*</span>
                  </label>
                  <div class="select-option-form">
                    <ul>
                      <li>
                        <input
                          type="radio"
                          name="gender"
                          value="male"
                          onChange={this.onChange}
                          checked={
                            this.state.fields["gender"] === "male"
                              ? "checked"
                              : ""
                          }
                        />
                        <label>Male</label>
                      </li>
                      <li>
                        {" "}
                        <input
                          type="radio"
                          name="gender"
                          value="female"
                          onChange={this.onChange}
                          checked={
                            this.state.fields["gender"] === "female"
                              ? "checked"
                              : ""
                          }
                        />
                        <label>Female</label>
                      </li>
                      <li>
                        {" "}
                        <input
                          type="radio"
                          name="gender"
                          value="other"
                          onChange={this.onChange}
                          checked={
                            this.state.fields["gender"] === "other"
                              ? "checked"
                              : ""
                          }
                        />
                        <label>Other</label>
                      </li>
                    </ul>
                    <span style={{ color: "red" }}>
                      {this.state.errors["gender"]}
                    </span>
                  </div>
                </div>
                {/*<div class="form-group">
                                    <div className="public-profile-btn">
                                        <button className="btn" type="button">View public Profile</button>
                                    </div>
                                </div>*/}

                <div className="form-group">
                  <button type="submit" className="btn continue_btn">
                    Save & Continue
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  personalDeatils: state.personalDeatils,
  errors: state.errors.error,
});
export default connect(mapStateToProps, {
  updatePersonalDetails,
  getPersonalDetails,
  updateProfile,
  GetProfilePic,
})(Personal);
