import React, { Component, useEffect, useState } from "react";
import {
  Button,
  Row,
  Col,
  Container,
  Overlay,
  Popover,
  OverlayTrigger,
} from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import httpService from "../../../services/httpService";

function AstrologerList() {
  const [AstrologersList, setAstrologersList] = useState();
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    getAstrologersList();
  }, []);

  const getAstrologersList = () => {
    const body = {
      pageNo: 1,
      searchtext: searchValue,
    };
    httpService
      .post("/api/client/profile/getAstroListv1", "", body)
      .then((resp) => {
        setAstrologersList(resp.data);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const onSearch = (value) => {
    setSearchValue(value);
    getAstrologersList();
  };

  const popoverClick = (
    <Popover
      className="filter-jar"
      id="popover-trigger-click"
      title="Popover bottom"
    >
      <div className="filter-cont">
        <table>
          <tr>
            <td>Experience</td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>High to Low</span>
                </label>
              </div>
            </td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>Low to High</span>
                </label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Price</td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>High to Low</span>
                </label>
              </div>
            </td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>Low to High</span>
                </label>
              </div>
            </td>
          </tr>
          <tr>
            <td>Rating</td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>High to Low</span>
                </label>
              </div>
            </td>
            <td>
              <div className="cs_rd_btn">
                <label class="blue">
                  <input type="radio" name="toggle" />
                  <span>Low to High</span>
                </label>
              </div>
            </td>
          </tr>
        </table>
      </div>
    </Popover>
  );
  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-gray p-3">
          <Container>
            <Row>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  100% Secure Payment
                </div>
              </Col>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  Verified Astrologer
                </div>
              </Col>
              <Col md={4}>
                <div className="text-center">
                  <img src="assets/img/Icon _verified_user.svg" width="20" />{" "}
                  Private & Confidential
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <Container>
          <div className="filter-wrap py-2 px-3 my-4">
            <Row className="align-items-center">
              <Col md={4}>
                <h5 className="set-cp-ico">
                  Astrologers <br />{" "}
                  <small className="fs-12">
                    Connect with our top Astrologers
                  </small>
                </h5>
              </Col>
              <Col md={8}>
                <div className="d-flex align-items-center">
                  <OverlayTrigger
                    trigger="click"
                    placement="bottom"
                    overlay={popoverClick}
                  >
                    <Button className="filter-dd">
                      Filter <i class="fas fa-filter"></i>
                    </Button>
                  </OverlayTrigger>

                  {/* <Button className="filter-dd" onClick={handleClick}>Filter <i class="fas fa-filter"></i>!</Button>

                    <Overlay
                      show={show}
                      target={target}
                      placement="bottom"
                      container={ref.current}
                      containerPadding={20}
                    >
                      <Popover id="popover-contained">
                        <Popover.Title as="h3">Popover bottom</Popover.Title>
                        <Popover.Content>
                          <strong>Holy guacamole!</strong> Check this info.
    </Popover.Content>
                      </Popover>
                    </Overlay> */}

                  <div className="search-input ml-3 flex-1">
                    <input
                      type="text"
                      onChange={(e) => onSearch(e.target.value)}
                      placeholder="Search Astrologers, by Name, Skill, language."
                    />
                    <i class="fas fa-search"></i>
                  </div>
                </div>
              </Col>
            </Row>
          </div>

          {AstrologersList && (
            <div className="p-sm-3">
              <Row>
                {AstrologersList?.checkResult?.map((e, index) => {
                  return (
                    <Col key={index} md={6} lg={4}>
                      <div className="rounded shadow-light p-2 al-card fs-12">
                        <div className="d-flex ">
                          <div className="al-view text-center">
                            <div className="al-img">
                              <img
                                src="assets/img/priest.png"
                                className="al-pic"
                              />
                              <img
                                src="assets/img/Icon _verified_user.svg"
                                className="al-verf"
                              />
                              <span>{e.minutes_free} min free</span>
                            </div>

                            <div className="mt-2">******</div>
                            <span className="rv">330 Review</span>
                          </div>

                          <div className="flex-1 px-2">
                            <img
                              src="assets/img/Icon_notif_empty.svg"
                              className="al-noti"
                            />
                            <table className="w-100">
                              <tr>
                                <td className="fw-500">{e.display_name}</td>
                              </tr>
                              <tr>
                                {AstrologersList.professionalskills[index]
                                  .categoriesList[0] &&
                                  AstrologersList.professionalskills[
                                    index
                                  ].categoriesList.map((item, index) => {
                                    return (
                                      <td key={index}>{item.category_label}</td>
                                    );
                                  })}
                              </tr>
                              <tr>
                                {AstrologersList.professionalskills[index]
                                  .languageList[0] &&
                                  AstrologersList.professionalskills[
                                    index
                                  ].languageList.map((item, index) => {
                                    return <td key={index}>{item.language}</td>;
                                  })}
                              </tr>
                            </table>
                          </div>
                        </div>

                        <div className="bg-gray p-2 mx-m8 mt-2">
                          <table className="w-100">
                            <tr>
                              <td className="pb-2">{e.experience} yrs exp.</td>
                              <td className="text-right pb-2">
                                <button className="btn btn-blue btn-xs font-weight-bold text-uppercase">
                                  <img src="assets/img/3.png" width="14" /> Call
                                </button>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Fees{" "}
                                <span className="tild-del">
                                  ₹ {e.astrologer_charges}/min
                                </span>{" "}
                                &nbsp;
                                <span className="text-color2 fw-500">
                                  ₹ 25/min
                                </span>
                              </td>
                              <td className="text-right">
                                <button className="btn btn-blue btn-xs font-weight-bold text-uppercase">
                                  <img src="assets/img/4.png" width="14" /> Chat
                                </button>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div className="text-right mb-m5">
                          <a href="#">Available Now</a>
                        </div>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </div>
          )}
        </Container>
      </section>
      <Footer />
    </div>
  );
}

export default AstrologerList;
