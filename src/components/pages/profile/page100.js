import React, { Component, useState, useEffect } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import moment from "moment";
import "./page100.css";
import { ToastContainer, toast } from "react-toastify";
import clientServiceAPI from "../../../services/clientServiceAPI";

const Notifications = () => {
  const [notification, setNotification] = useState();

  const [sdata, setData] = useState(0);

  const [pageno, setPageNo] = useState(1);

  const getNotification = (pageno) => {
    const body = { pageNo: pageno };
    clientServiceAPI
      .getClientNotification(body)
      .then((resp) => {
        setData(resp.data.data.length);
        if (pageno === 1) {
          setNotification(resp.data.data);
        } else {
          setNotification([...notification, ...resp.data.data]);
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const viewMore = (pageno) => {
    setPageNo(pageno);
    getNotification(pageno);
  };

  useEffect(() => {
    getNotification(pageno);
  }, []);

  const handleNotification = () => {
    clientServiceAPI.clearNotificationLog().then((resp) => {
      console.log(resp.data);
      if (resp?.data?.result === 1) {
        toast.success(resp.data.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.error(resp.data.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    });
  };

  return (
    <div className="layout-body">
      <Header />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-3">
          <Container>
            <div className="px-md-5 ">
              <h5 className="mb-0 font-weight-normal">Notifications</h5>
            </div>
          </Container>
        </div>
        <div>
          <a href="#" className="clear " onClick={handleNotification}>
            <u>Clear all</u>
          </a>
        </div>
        <Container>
          {notification?.map((item) => (
            <Row key={item.id} className="mt-4">
              <Col md={2}></Col>
              <Col md={8}>
                <div className="rounded shadow-light p-3  my-2 overflow-hidden current-noti">
                  <h6 className=" d-flex justify-content-between align-items-center fs-12 font-weight-normal">
                    <span className="fw-500">{item.title}</span>
                    <span>
                      Date {moment(item.created_on).format("DD/MM/YYYY")}
                    </span>
                  </h6>

                  <p className="fs-12 mb-0">{item.notification}</p>
                </div>
              </Col>
              <Col md={2}></Col>
            </Row>
          ))}
          {sdata && sdata == 10 && (
            <div className="p-2 text-center my-4">
              <Button
                variant="primary"
                className="btn-capsule px-5"
                size="sm"
                onClick={(e) => viewMore(2)}
              >
                View More
              </Button>
            </div>
          )}
          {!notification && (
            <div className="p-2 text-center my-4">
              <h6 className="rec">No Record Found</h6>
            </div>
          )}
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default Notifications;
