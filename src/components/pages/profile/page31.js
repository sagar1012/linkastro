import React, { Component, useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Tab,
  Tabs,
  Nav,
  Table,
} from "react-bootstrap";
import Moment from "moment";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import httpService from "../../../services/httpService";
import clientServiceAPI from "../../../services/clientServiceAPI";
import moment from "moment";

function FreeKundali() {
  const [personalDetails, setPersonalDetails] = useState();
  const [astroDetails, setAstroDetails] = useState();
  const [panchangDetails, setPanchangDetails] = useState();
  const [numrology, setNumrology] = useState();
  const [chartDetail, setCharts] = useState();
  const [vishdasha, setVishdasha] = useState();
  const [mahaDasha, setMahadasha] = useState();
  const [dasha, setDasha] = useState("Mahadasha");
  const [antarDasha, setAntarDasha] = useState("");
  const [data, setdata] = useState("");
  const [pratyData, setPratyData] = useState("");
  const [sadhesatiDasha, setSadhesatiDasha] = useState();
  const [sadhesatiStatus, setSadhesatiStatus] = useState();
  const [kaalsarpDosha, setKaalsarpDosha] = useState();
  const [ManglikDosha, setManglikDosha] = useState();
  const [pitraDosha, setPitraDosha] = useState();

  useEffect(() => {
    getPersonalDetails();
  }, []);

  const getPersonalDetails = () => {
    clientServiceAPI
      .getPersonalDetail()
      .then((resp) => {
        setPersonalDetails(resp.data.personal.result[0]);
        getAstrodetails(resp.data.personal.result[0]);
        getPanchangDetails(resp.data.personal.result[0]);
        getNumrology(resp.data.personal.result[0]);
        getKundaliCharts(resp.data.personal.result[0]);
        getVimshottariDasha(resp.data.personal.result[0]);
        getMahaDasha(resp.data.personal.result[0]);
        getSadhesatiDasha(resp.data.personal.result[0]);
        getSadhesatiStatus(resp.data.personal.result[0]);
        getKaalsarpDosha(resp.data.personal.result[0]);
        getManglikDosha(resp.data.personal.result[0]);
        getPitraDosha(resp.data.personal.result[0]);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getAstrodetails = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getAstrologerDetail(body)
      .then((resp) => {
        setAstroDetails(resp.data.astrodetail);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getPanchangDetails = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };

    clientServiceAPI
      .getPanchangeDetailList(body)
      .then((resp) => {
        setPanchangDetails(resp.data.panchangdetails);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getNumrology = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      name: data.first_name,
    };
    clientServiceAPI
      .getNormologyList(body)
      .then((resp) => {
        setNumrology(resp.data.numerology);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getKundaliCharts = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getKundaliChartDetail(body)
      .then((resp) => {
        setCharts(resp.data.svgdatadetail);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getVimshottariDasha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getVimshottaridashaDetail(body)
      .then((resp) => {
        setVishdasha(resp.data.dasha);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getMahaDasha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getMahaDashaDetail(body)
      .then((resp) => {
        setMahadasha(resp.data.mahadasha);
      })
      .catch((err) => {
        console.log("error");
      });
    setdata("");
    setPratyData("");
    setDasha("Mahadasha");
    setAntarDasha("");
  };

  const getAntarDash = (planet) => {
    const check = Moment(personalDetails.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(personalDetails.birth_time, "HH:mm").hours();
    const min = Moment(personalDetails.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: personalDetails.birth_latitude.toString(),
      lon: personalDetails.birth_longitude.toString(),
      tzone: personalDetails.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getAntarDashaDetail(planet, body)
      .then((resp) => {
        setMahadasha(resp.data.antardasha);
      })
      .catch((err) => {
        console.log("error");
      });
    setDasha("antardasha");
    setdata(planet.substring(0, 2));
    setAntarDasha(planet);
    setPratyData("");
  };

  const getPratyanterDasha = (planet) => {
    const check = Moment(personalDetails.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(personalDetails.birth_time, "HH:mm").hours();
    const min = Moment(personalDetails.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: personalDetails.birth_latitude.toString(),
      lon: personalDetails.birth_longitude.toString(),
      tzone: personalDetails.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getPratyantarDashaDetail(planet, body)
      .then((resp) => {
        let newName = "veve" + planet?.toLowerCase()?.substring(0, 2);
        if (newName) {
          setMahadasha(resp.data.pratyantardashavenus[newName]);
        }
      })
      .catch((err) => {
        console.log("error");
      });
    setDasha("");
    setPratyData(planet.substring(0, 2));
  };

  const getKaalsarpDosha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getKaalsarpDoshaDetail(body)
      .then((resp) => {
        setKaalsarpDosha(resp.data.kalsarpa);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getManglikDosha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getManglikDoshaDetail(body)
      .then((resp) => {
        setManglikDosha(resp.data.manglik);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getPitraDosha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getPitraDoshaDetail(body)
      .then((resp) => {
        setPitraDosha(resp.data.pitraDosha);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getSadhesatiStatus = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getSadhesatiStatusDetail(body)
      .then((resp) => {
        setSadhesatiStatus(resp?.data?.sadhesatistatus);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const getSadhesatiDasha = (data) => {
    const check = Moment(data.birth_date, "YYYY/MM/DD");
    const month = check.format("M");
    const day = check.format("D");
    const year = check.format("YYYY");
    const hour = Moment(data.birth_time, "HH:mm").hours();
    const min = Moment(data.birth_time, "HH:mm").minutes();

    const body = {
      day: day.toString(),
      month: month.toString(),
      year: year.toString(),
      hour: hour.toString(),
      min: min.toString(),
      lat: data.birth_latitude.toString(),
      lon: data.birth_longitude.toString(),
      tzone: data.birth_time_zone.toString(),
    };
    clientServiceAPI
      .getSadhesatiDashaDetail(body)
      .then((resp) => {
        setSadhesatiDasha(resp.data.sadhesati);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <div
            className="bg-white shadow-bold "
            style={{ position: "relative", zIndex: 1 }}
          >
            <Container>
              <div className="d-flex align-items-center tab--style">
                <h5 className="mb-0 mr-3 py-4 font-weight-normal">
                  Free Kundali
                </h5>
                <Nav variant="pills" className="flex-row flex-1 border rounded">
                  <Nav.Item>
                    <Nav.Link eventKey="first">Lagna Kundali</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="second">Kundali Charts</Nav.Link>
                  </Nav.Item>

                  <Nav.Item>
                    <Nav.Link eventKey="third">Current Dasha</Nav.Link>
                  </Nav.Item>

                  <Nav.Item>
                    <Nav.Link eventKey="fourth">Numrology</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="fifth">Kundali Dosha</Nav.Link>
                  </Nav.Item>
                </Nav>
              </div>
            </Container>
          </div>
          <Container>
            <Tab.Content>
              <Tab.Pane eventKey="first">
                <div className="text-center my-5">
                  <label className="d-block fw-500 mb-3">Lagna Kundali</label>
                  <img src="assets/img/kundali.svg" className="kundli-img-s " />
                </div>
                <div className="asd-container">
                  <div className="rounded border  my-5">
                    <label className="fw-500 p-3 mb-0">
                      Your Astro Details
                    </label>

                    <label className="py-2 px-3 mb-0 bg-color2 text-white d-block">
                      Basic Details
                    </label>

                    <Table className="table--style1" striped responsive>
                      {personalDetails && (
                        <tbody>
                          <tr>
                            <td className="pl-sm-5">Name</td>
                            <td>
                              {personalDetails.first_name}{" "}
                              {personalDetails.last_name}
                            </td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Gender</td>
                            <td>{personalDetails.gender}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Date of Birth</td>
                            <td>
                              {Moment(personalDetails.birth_date).format(
                                "DD/MM/YYYY"
                              )}
                            </td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Time of Birth</td>
                            <td>{personalDetails.birth_time}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Place</td>
                            <td>{personalDetails.birth_location}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Latitude</td>
                            <td>{personalDetails.birth_latitude}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Longitude</td>
                            <td>{personalDetails.birth_longitude}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Timezone</td>
                            <td>{personalDetails.birth_time_zone}</td>
                          </tr>
                        </tbody>
                      )}
                    </Table>
                    <label className="py-2 px-3 mb-0 bg-color2 text-white d-block">
                      Panchang Details
                    </label>

                    <Table className="table--style1" striped responsive>
                      {panchangDetails && (
                        <tbody>
                          <tr>
                            <td className="pl-sm-5">Karan</td>
                            <td>{panchangDetails.karanName}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Nakshtra</td>
                            <td>{panchangDetails.nakshtraName}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Ritu</td>
                            <td>{panchangDetails.ritu}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Sunrise</td>
                            <td>{panchangDetails.sunrise}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Sunset</td>
                            <td>{panchangDetails.sunset}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Tithi</td>
                            <td>{panchangDetails.tithiName}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Yog</td>
                            <td>{panchangDetails.yoyName}</td>
                          </tr>
                        </tbody>
                      )}
                    </Table>
                    <label className="py-2 px-3 mb-0 bg-color2 text-white d-block">
                      Astro Details
                    </label>

                    <Table className="table--style1" striped responsive>
                      {astroDetails && (
                        <tbody>
                          <tr>
                            <td className="pl-sm-5">Charan</td>
                            <td>{astroDetails.Charan}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Gan</td>
                            <td>{astroDetails.Gan}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Karan</td>
                            <td>{astroDetails.Karan}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Nadi</td>
                            <td>{astroDetails.Nadi}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Naksahtra</td>
                            <td>{astroDetails.Naksahtra}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">NaksahtraLord</td>
                            <td>{astroDetails.NaksahtraLord}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Charan</td>
                            <td>{astroDetails.Charan}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">SignLord</td>
                            <td>{astroDetails.SignLord}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Tithi</td>
                            <td>{astroDetails.Tithi}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Varna</td>
                            <td>{astroDetails.Varna}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Vashya</td>
                            <td>{astroDetails.Vashya}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Yog</td>
                            <td>{astroDetails.Yog}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Yoni</td>
                            <td>{astroDetails.Yoni}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Ascendant</td>
                            <td>{astroDetails.ascendant}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Ascendant lord</td>
                            <td>{astroDetails.ascendant_lord}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Name alphabet</td>
                            <td>{astroDetails.name_alphabet}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Paya</td>
                            <td>{astroDetails.paya}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Sign</td>
                            <td>{astroDetails.sign}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Tatva</td>
                            <td>{astroDetails.tatva}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Yunja</td>
                            <td>{astroDetails.yunja}</td>
                          </tr>
                        </tbody>
                      )}
                    </Table>
                  </div>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="second">
                <div className="px-sm-5 my-5">
                  <Row>
                    {chartDetail &&
                      chartDetail.map((e, i) => {
                        return (
                          <Col md={6} key={i}>
                            <div className="text-center mb-4">
                              <label className="d-block fw-500 mb-2">
                                {e.chartName}
                              </label>
                              <img src={e.url} className="kundli-img-s " />
                            </div>
                          </Col>
                        );
                      })}
                  </Row>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="third">
                <div className="my-3 my-xs-2">
                  <Row>
                    <Col lg={{ span: 4, offset: 1 }} md={6}>
                      <div className="br-8 border overflow-hidden">
                        <label className="p-2 mb-0 bg-color2 text-white fs-12 font-weight-bold d-flex justify-content-between">
                          <span>Current Vimshottari Dasha</span>
                          <span>
                            {moment(new Date()).format("ddd DD-MM-YYYY")}
                          </span>
                        </label>

                        <div className="p-3 cvd-box">
                          <table className="mt-2">
                            <tr>
                              <td>Major Dasha</td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.major?.start.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.major?.start.split(" ")[2]}
                              </td>
                            </tr>
                            <tr>
                              <td className="fw-500">
                                {vishdasha?.major?.planet}
                              </td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.major?.end.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.major?.end.split(" ")[2]}
                              </td>
                            </tr>
                          </table>

                          <table>
                            <tr>
                              <td>Antar Dasha</td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.minor?.start.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.minor?.start.split(" ")[2]}
                              </td>
                            </tr>
                            <tr>
                              <td className="fw-500">
                                {vishdasha?.minor?.planet}
                              </td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.minor?.end.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.minor?.end.split(" ")[2]}
                              </td>
                            </tr>
                          </table>
                          <table>
                            <tr>
                              <td>Prtyantar Dasha</td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.sub_minor?.start.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {" "}
                                {vishdasha?.sub_minor?.start.split(" ")[2]}
                              </td>
                            </tr>
                            <tr>
                              <td className="fw-500">
                                {vishdasha?.sub_minor?.planet}
                              </td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.sub_minor?.end.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.sub_minor?.end.split(" ")[2]}
                              </td>
                            </tr>
                          </table>

                          <table>
                            <tr>
                              <td>Sookshm Dasha</td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.sub_sub_minor?.start.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.sub_sub_minor?.start.split(" ")[2]}
                              </td>
                            </tr>
                            <tr>
                              <td className="fw-500">
                                {vishdasha?.sub_sub_minor?.planet}
                              </td>
                              <td className="fw-500 w80 text-center">
                                {vishdasha?.sub_sub_minor?.end.split(" ")[0]}
                              </td>
                              <td className="fw-500 w50 text-center">
                                {vishdasha?.sub_sub_minor?.end.split(" ")[2]}
                              </td>
                            </tr>
                          </table>

                          <table>
                            <tr>
                              <td>Pran Dasha</td>
                              <td className="fw-500 w80 text-center">
                                {
                                  vishdasha?.sub_sub_sub_minor?.start.split(
                                    " "
                                  )[0]
                                }
                              </td>
                              <td className="fw-500 w50 text-center">
                                {
                                  vishdasha?.sub_sub_sub_minor?.start.split(
                                    " "
                                  )[2]
                                }
                              </td>
                            </tr>
                            <tr>
                              <td className="fw-500">
                                {vishdasha?.sub_sub_sub_minor?.planet}
                              </td>
                              <td className="fw-500 w80 text-center">
                                {
                                  vishdasha?.sub_sub_sub_minor?.end.split(
                                    " "
                                  )[0]
                                }
                              </td>
                              <td className="fw-500 w50 text-center">
                                {
                                  vishdasha?.sub_sub_sub_minor?.end.split(
                                    " "
                                  )[2]
                                }
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </Col>

                    <Col lg={{ span: 5, offset: 1 }} md={6}>
                      <div className="border overflow-hidden br-8 my-xs-2">
                        <div className="d-flex justify-content-between align-items-center p-3">
                          <ul className="atd-process fs-12">
                            <li
                              className={dasha === "Mahadasha" ? "active" : ""}
                              onClick={(e) => getMahaDasha(personalDetails)}
                            >
                              Maha
                            </li>
                            <li
                              className={dasha === "antardasha" ? "active" : ""}
                              onClick={
                                antarDasha && ((e) => getAntarDash(antarDasha))
                              }
                            >
                              <i className="fas fa-angle-right"></i> Antar
                            </li>
                            <li className={!dasha ? "active" : ""}>
                              <i className="fas fa-angle-right"></i> Pratyantar
                            </li>
                          </ul>

                          <Button
                            variant="primary"
                            className="btn-xs fw-500"
                            onClick={
                              dasha === "antardasha"
                                ? (e) => getMahaDasha(personalDetails)
                                : !dasha
                                ? (e) => getAntarDash(antarDasha)
                                : ""
                            }
                          >
                            Level Up
                          </Button>
                        </div>
                        <label className="p-2 mb-0 bg-color2 text-white fs-12 font-weight-bold w-100 d-flex justify-content-between align-items-center">
                          <span>
                            {dasha === "Mahadasha"
                              ? "Maha"
                              : dasha === "antardasha"
                              ? "Antar"
                              : "Pratyantar"}
                            Dasha
                          </span>
                          <span>
                            {moment(new Date()).format("ddd DD-MM-YYYY")}
                          </span>
                        </label>
                        <div className="ant-box p-3">
                          {mahaDasha &&
                            mahaDasha?.map((item) => (
                              <div
                                key={item.planet_id}
                                className="ant-box-wr mb-2 "
                                style={{ cursor: "pointer" }}
                                onClick={
                                  dasha &&
                                  ((e) =>
                                    dasha === "antardasha"
                                      ? getPratyanterDasha(item.planet)
                                      : getAntarDash(item.planet))
                                }
                              >
                                <table>
                                  <tr>
                                    <td rowSpan="2">
                                      {dasha && (
                                        <i class="fas fa-angle-right set-a"></i>
                                      )}
                                      <span className="fw-500">
                                        {data} {pratyData && "-" + pratyData}
                                        {data
                                          ? "-" + item?.planet?.substring(0, 2)
                                          : pratyData
                                          ? "-" + item?.planet?.substring(0, 2)
                                          : item.planet}
                                      </span>
                                    </td>
                                    <td className="w50">Start</td>
                                    <td className="fw-500 w80 text-center">
                                      {item.start.split(" ")[0]}
                                    </td>
                                    <td className="fw-500 w50 text-center">
                                      {item.start.split(" ")[2]}
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="w50">End</td>
                                    <td className="fw-500 w80 text-center">
                                      {item.end.split(" ")[0]}
                                    </td>
                                    <td className="fw-500 w50 text-center">
                                      {item.end.split(" ")[2]}
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            ))}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="fourth">
                <div className="asd-container">
                  {numrology && (
                    <div className="rounded border  my-5 overflow-hidden">
                      <label className="fw-500 p-3 mb-0 bg-color2 text-white d-block">
                        Numerology For You
                      </label>
                      <br />
                      <div className="matc-r d-flex justify-content-between align-items-center my-3 mx-2">
                        <span>Destiny Number</span>
                        <div className="sm-capsul text-color2">
                          {numrology.destiny_number}
                        </div>
                      </div>
                      <div className="matc-r d-flex justify-content-between align-items-center my-3 mx-2">
                        <span>Radical Number</span>
                        <div className="sm-capsul text-color2">
                          {numrology.radical_num}
                        </div>
                      </div>
                      <div className="matc-r d-flex justify-content-between align-items-center my-3 mx-2">
                        <span>Name Number</span>
                        <div className="sm-capsul text-color2">
                          {numrology.name_number}
                        </div>
                      </div>
                      <div className="matc-r d-flex justify-content-between align-items-center my-3 mx-2">
                        <span>Evil Number</span>
                        <div className="sm-capsul text-color2">
                          {numrology.evil_num}
                        </div>
                      </div>
                      <br />
                    </div>
                  )}

                  <div className="rounded border  my-5">
                    <label className="fw-500 p-3 mb-0">
                      Your Astro Details
                    </label>

                    <label className="py-2 px-3 mb-0 bg-color2 text-white d-block">
                      Basic Details
                    </label>

                    <Table className="table--style1" striped responsive>
                      {numrology && (
                        <tbody>
                          <tr>
                            <td className="pl-sm-5">Name</td>
                            <td>{numrology.name}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Date of Birth</td>
                            <td>{numrology.date}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Destiny Number</td>
                            <td>{numrology.destiny_number}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Radical Number</td>
                            <td>{numrology.radical_number}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Name Number</td>
                            <td>{numrology.name_number}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Evil Number</td>
                            <td>{numrology.evil_num}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Color</td>
                            <td>{numrology.fav_color}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Day</td>
                            <td>{numrology.fav_day}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite God</td>
                            <td>{numrology.fav_god}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Mantra</td>
                            <td>{numrology.fav_mantra}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Metal</td>
                            <td>{numrology.fav_metal}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Stone</td>
                            <td>{numrology.fav_stone}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Favourite Substone</td>
                            <td>{numrology.fav_substone}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Friendly Number</td>
                            <td>{numrology.friendly_num}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Neutral Number</td>
                            <td>{numrology.neutral_num}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Radical Number</td>
                            <td>{numrology.radical_num}</td>
                          </tr>
                          <tr>
                            <td className="pl-sm-5">Radical Ruler</td>
                            <td>{numrology.radical_ruler}</td>
                          </tr>
                        </tbody>
                      )}
                    </Table>
                  </div>
                </div>
              </Tab.Pane>
              <Tab.Pane eventKey="fifth">
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                  <div
                    className="bg-white shadow-bold rounded"
                    style={{ position: "relative", marginTop: -9 }}
                  >
                    <Container
                      className="align-items-center"
                      style={{ maxWidth: "846px" }}
                    >
                      <div
                        className="d-flex align-items-center tab--style"
                        style={{ padding: "20px", top: "20px" }}
                      >
                        <Nav
                          variant="pills"
                          className="flex-row flex-1 rounded"
                          style={{ margin: "165px,180px", padding: "3px" }}
                        >
                          <Nav.Item style={{ margin: "12px" }}>
                            <Nav.Link eventKey="first" className="border">
                              Kaalsarp
                            </Nav.Link>
                          </Nav.Item>
                          <Nav.Item style={{ margin: "12px" }}>
                            <Nav.Link eventKey="second" className="border">
                              Manglik
                            </Nav.Link>
                          </Nav.Item>
                          <Nav.Item style={{ margin: "12px" }}>
                            <Nav.Link eventKey="third" className="border">
                              Pitra
                            </Nav.Link>
                          </Nav.Item>
                          <Nav.Item style={{ margin: "12px" }}>
                            <Nav.Link eventKey="fourth" className="border">
                              Sadhesati
                            </Nav.Link>
                          </Nav.Item>
                        </Nav>
                      </div>
                    </Container>
                  </div>
                  <Container>
                    <Tab.Content>
                      <Tab.Pane eventKey="first">
                        <div
                          style={{
                            background: "lightgray",
                            padding: "136px 70px 50px",
                            margin: "-100px -237px 0px -235px",
                          }}
                        >
                          <h2
                            className="align-items-center"
                            style={{ textAlign: "center" }}
                          >
                            <b style={{ color: "#42a4fa" }}>
                              Horoscope is free from Kaalsarp Dosh
                            </b>
                          </h2>
                        </div>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Report for Kaalsarpa Dosha
                              </label>
                              <div className="p-3">
                                <span className="mb-4">
                                  <p
                                    dangerouslySetInnerHTML={{
                                      __html: kaalsarpDosha?.report?.report,
                                    }}
                                  ></p>
                                </span>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <div
                          style={{
                            background: "lightgray",
                            padding: "136px 70px 19px",
                            margin: "-100px -237px 0px -235px",
                          }}
                        >
                          <div
                            style={{
                              marginLeft: "153px",
                              marginRight: "138px",
                            }}
                          >
                            <h2>
                              <b style={{ color: "#42a4fa" }}>Manglik Alert!</b>
                            </h2>
                            <span>
                              <p>{ManglikDosha?.report}</p>
                            </span>
                          </div>
                        </div>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Reason behind Manglik Dosha
                              </label>
                              <div className="p-3">
                                {ManglikDosha &&
                                  ManglikDosha?.reason?.based_on_aspect.map(
                                    (item, index) => (
                                      <ul className="mb-4" key={index}>
                                        <li>{item}</li>
                                      </ul>
                                    )
                                  )}
                                {ManglikDosha &&
                                  ManglikDosha?.reason?.based_on_house.map(
                                    (item, index) => (
                                      <ul className="mb-4" key={index}>
                                        <li>{item}</li>
                                      </ul>
                                    )
                                  )}
                              </div>
                            </div>
                          </Col>
                        </Row>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Remedies for Manglik Dosha
                              </label>
                              <div className="p-3">
                                <p>{ManglikDosha?.remedies?.ChantingMantra}</p>
                              </div>
                              <div className="p-3">
                                <p>
                                  {ManglikDosha?.remedies?.FastingonTuesdays}
                                </p>
                              </div>
                              <div className="p-3">
                                <p>{ManglikDosha?.remedies?.KumbhVivah}</p>
                              </div>
                              <div className="p-3">
                                <p>{ManglikDosha?.remedies?.Visit}</p>
                              </div>
                              <div className="p-3">
                                <p>{ManglikDosha?.remedies?.Visiting}</p>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="third">
                        <div
                          style={{
                            background: "lightgray",
                            padding: "136px 70px 19px",
                            margin: "-100px -237px 0px -235px",
                          }}
                        >
                          <div
                            style={{
                              marginLeft: "153px",
                              marginRight: "138px",
                            }}
                          >
                            <h2>
                              <b style={{ color: "#42a4fa" }}>Pitra Dosha!</b>
                            </h2>
                            <span>
                              <p>{pitraDosha?.conclusion}</p>
                            </span>
                          </div>
                        </div>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Rules matched
                              </label>
                              <div className="p-3">
                                {pitraDosha &&
                                  pitraDosha?.rulesMatched.map((item) => (
                                    <ul className="mb-4">
                                      <li>{item}</li>
                                    </ul>
                                  ))}
                              </div>
                            </div>
                          </Col>
                        </Row>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Effect of Pitra Dosha
                              </label>
                              <div className="p-3">
                                {pitraDosha &&
                                  pitraDosha?.effects.map((item) => (
                                    <ul className="mb-4">
                                      <li>{item}</li>
                                    </ul>
                                  ))}
                              </div>
                            </div>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="p-3 fw-500 bg-color2 text-white d-block">
                                Remedies of Pitra Dosha
                              </label>
                              <div className="p-3">
                                {pitraDosha &&
                                  pitraDosha?.remedies.map((item) => (
                                    <ul className="mb-4">
                                      <li>{item}</li>
                                    </ul>
                                  ))}
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Tab.Pane>
                      <Tab.Pane eventKey="fourth">
                        <div
                          style={{
                            background: "lightgray",
                            padding: "136px 70px 19px",
                            margin: "-100px -237px 0px -235px",
                          }}
                        >
                          <div
                            style={{
                              marginLeft: "153px",
                              marginRight: "138px",
                            }}
                          >
                            <h2>
                              <b style={{ color: "#42a4fa" }}>Sadhesati!</b>
                            </h2>
                            <span>
                              <b>Status</b>
                            </span>
                            <br />
                            <span>{sadhesatiStatus?.Status}</span>
                            <br />
                            <span>
                              <b>Moon Sign : {sadhesatiStatus?.moonSign}</b>
                            </span>
                            <br />
                            <span>
                              <b>Saturn Sign : {sadhesatiStatus?.saturnSign}</b>
                            </span>
                            <br />
                            <span>
                              <b>Date: {sadhesatiStatus?.currentDate}</b>
                            </span>
                          </div>
                        </div>
                        <Row className="align-items-center">
                          <Col md={{ span: 10, offset: 1 }}>
                            <div className="rounded overflow-hidden border my-4">
                              <label className="fw-500 p-3 mb-0">
                                SadheSati Life Details
                              </label>

                              <Table
                                className="table--style1"
                                striped
                                responsive
                              >
                                <thead>
                                  <tr className="bg-color2 text-white">
                                    <th>Date</th>
                                    <th>Moon</th>
                                    <th>Saturn</th>
                                    <th>Retro</th>
                                    <th>Type</th>
                                  </tr>
                                </thead>
                                {sadhesatiDasha &&
                                  sadhesatiDasha?.map((item, index) => (
                                    <tbody key={index}>
                                      <tr>
                                        <td>{item.date}</td>
                                        <td>{item.moon_sign}</td>
                                        <td>{item.saturn_sign}</td>
                                        <td>
                                          {item.is_saturn_retrograde === false
                                            ? "NO"
                                            : "YES"}
                                        </td>
                                        <td>{item.type}</td>
                                      </tr>
                                    </tbody>
                                  ))}
                              </Table>
                            </div>
                          </Col>
                        </Row>
                      </Tab.Pane>
                    </Tab.Content>
                  </Container>
                </Tab.Container>
              </Tab.Pane>
            </Tab.Content>
          </Container>
        </Tab.Container>
      </section>
      <Footer />
    </div>
  );
}

export default FreeKundali;
