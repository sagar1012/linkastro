import React, { Component, useState, useEffect } from "react";
import { Button, Row, Col, Container, Table, Modal } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import { ToastContainer, toast } from "react-toastify";
import clientServiceAPI from "../../../services/clientServiceAPI";

const AvaliableBalance = (props) => {
  const amount = props?.location?.state?.amount;
  const [balance, setBalance] = useState();
  const [cValue, setCouponValue] = useState(false);

  const percent = (18 / 100) * amount;
  const totalamount = parseInt(amount) + parseInt(percent);

  useEffect(() => {
    getWalletDetail();
  }, []);

  const getWalletDetail = () => {
    clientServiceAPI.getWalletBalance().then((resp) => {
      if (resp.status) {
        setBalance(resp?.data?.data);
      } else {
        console.log("error");
      }
    });
  };

  const couponValue = (e) => {
    setCouponValue(e.target.value);
  };

  const getCheckReferal = () => {
    const body = {
      referalCode: cValue,
    };
    if (cValue == "") {
      toast.error("Please Enter Referal Code", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      clientServiceAPI.checkReferalCode(body).then((resp) => {
        if (resp.status) {
          toast.success(resp.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error(resp.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
    }
  };

  return (
    <div className="layout-body">
      <Header />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-2">
          <Container>
            <div className="d-flex align-items-center p-1 bg-gray py-3 br-30 px-4 flex-wrap">
              <p className="mb-0 font-weight-bold mr-5">
                <img
                  src="assets/img/icon_wallet.svg"
                  className="mr-3"
                  width="30"
                />
                Available Balance
              </p>{" "}
              <span className="font-weight-bold">₹ {balance?.balance}</span>
            </div>
          </Container>
        </div>

        <Container>
          <div className="shadow-bolder p-2 br-8 balance-box my-4">
            <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
              <span>Total Amount</span>
              <span>₹ {amount}.00</span>
            </p>
            <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
              <span>GST @18%</span>
              <span>₹ {percent}.00</span>
            </p>
            <hr />
            <p className="d-flex justify-content-between my-3 fw-500 fs-12 px-3">
              <span>Total Payable Amount</span>
              <span>₹ {totalamount}.00</span>
            </p>
            <div className="mb-4 mt-100">
              <div className="coupon-in d-flex">
                <input
                  type="text"
                  placeholder="Enter a Valid Coupon code"
                  onChange={couponValue}
                />
                <Button variant="primary" size="sm" onClick={getCheckReferal}>
                  Apply
                </Button>
              </div>
              <small className="fw-500 sm-hint">
                Amount will be credited in your wallet
              </small>
            </div>

            <Button
              variant="primary"
              block
              className="d-flex px-4 py-2 fs-12 justify-content-between"
            >
              <span className="fw-500">Paytm</span>
              <span>₹ {totalamount}.00</span>
            </Button>

            <Button
              block
              variant="outline-primary"
              className="fs-12 px-4 py-2 my-2 fw-500"
            >
              Pay with other payment options
            </Button>
          </div>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default AvaliableBalance;
