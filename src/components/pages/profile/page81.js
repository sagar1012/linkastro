import React, { useEffect, useState } from "react";
import { Button, Row, Col, Container, Form, Table } from "react-bootstrap";
import Header from "../../templates/Header";
import Footer from "../../templates/Footer";
import moment from "moment";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import clientServiceAPI from "../../../services/clientServiceAPI";

const WalletLog = () => {
  const history = useHistory();
  const [balance, setBalance] = useState();
  const [transHistory, setWalletHistory] = useState();
  const [discount, setDiscount] = useState();
  const [rechargeAmt, setRechargeAmt] = useState(false);

  useEffect(() => {
    getWalletDetail();
    getWalletHistory();
    getCashbackWallet();
  }, []);

  const getWalletDetail = () => {
    clientServiceAPI.getWalletBalance().then((resp) => {
      if (resp.status) {
        setBalance(resp?.data?.data);
      } else {
        console.log("error");
      }
    });
  };

  const getWalletHistory = () => {
    clientServiceAPI.getWalletTransactionHistory().then((resp) => {
      if (resp.status) {
        setWalletHistory(resp?.data);
      } else {
        console.log("error");
      }
    });
  };

  const getCashbackWallet = () => {
    clientServiceAPI.getrechargeCashback().then((resp) => {
      if (resp.status) {
        setDiscount(resp.data.checkResult);
      } else {
        console.log("error");
      }
    });
  };

  const handleRecharge = (e) => {
    e.preventDefault();
    history.push({
      pathname: "/avaliableBalance",
      state: { amount: e.target.value },
    });
  };
  const handleInput = (e) => {
    e.preventDefault();
    setRechargeAmt(e.target.value);
  };

  const handleclick = () => {
    if (rechargeAmt == "") {
      toast.error("Please Enter Reacharge Amount", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      history.push({
        pathname: "/avaliableBalance",
        state: { amount: rechargeAmt },
      });
    }
  };

  return (
    <div className="layout-body">
      <Header />
      <section className="layout-content">
        <div className="bg-white shadow-bold p-2">
          <Container>
            <div className="d-flex align-items-center p-1 bg-gray py-3 br-30 px-4 flex-wrap">
              <p className="mb-0 font-weight-bold mr-5">
                <img
                  src="assets/img/icon_wallet.svg"
                  className="mr-3"
                  width="30"
                />
                Available Balance
              </p>{" "}
              <span className="font-weight-bold">₹ {balance?.balance}</span>
            </div>
          </Container>
        </div>

        <Container>
          <Row>
            <Col lg={{ span: 4, offset: 1 }} md={6}>
              <div className="shadow-bolder p-4 my-4 br-8">
                {discount &&
                  discount?.map((item, index) => {
                    return (
                      <Button
                        key={index}
                        block
                        size="lg"
                        className="mb-2 position-relative"
                        variant="outline-primary"
                        value={item?.amount}
                        onClick={handleRecharge}
                      >
                        <span className="b-in-label p-abs">
                          {item?.discount}% OFF
                        </span>{" "}
                        ₹ {item?.amount}
                      </Button>
                    );
                  })}
                <div className="text-center fs-12 my-4"> Or</div>

                <Form.Group controlId="formBasicEmail">
                  <Form.Control
                    className="fs-12 py-4"
                    type="number"
                    placeholder="Enter your recharge amount"
                    onChange={handleInput}
                  />
                </Form.Group>
                <Button variant="primary" block onClick={handleclick}>
                  Continue
                </Button>
                <small className="text-color2 d-block text-center mt-3 fw-500">
                  GST @18% Lorem ipsum dolor sit amet
                </small>
              </div>
            </Col>
            <Col md={5}>
              <div className="shadow-bold p-2 br-8 my-4 pb-sm-5">
                <p className="font-weight-bold text-center border-bottom pb-4">
                  Wallet Log
                </p>

                <Table responsive className="fs-12 mb-4">
                  <tbody>
                    {transHistory &&
                      transHistory?.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td className="border-top-0">
                              {item?.transactionName}
                            </td>
                            <td className="border-top-0">
                              <span className="outline-badge receipt text-uppercase">
                                Receipt
                              </span>
                            </td>
                            <td className="border-top-0">
                              {moment(item?.transactionDate).format(
                                "DD/MM/YYYY"
                              )}
                            </td>
                            <td className="border-top-0">
                              <span className="font-weight-bold">
                                {" "}
                                + ₹ {item?.amount}.0
                              </span>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </div>
  );
};

export default WalletLog;
