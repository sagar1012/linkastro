import React, { Component } from "react";
import { Link } from "react-router-dom";
import { login } from "../../actions/authAction";
import { reactLocalStorage } from "reactjs-localstorage";
import { toast } from "react-toastify";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Header from "../templates/Header";
import Footer from "../templates/Footer";
import { ToastContainer } from "react-toastify";

class Login extends Component {
  componentDidMount() {
    this.handleAuth();
    document.title = "Login";
  }
  state = {
    errors: {},
    fields: {},
  };

  onChange = (e) => {
    const fields = this.state.fields;
    if (e.target.name === "email") {
      if (e.target.value.match("^[0-9a-zA-Z.@]*$") != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields["email"];
      }
    } else {
      fields[e.target.name] = e.target.value;
    }
    this.setState(fields);
  };

  handleAuth() {
    var auth = reactLocalStorage.getObject("Auth");
    //var token = reactLocalStorage.getObject('token');
    var data = Object.keys(auth).length;
    if (data === 0) {
      this.props.history.push("/login");
    } else {
      // this.props.history.push('/AstroAppointment')
      this.props.history.push("/");
    }
  }

  handleValidation() {
    const fields = this.state.fields;
    const errors = {};
    let formIsValid = true;
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Please enter Username.";
    }
    /*if(typeof fields["email"] !== "undefined"){
           let lastAtPos = fields["email"].lastIndexOf('@');
           let lastDotPos = fields["email"].lastIndexOf('.');

           if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              formIsValid = false;
              errors["email"] = "Please enter username.";
            }
        } */

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Please enter Password.";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  onSubmit = async (e) => {
    e.preventDefault();
    if (this.handleValidation()) {
      const userData = {
        userName: this.state.fields["email"],
        password: this.state.fields["password"],
        language: "en",
        accessPlatform: "web",
        version: "1.24",
        deviceModel: "web",
      };
      await this.props.login(userData, this.props.history);
    } else {
      toast.error("Please Fill All Required Fields", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  };
  //start
  render() {
    return (
      <div className="main-wrapper">
        <Header />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <ToastContainer />
        <section className="middle-section">
          <div className="sign-page">
            <div className="container">
              <div className="sign-section section-bk">
                <div className="sign-main">
                  <div className="sign-close-btn">
                    <Link to=""></Link>
                  </div>
                  <div className="page-head">Sign In</div>
                  <form onSubmit={this.onSubmit}>
                    <div className="sign-form">
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Email/Mobile No.*"
                          onChange={this.onChange}
                          id="email"
                          name="email"
                          value={this.state.fields["email"]}
                          onInput={(e) => {
                            e.target.value = e.target.value.replace(
                              /[^0-9a-zA-Z.@]/,
                              ""
                            );
                          }}
                        />
                        <span style={{ color: "red" }}>
                          {this.state.errors["email"]}
                        </span>
                      </div>
                      <div className="form-group">
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Password *"
                          id="password"
                          onChange={this.onChange}
                          name="password"
                          value={this.state.fields["password"]}
                        />
                        <span style={{ color: "red" }}>
                          {this.state.errors["password"]}
                        </span>
                      </div>
                      <div className="form-group">
                        <div className="custom-forgot text-right">
                          <Link
                            to={`/forgotpassword/${this.state.fields["email"]}`}
                          >
                            Forgot Password?
                          </Link>
                        </div>
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn login_btn">
                          Sign In
                        </button>
                      </div>
                    </div>
                  </form>
                  <div className="or-div">or</div>
                  <div className="sign-with">
                    <div className="sign-btn">
                      <ul>
                        <li>
                          <Link to="/userSignup">
                            <img
                              src={require("../../assets/images/icon_Sign_up@2x.png")}
                              alt=""
                            />
                            Sign Up
                          </Link>
                        </li>
                        <li>
                          <Link to="/">
                            <img
                              src={require("../../assets/images/icon_Google_login@2x.png")}
                              alt=""
                            />{" "}
                            Google
                          </Link>
                        </li>
                        <li>
                          <Link to="/">
                            <img
                              src={require("../../assets/images/icon_Facebook_login@2x.png")}
                              alt=""
                            />{" "}
                            Facebook
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="logging-accepting">
                      By logging in you are accepting the{" "}
                      <Link to="/">T&C</Link> and{" "}
                      <Link to="/">Privacy Policy</Link>
                    </div>
                  </div>
                </div>
                <div className="sign-footer">
                  <div className="astrologers-head">For Astrologers</div>
                  <div className="astrologers-btn">
                    <Link to="/AstrologerReg">Astrologer Registration</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  errors: state.errors.error,
});
export default connect(mapStateToProps, { login })(Login);
