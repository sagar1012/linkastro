import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import { Otp } from 'react-otp-timer';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import Moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import Header from '../templates/Header';
import Footer from '../templates/Footer';
import { signup, sendOtp, checkUserName, veryfyOtp } from '../../actions/authAction';

class AstrologerReg extends Component {
  componentDidMount() {
    document.title = 'Astrologer Registration';
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password1');
    togglePassword.addEventListener('click', function (e) {
      const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
      password.setAttribute('type', type);
    });

    const togglePasswordCon = document.querySelector('#togglePassword_con');
    const con_password = document.querySelector('#con_password');
    togglePasswordCon.addEventListener('click', function (e) {
      const type = con_password.getAttribute('type') === 'password' ? 'text' : 'password';
      con_password.setAttribute('type', type);
    });

  }
  state = {
    readOnly: true,
    fields: {},
    errors: {},
    form_data: {},
    showModal: false,
    //startDate: new Date(),
    verifyToken: false,
    mobile_no_err: '',
    email_err: '',
    checked: true,
    count: 120,
    m: 0,
    s: 0,
    message: ''
  }
  // input handle  

  handleOpenModal = () => {
    this.setState({ showModal: true });
  };

  handleCloseModal = () => {
    this.setState({ showModal: false });
  };

  handleCheck = () => {
    this.setState({ checked: !this.state.checked });
    //console.log(this.state.checked)
  };

  sendOTP = async () => {

    if (this.handleValidation()) {

      const userData = {
        mobileNo: this.state.fields['mobile_no']
      }
      var token = await this.props.sendOtp(userData);

      if (token.data.result === 0) {
        this.setState({ 'mobile_no_err': 'Invalid Mobile Number' })
        toast.error(token.data.message, { position: toast.POSITION.TOP_RIGHT });
      } else {
        this.handleOpenModal()
        this.setState({ 'mobile_no_err': '' })
      }

    } else {

      // toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
    }
  };

  veryfyOTP = async () => {
    const userData = {
      mobileNo: this.state.fields['mobile_no'],
      OTP: document.getElementById('otp').value
    }
    if (document.getElementById('otp').value === '') {
      document.getElementById('otpError').innerHTML = 'Please enter OTP';
    } else if (document.getElementById('otp').value.length < 4 || document.getElementById('otp').value.length > 4) {
      document.getElementById('otpError').innerHTML = 'Please enter only 4 digit OTP';
    } else {
      var token = await this.props.veryfyOtp(userData);
      if (token.data.result === 1) {
        document.getElementById('verifyToken').value = token.data.verifyToken;
        var l = document.getElementById('continue_btn');
        l.click();
        document.getElementById('otpError').innerHTML = '';
      } else {
        document.getElementById('otpError').innerHTML = 'OTP does not match, retry';
      }
    }
  }

  async resendEvent() {

    const userData = {
      mobileNo: this.state.fields['mobile_no']
    }
    await this.props.sendOtp(userData);

  }

  onChange = async (e) => {
    this.handleValidation();
    const fields = this.state.fields;
    //fields[e.target.name] = e.target.value;
    fields['verifyToken'] = false;
    //this.setState(fields);

    if (e.target.name === 'mobile_no') {

      if (e.target.value.match('^[0-9]*$') != null) {
        fields[e.target.name] = e.target.value;
        if (e.target.name === 'mobile_no') {
          const userData = {
            userName: e.target.value
          }
          var token = await this.props.checkUserName(userData);
          if (token.data.result === 1 && token.data.data.exist === true) {
            this.setState({ 'mobile_no_err': 'Mobile Number already registered' })
            const errors = {};
            errors['mobile_no'] = '';
            //$('#btn_sub').prop('disabled', true);
          } else {
            //$('#btn_sub').prop('disabled', false);
            this.setState({ 'mobile_no_err': '' })
          }
        }
      } else {
        fields[e.target.name] = this.state.fields['mobile_no'];
      }
    } else if (e.target.name === 'alternate_no') {
      if (e.target.value.match('^[0-9]*$') != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields['alternate_no'];
      }
    } else if (e.target.name === 'pincode') {
      if (e.target.value.match('^[0-9]*$') != null) {
        fields[e.target.name] = e.target.value;
      } else {
        fields[e.target.name] = this.state.fields['pincode'];
      }
    } else if (e.target.name === 'email') {
      fields[e.target.name] = e.target.value;
      const userData = {
        userName: e.target.value
      }
      token = await this.props.checkUserName(userData);
      if (token.data.result === 1 && token.data.data.exist === true) {
        this.setState({ 'email_err': 'Email Address already registered' })
        const errors = {};
        errors['email'] = '';
        //$('#btn_sub').prop('disabled', true);
      } else {
        // $('#btn_sub').prop('disabled', false);
        this.setState({ 'email_err': '' })
      }

    } else {
      fields[e.target.name] = e.target.value;
    }
    this.setState(fields);
  }

  handleChange = date => {
    let formIsValid = true;
    const errors = {};
    const fields = this.state.fields;
    fields['dob'] = date;
    this.setState({
      startDate: date
    });
    //errors["dob"] = "";
    //$('#dob_error').text('');
    document.getElementById('dob_error').innerHTML = '';
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    var da = today.getDate() - birthDate.getDate();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (m < 0) {
      m += 12;
    }
    if (da < 0) {
      da += 30;
    }

    if (age < 18 || age > 100) {
      //errors["dob"] = "Your age should be 18 years old";
      //$('#dob_error').text('Your age should be 18 years old');
      document.getElementById('dob_error').innerHTML = 'Your age should be 18 years old';
      formIsValid = false;
    } else {
      //$('#dob_error').text('');
      document.getElementById('dob_error').innerHTML = '';
      //errors["dob"] = "";
    }
  };

  handleValidation() {
    const fields = this.state.fields;
    const errors = {};
    let formIsValid = true;
    var first_name = document.getElementById('first_name').value;
    if (first_name === '') {
      formIsValid = false;
      errors['first_name'] = 'Please enter Full Name';
    } else {
      errors['first_name'] = '';
    }

    var mobile_no = document.getElementById('mobile_no').value;
    if (mobile_no === '') {
      formIsValid = false;
      errors['mobile_no'] = 'Please enter Mobile Number';
    } else {
      errors['mobile_no'] = '';
      if (/[^0-9]/.test(mobile_no)) {
        formIsValid = false;
        errors['mobile_no'] = 'Please enter valid  Mobile Number';
      } else if (mobile_no.length < 10) {
        formIsValid = false;
        errors['mobile_no'] = 'Please enter valid  Mobile Number';
      } else if (mobile_no.length > 10) {
        formIsValid = false;
        errors['mobile_no'] = 'Please enter valid  Mobile Number';
      } else {
        errors['mobile_no'] = '';
      }

    }

    var alternate_no = document.getElementById('alternate_no').value;
    if (alternate_no.length > 0) {
      if (/[^0-9]/.test(alternate_no)) {
        formIsValid = false;
        errors['alternate_no'] = 'Please enter valid Alternate Mobile Number';
      } else if (alternate_no.length < 10) {
        formIsValid = false;
        errors['alternate_no'] = 'Please enter valid Alternate Mobile Number';
      } else if (alternate_no.length > 10) {
        formIsValid = false;
        errors['alternate_no'] = 'Please enter valid Alternate Mobile Number';
      } else {
        errors['alternate_no'] = '';
      }
    }
    var email = document.getElementById('email').value;
    if (email === 0) {
      formIsValid = false;
      errors['email'] = 'Please enter Email Address';
    } else {
      errors['email'] = '';
    }
    if (typeof fields['email'] !== 'undefined') {
      const lastAtPos = fields['email'].lastIndexOf('@');
      const lastDotPos = fields['email'].lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields['email'].indexOf('@@') === -1 && lastDotPos > 2 && (fields['email'].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors['email'] = 'Please enter valid Email Address';
      }
    }

    if (this.state.startDate === '' || this.state.startDate === undefined || this.state.startDate == null) {
      formIsValid = false;
      //errors["dob"] = "Please enter Date Of Birth";
      //$('#dob_error').text('Please enter Date Of Birth');
      document.getElementById('dob_error').innerHTML = 'Please enter Date Of Birth';
    } else {
      //errors["dob"] = "";
      //$('#dob_error').text('');
      document.getElementById('dob_error').innerHTML = '';
      var today = new Date();
      var birthDate = new Date(this.state.startDate);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      var da = today.getDate() - birthDate.getDate();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      if (m < 0) {
        m += 12;
      }
      if (da < 0) {
        da += 30;
      }

      if (age < 18 || age > 100) {
        //errors["dob"] = "Your age should be 18 years old";
        //$('#dob_error').text('Your age should be 18 years old');
        document.getElementById('dob_error').innerHTML = 'Your age should be 18 years old';
        formIsValid = false;
      } else {
        errors['dob'] = '';

      }
    }
    // var gender = $("input[name='gender']:checked").val();
    /* var gender = document.getElementById("gender").checked;
 
     if (gender === undefined || gender === '') {
       formIsValid = false;
       errors['gender'] = 'Please select Gender';
     } else {
       errors['gender'] = '';
     }*/

    if (!fields['gender']) {
      formIsValid = false;
      errors['gender'] = 'Please select Gender';
    } else {
      errors['gender'] = '';
    }

    var address = document.getElementById('address').value;
    if (address === '') {
      formIsValid = false;
      errors['address'] = 'Please enter Address';
    } else {
      errors['address'] = '';
    }

    var city = document.getElementById('city').value;
    if (city === '') {
      formIsValid = false;
      errors['city'] = 'Please enter City';
    } else {
      errors['state'] = '';
    }
    var state = document.getElementById('state').value;
    if (state === '') {
      formIsValid = false;
      errors['state'] = 'Please enter State';
    } else {
      errors['state'] = '';
    }

    var country = document.getElementById('country').value;
    if (country === '') {
      formIsValid = false;
      errors['country'] = 'Please enter Country';
    } else {
      errors['country'] = '';
    }

    var pincode = document.getElementById('pincode').value;
    if (pincode === '') {
      formIsValid = false;
      errors['pincode'] = 'Please enter Pincode';
    } else {
      if (/[^0-9]/.test(pincode)) {
        formIsValid = false;
        errors['pincode'] = 'Invalid Pincode';
      } else if (pincode.length < 6) {
        formIsValid = false;
        errors['pincode'] = 'Invalid Pincode';
      }
      else {
        errors['pincode'] = '';
      }
    }

    var password1 = document.getElementById('password1').value;
    if (password1 === '') {
      formIsValid = false;
      errors['password1'] = 'Please enter Password';
    } else {
      errors['password1'] = '';
      if (password1.length < 6) {
        formIsValid = false;
        errors['password1'] = 'Password must contain a minimum of 6 digit';
      }
    }

    var con_password = document.getElementById('con_password').value;
    if (con_password === '') {
      formIsValid = false;
      errors['con_password'] = 'Please re-enter Password';
    } else {
      if (password1.length < 6) {
        formIsValid = false;
        errors['con_password'] = 'Please re-enter valid Password';
      }

      else if (password1 !== con_password) {
        formIsValid = false;
        errors['con_password'] = 'Please enter valid Password';
      } else {
        errors['con_password'] = '';
      }
    }

    if (this.state.checked === false) {
      formIsValid = false;
      errors['checked'] = 'Please Agree Terms & Conditions and Privacy Policy by ticking';
    } else {
      errors['checked'] = '';
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  onSubmit = async e => {
    e.preventDefault();
    if (this.handleValidation()) {

      const DOB = Moment(this.state.startDate).format('YYYY-MM-DD')
      var name = this.state.fields['first_name'];
      var ret = name.split(' ');
      var str1 = ret[0];
      var str2 = ret[1];

      const userData = {
        firstName: str1,
        lastName: str2,
        mobileNo: this.state.fields['mobile_no'],
        alternateMobileNo: this.state.fields['alternate_no'],
        email: this.state.fields['email'],
        address: this.state.fields['address'],
        location: this.state.fields['city'],
        city: this.state.fields['city'],
        state: this.state.fields['state'],
        country: this.state.fields['country'],
        pinCode: this.state.fields['pincode'],
        password: this.state.fields['con_password'],
        dob: DOB,
        gender: this.state.fields['gender'],
        verifyToken: document.getElementById('verifyToken').value,
        language: 'en',
        lattitude: '19.07283',
        longitude: '72.88261',
        birthtime: '5.30',
        timezone: '5.50',
        //
      }
      // console.log(userData);
      await this.props.signup(userData, this.props.history);

    } else {
      // toast.error("Please Fill All Required Fields", { position: toast.POSITION.TOP_RIGHT })
    }
  }

  render() {

    const customStyles = {
      content: {
        color: 'darkred',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
      },
      overlay: {
        backgroundColor: '#444',
        opacity: '0.9'
      }
    };
    const style = {
      otpTimer: {
        margin: '10px',
        color: 'blue',
      },
      resendBtn: {
        backgroundColor: '#0980FE',
        color: 'white'
      }
    }

    return (
      <div className="main-wrapper">
        <Header />
        <section className="middle-section">
          <div className="sign-page astrologerReg">
            <div className="container">
              <div className="sign-section section-bk">
                <div className="sign-main">
                  <div class="sign-close-btn"><Link to=""></Link></div>
                  <div className="page-head">Astrologer Registration</div>
                  
                  <form onSubmit={this.onSubmit} >
                    <div className="sign-form">
                      <div className="form-group">
                        <input type="text" className="form-control" onChange={this.onChange} id="first_name" placeholder=" " name="first_name" value={this.state.fields['first_name']} />
                        <label for="first_name" className="form-label">Full Name<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['first_name']}</span>
                      </div>

                      {/* <div className="form-group">
                                                              <label>Last Name<span>*</span></label>
                                                              <input type="text" className="form-control" onChange={this.onChange} id="last_name" name="last_name" value={this.state.fields['last_name']}/>
                                                              <span style={{color: "red"}}>{this.state.errors["last_name"]}</span>
                                                          </div>*/}

                      <div className="form-group">

                        <input type="text" className="form-control Numeric1" onChange={this.onChange} id="mobile_no" placeholder=" " name="mobile_no" value={this.state.fields['mobile_no']} maxlength="10" onInput={(e) => { e.target.value = e.target.value.replace(/[^0-9]/g, '') }} />
                        <label for="mobile_no" className="form-label">Mobile Number<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['mobile_no']}</span>
                        <span style={{ color: 'red' }}>{this.state.mobile_no_err}</span>

                      </div>
                      <div className="form-group">
                        <input type="text" className="form-control Numeric1" onChange={this.onChange} id="alternate_no" placeholder=" " name="alternate_no" value={this.state.fields['alternate_no']} maxlength="10" onInput={(e) => { e.target.value = e.target.value.replace(/[^0-9]/g, '') }} />
                        <label for="alternate_no" className="form-label">Alternate Mobile Number</label>
                        <span style={{ color: 'red' }}>{this.state.errors['alternate_no']}</span>
                      </div>
                      <div className="form-group">
                        <input autocomplete="off" type="text" className="form-control" onChange={this.onChange} id="email" placeholder=" " name="email" value={this.state.fields['email']} />
                        <label for="email" className="form-label">Email<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['email']}</span>
                        <span style={{ color: 'red' }}>{this.state.email_err}</span>
                      </div>
                      <div class="form-group focused">
                        <label class="form-label">Date of Birth<span>*</span></label>
                        <div class="select-option-form">
                          <DatePicker showYearDropdown dateFormat="dd/MM/yyyy" id="dob" name="dob" className="form-control" selected={this.state.startDate} onChange={this.handleChange} value={this.state.fields['dob']} showDisabledMonthNavigation />
                          <span style={{ color: 'red' }} id="dob_error"></span>

                          {/* <ul>
                                                                          <li class="active">Day</li>
                                                                          <li>Month</li>
                                                                          <li>Year</li>
                                                                      </ul> */}
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Gender<span>*</span></label>
                        <div class="select-option-form">
                          <ul>
                            <li>
                              <input type="radio" name="gender" value="male" onChange={this.onChange} />
                              <label>Male</label>
                            </li>
                            <li> <input type="radio" name="gender" value="female" onChange={this.onChange} />
                              <label>Female</label>
                            </li>
                            <li> <input type="radio" name="gender" value="other" onChange={this.onChange} />
                              <label>Other</label>
                            </li>
                          </ul>
                          <span style={{ color: 'red' }}>{this.state.errors['gender']}</span>
                        </div>
                      </div>

                      <div className="form-group">

                        {/*<input type="text" className="form-control" onChange={this.onChange} id="address" placeholder=" " name="address" value={this.state.fields['address']}/>*/}

                        <textarea className="form-control" onChange={this.onChange} id="address" placeholder="" name="address" value={this.state.fields['address']}></textarea>

                        <label for="address" className="form-label">Address<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['address']}</span>
                      </div>
                      <div className="form-group">

                        <input type="text" className="form-control" onChange={this.onChange} id="city" placeholder=" " name="city" value={this.state.fields['city']} />
                        <label for="city" className="form-label">City<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['city']}</span>
                      </div>
                      <div className="form-group">

                        <input type="text" className="form-control" onChange={this.onChange} id="state" placeholder=" " name="state" value={this.state.fields['state']} />
                        <label for="state" className="form-label">State<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['state']}</span>

                      </div>
                      <div className="form-group">

                        <input type="text" className="form-control" onChange={this.onChange} id="country" placeholder=" " name="country" value={this.state.fields['country']} />
                        <label for="country" className="form-label">Country<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['country']}</span>
                      </div>
                      <div className="form-group">

                        <input type="text" maxlength="6" className="form-control Numeric1" onChange={this.onChange} id="pincode" placeholder=" " name="pincode" value={this.state.fields['pincode']} onInput={(e) => { e.target.value = e.target.value.replace(/[^0-9]/g, '') }} />
                        <label for="pincode" className="form-label">Pincode<span>*</span></label>
                        <span style={{ color: 'red' }}>{this.state.errors['pincode']}</span>
                      </div>
                      <div className="form-group">

                        <div className="password-type-div">
                          <input type="password" className="form-control" onChange={this.onChange} id="password1" placeholder=" " name="password1" value={this.state.fields['password1']} />
                          <label for="password1" className="form-label">Create Password<span>*</span></label>
                          <span className="change-type" id="togglePassword"></span>
                        </div>
                        <span style={{ color: 'red' }}>{this.state.errors['password1']}</span>
                      </div>
                      <div className="form-group">

                        <div className="password-type-div">
                          <input type="password" className="form-control" onChange={this.onChange} id="con_password" placeholder=" " name="con_password" value={this.state.fields['con_password']} />
                          <label for="con_password" className="form-label">Re Enter Password<span>*</span></label>
                          <span className="change-type" id="togglePassword_con"></span>
                        </div>
                        <span style={{ color: 'red' }}>{this.state.errors['con_password']}</span>
                      </div>
                      <div className="form-group">
                        <label ><input type="checkbox" name="terms" id="isCheck" onChange={this.handleCheck} defaultChecked={this.state.checked} /> I AGREE and ACCEPT <a target="_blank" href="https://linkastro.com:3000/astrologerAgreement">Terms & Conditions</a> and <a href="https://linkastro.com:3000/privacypolicy" target="_blank">Privacy Policy</a></label>

                        <span style={{ color: 'red' }}>{this.state.errors['checked']}</span>

                        <input type="hidden" id="verifyToken" />

                      </div>
                      <div className="form-group" style={{ 'display': 'none' }}>
                        <button type="submit" className="btn continue_btn" id="continue_btn">Continue</button>
                      </div>

                      <div className="form-group">
                        <button type="button" id="btn_sub" className="btn continue_btn" onClick={this.sendOTP}>Continue</button>
                      </div>
                    </div>
                  </form>
                  {/* <div className="sign-with">
                                                        <div className="logging-accepting">By logging in you are accepting the <a href="/">T&amp;C</a> and <a href="/">Privacy Policy</a></div>
                                                    </div>*/}
                </div>
              </div>
            </div>
          </div>
        </section>
        <ReactModal isOpen={this.state.showModal} contentLabel="onRequestClose Example" onAfterOpen={this.afterOpenModal}
          onRequestClose={this.handleCloseModal} shouldCloseOnOverlayClick={false} style={customStyles} >
          <h3>Enter OTP</h3>
          <div className="btn-modal-container">

            <div className="form-group">

              <input type="text" className="form-control" id="otp" name="otp" maxLength="4" onInput={(e) => { e.target.value = e.target.value.replace(/[^0-9]/g, '') }} />
              <span id="otpError">Please check the OTP on your Mobile Number</span>
              <br />

              <br />
              <Otp style={style} minutes={2.0} resendEvent={this.resendEvent.bind(this)} />

            </div>
            {/*<button type="button" className="btn btn-default" onClick={this.resendOTP}>Resend OTP</button>*/}
            <button type="button" className="btn btn-default" onClick={this.veryfyOTP}>Continue</button>
            <button type="button" className="btn btn-default" onClick={this.handleCloseModal}>Close</button>

          </div>
        </ReactModal>
        {/*  /> */}
        <Footer />
      </div>
    )
  }
}
const mapStateToProps = state => ({
  user: state.user,
  errors: state.errors.error
});
export default connect(mapStateToProps, { signup, sendOtp, checkUserName, veryfyOtp })(AstrologerReg);