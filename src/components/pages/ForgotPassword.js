import React, { useEffect } from "react";
import Header from "../templates/Header";
import Footer from "../templates/Footer";
import { useState } from "react";
import ReactModal from "react-modal";
import httpService from "../../services/httpService";
import { ToastContainer, toast } from "react-toastify";
import { Otp } from "react-otp-timer";
import clientServiceAPI from "../../services/clientServiceAPI";

const ForgotPassword = (props) => {
  const [OTPvalue, setOTP] = useState(false);
  const [verifyToken, setVerifyToken] = useState(false);
  const [show, setShow] = useState(false);
  const [password, setPassword] = useState();
  const [rePassword, setRePassword] = useState();
  const [mobilenumber, setMobile] = useState(false);
  const [showMobile, setShowMobile] = useState(false);

  const handleClose = () => {
    setShow(false);
    setShowMobile(false);
    props.history.push("/login");
  };

  const passwordHandler = (e) => {
    setPassword(e.target.value);
  };
  const rePasswordHandler = (e) => {
    setRePassword(e.target.value);
  };

  const sendOTP = (mobile) => {
    const payload = {
      mobileNo: mobile,
    };
    if (mobilenumber == "") {
      toast.error("Please Enter Mobile Number", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      setShowMobile(false);
      clientServiceAPI
        .ForgotPasswordSendOTP(payload)
        .then((resp) => {
          if (resp.data.result === 0) {
            toast.error(
              resp.data.message,
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              },
              props.history.push("/login")
            );
          } else {
            setShow(true);
            toast.success(resp.data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        })
        .catch((err) => {
          console.log("error");
        });
    }
  };
  useEffect(() => {
    setShowMobile(true);
  }, []);

  const submitOTP = () => {
    const body = {
      mobileNo: mobilenumber,
      OTP: OTPvalue,
    };
    if (OTPvalue == "") {
      toast.error("Please Enter OTP", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      clientServiceAPI
        .ForgotPasswordVerifyOTP(body)
        .then((resp) => {
          if (resp.data.result === 0) {
            toast.error(resp.data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            setShow(false);
            setVerifyToken(resp.data.verifyToken);
            toast.success(resp.data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        })
        .catch((err) => {
          console.log("error");
        });
    }
  };

  const SetPassword = (event) => {
    event.preventDefault();

    if (password !== rePassword) {
      toast.error("Passwords Doesn't Match", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      const payload = {
        mobileNo: mobilenumber,
        password: password,
        verifyToken: verifyToken.toString(),
      };
      clientServiceAPI
        .ForgotPasswordSetPassword(payload)
        .then((resp) => {
          if (resp.data.result === 0) {
            toast.error(resp.data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            toast.success(
              resp.data.message,
              {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              },
              props.history.push("/forgotPasswordMgs")
            );
          }
        })
        .catch((err) => {
          console.log("error");
        });
    }
  };

  const resendEvent = async () => {
    await sendOTP(mobilenumber);
  };

  const customStyles = {
    content: {
      color: "darkred",
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
    overlay: {
      backgroundColor: "#444",
      opacity: "0.9",
    },
  };
  const style = {
    otpTimer: {
      margin: "10px",
      color: "blue",
    },
    resendBtn: {
      backgroundColor: "#0980FE",
      color: "white",
    },
  };

  return (
    <div className="main-wrapper">
      <Header />
      <section className="middle-section">
        <div className="sign-page forgot-password">
          <div className="container">
            <div className="forgot-password-page section-bk">
              <div className="forgot-password-sec">
                <div className="sign-close-btn">
                  <a href="/"></a>
                </div>
                <div className="page-head">Enter New Password</div>
                <form onSubmit={SetPassword}>
                  <div className="sign-form">
                    <div className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Password"
                        id="password"
                        name="password"
                        onChange={passwordHandler}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Re - Password"
                        id="Repassword"
                        name="Repassword"
                        onChange={rePasswordHandler}
                      />
                    </div>
                    <div className="form-group">
                      <button type="Submit" className="btn">
                        Reset
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <ReactModal
        isOpen={show}
        contentLabel="onRequestClose Example"
        onRequestClose={handleClose}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
      >
        <h3>Enter OTP</h3>
        <div className="btn-modal-container">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="otp"
              name="otp"
              maxLength="4"
              onInput={(e) => setOTP(e.target.value.replace(/[^0-9]/g, ""))}
            />
            <span id="otpError">
              Please check the OTP on your Mobile Number
            </span>
            <br />

            <br />
            <Otp style={style} minutes={2.0} resendEvent={resendEvent} />
          </div>
          {/*<button type="button" className="btn btn-default" onClick={this.resendOTP}>Resend OTP</button>*/}
          <button type="button" className="btn btn-default" onClick={submitOTP}>
            Continue
          </button>
          <button
            type="button"
            className="btn btn-default"
            onClick={handleClose}
          >
            Close
          </button>
        </div>
      </ReactModal>
      <ReactModal
        isOpen={showMobile}
        contentLabel="onRequestClose Example"
        onRequestClose={handleClose}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
      >
        <h3>Mobile Number</h3>
        <div className="btn-modal-container">
          <div className="form-group">
            <label></label>
            <input
              type="text"
              className="form-control"
              id="Mob"
              name="mobile"
              maxLength="10"
              onInput={(e) => setMobile(e.target.value.replace(/[^0-9]/g, ""))}
            />
          </div>
          <button
            type="button"
            className="btn btn-default"
            onClick={(e) => sendOTP(mobilenumber)}
          >
            Continue
          </button>
          <button
            type="button"
            className="btn btn-default"
            onClick={handleClose}
          >
            Close
          </button>
        </div>
      </ReactModal>
      <Footer />
    </div>
  );
};

export default ForgotPassword;
