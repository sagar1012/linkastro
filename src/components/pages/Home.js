import React, { useState, Component, useEffect } from "react";

import OwlCarousel from "react-owl-carousel";
import { useHistory } from "react-router-dom";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import history from "../../history";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../templates/Header";
import Footer from "../templates/Footer";
import httpService from "../../services/httpService";
import Astro from "../commonComponents/astrologerList";
import clientServiceAPI from "../../services/clientServiceAPI";

function Home() {
  const [AstrologersList, setAstrologersList] = useState();
  const [AstrologersSkills, setAstrologersSkills] = useState();
  const [userName, setUser] = useState("");
  const [verifyOTP, setVerifyOTP] = useState(false);
  const [OTPvalue, setOTP] = useState(false);

  const Clickme = () => {
    const url = "/Login";
    history.push(url);
  };

  const service = (page) => {
    const url = "/" + page;
    history.push(url);
  };

  useEffect(() => {
    getAstrologersList();
  }, []);

  const getAstrologersList = () => {
    const body = {
      pageNo: 1,
    };
    clientServiceAPI
      .getAstroListv1(body)
      .then((resp) => {
        setAstrologersList(resp?.data?.checkResult);
        setAstrologersSkills(resp?.data?.professionalskills);
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const checkUser = () => {
    console.log("exist");
    const body = {
      userName: userName,
    };
    clientServiceAPI
      .UserNameExistOrNot(body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        if (resp.data.result === 1 && resp.data.data.exist) {
          toast.info(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }

        if (!resp.data.data.exist) {
          const payload = {
            mobileNo: userName,
          };
          clientServiceAPI
            .SendOTPtoMobile(payload)
            .then((resp) => {
              if (resp.data.result === 0) {
                toast.error(resp.data.message, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              } else {
                setVerifyOTP(true);
                toast.success(resp.data.message, {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
            })
            .catch((err) => {
              console.log("error");
            });
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const submitOTP = () => {
    const body = {
      mobileNo: userName,
      OTP: OTPvalue,
    };
    clientServiceAPI
      .VerifyOTP(body)
      .then((resp) => {
        if (resp.data.result === 0) {
          toast.error(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          setVerifyOTP(false);
          setUser("");
          toast.success(resp.data.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };

  const state = {
    responsive: {
      0: {
        items: 1,
      },
      450: {
        items: 2,
      },
      768: {
        items: 2,
      },
      1000: {
        items: 4,
      },
    },
  };

  return (
    <div className="main-wrapper home">
      <Header />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <ToastContainer />
      <section className="middle-section">
        <div className="container">
          <div className="banner">
            <img src={require("../../assets/images/component.png")} alt="" />
          </div>
          <div className="signup-sec">
            <div className="row">
              <div className="col-md-3">
                <div className="si-head">
                  Signup for free to consult with expert Astrologers
                </div>
              </div>

              <div className="col-md-9">
                <div className="sin-ta">
                  <table>
                    {!verifyOTP && (
                      <tr>
                        <td>
                          <input
                            type="text"
                            onChange={(e) => setUser(e.target.value)}
                            className="form-control fm-inp"
                            placeholder="Enter mobile number"
                            name=""
                          />
                        </td>
                        <td>
                          <button
                            type="submit"
                            onClick={() => history.push("userSignup")}
                            className="si-button"
                          >
                            Sign Up
                          </button>
                        </td>
                      </tr>
                    )}
                    {verifyOTP && (
                      <tr>
                        <td>
                          <input
                            type="text"
                            onChange={(e) => setOTP(e.target.value)}
                            className="form-control fm-inp"
                            placeholder="Enter OTP here"
                            name=""
                          />
                        </td>
                        <td>
                          <button
                            type="submit"
                            onClick={() => submitOTP()}
                            className="si-button"
                          >
                            Submit OTP
                          </button>
                        </td>
                      </tr>
                    )}
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div className="conecting">
            <ul>
              <li>
                <a href="#">
                  <span>
                    {" "}
                    <img src={require("../../assets/images/4.png")} alt="" />
                  </span>
                  <span>
                    Chat <br />
                    Astrologer
                  </span>
                </a>
              </li>

              <li>
                <a onClick={() => history.push("page25")}>
                  <span>
                    {" "}
                    <img src={require("../../assets/images/3.png")} alt="" />
                  </span>
                  <span>
                    Call <br />
                    Astrologer
                  </span>
                </a>
              </li>

              <li>
                <a href="#">
                  <span>
                    {" "}
                    <img src={require("../../assets/images/2.png")} alt="" />
                  </span>
                  <span>
                    Live <br />
                    VIDEO
                  </span>
                </a>
              </li>

              <li>
                <a
                  className="cursor"
                  style={{ cursor: "pointer" }}
                  onClick={() => history.push("getYourReport")}
                >
                  <span>
                    {" "}
                    <img src={require("../../assets/images/1.png")} alt="" />
                  </span>
                  <span>
                    Get Your
                    <br /> Report
                  </span>
                </a>
              </li>
            </ul>
          </div>

          <div className="free-service">
            <div className="sec-head">Free Services</div>

            <div className="free-ser-slider">
              <OwlCarousel
                className="owl-carousel owl-theme free-ser"
                loop
                margin={10}
                nav
                dots
                items={4}
                autoplay
                autoHeight={true}
                responsive={state.responsive}
              >
                <div className="item" onClick={() => service("freekundali")}>
                  <div className="img-head">Free Kundali</div>
                  <div className="free-ser-img">
                    <a>
                      <img
                        src={require("../../assets/images/free_03.png")}
                        alt=""
                      />
                    </a>
                  </div>
                </div>
                <div className="item" onClick={() => service("matchMaking")}>
                  <div className="img-head">Matchmaking</div>
                  <div className="free-ser-img">
                    <a>
                      {" "}
                      <img
                        src={require("../../assets/images/free_05.png")}
                        alt=""
                      />
                    </a>
                  </div>
                </div>
                <div
                  className="item"
                  onClick={() => service("dailyHoroscpeToday")}
                >
                  <div className="img-head">Daily Horoscope</div>
                  <div className="free-ser-img">
                    <a>
                      {" "}
                      <img
                        src={require("../../assets/images/free_07.png")}
                        alt=""
                      />
                    </a>
                  </div>
                </div>
                <div
                  className="item"
                  onClick={() => service("dailyPunchangToday")}
                >
                  <div className="img-head">Daily Panchang</div>
                  <div className="free-ser-img">
                    <a>
                      {" "}
                      <img
                        src={require("../../assets/images/free_09.png")}
                        alt=""
                      />
                    </a>
                  </div>
                </div>
                <div className="item" onClick={() => service("matchMaking")}>
                  <div className="img-head">Matchmaking</div>
                  <div className="free-ser-img">
                    <a>
                      {" "}
                      <img
                        src={require("../../assets/images/free_05.png")}
                        alt=""
                      />
                    </a>
                  </div>
                </div>
              </OwlCarousel>
            </div>
          </div>
        </div>

        <div className="Astrologers-sec">
          <div className="container">
            <div className="astro-head" onClick={() => history.push("page25")}>
              <span>Astrologers</span>
              <span>Connect with our top Astrologers</span>
            </div>

            {AstrologersList && AstrologersSkills && (
              <OwlCarousel
                className="owl-carousel owl-theme astrologers"
                loop
                margin={10}
                nav
                dots
                items={4}
                autoplay
                autoHeight={true}
                autoWidth={true}
                responsive={state.responsive}
              >
                <Astro
                  data={{ data: AstrologersList, skill: AstrologersSkills }}
                />
              </OwlCarousel>
            )}
          </div>
        </div>

        <div className="yearly-sec">
          <div className="astro-head text-center">
            <span onClick={() => history.push("page24")}>YEARLY HOROSCOPE</span>
            <span>Your daily zodiac sign reading free</span>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Aries",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Aries.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Aries</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Taurus",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Taurus.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Taurus</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Gemini",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Gemini.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Gemini</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Cancer",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Cancer.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Cancer</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Leo",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Leo.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Leo</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Virgo",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Virgo.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Virgo</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Libra",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Libra.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Libra</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Scorpio",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Scorpio.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Scorpio</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Sagittarius",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Sagittarius.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Sagittarius</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Capricon",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Capricon.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Capricon</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Aquarius",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Aquarius.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Aquarius</div>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div
                  className="yearly-inner"
                  onClick={() =>
                    history.push({
                      pathname: "/yearlyHoroscopesSingle1",
                      search: "Pisces",
                    })
                  }
                >
                  <div className="yearly-img">
                    <img
                      src={require("../../assets/images/icon_Pisces.svg")}
                      alt=""
                    />
                  </div>
                  <div className="yearly-name">Pisces</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="Astrologers-sec">
          <div className="container">
            <div className="astro-head">
              <span>Astrology Anytime Anywhere</span>
              <span>Download our app to always be connected</span>
            </div>

            <div className="anytime">
              <ul>
                <li>
                  <div className="anytime-inner">
                    <div className="anytime-inner-img">
                      <img
                        src={require("../../assets/images/n1_03.png")}
                        alt=""
                      />
                    </div>
                    <div className="anytime-inner-desc">
                      Download the <br />
                      LinkAstro app
                    </div>
                  </div>
                </li>

                <li>
                  <div className="anytime-inner">
                    <div className="anytime-inner-img">
                      <img
                        src={require("../../assets/images/n1_05.png")}
                        alt=""
                      />
                    </div>
                    <div className="anytime-inner-desc">
                      Sign Up in <br />
                      LinkAstro
                    </div>
                  </div>
                </li>

                <li>
                  <div className="anytime-inner">
                    <div className="anytime-inner-img">
                      <img
                        src={require("../../assets/images/n1_07.png")}
                        alt=""
                      />
                    </div>
                    <div className="anytime-inner-desc">
                      Add money in <br />
                      LinkAstro Wallet
                    </div>
                  </div>
                </li>

                <li>
                  <div className="anytime-inner">
                    <div className="anytime-inner-img">
                      <img
                        src={require("../../assets/images/n1_09.png")}
                        alt=""
                      />
                    </div>
                    <div className="anytime-inner-desc">
                      Call & Chat with
                      <br />
                      Expert Astrologers
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="yearly-sec blog-sec">
          <div className="container">
            <div className="astro-head text-center">
              <span>BLOG</span>
              <span>Explore the new world with us</span>
            </div>

            <OwlCarousel
              className="owl-carousel owl-theme blog"
              loop
              margin={10}
              nav
              dots
              items={4}
              autoplay
              autoHeight={true}
              responsive={state.responsive}
            >
              <div className="item">
                <div className="free-ser-img">
                  <a href="#">
                    <img src={require("../../assets/images/b1.png")} alt="" />
                  </a>
                </div>
                <div className="img-head">Yoga and our life</div>
              </div>

              <div className="item">
                <div className="free-ser-img">
                  <a href="#">
                    <img src={require("../../assets/images/b3.png")} alt="" />
                  </a>
                </div>

                <div className="img-head">What does the star says</div>
              </div>

              <div className="item">
                <div className="free-ser-img">
                  <a href="#">
                    <img src={require("../../assets/images/b4.png")} alt="" />
                  </a>
                </div>
                <div className="img-head">What Is kundali</div>
              </div>

              <div className="item">
                <div className="free-ser-img">
                  <a href="#">
                    <img src={require("../../assets/images/b2.png")} alt="" />
                  </a>
                </div>

                <div className="img-head">What Is cosmos</div>
              </div>
            </OwlCarousel>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default Home;

// import React, { Component } from 'react';
// import OwlCarousel from 'react-owl-carousel';
// import { useHistory } from 'react-router-dom';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel/dist/assets/owl.theme.default.css';
// import history from '../../history';

// import Header from '../templates/Header';
// import Footer from '../templates/Footer';

// export default class Home extends Component {
//   constructor(){
//     super()
//     this.state = {
//         responsive: {
//             0: {
//             items: 1,
//           },
//             450: {
//             items: 2,
//           },
//             768: {
//             items: 2,
//           },
//             1000: {
//             items: 4,
//           },
//         } ,
//   }
//   }

// Clickme(){
//       const url='/Login'
//       history.push(url);
//   }
//   /*
//   const routeChange = () =>{
//     let path = `newPath`;
//     history.push(path);
//   }
//   */
// /*
// Clickme =()=>{
// alert('hello')

// }*//*
//    Clickme=()=> {
//     let path = `/Login`;
//     let history = useHistory();
//     history.push(path);
//   }*/

//   render() {
//     return (
//         <div className="main-wrapper home">
//             <Header />
//             <section className="middle-section">
//                 <div className="container">
//                     <div className="banner">
//                         <img src={ require('../../assets/images/component.png') } alt="" />
//                     </div>
//                     <div className="signup-sec">
//                         <div className="row">
//                             <div className="col-md-3">
//                                 <div className="si-head">
//                     Signup for free to consult with expert Astrologers
//                                 </div>
//                             </div>

//                             <div className="col-md-9">
//                                 <div className="sin-ta">
//                                     <table>
//                                         <tr>
//                                             <td><input type="text" className="form-control fm-inp" placeholder="Enter mobile number Or email ID" name="" /></td>
//                                             <td>
//                                               <button type="submit" onClick={()=>this.Clickme()}  className="si-button">Sign Up</button>
//                                           </td>
//                                         </tr>
//                                     </table>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>

//                     <div className="conecting">
//                         <ul>
//                             <li>
//                                 <a href="#">
//                                     <span> <img src={ require('../../assets/images/4.png') } alt="" /></span>
//                                     <span>Chat <br />Astrologer</span>
//                                 </a>
//                             </li>

//                             <li>
//                                 <a href="#">
//                                     <span> <img src={ require('../../assets/images/3.png') } alt="" /></span>
//                                     <span>Call <br />Astrologer</span>
//                                 </a>
//                             </li>

//                             <li>
//                                 <a href="#">
//                                     <span> <img src={ require('../../assets/images/2.png') } alt="" /></span>
//                                     <span>Live <br />VIDEO</span>
//                                 </a>
//                             </li>

//                             <li>
//                                 <a href="#">
//                                     <span> <img src={ require('../../assets/images/1.png') } alt="" /></span>
//                                     <span>Get Your<br /> Report</span>
//                                 </a>
//                             </li>
//                         </ul>
//                     </div>

//                     <div className="free-service">
//                         <div className="sec-head">
//                 Free Services
//                         </div>

//                         <div className="free-ser-slider">
//                             <OwlCarousel
//                   className="owl-carousel owl-theme free-ser"
//                   loop
//                   margin={ 10 }
//                   nav
//                   dots
//                   items={ 4 }
//                   autoplay
//                   autoHeight={ true }
//                   responsive={ this.state.responsive }
//                 >
//                                 <div className="item">
//                                     <div className="img-head">
//                       Free Kundali
//                                     </div>
//                                     <div className="free-ser-img">
//                                         <a href="#"> <img src={ require('../../assets/images/free_03.png') } alt="" /></a>
//                                     </div>
//                                 </div>
//                                 <div className="item">
//                                     <div className="img-head">
//                       Matchmaking
//                                     </div>
//                                     <div className="free-ser-img">
//                                         <a href="#"> <img src={ require('../../assets/images/free_05.png') } alt="" /></a>
//                                     </div>
//                                 </div>
//                                 <div className="item">
//                                     <div className="img-head">
//                       Daily Horoscope
//                                     </div>
//                                     <div className="free-ser-img">
//                                         <a href="#"> <img src={ require('../../assets/images/free_07.png') } alt="" /></a>
//                                     </div>
//                                 </div>
//                                 <div className="item">
//                                     <div className="img-head">
//                       Daily Panchang
//                                     </div>
//                                     <div className="free-ser-img">
//                                         <a href="#"> <img src={ require('../../assets/images/free_09.png') } alt="" /></a>
//                                     </div>
//                                 </div>
//                                 <div className="item">
//                                     <div className="img-head">
//                       Matchmaking
//                                     </div>
//                                     <div className="free-ser-img">
//                                         <a href="#"> <img src={ require('../../assets/images/free_05.png') } alt="" /></a>
//                                     </div>
//                                 </div>
//                             </OwlCarousel>
//                         </div>

//                     </div>
//                 </div>

//                 <div className="Astrologers-sec">
//                     <div className="container">
//                         <div className="astro-head">
//                             <span>Astrologers</span>
//                             <span>Connect with our top Astrologers</span>
//                         </div>

//                         <OwlCarousel
//                 className="owl-carousel owl-theme astrologers"
//                 loop
//                 margin={ 10 }
//                 nav
//                 dots
//                 items={ 4 }
//                 autoplay
//                 autoHeight={ true }
//                 responsive={ this.state.responsive }
//               >
//                             <div className="item">
//                                 <div className="astrologers-inner">
//                                     <div className="as1">
//                                         <div className="as1-left">
//                                             <div className="asro-man">
//                                               <div className="astro-man-img">
//                                                 <img src={ require('../../assets/images/priest_vishnuvardhana.png') } alt="" />
//                                             </div>

//                                               <div className="veryfid">
//                                                 <img src={ require('../../assets/images/Icon _verified_user.svg') } alt="" />
//                                             </div>

//                                               <div className="free-minutes">
//                                                 <span>  <img src={ require('../../assets/images/blue_banner.svg') } alt="" /></span>
//                                                 <span>2 min free</span>
//                                             </div>
//                                           </div>

//                                             <div className="as-rating">
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart-o"></i>
//                                           </div>

//                                             <div className="as-review">
//                           330 Review
//                                           </div>
//                                         </div>

//                                         <div className="as1-right">
//                                             <div className="as-name">
//                           Aacharya Shantanu ji
//                                           </div>

//                                             <div className="as-work">
//                           Vedic Astrologer
//                                           </div>

//                                             <div className="as-lag">
//                           Hindi, English, Gujrati
//                                           </div>
//                                         </div>
//                                     </div>

//                                     <div className="as2">
//                                         <div className="as2-left">
//                                             <div className="exp">18 yrs exp.</div>
//                                             <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
//                                         </div>

//                                         <div className="as2-right">
//                                             <ul>
//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/3.png') } alt="" /> Call</a>
//                                             </li>

//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/4.png') } alt="" /> Chat</a>
//                                             </li>
//                                           </ul>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="astrologers-inner">
//                                     <div className="as1">
//                                         <div className="as1-left">
//                                             <div className="asro-man">
//                                               <div className="astro-man-img">
//                                                 <img src={ require('../../assets/images/priest_vishnuvardhana.png') } alt="" />
//                                             </div>

//                                               <div className="veryfid">
//                                                 <img src={ require('../../assets/images/Icon _verified_user.svg') } alt="" />
//                                             </div>

//                                               <div className="free-minutes">
//                                                 <span>  <img src={ require('../../assets/images/blue_banner.svg') } alt="" /></span>
//                                                 <span>2 min free</span>
//                                             </div>
//                                           </div>

//                                             <div className="as-rating">
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart-o"></i>
//                                           </div>

//                                             <div className="as-review">
//                           330 Review
//                                           </div>
//                                         </div>

//                                         <div className="as1-right">
//                                             <div className="as-name">
//                           Aacharya Shantanu ji
//                                           </div>

//                                             <div className="as-work">
//                           Vedic Astrologer
//                                           </div>

//                                             <div className="as-lag">
//                           Hindi, English, Gujrati
//                                           </div>
//                                         </div>
//                                     </div>

//                                     <div className="as2">
//                                         <div className="as2-left">
//                                             <div className="exp">18 yrs exp.</div>
//                                             <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
//                                         </div>

//                                         <div className="as2-right">
//                                             <ul>
//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/3.png') } alt="" /> Call</a>
//                                             </li>

//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/4.png') } alt="" /> Chat</a>
//                                             </li>
//                                           </ul>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="astrologers-inner">
//                                     <div className="as1">
//                                         <div className="as1-left">
//                                             <div className="asro-man">
//                                               <div className="astro-man-img">
//                                                 <img src={ require('../../assets/images/priest_vishnuvardhana.png') } alt="" />
//                                             </div>

//                                               <div className="veryfid">
//                                                 <img src={ require('../../assets/images/Icon _verified_user.svg') } alt="" />
//                                             </div>

//                                               <div className="free-minutes">
//                                                 <span>  <img src={ require('../../assets/images/blue_banner.svg') } alt="" /></span>
//                                                 <span>2 min free</span>
//                                             </div>
//                                           </div>

//                                             <div className="as-rating">
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart-o"></i>
//                                           </div>

//                                             <div className="as-review">
//                           330 Review
//                                           </div>
//                                         </div>

//                                         <div className="as1-right">
//                                             <div className="as-name">
//                           Aacharya Shantanu ji
//                                           </div>

//                                             <div className="as-work">
//                           Vedic Astrologer
//                                           </div>

//                                             <div className="as-lag">
//                           Hindi, English, Gujrati
//                                           </div>
//                                         </div>
//                                     </div>

//                                     <div className="as2">
//                                         <div className="as2-left">
//                                             <div className="exp">18 yrs exp.</div>
//                                             <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
//                                         </div>

//                                         <div className="as2-right">
//                                             <ul>
//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/3.png') } alt="" /> Call</a>
//                                             </li>

//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/4.png') } alt="" /> Chat</a>
//                                             </li>
//                                           </ul>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="astrologers-inner">
//                                     <div className="as1">
//                                         <div className="as1-left">
//                                             <div className="asro-man">
//                                               <div className="astro-man-img">
//                                                 <img src={ require('../../assets/images/priest_vishnuvardhana.png') } alt="" />
//                                             </div>

//                                               <div className="veryfid">
//                                                 <img src={ require('../../assets/images/Icon _verified_user.svg') } alt="" />
//                                             </div>

//                                               <div className="free-minutes">
//                                                 <span>  <img src={ require('../../assets/images/blue_banner.svg') } alt="" /></span>
//                                                 <span>2 min free</span>
//                                             </div>
//                                           </div>

//                                             <div className="as-rating">
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart-o"></i>
//                                           </div>

//                                             <div className="as-review">
//                           330 Review
//                                           </div>
//                                         </div>

//                                         <div className="as1-right">
//                                             <div className="as-name">
//                           Aacharya Shantanu ji
//                                           </div>

//                                             <div className="as-work">
//                           Vedic Astrologer
//                                           </div>

//                                             <div className="as-lag">
//                           Hindi, English, Gujrati
//                                           </div>
//                                         </div>
//                                     </div>

//                                     <div className="as2">
//                                         <div className="as2-left">
//                                             <div className="exp">18 yrs exp.</div>
//                                             <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
//                                         </div>

//                                         <div className="as2-right">
//                                             <ul>
//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/3.png') } alt="" /> Call</a>
//                                             </li>

//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/4.png') } alt="" /> Chat</a>
//                                             </li>
//                                           </ul>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="astrologers-inner">
//                                     <div className="as1">
//                                         <div className="as1-left">
//                                             <div className="asro-man">
//                                               <div className="astro-man-img">
//                                                 <img src={ require('../../assets/images/priest_vishnuvardhana.png') } alt="" />
//                                             </div>

//                                               <div className="veryfid">
//                                                 <img src={ require('../../assets/images/Icon _verified_user.svg') } alt="" />
//                                             </div>

//                                               <div className="free-minutes">
//                                                 <span>  <img src={ require('../../assets/images/blue_banner.svg') } alt="" /></span>
//                                                 <span>2 min free</span>
//                                             </div>
//                                           </div>

//                                             <div className="as-rating">
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart"></i>
//                                               <i className="fa fa-heart-o"></i>
//                                           </div>

//                                             <div className="as-review">
//                           330 Review
//                                           </div>
//                                         </div>

//                                         <div className="as1-right">
//                                             <div className="as-name">
//                           Aacharya Shantanu ji
//                                           </div>

//                                             <div className="as-work">
//                           Vedic Astrologer
//                                           </div>

//                                             <div className="as-lag">
//                           Hindi, English, Gujrati
//                                           </div>
//                                         </div>
//                                     </div>

//                                     <div className="as2">
//                                         <div className="as2-left">
//                                             <div className="exp">18 yrs exp.</div>
//                                             <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
//                                         </div>

//                                         <div className="as2-right">
//                                             <ul>
//                                               <li>
//                                                 <a href="#"> <img src={ require('../../assets/images/3.png') } alt="" /> Call</a>
//                                             </li>

//                                               <li>
//                                                 <a href="#"><img src={ require('../../assets/images/4.png') } alt="" /> Chat</a>
//                                             </li>
//                                           </ul>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>

//                         </OwlCarousel>
//                     </div>
//                 </div>

//                 <div className="yearly-sec">
//                     <div className="astro-head text-center">
//                         <span>YEARLY HOROSCOPE</span>
//                         <span>Your daily zodiac sign reading free</span>
//                     </div>
//                     <div className="container">
//                         <div className="row">
//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Aries.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Aries</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Taurus.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Taurus</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Gemini.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Gemini</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Cancer.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Cancer</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Leo.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Leo</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Virgo.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Virgo</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Libra.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Libra</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Scorpio.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Scorpio</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Sagittarius.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Sagittarius</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Capricon.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Capricon</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Aquarius.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Aquarius</div>
//                                 </div>
//                             </div>

//                             <div className="col-lg-2 col-md-3">
//                                 <div className="yearly-inner">
//                                     <div className="yearly-img">
//                                         <img src={ require('../../assets/images/icon_Pisces.svg') } alt="" />
//                                     </div>
//                                     <div className="yearly-name">Pisces</div>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>

//                 <div className="Astrologers-sec">
//                     <div className="container">
//                         <div className="astro-head">
//                             <span>Astrology Anytime Anywhere</span>
//                             <span>Download our app to always be connected</span>
//                         </div>

//                         <div className="anytime">
//                             <ul>
//                                 <li>
//                                     <div className="anytime-inner">
//                                         <div className="anytime-inner-img">
//                                             <img src={ require('../../assets/images/n1_03.png') } alt="" />
//                                         </div>
//                                         <div className="anytime-inner-desc">
//                         Download the <br />LinkAstro app
//                                         </div>
//                                     </div>
//                                 </li>

//                                 <li>
//                                     <div className="anytime-inner">
//                                         <div className="anytime-inner-img">
//                                             <img src={ require('../../assets/images/n1_05.png') } alt="" />
//                                         </div>
//                                         <div className="anytime-inner-desc">
//                         Sign Up in <br />LinkAstro
//                                         </div>
//                                     </div>
//                                 </li>

//                                 <li>
//                                     <div className="anytime-inner">
//                                         <div className="anytime-inner-img">
//                                             <img src={ require('../../assets/images/n1_07.png') } alt="" />
//                                         </div>
//                                         <div className="anytime-inner-desc">
//                         Add money in <br />LinkAstro Wallet
//                                         </div>
//                                     </div>
//                                 </li>

//                                 <li>
//                                     <div className="anytime-inner">
//                                         <div className="anytime-inner-img">
//                                             <img src={ require('../../assets/images/n1_09.png') } alt="" />
//                                         </div>
//                                         <div className="anytime-inner-desc">
//                         Call & Chat with<br />Expert Astrologers
//                                         </div>
//                                     </div>
//                                 </li>
//                             </ul>
//                         </div>
//                     </div>
//                 </div>

//                 <div className="yearly-sec blog-sec">
//                     <div className="container">
//                         <div className="astro-head text-center">
//                             <span>BLOG</span>
//                             <span>Explore the new world with us</span>
//                         </div>

//                         <OwlCarousel
//                 className="owl-carousel owl-theme blog"
//                 loop
//                 margin={ 10 }
//                 nav
//                 dots
//                 items={ 4 }
//                 autoplay
//                 autoHeight={ true }
//                 responsive={ this.state.responsive }
//               >
//                             <div className="item">
//                                 <div className="free-ser-img">
//                                     <a href="#"><img src={ require('../../assets/images/b1.png') } alt="" /></a>
//                                 </div>
//                                 <div className="img-head">
//                     Yoga and our life
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="free-ser-img">
//                                     <a href="#"><img src={ require('../../assets/images/b3.png') } alt="" /></a>
//                                 </div>

//                                 <div className="img-head">
//                     What does the star says
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="free-ser-img">
//                                     <a href="#"><img src={ require('../../assets/images/b4.png') } alt="" /></a>
//                                 </div>
//                                 <div className="img-head">
//                     What Is kundali
//                                 </div>
//                             </div>

//                             <div className="item">
//                                 <div className="free-ser-img">
//                                     <a href="#"><img src={ require('../../assets/images/b2.png') } alt="" /></a>
//                                 </div>

//                                 <div className="img-head">
//                     What Is cosmos
//                                 </div>
//                             </div>
//                         </OwlCarousel>
//                     </div>
//                 </div>

//             </section>
//             <Footer />
//         </div>
//     )
//   }
// }
