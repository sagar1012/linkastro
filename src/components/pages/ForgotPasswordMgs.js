import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute, NavLink } from 'react-router-dom';
import Header from '../templates/Header';
import Footer from '../templates/Footer';

class ForgotPasswordMgs extends Component {
  render() {
    
        return (
        <div className="main-wrapper">
            <Header />
            <section className="middle-section">
                <div className="sign-page thank-page">
                    <div className="container">
                        <div className="forgot-password-thanks sign-section section-bk">
                            <div className="thank-you-inn">
                                <div className="thank-you-img">
                                    <img src={ require('../../assets/images/thank-you-img.png') } alt="" />
                                </div>
                                <div className="thank-rested-txt">Password Rested</div>
                            </div>
                            <div className="thank-rested-btn">
                                <Link to="/login">Login</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    );
  }
}

export default ForgotPasswordMgs
