import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { reactLocalStorage } from "reactjs-localstorage";
import { ToastContainer } from "react-toastify";
import { connect } from "react-redux";
import { getProfileDetail } from "../../actions/profileAction";
import httpService from "../../services/httpService";
import clientServiceAPI from "../../services/clientServiceAPI";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      personalDetails: [],
      notification: 0,
      open: false,
    };
  }

  async componentDidMount() {
    this.getPersonalDetails();
    this.getNotification();
    this.getProfileDetailClient();
    window.scrollTo(0, 0);
    const expirationDuration = 1000 * 60 * 60 * 12; // 12 hours
    const prevAccepted = localStorage.getItem("accepted");
    const currentTime = new Date().getTime();
    const notAccepted = prevAccepted === undefined;
    const prevAcceptedExpired =
      prevAccepted !== undefined &&
      currentTime - prevAccepted > expirationDuration;
    if (notAccepted || prevAcceptedExpired) {
      localStorage.removeItem("Auth");
      localStorage.removeItem("token");
      //this.setState({ redirect: true });
      localStorage.setItem("accepted", "");
    }

    // -------------
    var token = reactLocalStorage.getObject("token");
    const data = Object.keys(token).length;
    if (data > 0) {
      const Profile = await this.props.getProfileDetail();

      if (Profile?.data?.result === 1) {
        var fields = this.state.fields;
        var rowData = Profile.data.data;

        Object.keys(rowData).forEach(function eachKey(key) {
          fields[key] = rowData[key];
          fields["displayName"] = rowData.displayName;
          fields["emailId"] = rowData.emailId;
          fields["contactNo"] = rowData.contactNo;
          fields["profilePicture"] = rowData.profilePicture;
          fields["profilePictureDownloadLink"] =
            rowData.profilePictureDownloadLink;
        });

        this.setState(fields);
      }
    }
  }

  getPersonalDetails = () => {
    clientServiceAPI
      .getPersonalDetail()
      .then((resp) => {
        this.setState({
          personalDetails: resp.data.personal.result,
        });
      })
      .catch((err) => {
        console.log("error");
      });
  };
  getNotification = () => {
    clientServiceAPI
      .getNotificationCount()
      .then((resp) => {
        this.setState({
          notification: resp.data.checkResult[0]?.notification_count,
        });
      })
      .catch((err) => {
        console.log("error");
      });
  };

  getProfileDetailClient = () => {
    clientServiceAPI.getProfileDetailClient().then((resp) => {
      if (resp.status) {
        var fields = this.state.fields;
        var rowData = resp.data.data;

        Object.keys(rowData).forEach(function eachKey(key) {
          fields[key] = rowData[key];
          fields["displayName"] = rowData.firstName;
          fields["emailId"] = rowData.emailId;
          fields["contactNo"] = rowData.contactNo;
          fields["profilePicture"] = rowData.profilePicture;
        });
        this.setState(fields);
      }
    });
  };

  handleButtonClick = () => {
    this.setState((state) => {
      return {
        open: !state.open,
      };
    });
  };
  state = {
    redirect: false,
    auth: reactLocalStorage.getObject("Auth"),
    token: reactLocalStorage.getObject("token"),
  };
  // authdata : Object.keys(this.state.auth).length
  handleOnclickLogout = (e) => {
    e.preventDefault();
    //console.log(this.props);
    localStorage.removeItem("Auth");
    localStorage.removeItem("token");
    this.setState({ redirect: true });
  };

  render() {
    //var auth = reactLocalStorage.getObject('Auth');
    var token = reactLocalStorage.getObject("token");
    var data = Object.keys(token).length;

    if (this.state.redirect === true) {
      return <Redirect to={"/"} />;
    }

    return (
      <header className="header">
        <ToastContainer
          position="top-right"
          autoClose={3000}
          newestOnTop={false}
          closeOnClick
          limit="3"
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <div className="container">
          <div className="row" style={{ inlineSize: "max-content" }}>
            <div className="col-md-2">
              <div className="logo">
                <Link to="/">
                  <img src={require("../../assets/images/logo.png")} alt="" />
                </Link>
              </div>
            </div>

            <div className="col-md-10">
              <div className="header-right">
                <Dropdown className="user-dd" show={this.state.open}>
                  <Dropdown.Toggle variant="" id="dropdown-basic">
                    <span className="dd-img">
                      <img
                        src={
                          this.state.fields["profilePictureDownloadLink"]
                            ? this.state.fields["profilePictureDownloadLink"]
                            : require("../../assets/images/download.png")
                        }
                        style={{ borderRadius: "50%", cursor: "pointer" }}
                        onClick={this.handleButtonClick}
                      />
                    </span>
                  </Dropdown.Toggle>

                  <Dropdown.Menu style={{ width: "535% !important" }}>
                    <img
                      src="/assets/img/cancel.svg"
                      style={{ right: "11px", opacity: "0.5" }}
                      className="position-absolute"
                      width="10"
                      onClick={this.handleButtonClick}
                    />
                    <div className="dd-pristDetail mb-2">
                      <div className="col-12">
                        <img
                          src={
                            this.state.fields["profilePictureDownloadLink"]
                              ? this.state.fields["profilePictureDownloadLink"]
                              : require("../../assets/images/download.png")
                          }
                          className="mt-3"
                        />
                      </div>
                      <div className="fs-12 fw-500 mt-3 mb-2">
                        {this.state.fields["displayName"]}
                      </div>
                      <br />
                      <small>{this.state.fields["contactNo"]}</small>
                      <br />
                      <small>{this.state.fields["emailId"]}</small>
                      <button
                        className="btn btn-outline-primary m-2 btn-sm"
                        style={{
                          padding: "2px 12px",
                        }}
                      >
                        REFER US
                      </button>
                    </div>
                    <Dropdown.Item href="/myAstrologer">
                      <i
                        class="fas fa-atom"
                        style={{ color: "#0066ff" }}
                        width="12"
                      ></i>
                      <span className="flex-1 ml-2">My Astrologer</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>
                    <Dropdown.Item href="/clientProfile">
                      <i
                        class="fas fa-wallet"
                        style={{ color: "#0066ff" }}
                        width="12"
                      ></i>
                      <span className="flex-1 ml-2">Wallet</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>
                    <Dropdown.Item href="/clientProfile">
                      <i
                        class="fas fa-user"
                        style={{ color: "#0066ff" }}
                        width="12"
                      ></i>
                      <span className="flex-1 ml-2">Profile</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>
                    <Dropdown.Item href="/userLog">
                      <i
                        class="fa fa-history"
                        style={{ color: "#0066ff" }}
                        width="12"
                      ></i>
                      <span className="flex-1 ml-2">Logs</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-1">
                      <img src="assets/img/Icon_b_password.svg" width="10" />
                      <span className="flex-1 ml-2">Reset Password</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>

                    <Dropdown.Item
                      href="#/action-1"
                      onClick={this.handleOnclickLogout}
                    >
                      <img src="assets/img/icon_logout.svg" width="12" />
                      <span className="flex-1 ml-2">Logout</span>
                      <i class="fas fa-angle-right"></i>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <div className="namskar">
                  <span>
                    <img
                      src={require("../../assets/images/header-icons_03.png")}
                      alt=""
                    />
                  </span>
                  {this.state.personalDetails &&
                    this.state.personalDetails[0] && (
                      <span>
                        Namskaar {this.state.personalDetails[0].first_name} Ji,
                      </span>
                    )}
                </div>

                <div className="con">
                  <ul>
                    <li>
                      <Link to="/">
                        <img
                          src={require("../../assets/images/header-icons_05.png")}
                          alt=""
                        />
                      </Link>
                    </li>
                    <li>
                      <Link to="/">
                        <img
                          src={require("../../assets/images/header-icons_07.png")}
                          alt=""
                        />
                      </Link>
                    </li>
                    <li>
                      <Link to="/">
                        <img
                          src={require("../../assets/images/header-icons_09.png")}
                          alt=""
                        />
                      </Link>
                    </li>
                    <li>
                      <Link to="/">
                        <img
                          src={require("../../assets/images/header-icons_11.png")}
                          alt=""
                        />
                      </Link>
                    </li>
                  </ul>
                </div>

                <div className="contact-number">
                  <span>Contact us</span>
                  <span>+91 987 654 3210</span>
                </div>

                <div className="d1">
                  <div className="notification">
                    <Link to="/walletLog">
                      <i className="fa fa-wallet"></i>
                    </Link>
                  </div>
                  <div className="notification">
                    <Link to="/Notifications">
                      <svg
                        fill="#2a7fff"
                        xmlns="http://www.w3.org/2000/svg"
                        width="18.051"
                        height="22"
                        viewBox="0 0 18.051 22"
                      >
                        <defs></defs>
                        <path
                          className="a"
                          d="M15.026,25.75a2.263,2.263,0,0,0,2.256-2.256H12.769A2.256,2.256,0,0,0,15.026,25.75Zm6.769-6.769V13.34c0-3.464-1.85-6.363-5.077-7.13V5.442a1.692,1.692,0,1,0-3.385,0v.767c-3.238.767-5.077,3.655-5.077,7.13v5.641L6,21.237v1.128H24.051V21.237Z"
                          transform="translate(-6 -3.75)"
                        />
                      </svg>
                    </Link>
                    <span className="count-noti">
                      {this.state.notification}
                    </span>
                  </div>
                  <div className="login-button">
                    {data === 0 ? (
                      <Link to="/login">Login</Link>
                    ) : (
                      <Link to="/" onClick={this.handleOnclickLogout}>
                        logout
                      </Link>
                    )}
                  </div>

                  <div className="lag">
                    <Link to="/">
                      Eng <i className="fa fa-sort-down"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  getProfileDetail: state.getProfileDetail,
  errors: state.errors.error,
});

export default connect(mapStateToProps, { getProfileDetail })(Header);
