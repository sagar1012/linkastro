import React, { Component } from "react";
import { ProgressBar } from "react-bootstrap";

const Progressbar = (props) => {
  return (
    <div className="d-flex align-items-center mb-3">
      <div className="rate-num mr-3">
        {props?.star} <i class="fas fa-heart text-color1"></i>
      </div>
      <ProgressBar className="progressbar-style flex-1" now={props?.progress} />
      <div className="rate-total ml-3">{props?.progress}</div>
    </div>
  );
};

export default Progressbar;
