import React from "react";
import { useHistory } from "react-router-dom";

const AstrologerList = (props) => {
  const history = useHistory();
  const astroDetail = props?.data?.data;
  const astroSkill = props?.data?.skill;

  const profileHandler = (member_id) => {
    history.push({
      pathname: "/astroProfile",
      state: {
        AstroId: member_id,
      },
    });
  };

  return (
    <>
      {astroDetail &&
        astroDetail.map((item, index) => {
          return (
            <div className="item" key={index}>
              <div className="astrologers-inner">
                <div
                  className="as1"
                  style={{ cursor: "pointer" }}
                  onClick={(e) => profileHandler(item.member_id)}
                >
                  <div className="as1-left">
                    <div className="al-view text-center">
                      <div className="al-img">
                        <img src="assets/img/priest.png" className="al-pic" />
                        <img
                          src="assets/img/Icon _verified_user.svg"
                          className="al-verf position-absolute"
                          style={{ top: 40 }}
                        />
                        <div className="ribbon">
                          {item.minutes_free} min free
                        </div>
                      </div>
                    </div>
                    <div className="as-rating ml-4">
                      <i className="fa fa-heart mr-1"></i>
                      <i className="fa fa-heart mr-1"></i>
                      <i className="fa fa-heart mr-1"></i>
                      <i className="fa fa-heart mr-1"></i>
                      <i className="fa fa-heart-o"></i>
                    </div>
                    <div className="as-review ml-4">330 Review</div>
                  </div>
                  <div className="as1-right">
                    <div className="as-name">{item.display_name}</div>
                    {astroSkill &&
                      astroSkill?.map((cat, index) => {
                        if (item.member_id === cat.memberid) {
                          return (
                            <div className="as-work row" key={index}>
                              {cat?.categoriesList.map((label, index) => {
                                return (
                                  <p className="mr-1" key={index}>
                                    {" "}
                                    {label.category_label}
                                  </p>
                                );
                              })}
                            </div>
                          );
                        }
                      })}
                    {astroSkill &&
                      astroSkill?.map((lan, index) => {
                        if (lan.memberid === item.member_id)
                          return (
                            <div className="as-lag row ml-0" key={index}>
                              {lan?.languageList.map((language, index) => {
                                return (
                                  <p className="mr-1" key={index}>
                                    {" "}
                                    {language.language}
                                  </p>
                                );
                              })}
                            </div>
                          );
                      })}
                  </div>
                </div>
                <div className="as2">
                  <div className="as2-left">
                    <div className="exp">{item.experience} yrs exp.</div>
                    <div className="fess">
                      Fees{" "}
                      <span className="can">
                        ₹ {item.astrologer_charges}/min
                      </span>{" "}
                      <span className="now">₹ 25/min</span>
                    </div>
                  </div>
                  <div className="as2-right">
                    <ul>
                      <li>
                        <a href="#">
                          {" "}
                          <img
                            src={require("../../assets/images/3.png")}
                            alt=""
                          />{" "}
                          Call
                        </a>
                      </li>
                      <li>
                        <a>
                          {" "}
                          <img
                            src={require("../../assets/images/4.png")}
                            alt=""
                          />{" "}
                          Chat
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};
export default AstrologerList;
