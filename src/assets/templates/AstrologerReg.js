import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute, NavLink } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';

export default class AstrologerReg extends Component {
  componentDidMount(){
    document.title = 'Astrologer Registration';
  }
    render() {
        return (
            <div className="main-wrapper">
                <Header />
                <section className="middle-section">
                    <div className="sign-page">
                        <div className="container">
                            <div className="sign-section">
                                <div className="sign-main">
                                    <div className="sign-head">Astrologer Registration</div>
                                    <div className="sign-form">
                                        <div className="form-group">
                                            <label>Full Name</label>
                                            <input type="text" className="form-control" placeholder="Full Name"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" className="form-control" placeholder="Mobile Number"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Alternate Mobile Number</label>
                                            <input type="text" className="form-control" placeholder="Alternate Mobile Number"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input type="text" className="form-control" placeholder="Email"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Date of Birth</label>
                                            <div className="select-option-form">
                                                <ul>
                                                    <li className="active">Day</li>
                                                    <li>Month</li>
                                                    <li>Year</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Gender</label>
                                            <div className="select-option-form">
                                                <ul>
                                                    <li className="active">Male</li>
                                                    <li>Female</li>
                                                    <li>Other</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Address</label>
                                            <input type="text" className="form-control" placeholder="Address"/>
                                        </div>
                                        <div className="form-group">
                                            <label>City</label>
                                            <input type="text" className="form-control" placeholder="City"/>
                                        </div>
                                        <div className="form-group">
                                            <label>State</label>
                                            <input type="text" className="form-control" placeholder="State"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Country</label>
                                            <input type="text" className="form-control" placeholder="Country"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Pincode</label>
                                            <input type="text" className="form-control" placeholder="Pincode"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Create Password</label>
                                            <input type="text" className="form-control" placeholder="Create Password"/>
                                        </div>
                                        <div className="form-group">
                                            <label>Re Enter Password</label>
                                            <input type="text" className="form-control" placeholder="Re Enter Password"/>
                                        </div>
                                         
                                        <div className="form-group">
                                            <button type="button" className="btn continue_btn">Continue</button>
                                        </div>
                                    </div>
                                    <div className="sign-with">
                                        <div className="logging-accepting">By logging in you are accepting the <a href="/">T&amp;C</a> and <a href="/">Privacy Policy</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}
