import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute } from 'react-router-dom';
import AOS from 'aos';

export default class Footer extends Component {
  componentDidMount(){
    AOS.init();
  }
    render() {
        return (
            <footer className="footer">
                <div className="footer-one">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="usefull-link">
                                    <ul>
                                        <li><Link to="/">About</Link></li>
                                        <li><Link to="/">FAQ</Link></li>
                                        <li><Link to="/">Support</Link></li>
                                        <li><Link to="/">Notifications</Link></li>
                                        <li><Link to="/">Policy</Link></li>
                                        <li><Link to="/">Terms</Link></li>
                                        <li><Link to="/">License</Link></li>
                                        <li><Link to="/">Legal</Link></li> 
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="footer-as">
                                    <ul>
                                        <li><Link to="/Login">Astrologer Login</Link></li>
                                        <li><Link to="/AstrologerReg">Astrologer Registration</Link></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="contact-us cont-pad">
                                    <h3>Contact us</h3>
                                    <p>Phone: +91 987 654 3210</p>
                                    <p>contact@linkastro.com</p>

                                    <ul className="footer-socisl">
                                        <li><Link to="/"><img src={ require('../images/social_03.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/social_05.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/social_07.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/social_09.png') } alt=""/></Link></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="contact-us text-center">
                                    <h3>Download <br/> our App</h3>

                                    <ul className="footer-socisl">
                                        <li><Link to="/"><img src={ require('../images/d2_03.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/d2_05.png') } alt=""/></Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="copyright">
                    <div className="container">
                  Copyright 2020 LinkAstro. All Rights Reserved
                    </div>
                </div>
            </footer>
        )
    }
}
