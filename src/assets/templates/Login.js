import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute, NavLink } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';

export default class Login extends Component {
  componentDidMount(){
    document.title = 'Login';
  }
    render() {
        return (
            <div className="main-wrapper">
                <Header />
                <section className="middle-section">
                    <div className="sign-page">
                        <div className="container">
                            <div className="sign-section">
                                <div className="sign-main">
                                    <div className="sign-head">Sign In</div>
                                    <div className="sign-form">
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Email/Mobile No." />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Password" />
                                        </div>
                                        <div className="form-group">
                                            <div class="custom-forgot text-right">
                                                <Link to="/">Forgot Password?</Link>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <button type="button" className="btn login_btn">Sign in</button>
                                        </div>
                                    </div>
                                    <div className="or-div">or</div>
                                    <div className="sign-with">
                                        <div className="sign-btn">
                                            <ul>
                                                <li><Link to="/"><img src={ require('../images/icon_Sign_up@2x.png') } alt=""/> Sign Up</Link></li>
                                                <li><Link to="/"><img src={ require('../images/icon_Google_login@2x.png') } alt=""/> Google</Link></li>
                                                <li><Link to="/"><img src={ require('../images/icon_Facebook_login@2x.png') } alt=""/> Facebook</Link></li>
                                            </ul>
                                        </div>
                                        <div className="logging-accepting">By logging in you are accepting the <Link to="/">T&C</Link> and <Link to="/">Privacy Policy</Link></div>
                                    </div>
                                </div>
                                <div className="sign-footer">
                                    <div className="astrologers-head">For Astrologers</div>
                                    <div className="astrologers-btn">
                                        <Link to="/AstrologerReg">Astrologer Registration</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}
