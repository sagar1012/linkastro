import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute, NavLink } from 'react-router-dom';

export default class Header extends Component {
    componentDidMount(){
       
    }
    render() {
        return (
            <header className="header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-2">
                            <div className="logo">
                                <Link to="/">
                                    <img src={ require('../images/logo.png') } alt=""/>
                                </Link>
                            </div>
                        </div>

                        <div className="col-md-10">
                            <div className="header-right">
                                <div className="namskar">
                                    <span><img src={ require('../images/header-icons_03.png') } alt=""/></span> <span>Namskaar Ji,</span> 
                                </div>

                                <div className="con">
                                    <ul>
                                        <li><Link to="/"><img src={ require('../images/header-icons_05.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/header-icons_07.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/header-icons_09.png') } alt=""/></Link></li>
                                        <li><Link to="/"><img src={ require('../images/header-icons_11.png') } alt=""/></Link></li>
                                    </ul>
                                </div>

                                <div className="contact-number">
                                    <span>Contact us</span>
                                    <span>+91 987 654 3210</span>
                                </div>

                                <div className="d1">
                                    <div className="notification">
                                        <Link to="/"><svg fill="#2a7fff" xmlns="http://www.w3.org/2000/svg" width="18.051" height="22" viewBox="0 0 18.051 22"><defs></defs><path className="a" d="M15.026,25.75a2.263,2.263,0,0,0,2.256-2.256H12.769A2.256,2.256,0,0,0,15.026,25.75Zm6.769-6.769V13.34c0-3.464-1.85-6.363-5.077-7.13V5.442a1.692,1.692,0,1,0-3.385,0v.767c-3.238.767-5.077,3.655-5.077,7.13v5.641L6,21.237v1.128H24.051V21.237Z" transform="translate(-6 -3.75)"/></svg></Link>
                                        <span className="count-noti">20</span>
                                    </div>

                                    <div className="login-button">
                                        <Link to="/login">Login</Link>
                                    </div>

                                    <div className="lag">
                                        <Link to="/">Eng <i className="fa fa-sort-down"></i></Link> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
