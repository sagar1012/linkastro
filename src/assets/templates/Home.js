import React, { Component, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, IndexRoute } from 'react-router-dom';

import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Header from './Header';
import Footer from './Footer';

export default class Home extends Component {
    render() {
        return (
            <div className="main-wrapper home">
                <Header />
                <section className="middle-section">
   
                    <div className="container">
                        <div className="banner">
                            <img src={ require('../images/component.png') } alt=""/>
                        </div> 
                        <div className="signup-sec">
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="si-head">
                                Signup for free to consult with expert Astrologers
                                    </div> 
                                </div> 

                                <div className="col-md-9">
                                    <div className="sin-ta">
                                        <table>
                                            <tr>
                                                <td><input type="text" className="form-control fm-inp" placeholder="Enter mobile number Or email ID" name="" /></td>
                                                <td>
                                                    <button type="submit" className="si-button">Sign Up</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div> 
                                </div> 
                            </div> 
                        </div> 

                        <div className="conecting">
                            <ul>
                                <li>
                                    <a href="#">
                                        <span> <img src={ require('../images/4.png') } alt=""/></span>
                                        <span>Chat <br/>Astrologer</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <span> <img src={ require('../images/3.png') } alt=""/></span>
                                        <span>Call <br/>Astrologer</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <span> <img src={ require('../images/2.png') } alt=""/></span>
                                        <span>Live <br/>VIDEO</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <span> <img src={ require('../images/1.png') } alt=""/></span>
                                        <span>Get Your<br/> Report</span>
                                    </a>
                                </li>
                            </ul>   
                        </div> 

                        <div className="free-service">
                            <div className="sec-head">
                            Free Services
                            </div>

                            <div className="free-ser-slider">
                                <OwlCarousel
                              className="owl-carousel owl-theme free-ser"
                              loop
                              margin={ 10 }
                              nav
                              dots
                              items={ 4 }
                              autoplay
                              autoHeight={ true }
                            >
                                    <div className="item">
                                        <div className="img-head">
                                      Free Kundali
                                        </div>
                                        <div className="free-ser-img">
                                            <a href="#"> <img src={ require('../images/free_03.png') } alt=""/></a>
                                        </div> 
                                    </div>
                                    <div className="item">
                                        <div className="img-head">
                                    Matchmaking
                                        </div>
                                        <div className="free-ser-img">
                                            <a href="#"> <img src={ require('../images/free_05.png') } alt=""/></a>
                                        </div> 
                                    </div>
                                    <div className="item">
                                        <div className="img-head">
                                      Daily Horoscope
                                        </div>
                                        <div className="free-ser-img">
                                            <a href="#"> <img src={ require('../images/free_07.png') } alt=""/></a>
                                        </div> 
                                    </div>
                                    <div className="item">
                                        <div className="img-head">
                                    Daily Panchang
                                        </div>
                                        <div className="free-ser-img">
                                            <a href="#"> <img src={ require('../images/free_09.png') } alt=""/></a>
                                        </div> 
                                    </div>
                                    <div className="item">
                                        <div className="img-head">
                                    Matchmaking
                                        </div>
                                        <div className="free-ser-img">
                                            <a href="#"> <img src={ require('../images/free_05.png') } alt=""/></a>
                                        </div> 
                                    </div>
                                </OwlCarousel>
                            </div>
                  
                        </div> 
                    </div> 

                    <div className="Astrologers-sec">
                        <div className="container">
                            <div className="astro-head">
                                <span>Astrologers</span>
                                <span>Connect with our top Astrologers</span>
                            </div> 

                            <OwlCarousel
                              className="owl-carousel owl-theme astrologers"
                              loop
                              margin={ 10 }
                              nav
                              dots
                              items={ 4 }
                              autoplay
                              autoHeight={ true }
                            >
                                <div className="item">
                                    <div className="astrologers-inner">
                                        <div className="as1">
                                            <div className="as1-left">
                                                <div className="asro-man">
                                                    <div className="astro-man-img">
                                                        <img src={ require('../images/priest_vishnuvardhana.png') } alt=""/>
                                                    </div> 

                                                    <div className="veryfid">
                                                        <img src={ require('../images/Icon _verified_user.svg') } alt=""/>
                                                    </div> 

                                                    <div className="free-minutes">
                                                        <span>  <img src={ require('../images/blue_banner.svg') } alt=""/></span>
                                                        <span>2 min free</span>
                                                    </div> 
                                                </div> 

                                                <div className="as-rating">
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart-o"></i>
                                                </div> 

                                                <div className="as-review">
                                              330 Review
                                                </div> 
                                            </div> 

                                            <div className="as1-right">
                                                <div className="as-name">
                                                Aacharya Shantanu ji
                                                </div> 

                                                <div className="as-work">
                                                Vedic Astrologer
                                                </div> 

                                                <div className="as-lag">
                                                Hindi, English, Gujrati
                                                </div> 
                                            </div> 
                                        </div> 

                                        <div className="as2">
                                            <div className="as2-left">
                                                <div className="exp">18 yrs exp.</div>
                                                <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
                                            </div> 

                                            <div className="as2-right">
                                                <ul>
                                                    <li>
                                                        <a href="#"> <img src={ require('../images/3.png') } alt=""/> Call</a>
                                                    </li>

                                                    <li>
                                                        <a href="#"> <img src={ require('../images/4.png') } alt=""/> Chat</a>
                                                    </li>
                                                </ul>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="astrologers-inner">
                                        <div className="as1">
                                            <div className="as1-left">
                                                <div className="asro-man">
                                                    <div className="astro-man-img">
                                                        <img src={ require('../images/priest_vishnuvardhana.png') } alt=""/>
                                                    </div> 

                                                    <div className="veryfid">
                                                        <img src={ require('../images/Icon _verified_user.svg') } alt=""/>
                                                    </div> 

                                                    <div className="free-minutes">
                                                        <span>  <img src={ require('../images/blue_banner.svg') } alt=""/></span>
                                                        <span>2 min free</span>
                                                    </div> 
                                                </div> 

                                                <div className="as-rating">
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart-o"></i>
                                                </div> 

                                                <div className="as-review">
                                              330 Review
                                                </div> 
                                            </div> 

                                            <div className="as1-right">
                                                <div className="as-name">
                                                Aacharya Shantanu ji
                                                </div> 

                                                <div className="as-work">
                                                Vedic Astrologer
                                                </div> 

                                                <div className="as-lag">
                                                Hindi, English, Gujrati
                                                </div> 
                                            </div> 
                                        </div> 

                                        <div className="as2">
                                            <div className="as2-left">
                                                <div className="exp">18 yrs exp.</div>
                                                <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
                                            </div> 

                                            <div className="as2-right">
                                                <ul>
                                                    <li>
                                                        <a href="#"> <img src={ require('../images/3.png') } alt=""/> Call</a>
                                                    </li>

                                                    <li>
                                                        <a href="#"> <img src={ require('../images/4.png') } alt=""/> Chat</a>
                                                    </li>
                                                </ul>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="astrologers-inner">
                                        <div className="as1">
                                            <div className="as1-left">
                                                <div className="asro-man">
                                                    <div className="astro-man-img">
                                                        <img src={ require('../images/priest_vishnuvardhana.png') } alt=""/>
                                                    </div> 

                                                    <div className="veryfid">
                                                        <img src={ require('../images/Icon _verified_user.svg') } alt=""/>
                                                    </div> 

                                                    <div className="free-minutes">
                                                        <span>  <img src={ require('../images/blue_banner.svg') } alt=""/></span>
                                                        <span>2 min free</span>
                                                    </div> 
                                                </div> 

                                                <div className="as-rating">
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart-o"></i>
                                                </div> 

                                                <div className="as-review">
                                              330 Review
                                                </div> 
                                            </div> 

                                            <div className="as1-right">
                                                <div className="as-name">
                                                Aacharya Shantanu ji
                                                </div> 

                                                <div className="as-work">
                                                Vedic Astrologer
                                                </div> 

                                                <div className="as-lag">
                                                Hindi, English, Gujrati
                                                </div> 
                                            </div> 
                                        </div> 

                                        <div className="as2">
                                            <div className="as2-left">
                                                <div className="exp">18 yrs exp.</div>
                                                <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
                                            </div> 

                                            <div className="as2-right">
                                                <ul>
                                                    <li>
                                                        <a href="#"> <img src={ require('../images/3.png') } alt=""/> Call</a>
                                                    </li>

                                                    <li>
                                                        <a href="#"> <img src={ require('../images/4.png') } alt=""/> Chat</a>
                                                    </li>
                                                </ul>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="astrologers-inner">
                                        <div className="as1">
                                            <div className="as1-left">
                                                <div className="asro-man">
                                                    <div className="astro-man-img">
                                                        <img src={ require('../images/priest_vishnuvardhana.png') } alt=""/>
                                                    </div> 

                                                    <div className="veryfid">
                                                        <img src={ require('../images/Icon _verified_user.svg') } alt=""/>
                                                    </div> 

                                                    <div className="free-minutes">
                                                        <span>  <img src={ require('../images/blue_banner.svg') } alt=""/></span>
                                                        <span>2 min free</span>
                                                    </div> 
                                                </div> 

                                                <div className="as-rating">
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart-o"></i>
                                                </div> 

                                                <div className="as-review">
                                              330 Review
                                                </div> 
                                            </div> 

                                            <div className="as1-right">
                                                <div className="as-name">
                                                Aacharya Shantanu ji
                                                </div> 

                                                <div className="as-work">
                                                Vedic Astrologer
                                                </div> 

                                                <div className="as-lag">
                                                Hindi, English, Gujrati
                                                </div> 
                                            </div> 
                                        </div> 

                                        <div className="as2">
                                            <div className="as2-left">
                                                <div className="exp">18 yrs exp.</div>
                                                <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
                                            </div> 

                                            <div className="as2-right">
                                                <ul>
                                                    <li>
                                                        <a href="#"> <img src={ require('../images/3.png') } alt=""/> Call</a>
                                                    </li>

                                                    <li>
                                                        <a href="#"> <img src={ require('../images/4.png') } alt=""/> Chat</a>
                                                    </li>
                                                </ul>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="astrologers-inner">
                                        <div className="as1">
                                            <div className="as1-left">
                                                <div className="asro-man">
                                                    <div className="astro-man-img">
                                                        <img src={ require('../images/priest_vishnuvardhana.png') } alt=""/>
                                                    </div> 

                                                    <div className="veryfid">
                                                        <img src={ require('../images/Icon _verified_user.svg') } alt=""/>
                                                    </div> 

                                                    <div className="free-minutes">
                                                        <span>  <img src={ require('../images/blue_banner.svg') } alt=""/></span>
                                                        <span>2 min free</span>
                                                    </div> 
                                                </div> 

                                                <div className="as-rating">
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart"></i>
                                                    <i className="fa fa-heart-o"></i>
                                                </div> 

                                                <div className="as-review">
                                              330 Review
                                                </div> 
                                            </div> 

                                            <div className="as1-right">
                                                <div className="as-name">
                                                Aacharya Shantanu ji
                                                </div> 

                                                <div className="as-work"> 
                                                Vedic Astrologer
                                                </div> 

                                                <div className="as-lag">
                                                Hindi, English, Gujrati
                                                </div> 
                                            </div> 
                                        </div> 

                                        <div className="as2">
                                            <div className="as2-left">
                                                <div className="exp">18 yrs exp.</div>
                                                <div className="fess">Fees <span className="can">₹ 35/min</span> <span className="now">₹ 25/min</span></div>
                                            </div> 

                                            <div className="as2-right">
                                                <ul>
                                                    <li>
                                                        <a href="#"> <img src={ require('../images/3.png') } alt=""/> Call</a>
                                                    </li>

                                                    <li>
                                                        <a href="#"><img src={ require('../images/4.png') } alt=""/> Chat</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                            </OwlCarousel>
                        </div>     
                    </div>

                    <div className="yearly-sec">
                        <div className="astro-head text-center">
                            <span>YEARLY HOROSCOPE</span>
                            <span>Your daily zodiac sign reading free</span>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Aries.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Aries</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Taurus.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Taurus</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Gemini.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Gemini</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Cancer.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Cancer</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Leo.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Leo</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Virgo.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Virgo</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Libra.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Libra</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Scorpio.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Scorpio</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Sagittarius.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Sagittarius</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Capricon.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Capricon</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img"> 
                                            <img src={ require('../images/icon_Aquarius.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Aquarius</div>
                                    </div> 
                                </div> 

                                <div className="col-lg-2 col-md-3">
                                    <div className="yearly-inner">
                                        <div className="yearly-img">
                                            <img src={ require('../images/icon_Pisces.svg') } alt=""/>
                                        </div> 
                                        <div className="yearly-name">Pisces</div>
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 

                    <div className="Astrologers-sec">
                        <div className="container">
                            <div className="astro-head">
                                <span>Astrology Anytime Anywhere</span>
                                <span>Download our app to always be connected</span>
                            </div> 

                            <div className="anytime">
                                <ul>
                                    <li>
                                        <div className="anytime-inner">
                                            <div className="anytime-inner-img">
                                                <img src={ require('../images/n1_03.png') } alt=""/>
                                            </div> 
                                            <div className="anytime-inner-desc">
                                    Download the <br/>LinkAstro app
                                            </div> 
                                        </div> 
                                    </li>

                                    <li>
                                        <div className="anytime-inner">
                                            <div className="anytime-inner-img">
                                                <img src={ require('../images/n1_05.png') } alt=""/>
                                            </div> 
                                            <div className="anytime-inner-desc">
                                  Sign Up in <br/>LinkAstro
                                            </div> 
                                        </div> 
                                    </li> 

                                    <li>
                                        <div className="anytime-inner">
                                            <div className="anytime-inner-img">
                                                <img src={ require('../images/n1_07.png') } alt=""/>
                                            </div> 
                                            <div className="anytime-inner-desc">
                                    Add money in <br/>LinkAstro Wallet
                                            </div> 
                                        </div> 
                                    </li>

                                    <li>
                                        <div className="anytime-inner">
                                            <div className="anytime-inner-img">
                                                <img src={ require('../images/n1_09.png') } alt=""/>
                                            </div> 
                                            <div className="anytime-inner-desc">
                                    Call & Chat with<br/>Expert Astrologers
                                            </div> 
                                        </div> 
                                    </li>
                                </ul>
                            </div> 
                        </div> 
                    </div> 

                    <div className="yearly-sec blog-sec">
                        <div className="container">
                            <div className="astro-head text-center">
                                <span>BLOG</span>
                                <span>Explore the new world with us</span>
                            </div>

                            <OwlCarousel
                              className="owl-carousel owl-theme blog"
                              loop
                              margin={ 10 }
                              nav
                              dots
                              items={ 4 }
                              autoplay
                              autoHeight={ true }
                            >
                                <div className="item">
                                    <div className="free-ser-img">
                                        <a href="#"><img src={ require('../images/b1.png') } alt=""/></a>
                                    </div> 
                                    <div className="img-head">
                                Yoga and our life
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="free-ser-img">
                                        <a href="#"><img src={ require('../images/b3.png') } alt=""/></a>
                                    </div> 

                                    <div className="img-head">
                                What does the star says
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="free-ser-img">
                                        <a href="#"><img src={ require('../images/b4.png') } alt=""/></a>
                                    </div> 
                                    <div className="img-head">
                                  What Is kundali
                                    </div> 
                                </div> 

                                <div className="item">
                                    <div className="free-ser-img">
                                        <a href="#"><img src={ require('../images/b2.png') } alt=""/></a>
                                    </div> 

                                    <div className="img-head">
                                What Is cosmos
                                    </div>     
                                </div>          
                            </OwlCarousel>
                        </div> 
                    </div> 
                     
                </section>
                <Footer />
            </div>
        )
    }
}
