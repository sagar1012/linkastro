jQuery(document).ready(function($) {

    setTimeout(function() {
      jQuery ('.loader_image').addClass('fullloader');
    }, 1000);
    setTimeout(function() {
      jQuery ('.loader_image').addClass('fullloader2');
    }, 2000);

    setTimeout(function() {
      jQuery ('.banner-slide').addClass('anim');
    }, 2000);

    // jQuery(".thoughtful-main-div li").hover(
    //   function () {
    //     jQuery('.thoughtful-main-div li').removeClass("result_hover");
    //     jQuery(this).addClass("result_hover");
    //   }
    // );
  
    // jQuery(".port-btn").on("click", function(){
    //   jQuery(this).parents('.single_portfolio').addClass('open-popup').siblings().removeClass('open-popup');
    //   jQuery('body').addClass("open-p-popup");
    // });

    // jQuery(".popup-close-btn").on("click", function(){
    //   jQuery(this).parents('.single_portfolio.open-popup').removeClass('open-popup').siblings().removeClass('open-popup');
    //   jQuery('body.open-p-popup').removeClass("open-p-popup");
    // });

    // $('.quality-left ul li').hover(function() {
    //   var index = $(this).index();
    //   $(this).addClass('active').siblings().removeClass('active');
    //   $('.quality-right ul li').eq(index).addClass('active').siblings().removeClass('active');
    // });

    // $('.technical-tk-ul ul li').hover(function() {
    //   $(this).toggleClass('active');
    //   $('.technical-stack-main').toggleClass('index-top');
    // });

    if (ww < 992) {
      $('.technical-tk-ul ul li').unbind('mouseenter mouseleave');
      $('.technical-tk-ul ul li').unbind('click').bind('click', function(e) {
          e.preventDefault();
          $('.technical-tk-ul ul li.active').removeClass('active');
          $(this).toggleClass('active');
      });
    } else if (ww >= 992) {
      $('.technical-tk-ul ul li').hover(function() {
        $(this).toggleClass('active');
        $('.technical-stack-main').toggleClass('index-top');
      });
    }

    // $('.contact-form .field.form-group .form-control').focus(function(){
    //   $(this).parents('.field.form-group').addClass('focused');
    // });
    
    // $('.contact-form .field.form-group .form-control').blur(function(){
    //   var inputValue = $(this).val();
    //   if ( inputValue == "" ) {
    //     $(this).removeClass('filled');
    //     $(this).parents('.field.form-group').removeClass('focused');  
    //   } else {
    //     $(this).addClass('filled');
    //   }
    // })
    
    $('.stack-toggle-btn').click(function() {
      var index = $(this).index();
      $(this).parent('.stack-item').toggleClass('stack-open').siblings().removeClass('stack-open');
    });
    
    $('.portfolio-filter ul li').on('click', function () {
      $('.portfolio-filter ul li').removeClass('active');
      $(this).addClass('active');
  
      var data = $(this).attr('data-filter');
      $workGrid.isotope({
        filter: data
      });
    });
  
    if (document.getElementById('portfolio')) {
      var $workGrid = $('.portfolio-grid').isotope({
        itemSelector: '.all',
        percentPosition: true,
        masonry: {
          columnWidth: '.grid-sizer'
        }
      });
    }

})

// 'use strict';

// ;( function ( document, window, index )
// {
// 	var inputs = document.querySelectorAll( '.inputfile' );
// 	Array.prototype.forEach.call( inputs, function( input )
// 	{
// 		var label	 = input.nextElementSibling,
// 			labelVal = label.innerHTML;

// 		input.addEventListener( 'change', function( e )
// 		{
// 			var fileName = '';
// 			if( this.files && this.files.length > 1 )
// 				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
// 			else
// 				fileName = e.target.value.split( '\\' ).pop();

// 			if( fileName )
// 				label.querySelector( 'span' ).innerHTML = fileName;
// 			else
// 				label.innerHTML = labelVal;
// 		});

// 		// Firefox bug fix
// 		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
// 		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
// 	});
// }( document, window, 0 ));
