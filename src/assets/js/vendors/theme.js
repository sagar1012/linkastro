$(window).on('load', function () {
	$('.portfolio-filter ul li').on('click', function () {
		$('.portfolio-filter ul li').removeClass('active');
		$(this).addClass('active');

		var data = $(this).attr('data-filter');
		$workGrid.isotope({
			filter: data
		});
	});

	if (document.getElementById('portfolio')) {
		var $workGrid = $('.portfolio-grid').isotope({
			itemSelector: '.all',
			percentPosition: true,
			masonry: {
				columnWidth: '.grid-sizer'
			}
		});
	}
});