	         //****Base Url ****//
	//const API_URL = "http://ec2-user@ec2-3-6-88-187.ap-south-1.compute.amazonaws.com:3000";
	const API_URL = 'https://linkastro.com:4000';
	      //****Auth Api Url****// 
	export const URL_LOGIN = API_URL+'/public/client/signinv1';
	export const URL_SIGNUP = API_URL+'/public/astrologer/singUpWithDOB';
	export const SEND_OTP = API_URL+'/public/astrologer/authorization/sendOTPToMobile';
	export const URL_CHECK_USERNAME = API_URL+'/public/client/checkUserNameExistOrNot';
	export const URL_VERYFY_OTP = API_URL+'/public/astrologer/authorization/verifyOTPsentToMobile';
	      //****Manage User Prifile Api Url****//
	export const URL_GET_BANK_DETAILS = API_URL+'/api/astrologer/profile/getBankDetails';
	export const URL_UPDATE_BANK_DETAILS = API_URL+'/api/astrologer/profile/updateBankDetails';


	export const URL_GET_PERSONAL_DETAILS = API_URL+'/api/astrologer/web/profile/getPersonalDetails';


	export const URL_UPDATE_PERSONAL_DETAILS = API_URL+'/api/astrologer/profile/updatePersonalDetails';
	export const URL_GET_PROFESSIONAL_DETAILS = API_URL+'/api/astrologer/profile/getProfessionDetails';
	export const URL_UPDATE_PROFESSIONAL_DETAILS = API_URL+'/api/astrologer/profile/updateProfessionalDetails';
	export const URL_GET_LOCATION_DETAILS = API_URL+'/api/astrologer/profile/getLocationDetails';
	export const URL_UPDATE_LOCATION_DETAILS = API_URL+'/api/astrologer/profile/updateLocationDetails';
	export const URL_GET_CATEGORY = API_URL+'/api/astrologer/profile/getCategoryList';
	export const URL_GET_SKILLS = API_URL+'/api/astrologer/profile/getSkillsList';
	export const URL_GET_LANGUAGE = API_URL+'/api/astrologer/profile/getLanguageList';
	export const URL_UPDATE_PROFILE = API_URL+'/api/astrologer/profile/uploadProfilePicture';
	export const URL_GET_IMAGE = API_URL+'/api/astrologer/profile/downloadProfile';
	export const URL_UPLOAD_BANK_ID = API_URL+'/api/astrologer/profile/uploadBankIdProof';
	export const URL_GET_BANK_ID = API_URL+'/api/astrologer/profile/downloadIdProof';
	export const URL_UPLOAD_GALLARY = API_URL+'/api/astrologer/gallary/uploadFilesToGallary';
	export const URL_GET_GALLARY = API_URL+'/api/astrologer/gallary/getGallaryFilesDownloadLink';
	export const URL_GET_PROFILE_DETAIL=API_URL+'/api/astrologer/profile/getPersonalDetailsv1'


	export const URL_GET_FOLLOWER_DETAILS =API_URL+'/api/astrologer/profile/getAstrologerFollower';
	export const URL_GET_EARNING_DETAILS=API_URL+'/api/astrologer/wallet/getTodaysEarningListV1';
	export const URL_GET_WALLET_DETAILS=API_URL+'/api/astrologer/wallet/getWalletBalance';
	export const URL_GET_SHADULE_DETAILS =API_URL+'/api/astrologer/call/getScheduleDetails';
	export const URL_UPDATE_SHADULE_DETAILS =API_URL+'/api/astrologer/call/astroSchedulev1';
	export const URL_GET_REPORT_DETAILS=API_URL+'/api/astrologer/report/getReportCount';
	export const URL_GET_REPORTLIST_DETAILS =API_URL+'/api/astrologer/report/getReportList';
	export const URL_GET_PERSONALV_DETAILS=API_URL+'/api/astrologer/profile/getPersonalDetailsv1'

	export const URL_GET_CALLOG_DETAIL=API_URL+'/api/astrologer/call/getAstroCallLog'
	export const URL_GET_CHATHISTORY_DETAIL=API_URL+'/api/astrologer/chat/getChatHistory'
	export const URL_GET_REPORTLOG_DETAIL=API_URL+'/api/astrologer/report/getReportLog'


	///----------Order History-----------
	export const URL_GET_ALLCHATORD_DETAIL=API_URL+'/api/astrologer/orderhistory/allChatOrder'
	export const URL_GET_MISSEDORDCHAT_DETAIL=API_URL+'/api/astrologer/orderhistory/MissedOrderChat'
	export const URL_GET_FINISHEDORDCHAT_DETAIL=API_URL+'/api/astrologer/orderhistory/finishedOrderChat'
	export const URL_GET_ONGOINGORDCHAT_DETAIL=API_URL+'/api/astrologer/orderhistory/OngoingOrderCall'
	export const URL_GET_CLIENTRATINNG_DETAIL=API_URL+'/api/astrologer/orderhistory/clientRating'

	export const URL_GET_CHAR_DETAIL=API_URL+"/api/astrologer/kundali/chardasha"
	 /*

	------ALL CHATE -------
	12|linkAstroStag  | 2021-04-09T10:55:35.893Z [astro] info: {"Request":{"URL":"/api/astrologer/orderhistory/allChatOrder","Params":{"pageNo":1},"Header":{"x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjMxMSwicm9sZUlkIjoyLCJleHAiOjE2MTg1Njk3MDQ4MTF9.odbPydXDVbqxU7Eh5ZGLPS7E656MS5j-hCxr29MxSrY","content-type":"application/json; charset=UTF-8","content-length":"12","host":"linkastro.com:4000","connection":"Keep-Alive","accept-encoding":"gzip","user-agent":"okhttp/3.12.1"}}}
	12|linkAstroStag  | POST /api/astrologer/orderhistory/allChatOrder 200 27.642 ms - 8141


	----------missedorderchate----
	12|linkAstroStag  | 2021-04-09T10:55:35.879Z [astro] info: {"Request":{"URL":"/api/astrologer/orderhistory/MissedOrderChat","Params":{"pageNo":1},"Header":{"x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjMxMSwicm9sZUlkIjoyLCJleHAiOjE2MTg1Njk3MDQ4MTF9.odbPydXDVbqxU7Eh5ZGLPS7E656MS5j-hCxr29MxSrY","content-type":"application/json; charset=UTF-8","content-length":"12","host":"linkastro.com:4000","connection":"Keep-Alive","accept-encoding":"gzip","user-agent":"okhttp/3.12.1"}}}
	12|linkAstroStag  | POST /api/astrologer/orderhistory/MissedOrderChat 200 13.129 ms - 4475


	-------finishedorderchat---------
	2021-04-09T10:50:00.423Z [astro] info: {"Request":{"URL":"/api/astrologer/orderhistory/finishedOrderChat","Params":{"pageNo":1},"Header":{"x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjMxMSwicm9sZUlkIjoyLCJleHAiOjE2MTg1Njk3MDQ4MTF9.odbPydXDVbqxU7Eh5ZGLPS7E656MS5j-hCxr29MxSrY","content-type":"application/json; charset=UTF-8","content-length":"12","host":"linkastro.com:4000","connection":"Keep-Alive","accept-encoding":"gzip","user-agent":"okhttp/3.12.1"}}}
	12|linkAstroStag  | POST /api/astrologer/orderhistory/finishedOrderChat 200 31.141 ms - 7886


	------ongoingorderchte-----
	2021-04-09T10:49:23.394Z [astro] info: {"Request":{"URL":"/api/astrologer/orderhistory/OngoingOrderCall","Params":{"pageNo":1},"Header":{"x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjMxMSwicm9sZUlkIjoyLCJleHAiOjE2MTg1Njk3MDQ4MTF9.odbPydXDVbqxU7Eh5ZGLPS7E656MS5j-hCxr29MxSrY","content-type":"application/json; charset=UTF-8","content-length":"12","host":"linkastro.com:4000","connection":"Keep-Alive","accept-encoding":"gzip","user-agent":"okhttp/3.12.1"}}}
	12|linkAstroStag  | POST /api/astrologer/orderhistory/OngoingOrderCall

	------RATING-------
	12|linkAstroStag  | 2021-04-09T10:50:00.673Z [astro] info: {"Request":{"URL":"/api/astrologer/orderhistory/clientRating","Params":{"clientId":404},"Header":{"x-access-token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjMxMSwicm9sZUlkIjoyLCJleHAiOjE2MTg1Njk3MDQ4MTF9.odbPydXDVbqxU7Eh5ZGLPS7E656MS5j-hCxr29MxSrY","content-type":"application/json; charset=UTF-8","content-length":"16","host":"linkastro.com:4000","connection":"Keep-Alive","accept-encoding":"gzip","user-agent":"okhttp/3.12.1"}}}
	12|linkAstroStag  | POST /api/astrologer/orderhistory/clientRating 200 5.854 ms - 2811

	// export const URL_GET_RPORT_DETAILS =API_URL+'/api/astrologer/report/getReportCount","Params'
	// export const URL_GET_REPRT_DETAILS =API_URL+'/api/astrologer/report/fgetReportList'
	*/
