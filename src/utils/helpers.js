import Http from './http';
import { reactLocalStorage } from 'reactjs-localstorage';
import { URL_ADMIN_EDIT_ROLE,URL_ADMIN_MENU_LIST,URL_ADMIN_MENU_PERMISSION_LIST } from './api-url';
  
 //********get role name********//
export async  function getRoleNameById(id) {
	 const response = await Http.get(`${ URL_ADMIN_EDIT_ROLE }/${ id }`);
	if( response.status == true ){
		return response.data.role_name;
	 }
}

//******* Get Menu list ********//
export async  function menuList() {
    const id =  reactLocalStorage.getObject('Auth').id;
    const response = await Http.get(`${ URL_ADMIN_MENU_PERMISSION_LIST }/${ id }`);
	if( response.status == true ){
		return response.data;
	 }else{
	 	return {}; 
	 }
}
