import { GET_BANKDETAILS,GET_PERSONALDETAILS,GET_LOCATIONDETAILS,GET_PROFESSIONALDETAILS } from '../actions/types';
const initialState = {
  bankDetails: {},
  personalDeatils: {},
  professionalDeatils: {},
  locationDeatils: {},
  loading: false,
  errors: {}
};
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_BANKDETAILS:
      return {
        ...state,
        bankDetails: action.payload,
        loading: false
      };
    case GET_PERSONALDETAILS:
      return {
        ...state,
        personalDeatils: action.payload,
        loading: false
      };
    case GET_LOCATIONDETAILS:
    return {
      ...state,
      locationDeatils: action.payload,
      loading: false
    };
    case GET_PROFESSIONALDETAILS:
    return {
      ...state,
      professionalDeatils: action.payload,
      loading: false
    };
    default:
      return state;
  }
};
