import { GET_USERS, GET_USER, REMOVE_USER, USER_LOADING } from '../actions/types';
const initialState = {
  users: [],
  user: {},
  loading: false,
  errors: {}
};
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
        loading: false
      };
    case GET_USER:
      return {
        ...state,
        user: action.payload,
        loading: false
      };
    case REMOVE_USER:
      return {
        ...state,
        users: [ ...state.users.filter(user => user.id !== action.payload) ],
        loading: false
      }
    case USER_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};
