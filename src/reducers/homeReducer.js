import { GET_CONTACTS, GET_CONTACT, REMOVE_CONTACT, CONTACT_LOADING } from '../actions/types';
const initialState = {
  contacts: [],
  contact: {},
  loading: false,
  errors: {}
};
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CONTACTS:
      return {
        ...state,
        contacts: action.payload,
        loading: false
      };
    case GET_CONTACT:
      return {
        ...state,
        contact: action.payload,
        loading: false
      };
    case REMOVE_CONTACT:
      return {
        ...state,
        contacts: [ ...state.contacts.filter(contact => contact.id !== action.payload) ],
        loading: false
      }
    case CONTACT_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};