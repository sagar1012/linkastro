import { combineReducers } from 'redux';
import errors from './errorsReducer';
import profile from './userprofileReducer';
export default combineReducers({
  profile,
  errors,
 
});
