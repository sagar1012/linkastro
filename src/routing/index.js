export const isLogin = () => {
  if (localStorage.getItem("token")) {
    return true;
  }

  return false;
};

export const astroApp = () => {
  if (localStorage.getItem("AdminVerified") === 1) {
    return true;
  }
  return false;
};
